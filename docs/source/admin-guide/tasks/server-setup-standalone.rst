Plexflow Server standalone setup
################################

Installation
************

Launching
*********

Using native launcher
=====================

http://help.eclipse.org/juno/topic/org.eclipse.platform.doc.isv/reference/misc/launcher_ini.html

Using Java launcher
===================

If you for whatever reason do not prefer native launcher, you can start
standalone server with pure Java launcher. The command will look like::

   $ java <JVM arguments> -jar plugins/org.eclipse.equinox.launcher_<version>.jar <server arguments>
   
where
* ``<JVM arguments>`` is list of arguments for Java Virtual Machine itself.
  These arguments are placed in ``plexflow-server.ini`` file just after the
  ``-vmargs`` line.
  
* ``<server arguments`` is list of arguments for Plexflow Server standalone.
  These arguments are placed in ``plexflow-ini file`` before ``-vmargs`` line.

It is worthwhile to notice that commandline arguments (either JVM or server)
stored in ``plexflow-server.ini`` file are not honored by Java launcher and
must be specified manually. For example, the following content of
``plexflow-server.ini``::

   -console
   -vmargs
   -Dorg.osgi.service.http.port=8080

will be equal to issuing the command::

   $ java -Dorg.osgi.service.http.port=8080 -jar plugins/org.eclipse.equinox.launcher_<version>.jar -console

.. tip:: See
   `java <http://docs.oracle.com/javase/7/docs/technotes/tools/windows/java.html>`_
   application launcher manual page.


Shutting down
*************

Using console
=============

Using HTTP command
==================

Running as daemon/service
*************************

Linux
=====

Windows
=======
