Error codes
###########

+------+-----------------------------------------------------------------------+
| Code | Description                                                           |
+======+=======================================================================+
| 1    | **Cannot authenticate due to underlying database exception.**         |
|      | Database cannot be contacted to do authentication. Check if database  |
|      | is running.                                                           |
+------+-----------------------------------------------------------------------+
| 2    | **Cannot authenticate due to underlying database exception.**         |
|      | Database cannot be contacted to do authentication. Check if database  |
|      | is running.                                                           |
+------+-----------------------------------------------------------------------+
