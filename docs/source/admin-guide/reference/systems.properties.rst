systems.properties file
#######################

This file is used to configure available systems to work with for richclient
and webapp applications. It is simple text file with well-known
"INI file"-like syntax.

.. warning:: Beware of typos! Filename is not systems, but systems as it can
   contains multiple system definition.

Location
********

If not explicitly specified through system property

* com.virtage.plexflow.systemsProperties.path for Richclient or
* com.virtage.plexflow.http.systemsProperties.path for HTTP webapp
then systems.properties must be present either in user-wide or system-wide location (this order).

User-wide location is $HOME/.plexflow/systems.properties, where $HOME is home directory of user e.g. /home/joe/ for Linux, C:\Documents and Settings\joe\ for Windows XP or C:\Users\joe\ for Windows Vista and above.

System-wide location is

* /opt/plexflow/etc/systems.properties on Linux or
* C:\Program Files\Plexflow\etc\system.properties on Windows.

System-wide location is default for HTTP webapp that does not run under a real user account.

Example
*******

.. code-block:: properties
   
   ################################################################################
   # SYSTEMS DEFINITION
   ################################################################################
   # This file defines systems available to authenticate against.
   #
   # Important notes:
   # * Only supported encoding of file is ISO 8859-1
   # * File must be placed as $HOME/.plexflow/systems.properties
    
   # Define systems ID (delimeted by colon) to be later configured in detail
   systems = dev:test:prod
    
   # *** DEV system ***
   # system.someId.displayName = Human name of system
   # Default username is "plexflow" with "changeme" password.
   # (Encrypted password using default seckey is "dc0004c80e34909651e9ae6af3707c5f")
   system.dev.displayName = Plexflow DEV
   system.dev.disabled = false
   system.dev.jdbcUrl = jdbc:derby://localhost:1527/pxdev
   system.dev.jdbcUser = plexflow
   system.dev.jdbcKey = dc0004c80e34909651e9ae6af3707c5f
    
   # *** TEST system ***
   system.test.displayName = Plexflow TEST
   system.test.disabled = true
   system.test.jdbcUrl = jdbc:derby://localhost:1527/pxdev
   system.test.jdbcUser = plexflow
   system.test.jdbcKey = dc0004c80e34909651e9ae6af3707c5f
    
   # *** PROD system ***
   system.prod.displayName = Plexflow PROD
   system.prod.disabled = true
   system.prod.jdbcUrl = jdbc:derby://localhost:1527/pxdev
   system.prod.jdbcUser = plexflow
   system.prod.jdbcKey = dc0004c80e34909651e9ae6af3707c5f
