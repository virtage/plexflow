Maintenance job
###############

Maintenance job is collection of tasks responsible to

* check consistency of managed files

   * subnodes of trashed folder are also set trashed
   * physical files are not currupted or modified outside Plexflow (CRC and
     size corresponds to metadata)
   * orphan files (present on disk but missing in metadata)
   * stub files (missing on disk but present in metadata)
   * delete storage temporary (tmp/) files
   * purge empty folders from files/ after deletion or tmp/ after push commit
 
* delete trashed nodes older then [[trash retention period]] (90 days by default)
* delete file versions older then [[version retention period]] (90 days by default)
* index contents for searching

Maintenance should be run on regular basis, usually **every night and always before backuping**.

Technically, maintenance job is a pure Java [[http://ant.apache.org|Ant script]]
not relying on platform-specific means like bash for Linux or Power Shell for Windows.

.. todo:: Scheduling job on Linux

.. todo:: Scheduling job on Windows