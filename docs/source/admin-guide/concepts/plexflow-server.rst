Plexflow Server
###############

Plexflow Server overview
************************

Standalone vs. WAR distribution
*******************************

Standalone server is special distribution form of Plexflow Server suitable for
single workspace environments with very easy setup procedure. Standalone server
itself is contained from small executable native launcher
(``plexflow-server`` for Linux, or ``plexflow-server.exe`` for Windows) and
Jetty-powered HTTP server. Native launcher finds Java Runtime, initializes and
starts HTTP server with no or minimal configuration hassle for administrator.

From usage and functional point of view, there is no difference in comparison
with WAR distribution of Plexflow Server.

Which distribution type to choose?
**********************************

Configuration
*************

Default file and folder layout
******************************

+---------------------------+------------------------------------------------------+
| Element                   | Description                                          |
+===========================+======================================================+
| binaries folder           | Plexflow Server standalone binaries folder.          |
| *(standalone only)*       | :program:`Linux` `/opt/plexflow`                     |
|                           | :program:`Windows` `C:\Program Files\Plexflow``      |
+---------------------------+------------------------------------------------------+
| :ref:`datafolder`         | Datafolder location for storing Plexflow files.      |
|                           | :program:`Linux` `/var/plexflow-data`                |
|                           | :program:`Windows` `C:\Program Files\Plexflow\data`` |
+---------------------------+------------------------------------------------------+
| configuration folder      | Folder for configuration files.                      |
|                           | :program:`Linux` `/etc/plexflow`                     |
|                           | :program:`Windows` `C:\Program Files\Plexflow\etc``  |
+---------------------------+------------------------------------------------------+
| :ref:`systems.properties` | Systems definition file.                             |
|                           | ``<configuration folder>/systems.properties``        |
+---------------------------+------------------------------------------------------+
| :ref:`seckey`             | Encryption security key.                             |
|                           | ``<configuration folder>/seckey``                    |
+---------------------------+------------------------------------------------------+
| logback.server.xml        | System logging configuration.                        |
|                           | ``<configuration folder>/logback.server.xml``        |
+---------------------------+------------------------------------------------------+

Console
*******




Troubleshooting
***************

Error "bash: ./plexflow-server: cannot execute binary file" or "bash: ./plexflow-server: No such file or directory"
===================================================================================================================
:program:`Linux` You have tried to execute 32bit launcher on 64bit system or
vice versa. Download correct launcher for your architecture.

TODO: How to determinate 32/64bit architecture? ....

java.net.SocketException: Permission denied
===========================================
:program:`Linux` Non root users doesn't have a permission to use ports < 1024::

   2012-09-03 15:34:20.294:WARN:oejuc.AbstractLifeCycle:FAILED SelectChannelConnector@0.0.0.0:80: java.net.SocketException: Permission denied
   java.net.SocketException: Permission denied

java.net.BindException: Address already in use
==============================================

.. code-block:
   2012-09-03 15:37:28.659:WARN:oejuc.AbstractLifeCycle:FAILED org.eclipse.jetty.server.Server@1102b93: java.net.BindException: Address already in use

