Int and long max values
#######################

long
****

19 digits length max value of long is 9 223 372 036 854 780 000.

which is

* 8 589 934 592 gigabytes (GiB)
* 8 388 608 terabytes
* 8 192 petabytes

int
***
Integer.MAX_VALUE is 2\ :sup:`31`\ -1, i.e. 2,147,483,647.

which is

* cca 2048 MiB
* 2 GiB