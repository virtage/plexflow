.. _server-protocol:

######################################
Plexflow Server protocol specification
######################################

Plexflow Server speaks `RESTful <http://en.wikipedia.org/wiki/Restful>`_-like
protocol that is based on HTTP and using its
normal request methods like GET or PUT. RESTful APIs are simple to use, may be
secured with SSL, and firewall-friendly. Another major benefit is easy
communication from diverse clients and platforms.

.. _requests:

****************
Issuing requests
****************

Security
========

Commands may be secured with SSL or HTTP authentication depending on deployment
setup.

However, VERSION command must be always accessible without authentication (but
can be encrypted with SSL).

In examples bellow, add ``Authorization`` header and/or change protocol to
``https`` as needed.

.. _malformed-request:

Malformed request
=================

If server receives request that doesn't conform this specification, it is called
malformed request.

.. _responses:

Trace parameter
===============

.. warning:: This feature is not part of protocol specification and may be
   removed in future protocol version or disabled for production deployment.
   
For commands that are failing with BAD response you can force server to include
full Java exception stack trace in body of response with no-value ``trace``
parameter.

**Example**

Append ``trace`` parameter to failing PULL command::

   http://liam/plexflow-api/pull?fileId=823&trace

Output (body only)::

   BAD: Exception class com.virtage.plexflow.storage.datafolder.provider.DatafolderProviderException: Inconsistency detected on file '/složka v rootu/testfile' (id 823). Actual file length (10,485,760 bytes) is different from value in database (1,048,576 bytes).
   
   com.virtage.plexflow.storage.datafolder.provider.DatafolderProviderException: Inconsistency detected on file '/složka v rootu/testfile' (id 823). Actual file length (10,485,760 bytes) is different from value in database (1,048,576 bytes).
     at com.virtage.plexflow.server.commands.PullCommand.doGet(PullCommand.java:74)
     at javax.servlet.http.HttpServlet.service(HttpServlet.java:707)
     at javax.servlet.http.HttpServlet.service(HttpServlet.java:820)
     ...

**********************
Interpreting responses
**********************

Server sends standard HTTP responses augmented with
:ref:`Plexflow-specific headers <headers-reference>` described above.

.. _response-type:

Response types
==============

To issued request it may be reacted in many ways depending on HTTP response
status:

#. **OK response** -- HTTP 200 with ``X-Plexflow-Response-Type: OK`` header
#. **BAD response** -- HTTP 200 with ``X-Plexflow-Response-Type: BAD`` header
#. **malformed response** -- HTTP 200 with but not confirming this specification
   (e.g. missing required header)
#. **error response** -- HTTP != (other then) 200 (server is down,
   unauthorized, etc.)

Response body
=============

Actual response body (and required ``Content-Type``) depends on previous
request, indeed.

OK and BAD responses not sending binary content but plain text
(``text/plain;UTF-8``) reuse value of :ref:`X-Plexflow-Response-Message`
header also as body for easier interpretation for human.

In this case, a message in body is not encoded in quoted-printable as in
value of :ref:`X-Plexflow-Response-Message` header.

.. _OK-response:

OK response
===========

* HTTP response code 200
* header ``X-Plexflow-Response-Type: OK``
* header ``X-Plexflow-Response-Message: <something>``
* header ``X-Plexflow-Server-Version: <version>``
* header ``Content-Type: <mime>``

OK responses represents that command has completed without errors.

.. _BAD-response:

BAD response
============

* HTTP response code 200
* header ``X-Plexflow-Response-Type: BAD``
* header ``X-Plexflow-Response-Message: <something>``
* header ``X-Plexflow-Server-Version: <version>``
* header ``Content-Type: <mime>``

BAD responses denotes *handled but unrecoverable error* encountered in execution
of command. In other words, the Server has caught an exception and with BAD
response offers to Client a chance to react in controlled manner (this is the
difference in comparison to malformed response).


.. _malformed-response:

Malformed response
==================

* **HTTP response code:** 200

Malformed responses indicate that received HTTP response cannot be accepted due
to this specification violation. Every response that *doesn't include* at least

* :ref:`X-Plexflow-Response-Type` header *and*
* :ref:`X-Plexflow-Response-Message` header *and*
* :ref:`X-Plexflow-Server-Version` header *and*
* :ref:`Content-Type` header

is considered as malformed. Also all header values must conform their data types
and allowed values for particular context, indeed.

.. _error-response:

Error response
==============


.. _operations:

*********************
Operations logic flow
*********************

Or *what can I do with Plexflow Server API?* section.


.. _headers-reference:

*****************
Headers reference
*****************

**Data type and value**

Strictly speaking all header values are strings by HTTP specification, data type
bellow tells to what Java type this value must be possible to cast to.

If header is required by command its value

* must not be null or blank value [#f1]_
* must be possible to cast it to specified Java type

Otherwise a request containing such header is considered as
:ref:`malformed <malformed-request>`.

.. [#f1] By blank value or just blank is meant value that is recognized as
   being blank by Commons Lang StringUtils method isBlank(). To cite this
   method javadoc:
      
   .. code-block:: java
      
      StringUtils.isBlank(null)      = true
      StringUtils.isBlank("")        = true
      StringUtils.isBlank(" ")       = true
      StringUtils.isBlank("bob")     = false
      StringUtils.isBlank("  bob  ") = false

.. _X-Plexflow-Server-Version:

X-Plexflow-Server-Version
=========================

* **Used for:** response
* **Date type:** String
* **Example value:** ``Virtage Plexflow Server/0.1 (build 201211241209)``

Version of server software.

.. _X-Plexflow-File-CRC32:

X-Plexflow-File-CRC32
=====================

* **Used for:** response
* **Date type:** long
* **Example value:** ``2414559977``

File CRC32 checksum value.

.. _X-Plexflow-File-Length:

X-Plexflow-File-Length
======================

* **Used for:** response
* **Date type:** long
* **Example value:** ``1024``

File length (size) in bytes.

.. _X-Plexflow-Response-Type:

X-Plexflow-Response-Type
========================

* **Used for:** response
* **Date type:** String. Possible value ``OK`` or ``BAD``
* **Example value:** ``OK``

Header determining :ref:`response-type`.

.. _X-Plexflow-Response-Message:

X-Plexflow-Response-Message
===========================

* **Used for:** response
* **Date type:** String encoded in RFC 1521 (Quoted-Printable encoding)
* **Example value:** ``Inconsistency detected on file '/home/joe/slo=C4=8Dka' (ID 823). Actual file length (10,485,760 bytes) is different from value in database (1,048,576 bytes).``

Textual response to a client request. Usually a detailed result of command
execution or exception reason. BAD messages can be presented to user and
logged to application log, while OK message are commonly ignored.

Header value can contain a characters from non-US alphabets. HTTP specification
allows only US-ASCII characters to be carried in header value. Non US-ASCII
character may cause Server and/or Clients to crash. To prevent this, a header
value is encoded with `Quoted-Printable encoding <http://tools.ietf.org/html/rfc1521#page-18>`_,
specified in `RFC 1521 <http://tools.ietf.org/html/rfc1521>`_,
that encodes only non US-ASCII characters so text is readable on ASCII terminal
without decoding.

In Java, this encoding is implemented by Apache Commons Codec
`QuotedPritableCodec <http://commons.apache.org/codec/api-release/src-html/org/apache/commons/codec/net/QuotedPrintableCodec.html>`_
class. The following snippet

.. code-block:: java

   QuotedPrintableCodec codec = new QuotedPrintableCodec("UTF-8");
   String str = codec.encode("péro z gauče");
   System.out.println("encoded: " + str);
   System.out.println("decoded: " + codec.decode(str));

has an output

.. code-block:: java

   encoded: p=C3=A9ro z gau=C4=8De
   decoded: péro z gauče

.. _X-Plexflow-File-ID:

X-Plexflow-File-ID
==================

* **Used for:** response
* **Date type:** long
* **Example value:** ``4489``

.. _Content-Type:

Content-Type
============

* **Used for:** request, response
* **Date type:** String. Possible value ``text/plain;UTF-8`` or ``application/octet-stream``.
* **Example value:** ``4489``

Standard HTTP protocol header specifying MIME type of request or response body.
Actually speaking, Plexflow uses and accepts only two MIME types:

* ``application/octet-stream`` for requests/responses of binary content (like pushing a file)
* ``text/plain;charset=UTF-8`` denominating plain text body in UTF-8 charset.

*Any other value is considered as :ref:`malformed-request`.*



******************
Commands reference
******************

Commands names has to be always spelled in UPPERCASE.



.. _version-command:

VERSION command
===============

The VERSION commands is used for determining version of Plexflow Server
software.

VERSION command must be accessible without authentication even if
other commands are protected in this way.

.. tip:: This is frequently used for simple up/down test of Plexflow Server.
   Just open web browser of your choice and
   go to ``http[s]://<server>/<prefix>/version``. If you see *something*,
   Plexflow Server is up and running.

Request
-------

* command URI: ``version``
* HTTP method: GET

**Request headers**

None.

**Request parameters**

None.

**Request body**

None.

**Request example**

.. code-block:: none
   
   GET /plexflow-api/version HTTP/1.1

Response
--------

**OK response headers**

* :ref:`standard OK response headers <OK-response>`
* ``Content-Type: text/plain;charset=UTF-8``

Server software version is sent twice in both ``X-Plexflow-Version`` and
``X-Plexflow-Server-Version``.

**OK response body**

``OK: <message>`` (not encoded in quoted-printable as in ``X-Plexflow-Response-Message``).

**OK response example**

.. code-block:: none

   HTTP/1.1 200 OK
   Content-Type: text/plain;charset=UTF-8
   X-Plexflow-Server-Version: Virtage Plexflow Server/0.1
   X-Plexflow-Response-Type: OK
   X-Plexflow-Response-Message: Virtage Plexflow Server/0.1
   Content-Length: 32
   Server: Jetty(8.1.3.v20120522)
   
   OK: Virtage Plexflow Server/0.1



.. _pull-command:

PULL command
============

With PULL command you can download a file from remote datafolder.

Request
-------

* command URI: ``pull``
* HTTP method: GET

**Request headers**

None.

**Request parameters**

* ``fileId`` -- required, ``long`` -- file ID

**Request body**

None.

**Request example**

.. code-block:: none
   
   GET /plexflow-api/pull?fileId=82318 HTTP/1.1

Response
--------

**OK response headers**

* :ref:`standard OK response headers <OK-response>`
* ``Content-Type: application/octet-stream``
* ``X-Plexflow-File-CRC32: <crc32>``
* ``X-Plexflow-File-Length: <length>``
* ``X-Plexflow-File-ID: <fileId>``

**OK response body**

File content.

**OK response example**

.. code-block:: none
   
   HTTP/1.1 200 OK
   X-Plexflow-Server-Version: Virtage Plexflow Server/0.1
   X-Plexflow-File-ID: 823
   Content-Type: application/octet-stream
   X-Plexflow-File-Length: 10485760
   X-Plexflow-File-CRC32: 2414559977
   Transfer-Encoding: chunked
   Server: Jetty(8.1.3.v20120522)
    
   [body not shown]



.. _push-begin-command:

PUSH-BEGIN command
==================

With PUSH-BEGIN command you request upload of a file to the remote datafolder.

Request
-------

* command URI: ``push-begin``
* HTTP method: PUT

**Request headers**

* ``Content-Type: application/octet-stream``
* ``X-Plexflow-File-CRC32: <crc32>`` -- length of local file counted by Client
* ``X-Plexflow-File-Length: <length>`` -- length of local file counted by Client

**Request parameters**

* ``fileId`` -- required, ``long`` -- file ID

**Request body**

File content.

**Request example**

.. code-block:: none
   
   PUT /plexflow-api/push-begin?fileId=82318 HTTP/1.1
   Content-Type: application/octet-stream
   X-Plexflow-File-CRC32: 2414559977
   X-Plexflow-File-Length: 10485760
   
   [body not shown]

Response
--------

**OK response headers**

* :ref:`standard OK response headers <OK-response>`
* ``Content-Type: text/plain;charset=UTF-8``
* ``X-Plexflow-File-CRC32: <crc32>`` -- checksum of received file counted by Server
* ``X-Plexflow-File-Length: <length>`` -- length of received file counted by Server
* ``X-Plexflow-File-ID: <fileId>``

**OK response body**

``OK: <message>`` (not encoded in quoted-printable as in ``X-Plexflow-Response-Message``).

**OK response example**

.. code-block:: none
   
   HTTP/1.1 200 OK
   X-Plexflow-File-CRC32: 2414559977
   X-Plexflow-File-Length: 10485760
   X-Plexflow-Server-Version: Virtage Plexflow Server/0.1
   X-Plexflow-File-ID: 823
   Content-Type: text/plain;charset=UTF-8
   X-Plexflow-Response-Type: OK
   X-Plexflow-Response-Message: Command PUSH-BEGIN on file ID 823 succeeded. Received length: 10,485,760 bytes (10.0 MiB). Processed in 735 ms. Continue with PUSH-COMMIT or PUSH-ROLLBACK command.
   Content-Length: 147
   Server: Jetty(8.1.3.v20120522)
   
   OK: Command PUSH-BEGIN on file ID 823 succeeded. Received length: 10,485,760 bytes (10.0 MiB). Processed in 735 ms. Continue with PUSH-COMMIT or PUSH-ROLLBACK command.



.. _push-commit-command:

PUSH-COMMIT command
===================



.. _push-rollback-command:

PUSH-ROLLBACK command
=====================



*****************************************
Appendix A: Operating with curl as client
*****************************************

For testing and debugging purposes, commands may be issued from commandline
with `curl <http://curl.haxx.se/docs/manpage.html>`_ software.

Useful resources for curl:

* man page -- http://curl.haxx.se/docs/manpage.html
* scripting HTTP -- http://curl.haxx.se/docs/httpscripting.html

Some common used options include:

* ``--verbose`` prints request and response headers and body
* ``--user <username>:<password>`` credentials for authentication if needed
* ``--request, -X <method>`` specifies HTTP request method
* URL in single quotes (``'``) to protect against bash variable expansion
* ``-#`` shows progress bar instead of progress and speed table during transfer

.. note:: Examples bellows are split to multiple lines for easy reading, but
   has to be written as single line.

**VERSION command**

.. code-block:: bash

   $ curl -# --verbose --basic --user pxsu:changeme
   'http://localhost:8080/plexflow-api/version?trace'

**PULL command**

.. code-block:: bash

   $ curl -# --verbose --basic --user pxsu:changeme
   'http://localhost:8080/plexflow-api/pull?fileId=823&trace'
   --output path/to/file

**PUSH-BEGIN command**

.. code-block:: bash

   $ curl -# --verbose --basic --user pxsu:changeme
   --upload-file /var/plexflow-testfiles/10MiB.bin
   'http://localhost:8080/plexflow-api/push-begin?fileId=823&trace'
   --output 823

According to curl man page, a progress bar (``-#``) is ignored for PUT requests
unless you redirect output to a file (by normal bash means (``>``) or
``--output <filename>``).

**PUSH-COMMIT command**

.. code-block:: bash

   $ curl -# --verbose --basic --user pxsu:changeme
   'http://localhost:8080/plexflow-api/push-commit?fileId=823&trace'

**PUSH-ROLLBACK command**

.. code-block:: bash

   $ curl -# --verbose --basic --user pxsu:changeme
   'http://localhost:8080/plexflow-api/push-rollback?fileId=823&trace'