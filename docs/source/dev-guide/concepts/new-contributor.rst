New contributor handbook
########################

Terminology and naming conventions
**********************************

English and general terms
=========================

* **sign in/up/out, signing in** -- not login, register, logout, logging
* **folder, not directory** -- directory is seemed as out-dated, we never use it
* **file size vs. length** -- Term length is used through sources, API methods,
  DB schema, while size is tend to be more UI or user familiar.
* **Richclient vs. UI** – Richclient is for technical purposes referred simply
  as UI (also used for bundle and package names).
* **American spelling**


Other conventions
*****************

Text editor settings
====================

* **tab, tab, tabs!** -- don't use spaces
* **UTF-8 charset** -- other then UTF-8 only when absolutely required (like for
  Java .properties file)
  

Comments
========

Authored comments
-----------------

In non-Java files, like text files, bash scripts or similar, use comments in
the form::

   # YourName, YYYY-MM-DD: Commentary message

For example::

   # Libor, 2010-10-19: Allow raw HTML sections
   $wgRawHtml = true;

   # Libor, 2010-10-19: Enable syntax highlighter plugin
   require_once("$IP/extensions/SyntaxHighlight_GeSHi/SyntaxHighlight_GeSHi.php");

Tasks in comments
-----------------

Where appliable comment task prefixes

* ``TODO`` -- must be done
* ``MAYBE`` -- maybe/sometime
* ``FIXME`` -- a bug to solve
* ``KLUDGE`` -- qick-and-dirty clumsy inelegant workaround to a problem (hack)


Miscellaneous
=============

* **Default password "changeme"** to suggest to change it.
* **End folder paths with \\**, e.g. ``/path/no/nowhere/`` is path to nowhere/
  folder, whereas ``/path/to/somewhere`` is path to somewhere file.

Thirdparty software
===================

Any used thirdparty software should be listen in docs in :ref:`thirdparty-software`.


Java development
****************

Class template
==============
Every Java class or interface must conforms :download:`class-template.java <../reference/class-template.java>`.

.. warning:: All files should use template file as base, otherwise will not be
   accepted into master VCS repo.

package-info.java
=================
Every above-trivial package should supply overview level javadoc in
package-info.java.

@Override
=========
Missing ``@Override`` annotation is considered as error!

SLF4J logging
=============
We strictly rely on SLF4J backed with Logback. By convention, a SLF4J logger is
stored as static class field ``SYSLOG``::

	public class ThisClass {
		...
		private static final Logger SYSLOG = LoggerFactory.getLogger(ThisClass.class);
		...
	}

Exceptions
==========
Exception message should be as much descriptive as possible and includes
**unique error code** listed in :ref:`error-codes`. E.g.::
   
   throw new DbException("Cannot authenticate due to underlying database exception (error 1).", e);

Eclipse & OSGi development
**************************


Tools
*****

Eclipse IDE
===========

Versions
--------
Since Oct 2012 all development should be done in Eclipse 4.2 (Juno).

Git projects outside workspace
------------------------------

Eclipse Git client (EGit) strictly discourage to have git repos (``.git``) inside
workspace folder.

Your git projects should come to e.g. ``~/git/``, whereas workspace is usually
``~/workspace/``.

Recommended plug-ins
--------------------

* `Logback Beagle <http://logback.qos.ch/beagle/>`_ -- Logback logs viewer
* `ReST Editor <http://marketplace.eclipse.org/content/rest-editor>`_ -- confortable
  editing reStructured text makrup of :ref:`Plexflow docs <sphinx>`.
* `Eclipse Colorer <http://colorer.sourceforge.net/eclipsecolorer/>` -- syntax
  highlighting for more then 150 languages (including bash scripts).

Spell check
-----------
If you wish to do spell check, you may save some time with starting with
already existing community shared user dictionary.

In *Preferences / General / Editors / Text Editors / Spelling* choose 
``~/Dropbox/eclipse-user-dictionary`` as user dictionary.

Missing @Override
-----------------

Group by working sets
---------------------

Large number of projects can be grouped into working sets (WS). Usually,
developers has e.g.

* "Plexflow master" WS for mainline projects living in ``sources/`` folder
* "Plexflow drafts" WS for draft projects on they are working on in
  ``drafts/`` folder.
  
WS are not displayed by default in JDT Package Explorer, so change it as
depicted in the figure:

.. image:: group-by-ws.png
   :scale: 50%

Clone from within Eclipse
-------------------------
#. :menuselection:`File --> Import --> Git category --> Import Projects from Git wizard`.

#. As :guilabel:`URI` paste ``git@bitbucket.org:virtage/plexflow.git``.

   .. image:: eclipse-git-import1.png

#. Local destination for Git repository leave default ``~/git/plexflow/``.

   .. image:: eclipse-git-import2.png
      :scale: 50%

#. Keep pre-selected *Import existing projects* as *Wizard for project import*.

#. In next step, import projects that you are interested to work at.
   Usually you will want to import all projects in ``sources/`` folder (folder
   constituting master Plexflow sources) to "Plexflow master" working set,
   and all projects in ``drafts/`` to "Plexflow Drafts" workign set.
   
   .. image:: eclipse-git-import3.png
      :scale: 50%
   
   #. To filter e.g. sources projects, click *Deselect All* and type sources as filter.
   #. Select desired projects (or *Select All* button).
   #. Set target working set.

SQL Workbench/J
===============

MyBatis Migrations
==================

Git versioning
==============

Project sources are hosted externally at https://bitbucket.org/virtage/plexflow/::

   $ git clone git@bitbucket.org:virtage/plexflow.git

After clone steps
-----------------

#. **Protect run configs.** Eclipse very like to unintentionally change
   (read damage) run configuration (``.launch`` files). To prevent millions
   revisions of them, it's best to set to them immutable (on ext2+ file
   systems) or read-only attribute (Win).
   
   For ext2+, just run ``misc/protect_run_configs/protect_run_configs_on.sh`` or
   ``protect_run_configs_off.sh`` script respectively.
   
#. **Prepare target platforms.** Target platforms are stored outside VCS in
   Dropbox shared folders. Read README.TXT file in someproject.target folders
   how to prepare target for your local copy. Never place anything from target
   in VCS!

Commit message
--------------
* A hard wrap at 72 characters.
* A single, short, summary of the commit in present time
  ("fix console logging", not "fixed console logging").
* Followed by a single blank line.
* Followed by supporting details.

No binaries
-----------
Never place (any) binary datas in VCS!

Branches
--------
* Develop and test new features in ``draft/<feature-name>`` branches.
* Bug fixes should come in ``bug/<bug-id>-<short-name>`` branches.

vcs-ignore
----------
Folder or file having ``vcs-ignore`` in the name is ignored by Git. This is
utilized in particular by large binary datas stored outside repo like Eclipse
target platforms.

Empty folders
-------------
Git cannot store empty folders. If you need to preserve them in commit, place
inside empty ``.gitignore`` file beneath folder with content e.g.::

   # Git, track this empty folder!

Delete this file when it becomes unnecessary.


Sphinx documentation
====================
Plexflow maintains its documentation using great `Sphinx <sphinx.pocoo.org>`
documentation tool. Sphinx reads documents written using
`reStructuredText <http://docutils.sourceforge.net/docs/user/rst/quickref.html>`
markup language.

For best user experience, install `ReST Editor <http://marketplace.eclipse.org/content/rest-editor>`_
plug-in for Eclipse.

There is also Eclipse run configuration in ``docs/plexflow-docs.launch``.

**Install procedure on vanilla fresh Ubuntu**::

   $ sudo apt-get install python-setuptools
   $ sudo easy_install -U Sphinx 