.. _getting_started:


I am Getting started
####################

.. _installing-docdir:

Installing your doc directory
*****************************

IPython example:

.. ipython::

   In [136]: x = 2

   In [137]: x**3
   Out[137]: 8
   
Here is *ita\*lics*, **bold**, ``code``. Hi, thisis\ *one*\ word!
Another way to :strong:`have bold`.

* This is a bulleted list.
* It has two items, the second
  item uses two lines.

1. This is a numbered list.
2. It has two items too.

#. This is a numbered list.
#. It has two items too.

* this is
* a list

  * with a nested list
  * and some subitems

* and here the parent list continues

term (up to a line of text)
   Definition of the term, which must be indented

   and can even consist of multiple paragraphs

next term
   Description.
   
| These lines are
| broken exactly like in
| the source file.

This is a normal text paragraph. The next paragraph is a code sample::

   It is not processed in any way, except
   that the indentation is removed.

   It can span multiple lines.

This is a normal text paragraph again.

+------------------------+----------+----------+----------+
| Header row, column 1   | Header 2 | Header 3 | Header 4 |
| (header rows optional) |          |          |          |
+========================+==========+==========+==========+
| body row 1, column 1   | column 2 | column 3 | column 4 |
+------------------------+----------+----------+----------+
| body row 2             | ...      | ...      |          |
+------------------------+----------+----------+----------+

or a simple table

=====  =====  =======
A      B      A and B
=====  =====  =======
False  False  False
True   False  False
False  True   False
True   True   True
=====  =====  =======

This is a paragraph that contains `a link`_.

.. _a link: http://example.com/

.. code-block:: python

   01 def energy (mass, c):
   02 """
   03 Energy is equivalent to mass m times
   04 the square of the speed of light c.
   05 """
   06 return mass * c * c
   
.. code-block:: java

   System.currentTimeMillis();
   
.. code-block:: html

this is example of 
   
   

This is a heading asses
=======================

.. NOTE::
   Beware killer rabbits!
   
Lorem ipsum [#f1]_ dolor sit amet ... [#f2]_

.. rubric:: Footnotes

.. [#f1] Text of the first footnote.
.. [#f2] Text of the second footnote.

Lorem ipsum [Ref]_ dolor sit amet.

.. [Ref] Book or article reference, URL or whatever.

.. |name| replace:: replacement *text*

Hello |name|, how are you?

.. This is a comment.

..
   This whole indented block
   is a comment.

   Still in the comment.

You may already have sphinx `sphinx <http://sphinx.pocoo.org/>`_
installed -- you can check by doing::

  python -c 'import sphinx'

If that fails grab the latest version of and install it with::

  > sudo easy_install -U Sphinx

Now you are ready to build a template for your docs, using
sphinx-quickstart::

  > sphinx-quickstart
    
OR this is my
  
  pretext with two spaces

accepting most of the defaults.  I choose "sampledoc" as the name of my
project.  cd into your new directory and check the contents::

  home:~/tmp/sampledoc> ls
  Makefile	_static		conf.py
  _build		_templates	index.rst

The index.rst is the master ReST for your project, but before adding
anything, let's see if we can build some html::

  make html

If you now point your browser to :file:`_build/html/index.html`, you
should see a basic sphinx site.

.. image:: _static/basic_screenshot.png

.. _fetching-the-data:

Fetching the data
-----------------

Now we will start to customize out docs.  Grab a couple of files from
the `web site
<http://matplotlib.svn.sourceforge.net/viewvc/matplotlib/trunk/sampledoc_tut/>`_
or svn.  You will need :file:`getting_started.rst` and
:file:`_static/basic_screenshot.png`.  All of the files live in the
"completed" version of this tutorial, but since this is a tutorial,
we'll just grab them one at a time, so you can learn what needs to be
changed where.  Since we have more files to come, I'm going to grab
the whole svn directory and just copy the files I need over for now.
First, I'll cd up back into the directory containing my project, check
out the "finished" product from svn, and then copy in just the files I
need into my :file:`sampledoc` directory::

  home:~/tmp/sampledoc> pwd
  /Users/jdhunter/tmp/sampledoc
  home:~/tmp/sampledoc> cd ..
  home:~/tmp> svn co https://matplotlib.svn.sourceforge.net/svnroot/\
  matplotlib/trunk/sampledoc_tut
  A    sampledoc_tut/cheatsheet.rst
  A    sampledoc_tut/_static
  A    sampledoc_tut/_static/basic_screenshot.png
  A    sampledoc_tut/conf.py
  A    sampledoc_tut/Makefile
  A    sampledoc_tut/_templates
  A    sampledoc_tut/_build
  A    sampledoc_tut/getting_started.rst
  A    sampledoc_tut/index.rst
  Checked out revision 7449.
  home:~/tmp> cp sampledoc_tut/getting_started.rst sampledoc/
  home:~/tmp> cp sampledoc_tut/_static/basic_screenshot.png \
  sampledoc/_static/

The last step is to modify :file:`index.rst` to include the
:file:`getting_started.rst` file (be careful with the indentation, the
"g" in "getting_started" should line up with the ':' in ``:maxdepth``::

  Contents:

  .. toctree::
     :maxdepth: 2

     getting_started.rst

and then rebuild the docs::

  cd sampledoc
  make html

Please see :ref:`tilda.html`.

When you reload the page by refreshing your browser pointing to
:file:`_build/html/index.html`, you should see a link to the
"Getting Started" docs, and in there this page with the screenshot.
`Voila!`

Note we used the image directive to include to the screenshot above
with::

  .. image:: _static/basic_screenshot.png


Next we'll customize the look and feel of our site to give it a logo,
some custom css, and update the navigation panels to look more like
the `sphinx <http://sphinx.pocoo.org/>`_ site itself -- see
:ref:`custom_look`.

