package apachehttpclientrcp.draft;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.FileEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.ui.IStartup;

import com.virtage.plexflow.utils.IoUtils;

public class Startup implements IStartup {
	

	//--------------------------------------------------------------------------
	// Class fields - public
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Class fields - non-public
	//--------------------------------------------------------------------------

	//private static final Logger SYSLOG = LoggerFactory.getLogger(ThisClass.class);

	//--------------------------------------------------------------------------
	// Instance fields - public
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Instance fields - non-public
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Instance fields - bean properties
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Constructors, static initializers, factories
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Public API - class (static)
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Public API - instance
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Overrides and implementations
	//--------------------------------------------------------------------------

	@Override
	public void earlyStartup() {				
		
		DefaultHttpClient cli = new DefaultHttpClient();
		cli.getCredentialsProvider().setCredentials(AuthScope.ANY, new UsernamePasswordCredentials("pxsu", "changeme"));
		
		// Pull
		//HttpGet req = new HttpGet("http://localhost:8080/plexflow-api/pull?fileId=823");
		//HttpGet req = new HttpGet("http://localhost:8080/plexflow-api/version");
		
		HttpPut req = new HttpPut("http://localhost:8080/plexflow-api/push-begin?fileId=823");		
		
		req.setEntity(new FileEntity(new File("/var/plexflow-testfiles/1KiB.bin"), "application/octet-stream"));
		
//			// Download to temp file ("null,null" params - no prefix and suffix)
//			Path tmpFile = Files.createTempFile(null, null);				
//					
//			try (InputStream is = entity.getContent();
//				 OutputStream os = Files.newOutputStream(tmpFile, StandardOpenOption.DELETE_ON_CLOSE)) {
//				IoUtils.copyStream(is, os, true, new StdoutProgressMonitor(), 10485760);				
//			}
		
			
		
		
		long start = System.currentTimeMillis();
		try {
			HttpResponse resp = cli.execute(req);
			System.out.println(resp);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		long end = System.currentTimeMillis();
		
		System.out.println("Elapsed time (ms) = " + (end - start));
		
	}
	
	//--------------------------------------------------------------------------
	// Consider toString(), equals(), hashCode()
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Getters/setters of properties
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Helper methods (mostly private)
	//--------------------------------------------------------------------------
	
}
