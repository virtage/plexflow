package apachehttpclientrcp.draft;

import org.eclipse.core.runtime.IProgressMonitor;

public class StdoutProgressMonitor implements IProgressMonitor {

	@Override
	public void worked(int work) {
		System.out.print("worked " + work + "... ");
	}

	@Override
	public void subTask(String name) {
		// TODO Auto-generated method stub

	}

	@Override
	public void setTaskName(String name) {
		// TODO Auto-generated method stub

	}

	@Override
	public void setCanceled(boolean value) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean isCanceled() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void internalWorked(double work) {
		// TODO Auto-generated method stub

	}

	@Override
	public void done() {
		System.out.println("done");
	}

	@Override
	public void beginTask(String name, int totalWork) {
		System.out.println("beginTask " + name + " (" + totalWork + ")");
	}
}
