package app;

import org.eclipse.equinox.app.IApplication;
import org.eclipse.equinox.app.IApplicationContext;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.PlatformUI;

import com.virtage.plexflow.system.LoginService;
import com.virtage.plexflow.system.SystemInstance;
import com.virtage.plexflow.system.SystemInstanceFactory;
import com.virtage.plexflow.system.SystemsProperties;
import com.virtage.plexflow.system.SystemsPropertiesRecord;
import com.virtage.plexflow.ui.PxUiConstants;
import com.virtage.plexflow.ui.wizards.bulkimport.BulkImportWizard;

/**
 * This class controls all aspects of the application's execution
 */
public class Application implements IApplication {

	/* (non-Javadoc)
	 * @see org.eclipse.equinox.app.IApplication#start(org.eclipse.equinox.app.IApplicationContext)
	 */
	@Override
	public Object start(IApplicationContext context) throws Exception {
		Display display = PlatformUI.createDisplay();
		try {
			
			// Sign in
//			SystemsPropertiesRecord spr = SystemsProperties.listAvailableSystems().get(0);			
//			SystemInstance si = LoginService.getInstance().loginAsSu(spr);
//			SystemInstanceFactory.setInstance(si);
			
			SystemsPropertiesRecord spr = SystemsProperties.listAvailableSystems().get(0);			
			SystemInstance si = LoginService.getInstance().login(spr, "pxsu", "changeme");
			SystemInstanceFactory.setInstance(si);
						
			// Start workbench			
			int returnCode = PlatformUI.createAndRunWorkbench(display, new ApplicationWorkbenchAdvisor());
			if (returnCode == PlatformUI.RETURN_RESTART)
				return IApplication.EXIT_RESTART;
			else
				return IApplication.EXIT_OK;
			
		} finally {
			display.dispose();
		}
		
	}

	/* (non-Javadoc)
	 * @see org.eclipse.equinox.app.IApplication#stop()
	 */
	@Override
	public void stop() {
		if (!PlatformUI.isWorkbenchRunning())
			return;
		final IWorkbench workbench = PlatformUI.getWorkbench();
		final Display display = workbench.getDisplay();
		display.syncExec(new Runnable() {
			@Override
			public void run() {
				if (!display.isDisposed())
					workbench.close();
			}
		});
	}
}
