package app;

import org.eclipse.jface.action.IContributionItem;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.ui.IWorkbenchActionConstants;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.actions.ActionFactory;
import org.eclipse.ui.actions.ActionFactory.IWorkbenchAction;
import org.eclipse.ui.actions.ContributionItemFactory;
import org.eclipse.ui.application.ActionBarAdvisor;
import org.eclipse.ui.application.IActionBarConfigurer;

public class ApplicationActionBarAdvisor extends ActionBarAdvisor {

	public static final String MB_FILE = "com.virtage.plexflow.ui.menu.file";
	
	
	private IContributionItem newWizardShortList;
	private IWorkbenchAction close;
	private IWorkbenchAction closeAll;
	private IWorkbenchAction quit;
	private IWorkbenchAction properties;
	
	
    public ApplicationActionBarAdvisor(IActionBarConfigurer configurer) {
        super(configurer);
    }

    @Override
	protected void makeActions(IWorkbenchWindow window) {
    	// File menu
		newWizardShortList = ContributionItemFactory.NEW_WIZARD_SHORTLIST.create(window);
		close = ActionFactory.CLOSE.create(window);
		closeAll = ActionFactory.CLOSE_ALL.create(window);
		quit = ActionFactory.QUIT.create(window);
		properties = ActionFactory.PROPERTIES.create(window);    	
    }

    @Override
	protected void fillMenuBar(IMenuManager menuBar) {
    	MenuManager file = new MenuManager("&File", MB_FILE);
		file.add(newWizardShortList);
		file.add(new Separator());
		file.add(close);
		file.add(closeAll);
		file.add(new Separator());
		// --> Declaratively contributions will be placed here <--
		file.add(new Separator(IWorkbenchActionConstants.MB_ADDITIONS));		
		file.add(properties);
		file.add(new Separator());
		file.add(quit);
		
		menuBar.add(file);
    }
    
}
