package app;

import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.SWT;
import org.eclipse.ui.IStartup;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;

import com.virtage.plexflow.storage.IFile;
import com.virtage.plexflow.storage.IFolder;
import com.virtage.plexflow.storage.Storage;
import com.virtage.plexflow.storage.datafolder.client.DatafolderClient;
import com.virtage.plexflow.storage.datafolder.client.IDatafolderClient;
import com.virtage.plexflow.storage.exceptions.NodeAlreadyExistsException;
import com.virtage.plexflow.storage.exceptions.NodeDeletedException;
import com.virtage.plexflow.storage.exceptions.NodeNameForbiddenException;
import com.virtage.plexflow.storage.exceptions.NodeNotFoundException;
import com.virtage.plexflow.storage.exceptions.NodeTrashedException;
import com.virtage.plexflow.ui.PxUiConstants;
import com.virtage.plexflow.ui.wizards.bulkimport.BulkImportWizard;
import com.virtage.plexflow.utils.IoUtils;
import com.virtage.plexflow.utils.PxUtils;


public class Startup implements IStartup {

	@Override
	public void earlyStartup() {		
		final IWorkbench workbench = PlatformUI.getWorkbench();
		workbench.getDisplay().asyncExec(new Runnable() {
			@Override
			public void run() {
				IWorkbenchWindow window = workbench.getActiveWorkbenchWindow();
				if (window != null) {
					
					// Bulk import wizard
//					BulkImportWizard wiz = new BulkImportWizard();			
//					WizardDialog wizDiag = new WizardDialog(null, wiz);										
//					wizDiag.create();
//					wizDiag.open();
					
					System.out.println("maxMemory = " + PxUtils.humanReadableByteCount(Runtime.getRuntime().maxMemory()));
					System.out.println("totalMemory = " + PxUtils.humanReadableByteCount(Runtime.getRuntime().totalMemory()));
					System.out.println("freeMemory = " + PxUtils.humanReadableByteCount(Runtime.getRuntime().freeMemory()));
					
					
					try {
						IDatafolderClient DF = DatafolderClient.getInstance();		
						
						String sourceFolder = "/var/plexflow-testfiles/";
						String sourceFile = "1GiB.bin";
						IFolder targetFolder = Storage.get().getWorkspace().getFolder("složka v rootu");
						
						// nový PX soubor
						//IFile targetFile = targetFolder.createFile(sourceFile);
						IFile targetFile = targetFolder.getFile("testfile");
						
						// pušnout
						DF.push(targetFile, Paths.get(sourceFolder, sourceFile), new SysoutProgressMonitor());
						
//						try (
//							InputStream is = Files.newInputStream(Paths.get(sourceFolder, sourceFile));
//							OutputStream os = Files.newOutputStream(Paths.get("/home/libor/tmp/target.bin"))
//							 ) {
//							IoUtils.copyStream(is, os, true, new SysoutProgressMonitor(), Files.size(Paths.get(sourceFolder, sourceFile)));
//						}
						
						
					} catch (Exception e) {
						e.printStackTrace();
					}
					
				}
			}
		});

		

	}
	//--------------------------------------------------------------------------
	// Class fields - public
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Class fields - non-public
	//--------------------------------------------------------------------------

	//private static final Logger SYSLOG = LoggerFactory.getLogger(ThisClass.class);

	//--------------------------------------------------------------------------
	// Instance fields - public
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Instance fields - non-public
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Instance fields - bean properties
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Constructors, static initializers, factories
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Public API - class (static)
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Public API - instance
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Overrides and implementations
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Consider toString(), equals(), hashCode()
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Getters/setters of properties
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Helper methods (mostly private)
	//--------------------------------------------------------------------------
	
	private static final class SysoutProgressMonitor implements
			IProgressMonitor {
		@Override
		public void worked(int work) {
			System.out.println("worked " + work);
		}

		@Override
		public void subTask(String name) {
			// TODO Auto-generated method stub

		}

		@Override
		public void setTaskName(String name) {
			// TODO Auto-generated method stub

		}

		@Override
		public void setCanceled(boolean value) {
			// TODO Auto-generated method stub

		}

		@Override
		public boolean isCanceled() {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public void internalWorked(double work) {
			// TODO Auto-generated method stub

		}

		@Override
		public void done() {
			System.out.println("done");
		}

		@Override
		public void beginTask(String name, int totalWork) {
			System.out.println("beginTask " + name + " (" + totalWork + ")");
		}
	}
	
}
