package com.virtage.plexflow.ui.wizards.bulkimport;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;

public class ProgressPage extends WizardPage {

	//--------------------------------------------------------------------------
	// Class fields - public
	//--------------------------------------------------------------------------
	
	public static final String PAGE_NAME = "PROGRESS";
	private Text txtLog;
	
	//--------------------------------------------------------------------------
	// Class fields - non-public
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Instance fields - public
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Instance fields - non-public
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Instance fields - bean properties
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Constructors, static initializers, factories
	//--------------------------------------------------------------------------

	/**
	 * Create the wizard.
	 */
	public ProgressPage() {
		super(PAGE_NAME);
		setTitle("Bulk import in progress...");
		setDescription("Please wait until import finishes");		
		
		// This page is only "summary", i.e. always complete
		setPageComplete(true);
	}
	
	//--------------------------------------------------------------------------
	// Public API - class (static)
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Public API - instance
	//--------------------------------------------------------------------------

	/**
	 * 
	 */
	void startImport() {
		try {
			getContainer().run(true, true, new IRunnableWithProgress() {				
				@Override
				public void run(IProgressMonitor monitor) throws InvocationTargetException,
						InterruptedException {
				
					monitor.beginTask("Jdi do prdele", 100);
					
					for (int i = 1; i < 101; i += 20) {
						Thread.sleep(2000);
						monitor.worked(i);			
						System.out.println("odpracováno " + i + "...");
					}
					
					monitor.done();
				}
			});
		} catch (InvocationTargetException | InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	//--------------------------------------------------------------------------
	// Overrides and implementations
	//--------------------------------------------------------------------------

	/**
	 * Create contents of the wizard.
	 * @param parent
	 */
	@Override
	public void createControl(Composite parent) {
		Composite container = new Composite(parent, SWT.NULL);
		//container.setSize(new Point(500, 300));

		setControl(container);
		container.setLayout(new FillLayout(SWT.HORIZONTAL));
		
		txtLog = new Text(container, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL | SWT.CANCEL | SWT.WRAP);		
		//txtLog.setText("ahoj");
		txtLog.setText("Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Sed ac dolor sit amet purus malesuada congue. Mauris tincidunt sem sed arcu. Integer in sapien. Nullam dapibus fermentum ipsum. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hymenaeos. Integer imperdiet lectus quis justo. Donec iaculis gravida nulla. Duis ante orci, molestie vitae vehicula venenatis, tincidunt ac pede. Cras elementum. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. In dapibus augue non sapien. In laoreet, magna id viverra tincidunt, sem odio bibendum justo, vel imperdiet sapien wisi sed libero.\n\nCras elementum. Nunc auctor. Nulla est. Sed ac dolor sit amet purus malesuada congue. Nunc auctor. Mauris suscipit, ligula sit amet pharetra semper, nibh ante cursus purus, vel sagittis velit mauris vel metus. Proin in tellus sit amet nibh dignissim sagittis. Duis viverra diam non justo. Integer imperdiet lectus quis justo. Mauris metus. In sem justo, commodo ut, suscipit at, pharetra vitae, orci. Mauris elementum mauris vitae tortor. In laoreet, magna id viverra tincidunt, sem odio bibendum justo, vel imperdiet sapien wisi sed libero. Donec iaculis gravida nulla. Quisque porta. Nulla pulvinar eleifend sem.\n\nSuspendisse nisl. Suspendisse sagittis ultrices augue. Etiam bibendum elit eget erat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Phasellus rhoncus. Integer lacinia. Nulla pulvinar eleifend sem. Integer lacinia. Nullam feugiat, turpis at pulvinar vulputate, erat libero tristique tellus, nec bibendum odio risus sit amet ante. Vestibulum erat nulla, ullamcorper nec, rutrum non, nonummy ac, erat. Morbi leo mi, nonummy eget tristique non, rhoncus non leo. In convallis. Maecenas fermentum, sem in pharetra pellentesque, velit turpis volutpat ante, in pharetra metus odio a lectus.");
		//txtLog.setSize(new Point(100, 50));
		txtLog.setEditable(false);
		
		
		
	}
	
	@Override
	public void dispose() {
		super.dispose();
		
		// TODO: dispose locked files
		
	}	
}
