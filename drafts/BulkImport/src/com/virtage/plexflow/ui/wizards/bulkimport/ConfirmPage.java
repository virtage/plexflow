package com.virtage.plexflow.ui.wizards.bulkimport;

import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ColumnWeightData;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TableLayout;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;

import com.virtage.plexflow.ui.wizards.bulkimport.BulkImportWizard.FolderPair;

public class ConfirmPage extends WizardPage {

	//--------------------------------------------------------------------------
	// Class fields - public
	//--------------------------------------------------------------------------
	
	public static final String PAGE_NAME = "CONFIRM";
	
	//--------------------------------------------------------------------------
	// Class fields - non-public
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Instance fields - public
	//--------------------------------------------------------------------------

	TableViewer tableViewer;
	
	//--------------------------------------------------------------------------
	// Instance fields - non-public
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Instance fields - bean properties
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Constructors, static initializers, factories
	//--------------------------------------------------------------------------

	/**
	 * Create the wizard.
	 */
	public ConfirmPage() {
		super(PAGE_NAME);
		setTitle("Confirm bulk import settings");
		setDescription("Confirm source and target folder pairs");		
		
		// Default is false until a checkbox to really proceed is selected 
		setPageComplete(false);
	}
	
	//--------------------------------------------------------------------------
	// Public API - class (static)
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Public API - instance
	//--------------------------------------------------------------------------

//	/**
//	 * Called by last selection page to fill in table viewer with 
//	 * {@link BulkImportWizard#folderPairs}.
//	 */
//	void fillTableViewerWithFolderPairs() {
//		BulkImportWizard wiz = (BulkImportWizard) getWizard();
//		tableViewer.setInput(wiz.folderPairs);
//	}
	
	//--------------------------------------------------------------------------
	// Overrides and implementations
	//--------------------------------------------------------------------------

	/**
	 * Create contents of the wizard.
	 * @param parent
	 */
	@Override
	public void createControl(Composite parent) {
		Composite container = new Composite(parent, SWT.NULL);
		container.setLayout(new GridLayout(2, false));

		Label lblTheFollowingSourcetarge = new Label(container, SWT.NONE);
		lblTheFollowingSourcetarge
				.setText("The following source-targe folder pair(s) have been defined:");	

		final Button btnRemoveSelected = new Button(container, SWT.NONE);
		btnRemoveSelected.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false,
				false, 1, 1));
		btnRemoveSelected.setText("Remove selected");
		btnRemoveSelected.setEnabled(false);	// disable until something is selected in table
		btnRemoveSelected.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				IStructuredSelection ss = (IStructuredSelection) tableViewer.getSelection();
				
				if (ss.isEmpty()) return;
				
				FolderPair selectedFolderPair = (FolderPair) ss.getFirstElement();				
				((BulkImportWizard) getWizard()).pairs.remove(selectedFolderPair);
				
				tableViewer.remove(selectedFolderPair);
			}
		});

		this.tableViewer = new TableViewer(container, SWT.BORDER
				| SWT.FULL_SELECTION);
		Table table = tableViewer.getTable();
		
		TableLayout layout = new TableLayout();
		layout.addColumnData(new ColumnWeightData(50));
		layout.addColumnData(new ColumnWeightData(50));

		table.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 2, 1));
		table.setLinesVisible(true);
		table.setHeaderVisible(true);
		table.setLayout(layout);

		TableColumn column1 = new TableColumn(table, SWT.LEAD);
		column1.setText("Source regular folder");
		column1.setAlignment(SWT.RIGHT);
		TableColumn column2 = new TableColumn(table, SWT.LEAD);
		column2.setText("Target Plexflow folder");

		tableViewer.setContentProvider(ArrayContentProvider.getInstance());		
		tableViewer.setLabelProvider(new ITableLabelProvider() {
			@Override
			public void removeListener(ILabelProviderListener listener) {
			}

			@Override
			public boolean isLabelProperty(Object element, String property) {
				return false;
			}

			@Override
			public void dispose() {
			}

			@Override
			public void addListener(ILabelProviderListener listener) {
			}

			@Override
			public String getColumnText(Object element, int columnIndex) {				
				FolderPair pair = (FolderPair) element;				
				if (columnIndex == 0) {
					return pair.source.toString();					
				} else {
					return pair.target.getPath().toString();					
				}
			}

			@Override
			public Image getColumnImage(Object element, int columnIndex) {
				return null;
			}
		});		
		// tableViewer.setInput() -- called later from setVisible()
		
		tableViewer.addSelectionChangedListener(new ISelectionChangedListener() {			
			@Override
			public void selectionChanged(SelectionChangedEvent event) {
				IStructuredSelection ss = (IStructuredSelection) event.getSelection();
				
				if (ss.isEmpty()) {
					btnRemoveSelected.setEnabled(false);
					return;
				}
				
				btnRemoveSelected.setEnabled(true);
			}
		});
		
		final Button chbProceed = new Button(container, SWT.CHECK);		
		chbProceed.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 2, 1));
		chbProceed.setText("Proceed to start bulk import process. This cannot be undone or cancelled.");
		chbProceed.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				/** Set page completeness if checkbox is selected and something
				is in the {@link BulkImportWizard#pairs}. */ 			
				BulkImportWizard bulkImportWizard = (BulkImportWizard) getWizard();
				
				if (chbProceed.getSelection() && (!bulkImportWizard.pairs.isEmpty()))
					setPageComplete(true);
				else
					setPageComplete(false);
			}
		});
		
		// Required to avoid an error in the system
		setControl(container);
	}
	
	
	/**
	 * When page is made visible, fill table viewer with 
	 * {@link BulkImportWizard#folderPairs}.
	 */
	@Override
	public void setVisible(boolean visible) {
		super.setVisible(visible);
		
		if (visible) {
			BulkImportWizard wiz = (BulkImportWizard) getWizard();
			tableViewer.setInput(wiz.pairs);
		}
	}

	//--------------------------------------------------------------------------
	// Consider toString(), equals(), hashCode()
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Getters/setters of properties
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Helper methods (mostly private)
	//--------------------------------------------------------------------------
	
}
