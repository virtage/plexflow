package com.virtage.plexflow.ui.wizards.bulkimport;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.dialogs.IPageChangedListener;
import org.eclipse.jface.dialogs.IPageChangingListener;
import org.eclipse.jface.dialogs.PageChangedEvent;
import org.eclipse.jface.dialogs.PageChangingEvent;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IImportWizard;
import org.eclipse.ui.IWorkbench;

import com.virtage.plexflow.storage.IFolder;
import com.virtage.plexflow.ui.PxUiConstants;

/**
 * Bulk import wizard implementation. 
 * 
 * Usage example:
 * <pre>
 * BulkImportWizard wiz = new BulkImportWizard();			
 * WizardDialog wizDiag = new WizardDialog(shell, wiz);										
 * wizDiag.create();
 * wizDiag.open();
 * </pre>
 * </p>
 * 
 * 
 * @author libor
 *
 */
public class BulkImportWizard extends Wizard implements IImportWizard {
	
	//--------------------------------------------------------------------------
	// Class fields - public
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Class fields - non-public
	//--------------------------------------------------------------------------

	/**
	 * Indicates how many {@link SelectionPage}s will be created in
	 * advance, i.e. how many source-target pairs can be defined at most.
	 */
	private static final int selectionPagesLimit = 30;
	
	//--------------------------------------------------------------------------
	// Instance fields - public
	//--------------------------------------------------------------------------	
	
	//--------------------------------------------------------------------------
	// Instance fields - non-public
	//--------------------------------------------------------------------------		
	
	/**
	 * Enables/disabled Finish button. Returned by {@link #canFish()}. Set by
	 * individual wizard pages. 
	 **/
	boolean canFinish = false;	
	
	/** Collection to save in-advance instantiated {@link SelectionPage}s. */ 
	List<SelectionPage> selectionPages = new ArrayList<>(selectionPagesLimit);	
	
	/**
	 * Collection to save defined source-target pairs. Contributed by
	 * individual {@link SelectionPage}s. 
	 */
	List<FolderPair> pairs = new ArrayList<>();
	
	//--------------------------------------------------------------------------
	// Instance fields - bean properties
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Constructors, static initializers, factories
	//--------------------------------------------------------------------------

	/**
	 * Creates new {@link BulkImportWizard} instance. Instantiates predefined 
	 * number of {@link #selectionPagesLimit} in advance.
	 * */
	public BulkImportWizard() {
		setWindowTitle("Bulk import wizard");
		
		// Default order is this:
		addPage(new WelcomePage());
		
		// Pre-created selection pages
		for (int i = 0; i < selectionPagesLimit; i++) {
			SelectionPage selPage = new SelectionPage(i);
			selectionPages.add(selPage);
			addPage(selPage);
		}
		
		addPage(new ConfirmPage());
		addPage(new ProgressPage());
		addPage(new SummaryPage());
	}
	
	//--------------------------------------------------------------------------
	// Public API - class (static)
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Public API - instance
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Overrides and implementations
	//--------------------------------------------------------------------------
	
	@Override
	public void createPageControls(Composite pageContainer) {
		super.createPageControls(pageContainer);		
		
		// Ensure wizard is embedded into WizardDialog
		if (getContainer() instanceof WizardDialog == false) {
			throw new IllegalStateException("This wizard container isn't " +
				"org.eclipse.jface.wizard.WizardDialog! Easiest to be used as: " +
				"\n\t" +
				"new WizardDialog(null, new BulkImportWizard())");
		}
		
		// Set dimensions
		// QUIRK: For some reason bellow is ignored....
		//wizDiag.setPageSize(PxUiConstants.WIZARD_WIDTH, PxUiConstants.WIZARD_HEIGHT);
		// but works this:
		getShell().setSize(PxUiConstants.WIZARD_WIDTH, PxUiConstants.WIZARD_HEIGHT);
		
		
		
		// On progress page, start import
		WizardDialog wizDiag = ((WizardDialog) getContainer());
		wizDiag.addPageChangedListener(new IPageChangedListener() {			
			@Override
			public void pageChanged(PageChangedEvent event) {							
				if (event.getSelectedPage() instanceof ProgressPage) {
					((ProgressPage) event.getSelectedPage()).startImport();
				}
			}
		});
		
		// Attach page changing listener
		wizDiag.addPageChangingListener(new IPageChangingListener() {
			@Override
			public void handlePageChanging(PageChangingEvent event) {
				IWizardPage currentPage = (IWizardPage) event.getCurrentPage();
				IWizardPage targetPage = (IWizardPage) event.getTargetPage();
				
				// For every selection page
				if (currentPage.getName().startsWith(SelectionPage.PAGE_NAME)) {
					// Ask whether its FolderPair is already in collection
					FolderPair pair = ((SelectionPage) currentPage).getFolderPair();

					if (!pairs.contains(pair)) {
						pairs.add(pair);		// If not, let's add it
					}
				}
			}
		});
		
		
	};
	
	@Override
	public boolean needsProgressMonitor() {
		return true;
	}
	
	@Override
	public boolean performFinish() {
		return false;
		// TODO: unlock files
	}
	
	@Override
	public boolean canFinish() {
		return canFinish;
	}
	
	@Override
	public IWizardPage getNextPage(IWizardPage page) {
		// Only for selection page special logic
		if (page instanceof SelectionPage) {
			// to continue with next selection page if checkbox selected
			if (!((SelectionPage) page).repeatSelection()) {
				return getPage(ConfirmPage.PAGE_NAME); 
			}
		}
		
		// Or normal page flow
		return super.getNextPage(page);		
	}
	
	@Override
	public void init(IWorkbench workbench, IStructuredSelection selection) {
		// intentionally nothing
	}
	
	//--------------------------------------------------------------------------
	// Consider toString(), equals(), hashCode()
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Getters/setters of properties
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Helper methods (mostly private)
	//--------------------------------------------------------------------------

	/**
	 * Value object for source-target folder pair.
	 */
	public static class FolderPair {
		public Path source;
		public IFolder target;

		public FolderPair(Path source, IFolder target) {
			this.source = source;
			this.target = target;
		}

		@Override
		public String toString() {
			return "FolderPair [source=" + source + ", target=" + target + "]";
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result
					+ ((target == null) ? 0 : target.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			FolderPair other = (FolderPair) obj;
			if (target == null) {
				if (other.target != null)
					return false;
			} else if (!target.equals(other.target))
				return false;
			return true;
		}
	}
}
