package com.virtage.plexflow.ui.wizards.bulkimport;

import java.nio.file.Files;
import java.nio.file.Paths;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.fieldassist.ControlDecoration;
import org.eclipse.jface.fieldassist.FieldDecorationRegistry;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.virtage.plexflow.storage.IFolder;
import com.virtage.plexflow.ui.explorer.dialogs.StorageTreeDialog;
import com.virtage.plexflow.ui.wizards.bulkimport.BulkImportWizard.FolderPair;

public class SelectionPage extends WizardPage {
	//--------------------------------------------------------------------------
	// Class fields - public
	//--------------------------------------------------------------------------

	public static final String PAGE_NAME = "SELECTION_";
	
	//--------------------------------------------------------------------------
	// Class fields - non-public
	//--------------------------------------------------------------------------
	
	private static final Logger SYSLOG = LoggerFactory.getLogger(SelectionPage.class);
	
	//--------------------------------------------------------------------------
	// Instance fields - public
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Instance fields - non-public
	//--------------------------------------------------------------------------
	
	/** Order number of this instance to be used in page name identifier. */
	private int orderNumber;
	
	/** Holds selected regular source folder as string. */
	private String source;
	/** Holds selected Plexflow target. */ 
	private IFolder target;
	
	/** [x] Continue to define next source-target pair? */
	private Button chbAnotherPair;

	private Text txtSource;
	private Text txtTarget;
	ControlDecoration txtSourceDecoration;
	ControlDecoration txtTargetDecoration;
	
	//--------------------------------------------------------------------------
	// Instance fields - bean properties
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Constructors, static initializers, factories
	//--------------------------------------------------------------------------

	/**
	 * Create the wizard.
	 * 
	 * @param orderNumber Required number to be used as page name identifier.
	 */
	public SelectionPage(int orderNumber) {
		super(PAGE_NAME + orderNumber);
		this.orderNumber = orderNumber;
		
		setTitle("Select import source and target");
		setDescription("Choose source and target folder pair for import");
		
		setPageComplete(false);
	}
	
	//--------------------------------------------------------------------------
	// Public API - class (static)
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Public API - instance
	//--------------------------------------------------------------------------	
	
	/**
	 * Tells whether user selected "Define another source-target pair"
	 * checkbox on the page.
	 **/
	public boolean repeatSelection() {
		return chbAnotherPair.getSelection();
	}
	
	/** Returns order number of this selection page instance. */
	public int getOrderNumber() {
		return orderNumber;
	}
	
	/**
	 * Returns user filled input as {@link FolderPair} value object.
	 * 
	 * @return <tt>null</tt> if users not filled something or
	 * entry(ies) is(are) invalid. 
	 **/
	public BulkImportWizard.FolderPair getFolderPair() {
		if ((source == null) || (target == null)) {
			return null;
			
		} else {
			return new FolderPair(Paths.get(source), target);
			
		}
	}
	
	//--------------------------------------------------------------------------
	// Overrides and implementations
	//--------------------------------------------------------------------------

	/**
	 * Create contents of the wizard.
	 * @param parent
	 */
	@Override
	public void createControl(Composite parent) {
		Composite container = new Composite(parent, SWT.NONE);
		GridLayout gl_container = new GridLayout(3, false);
		container.setLayout(gl_container);
		
		Label lblSelectLegacyFilesystem = new Label(container, SWT.NONE);
		lblSelectLegacyFilesystem.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 3, 1));
		lblSelectLegacyFilesystem.setText("Select regular filesystem folder to import from:");
		
		Label lblSource = new Label(container, SWT.NONE);
		GridData gd_lblSource = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_lblSource.horizontalIndent = 20;
		lblSource.setLayoutData(gd_lblSource);
		lblSource.setText("Source:");
		
		txtSource = new Text(container, SWT.BORDER | SWT.READ_ONLY);
		GridData gd_txtSource = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_txtSource.horizontalIndent = 10;
		txtSource.setLayoutData(gd_txtSource);
		
		txtSourceDecoration = new ControlDecoration(txtSource, SWT.TOP | SWT.LEFT);	
		txtSourceDecoration.setImage(FieldDecorationRegistry.getDefault()
				.getFieldDecoration(FieldDecorationRegistry.DEC_ERROR)
				.getImage());		
		txtSourceDecoration.hide();
		
		Button btnSource = new Button(container, SWT.NONE);
		btnSource.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				DirectoryDialog diag = new DirectoryDialog(getShell());
				diag.setText("Choose a folder to import");				
				source = diag.open();
				
				if (source != null) {		// Something has been chosen
					txtSource.setText(source);
					validFields();
				}			
			}
		});
		btnSource.setText(Dialog.ELLIPSIS);		
		
		Label lblSelectPlexflowFolder = new Label(container, SWT.NONE);
		lblSelectPlexflowFolder.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 3, 1));
		lblSelectPlexflowFolder.setText("Select Plexflow folder to import to: (folder will be locked during import)");
		
		Label lblTarget = new Label(container, SWT.NONE);
		lblTarget.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblTarget.setText("Target:");
		
		txtTarget = new Text(container, SWT.BORDER | SWT.READ_ONLY);
		GridData gd_txtTarget = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_txtTarget.horizontalIndent = 10;
		txtTarget.setLayoutData(gd_txtTarget);
		
		txtTargetDecoration = new ControlDecoration(txtTarget, SWT.TOP | SWT.LEFT);	
		txtTargetDecoration.setImage(FieldDecorationRegistry.getDefault()
				.getFieldDecoration(FieldDecorationRegistry.DEC_ERROR)
				.getImage());
		txtTargetDecoration.hide();
		
		Button btnTarget = new Button(container, SWT.NONE);
		btnTarget.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				// Open folder select dialog and return selection
				target = (IFolder) StorageTreeDialog.openAndReturn(
					getShell(),
					StorageTreeDialog.STYLE_FOLDER_WITH_NEW_BUTTON);
				
				SYSLOG.debug("txtTarget == null : {}, target == null : {}.", txtTarget == null, target == null);
				txtTarget.setText(target.getPath().toString());
				
				validFields();
			}
		});
		btnTarget.setText(Dialog.ELLIPSIS);
		
		
		chbAnotherPair = new Button(container, SWT.CHECK);
		chbAnotherPair.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 3, 1));
		chbAnotherPair.setText("Continue with selecting another source and target folder pair");
		
		// Required to avoid an error in the system
		setControl(container);
		
		
		
		// DEBUG ONLY!
//		source = "/home/libor/tmp";
//		target = com.virtage.plexflow.storage.Storage.get().getHomeForCurrentUser();
//		setPageComplete(true);
	}
	
	//--------------------------------------------------------------------------
	// Consider toString(), equals(), hashCode()
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Getters/setters of properties
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Helper methods (mostly private)
	//--------------------------------------------------------------------------
	
	/**
	 * Checks validity of dialog fields, sets error messages and page
	 * completeness flag. Called from all control change events.
	 */
	private void validFields() {
		boolean sourceOk = false, targetOk = false;
		
		// Existence of regular folder
		if (source != null) {
			if (Files.notExists(Paths.get(source))) {
				String msg = "Source filesystem path is invalid.";
				setErrorMessage(msg);			
				txtSourceDecoration.setDescriptionText(msg);
				txtSourceDecoration.show();
				
			} else {
				sourceOk = true;
			}
		}
			
		// Check PX folder
		if ((target != null) && (source != null)) {
			// Check for duplicity of target with rest of pairs
			BulkImportWizard wiz = (BulkImportWizard) getWizard();
			
			if (wiz.pairs.contains(new FolderPair(Paths.get(source), target))) {
				String msg = "Target folder is already used in another source-target pair.";
				setErrorMessage(msg);
				txtTargetDecoration.setDescriptionText(msg);
				txtTargetDecoration.show();				
				
			} else {				
				// Page is really complete
				setErrorMessage(null);	// clears error message if any
				
				if (txtSourceDecoration != null) txtSourceDecoration.hide();
				if (txtTargetDecoration != null) txtTargetDecoration.hide();
				
				targetOk = true;
			}
		}
					
		// If reached this, fields are complete
		setPageComplete(sourceOk && targetOk);
	}
}
