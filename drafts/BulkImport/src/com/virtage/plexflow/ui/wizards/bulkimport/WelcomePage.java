package com.virtage.plexflow.ui.wizards.bulkimport;

import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import com.virtage.plexflow.ui.PxUiConstants;

public class WelcomePage extends WizardPage {
	
	//--------------------------------------------------------------------------
	// Class fields - public
	//--------------------------------------------------------------------------

	public static final String PAGE_NAME = "WELCOME";
	
	//--------------------------------------------------------------------------
	// Class fields - non-public
	//--------------------------------------------------------------------------
	
	//--------------------------------------------------------------------------
	// Instance fields - public
	//--------------------------------------------------------------------------
	
	//--------------------------------------------------------------------------
	// Instance fields - non-public
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Instance fields - bean properties
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Constructors, static initializers, factories
	//--------------------------------------------------------------------------

	/**
	 * Create the wizard.
	 */
	public WelcomePage() {
		super(PAGE_NAME);
		setTitle("Bulk import wizard welcomes you");
		setDescription("Import your existing files to Plexflow storage.");
	}
	
	//--------------------------------------------------------------------------
	// Public API - class (static)
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Public API - instance
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Overrides and implementations
	//--------------------------------------------------------------------------

	/**
	 * Create contents of the wizard.
	 * @param parent
	 */
	@Override
	public void createControl(Composite parent) {				
		Composite container = new Composite(parent, SWT.NULL);
		container.setLayout(new RowLayout(SWT.VERTICAL));
		
		Label lblNewLabel = new Label(container, SWT.NONE);		
		lblNewLabel.setFont(JFaceResources.getFont(PxUiConstants.FONT_DIALOG_BOLD));
		lblNewLabel.setText("What is bulk import?");
		
		Label lblWhatIsBulk = new Label(container, SWT.WRAP);
		lblWhatIsBulk.setLayoutData(PxUiConstants.ROW_DATA_MAX_WIDTH);
		lblWhatIsBulk.setText("Bulk import wizard is tool for mass import of existing files and folders stored on regular filesystems (like local disk or network shared folders). It is usually run when staring with Plexflow to easy transfer all your previous files and folders into Plexflow storage.");
		
		Label lblNewLabel_1 = new Label(container, SWT.NONE);
		lblNewLabel_1.setFont(JFaceResources.getFont(PxUiConstants.FONT_DIALOG_BOLD));
		lblNewLabel_1.setText("Caution");
		
		Label lblNewLabel_2 = new Label(container, SWT.WRAP);
		lblNewLabel_2.setLayoutData(PxUiConstants.ROW_DATA_MAX_WIDTH);
		lblNewLabel_2.setText("Initial import of large data amounts will take a significant time and make serious workload on disk drives and network. It is advisable to launch import  during night or weekend. Importing isn't atomic and this wizard must be kept running. Therefore ensure that this computer is plugged in and e.g. on UPS battery.");
			
		setPageComplete(true);
		
		// Required to avoid an error in the system
		setControl(container);
	}
	
	//--------------------------------------------------------------------------
	// Consider toString(), equals(), hashCode()
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Getters/setters of properties
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Helper methods (mostly private)
	//--------------------------------------------------------------------------
	
}
