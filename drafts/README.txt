--------------------------------------------------------------------------------
BulkImport
--------------------------------------------------------------------------------
Maintainer:		Libor
Added:			2012-08-21
State:			in progress

Draft of plug-in containing
* bulk import wizard and
* non-UI bulk import API

--------------------------------------------------------------------------------
StorageTreeDialog
--------------------------------------------------------------------------------
Maintainer:		Libor
Added:			2012-07-31
State:			merged in master

Dialog, similar to SWT [Directory|File]Dialog to browse for file/folder
respectively.


--------------------------------------------------------------------------------
ApacheHttpClient
--------------------------------------------------------------------------------
Maintainer:		Libor
Added:			2012-09-13
State:			in progress

Tests with Apache HTTP Client in
* OSGi environment and
* logging to SLF4J instead of Commons Logging
