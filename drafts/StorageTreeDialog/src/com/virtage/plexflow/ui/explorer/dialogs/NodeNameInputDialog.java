package com.virtage.plexflow.ui.explorer.dialogs;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.IInputValidator;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.swt.widgets.Shell;

import com.virtage.plexflow.storage.NodeName;
import com.virtage.plexflow.storage.exceptions.NodeNameForbiddenException;
import com.virtage.plexflow.utils.PxUtils;

/** 
 * Input dialog to enter node name validating entered string.
 * 
 * <p>Usage is as simple as 
 * <pre>
 * NodeName folderName = NodeNameInputDialog.openAndReturn(
 *     getShell(),
 *     NodeNameInputDialog.STYLE_FOLDER | NodeNameInputDialog.STYLE_NEW,
 *     null);		// no initial name
 * </pre>
 * </p>
 * 
 * <p>Supported styles:
 * <ul>
 * <li>{@link #STYLE_FILE} | {@link #STYLE_NEW} -- dialog texts mention naming new file.</li>
 * <li>{@link #STYLE_FILE} | {@link #STYLE_RENAME} -- dialog texts mention renaming file.</li>
 * <li>{@link #STYLE_FOLDER} | {@link #STYLE_NEW} -- dialog texts mention naming new folder.</li>
 * <li>{@link #STYLE_FOLDER} | {@link #STYLE_RENAME} -- dialog texts mention renaming folder.</li>
 * </ul>
 * </p>
 * 
 * @author libor
 *
 */
public class NodeNameInputDialog extends InputDialog {

	//--------------------------------------------------------------------------
	// Class fields - public
	//--------------------------------------------------------------------------

	public static final int STYLE_FOLDER = 0;
	public static final int STYLE_FILE = 2;
	public static final int STYLE_NEW = 4;
	public static final int STYLE_RENAME = 8;
	
	//--------------------------------------------------------------------------
	// Class fields - non-public
	//--------------------------------------------------------------------------
	
	private static String dialogTitle;
	private static String dialogMessage;
	
	//--------------------------------------------------------------------------
	// Instance fields - public
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Instance fields - non-public
	//--------------------------------------------------------------------------
	
	//--------------------------------------------------------------------------
	// Instance fields - bean properties
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Constructors, static initializers, factories
	//--------------------------------------------------------------------------
	
	private NodeNameInputDialog(Shell parentShell, String dialogTitle,
			String dialogMessage, String initialValue, IInputValidator validator) {
		super(parentShell, dialogTitle, dialogMessage, initialValue, validator);
	}
	
	//--------------------------------------------------------------------------
	// Public API - class (static)
	//--------------------------------------------------------------------------

	/**
	 * Opens, blocks and returns valid NodeName or null if dialog has been
	 * closed by Cancel or X. Easier to use then standard JFace <tt>open()</tt>. 
	 * 
	 * @param parentShell
	 * @param style See {@link NodeNameInputDialog} class javadoc for supported style bits.
	 * @param initialName initial node name (string must still be a valid node name) or <tt>null</tt> for no initial value. 
	 * @return NodeName or <tt>null</tt> when dialog has been closed by Cancel or X.
	 * @throws IllegalArgumentException if invalid style bit combination used
	 */
	public static NodeName openAndReturn(Shell parentShell, int style,
			String initialName) throws IllegalArgumentException {
		// throw IEA for invalid style or set texts
		checkAndSetStyle(style); 
		
		NodeNameInputDialog dg = new NodeNameInputDialog(
				parentShell,
				dialogTitle,
				dialogMessage,
				(initialName == null) ? null : initialName.toString(),
				new NodeNameInputValidator());
		
		dg.setBlockOnOpen(true);	
		
		// Cancel or X pressed
		if (dg.open() != IDialogConstants.OK_ID) {
			return null;
		}		
		
		// Defensively try to convert to NodeName
		NodeName nodeName = null;		
		try {
			nodeName = new NodeName(dg.getValue());
			
		} catch (NodeNameForbiddenException e) {
			throw new IllegalStateException("Entered string '" + dg.getValue() +
					"' is not valid NodeName but has passed through " +
					"NodeNameInputValidator!", e);
		}
		
		return nodeName;
	}	

	//--------------------------------------------------------------------------
	// Public API - instance
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Overrides and implementations
	//--------------------------------------------------------------------------
	
	/** Use {@link #openAndReturn(Shell, String)} instead. */
	@Override
	public int open() {
		return super.open();
	}
	
	//--------------------------------------------------------------------------
	// Consider toString(), equals(), hashCode()
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Getters/setters of properties
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Helper methods (mostly private)
	//--------------------------------------------------------------------------
	
	/** Throw IEA for invalid style or set texts. */
	private static void checkAndSetStyle(int style) {
		if (PxUtils.isBitPresent(STYLE_FILE, style)) {
			if (PxUtils.isBitPresent(STYLE_NEW, style)) {
				dialogTitle = "Create new file";
				dialogMessage = "Please enter new file name";
				
			} else if (PxUtils.isBitPresent(STYLE_RENAME, style)) {
				dialogTitle = "Rename file";
				dialogMessage = "Please enter new file name";
				
			} else {
				throwStyleIAE(style);
			}
			
		} else if (PxUtils.isBitPresent(STYLE_FOLDER, style)) {
			if (PxUtils.isBitPresent(STYLE_NEW, style)) {
				dialogTitle = "Create new folder";
				dialogMessage = "Please enter new folder name";
				
			} else if (PxUtils.isBitPresent(STYLE_RENAME, style)) {
				dialogTitle = "Rename folder";
				dialogMessage = "Please enter new folder name";
				
			} else {
				throwStyleIAE(style);
			}
			
		} else {
			throwStyleIAE(style);
		}		
	}
	
	private static void throwStyleIAE(int style) {
		throw new IllegalArgumentException(
				"Invalid style specified (" + style + ")!");
	}
	
}
