package com.virtage.plexflow.ui.explorer;

import java.util.Date;
import java.util.List;

import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.ui.model.IWorkbenchAdapter;

import com.virtage.plexflow.storage.IFile;
import com.virtage.plexflow.storage.IFolder;
import com.virtage.plexflow.storage.ILock;
import com.virtage.plexflow.storage.INode;
import com.virtage.plexflow.storage.ITags;
import com.virtage.plexflow.storage.NodeName;
import com.virtage.plexflow.storage.events.NodeListener;
import com.virtage.plexflow.storage.exceptions.NodeAlreadyExistsException;
import com.virtage.plexflow.storage.exceptions.NodeDeletedException;
import com.virtage.plexflow.storage.exceptions.NodeNameForbiddenException;
import com.virtage.plexflow.storage.exceptions.NodeNotFoundException;
import com.virtage.plexflow.storage.exceptions.NodeTrashedException;
import com.virtage.plexflow.system.User;

/**
 * Fake workspace folder to render workspace as a only root element in JFace
 * tree viewers.
 * 
 * <p>Workspace folder is regular folder, therefore its <tt>childNodes()</tt>
 * returns workspace subfolders as top-level tree elements:
 * <pre>
 * + home/
 *   + pxsu/
 *   + joe/
 * + public/ 
 * </pre></p>
 * 
 * <p>Use this fake workspace {@link IFolder} implementation to achieve tree
 * looking like
 * <pre>
 * + Workspace
 *   + home/
 *     + pxsu/
 *     + joe/
 *   + public/
 * </pre></p>
 * 
 * <p>Class simply delegates all its methods except {@link #isWorkspace()} to actual
 * {@link IFolder} implementation supplied to {@link #FakeWorkspaceFolder(IFolder)}
 * constructor. The {@link #isWorkspace()} always returns false to prevent
 * recursive infinite loop.</p>
 * 
 * <p>Here is excerpt from {@link IWorkbenchAdapter#getChildren(Object)} body.
 * Note that if parent element is workspace, a fake workspace is returned.
 * In next invocation of method, a fake workspace responds to
 * {@link #isWorkspace()} with <tt>false</tt> thus proceeding to fetching
 * actual children of workspace but having fake workspace folder as parent.   
 * <pre>
 * public Object[] getChildren(Object o) {
 *    IFolder parent = (IFolder) o;
 *
 *    <b>if (parent.isWorkspace()) {
 *       return new IFolder[] { new FakeWorkspaceFolder(parent) };
 *    }</b>
 *
 *    List&lt;INode&gt; childNodes = null;
 *    try {
 *       childNodes = parent.childNodes();			
 *    } catch (NodeTrashedException | NodeDeletedException e) {
 *       ...			
 * }
 * </pre></p>
 * 
 * @author libor
 * @see https://bugs.eclipse.org/bugs/show_bug.cgi?id=9262
 * @see http://manuelselva.wordpress.com/2009/01/16/bug-9262/
 */
public class FakeWorkspaceFolder implements IFolder {
	
	//--------------------------------------------------------------------------
	// Class fields - public
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Class fields - non-public
	//--------------------------------------------------------------------------

	//private static final Logger SYSLOG = LoggerFactory.getLogger(ThisClass.class);

	//--------------------------------------------------------------------------
	// Instance fields - public
	//--------------------------------------------------------------------------
	
	private IFolder workspace;
	
	//--------------------------------------------------------------------------
	// Instance fields - non-public
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Instance fields - bean properties
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Constructors, static initializers, factories
	//--------------------------------------------------------------------------

	/**
	 * Instantiate fake workspace delegating to specified actual workspace
	 * folder.
	 * 
	 * @param workspace Actual workspace folder instance.
	 * @throws IllegalArgumentException If argument is not workspace folder. 
	 */
	public FakeWorkspaceFolder(IFolder workspace) throws IllegalArgumentException {
		if (!workspace.isWorkspace())
			throw new IllegalArgumentException("Supplied folder '" +
				workspace.getPath().toString() + "' isn't a workspace!");
		
		this.workspace = workspace;
	}
	
	//--------------------------------------------------------------------------
	// Public API - class (static)
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Public API - instance
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Overrides and implementations
	//--------------------------------------------------------------------------

	// THIS IS ONLY CHANGED METHOD. ALL OTHER ARE SIMPLE DELEGATORS. 
	@Override
	public boolean isWorkspace() {
		return false;		// always false
	}
	
	@Override
	public ILock lock() {
		return workspace.lock();
	}

	@Override
	public ILock lock(long timeOutMillis) {
		return workspace.lock(timeOutMillis);
	}

	@Override
	public ILock getLock() {
		return workspace.getLock();
	}

	@Override
	public void unLock() {
		workspace.unLock();
	}

	@Override
	public boolean canUnlock() {
		return workspace.canUnlock();
	}

	@Override
	public boolean isLocked() {
		return workspace.isLocked();
	}

	@Override
	public IFolder createFolder(NodeName name) throws NodeTrashedException,
			NodeDeletedException, NodeAlreadyExistsException {
		return workspace.createFolder(name);
	}

	@Override
	public INode copy(IFolder targetFolder, IProgressMonitor monitor)
			throws NodeTrashedException, NodeDeletedException,
			NodeAlreadyExistsException {
		return workspace.copy(targetFolder, monitor);
	}

	@Override
	public IFolder createFolder(String name) throws NodeTrashedException,
			NodeDeletedException, NodeAlreadyExistsException,
			NodeNameForbiddenException {
		return workspace.createFolder(name);
	}

	@Override
	public INode copy(IFolder targetFolder) throws NodeTrashedException,
			NodeDeletedException, NodeAlreadyExistsException {
		return workspace.copy(targetFolder);
	}

	@Override
	public IFile createFile(NodeName name) throws NodeTrashedException,
			NodeDeletedException, NodeAlreadyExistsException {
		return workspace.createFile(name);
	}

	@Override
	public INode move(IFolder targetFolder, IProgressMonitor monitor)
			throws NodeTrashedException, NodeDeletedException,
			NodeAlreadyExistsException {
		return workspace.move(targetFolder, monitor);
	}

	@Override
	public Object getAdapter(@SuppressWarnings("rawtypes") Class adapter) {
		return workspace.getAdapter(adapter);
	}

	@Override
	public INode move(IFolder targetFolder) throws NodeTrashedException,
			NodeDeletedException, NodeAlreadyExistsException {
		return workspace.move(targetFolder);
	}

	@Override
	public IFile createFile(String name) throws NodeTrashedException,
			NodeDeletedException, NodeAlreadyExistsException,
			NodeNameForbiddenException {
		return workspace.createFile(name);
	}

	@Override
	public INode rename(NodeName newName) throws NodeTrashedException,
			NodeDeletedException, NodeAlreadyExistsException {
		return workspace.rename(newName);
	}

	@Override
	public List<String> childNames(IProgressMonitor monitor)
			throws NodeTrashedException, NodeDeletedException {
		return workspace.childNames(monitor);
	}

	@Override
	public INode rename(String newName) throws NodeTrashedException,
			NodeDeletedException, NodeAlreadyExistsException,
			NodeNameForbiddenException {
		return workspace.rename(newName);
	}

	@Override
	public List<String> childNames() throws NodeTrashedException,
			NodeDeletedException {
		return workspace.childNames();
	}

	@Override
	public long getId() {
		return workspace.getId();
	}

	@Override
	public NodeName getNodeName() {
		return workspace.getNodeName();
	}

	@Override
	public List<INode> childNodes(IProgressMonitor monitor)
			throws NodeTrashedException, NodeDeletedException {
		return workspace.childNodes(monitor);
	}

	@Override
	public IFolder getParent() {
		return workspace.getParent();
	}

	@Override
	public IPath getPath() {
		return workspace.getPath();
	}

	@Override
	public List<INode> childNodes() throws NodeTrashedException,
			NodeDeletedException {
		return workspace.childNodes();
	}

	@Override
	public Date getCreatedAt() {
		return workspace.getCreatedAt();
	}

	@Override
	public int compareTo(INode o) {
		return workspace.compareTo(o);
	}

	@Override
	public List<String> childNamesRecursively(IProgressMonitor monitor)
			throws NodeTrashedException, NodeDeletedException {
		return workspace.childNamesRecursively(monitor);
	}

	@Override
	public void addNodeListener(NodeListener listener)
			throws NodeTrashedException, NodeDeletedException {
		workspace.addNodeListener(listener);
	}

	@Override
	public void removeNodeListener(NodeListener listener)
			throws NodeTrashedException, NodeDeletedException {
		workspace.removeNodeListener(listener);
	}

	@Override
	public List<String> childNamesRecursively() throws NodeTrashedException,
			NodeDeletedException {
		return workspace.childNamesRecursively();
	}

	@Override
	public String getDescription() {
		return workspace.getDescription();
	}

	@Override
	public void setDescription(String desc) {
		workspace.setDescription(desc);
	}

	@Override
	public List<INode> childNodesRecursively(IProgressMonitor monitor)
			throws NodeTrashedException, NodeDeletedException {
		return workspace.childNodesRecursively(monitor);
	}

	@Override
	public List<INode> childNodesRecursively() throws NodeTrashedException,
			NodeDeletedException {
		return workspace.childNodesRecursively();
	}

	@Override
	public boolean existsFile(NodeName name) throws NodeTrashedException,
			NodeDeletedException {
		return workspace.existsFile(name);
	}

	@Override
	public boolean existsFile(String name) throws NodeNameForbiddenException,
			NodeTrashedException, NodeDeletedException {
		return workspace.existsFile(name);
	}

	@Override
	public boolean existsFolder(NodeName name) throws NodeTrashedException,
			NodeDeletedException {
		return workspace.existsFolder(name);
	}

	@Override
	public boolean existsFolder(String name) throws NodeTrashedException,
			NodeDeletedException, NodeNameForbiddenException {
		return workspace.existsFolder(name);
	}

	@Override
	public IFile getFile(NodeName name) throws NodeTrashedException,
			NodeDeletedException, NodeNotFoundException {
		return workspace.getFile(name);
	}

	@Override
	public IFile getFile(String name) throws NodeTrashedException,
			NodeDeletedException, NodeNotFoundException,
			NodeNameForbiddenException {
		return workspace.getFile(name);
	}

	@Override
	public IFolder getFolder(NodeName name) throws NodeTrashedException,
			NodeDeletedException, NodeNotFoundException {
		return workspace.getFolder(name);
	}

	@Override
	public IFolder getFolder(String name) throws NodeTrashedException,
			NodeDeletedException, NodeNotFoundException,
			NodeNameForbiddenException {
		return workspace.getFolder(name);
	}

	@Override
	public boolean isHomeForCurrentUser() {
		return workspace.isHomeForCurrentUser();
	}

	@Override
	public boolean isHomeForUser(User user) {
		return workspace.isHomeForUser(user);
	}

	@Override
	public boolean isHome() {
		return workspace.isHome();
	}

	@Override
	public boolean isHomeRoot() {
		return workspace.isHomeRoot();
	}	

	@Override
	public boolean isParentOf(INode node) throws NodeTrashedException,
			NodeDeletedException {
		return workspace.isParentOf(node);
	}

	@Override
	public boolean hasChildNodes() throws NodeTrashedException,
			NodeDeletedException {
		return workspace.hasChildNodes();
	}

	@Override
	public ITags getTags() {
		return workspace.getTags();
	}
	
	//--------------------------------------------------------------------------
	// Consider toString(), equals(), hashCode()
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Getters/setters of properties
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Helper methods (mostly private)
	//--------------------------------------------------------------------------
}
