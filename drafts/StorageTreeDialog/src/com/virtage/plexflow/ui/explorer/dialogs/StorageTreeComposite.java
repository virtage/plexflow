package com.virtage.plexflow.ui.explorer.dialogs;

import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.model.BaseWorkbenchContentProvider;
import org.eclipse.ui.model.WorkbenchLabelProvider;

import com.virtage.plexflow.storage.IFolder;
import com.virtage.plexflow.storage.Storage;
import com.virtage.plexflow.ui.explorer.filters.FolderOnlyFilter;
import com.virtage.plexflow.ui.explorer.filters.RootFolderFilter;

/**
 * <p>Composite showing storage tree viewer. Designed to be used from
 * {@link StorageTreeDialog} but others may also make use of it.</p>
 * 
 * <p>Tree viewer managed by this composite is achievable as public field
 * {@link #treeViewer}.</p>
 * 
 * @author libor
 *
 */
public class StorageTreeComposite extends Composite {
	
	//--------------------------------------------------------------------------
	// Class fields - public
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Class fields - non-public
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Instance fields - public
	//--------------------------------------------------------------------------

	/** Tree viewer managed by this composite. */ 
	public TreeViewer treeViewer;
	
	//--------------------------------------------------------------------------
	// Instance fields - non-public
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Instance fields - bean properties
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Constructors, static initializers, factories
	//--------------------------------------------------------------------------

	/**
	 * Create folder tree composite limiting folders to select from.
	 * 
	 * @param parent
	 * @param style
	 * @param filterFolder
	 *            or <tt>null</tt> to workspace as a filter (which
	 *            effectively means no filter).
	 * @param foldersOnly whether to limit content to folders only
	 */
	public StorageTreeComposite(Composite parent, int style, IFolder filterFolder, boolean foldersOnly) {
		super(parent, style);

		// Set filter folder to workspace if null
		if (filterFolder == null) {
			filterFolder = Storage.get().getWorkspace();
		} // otherwise left unchanged
		
		setLayout(new FillLayout(SWT.HORIZONTAL));

		treeViewer = new TreeViewer(this, SWT.SINGLE);

		// 2. Assign content provider
		treeViewer.setContentProvider(new BaseWorkbenchContentProvider());

		// 3. Assign label provider
		treeViewer.setLabelProvider(new WorkbenchLabelProvider());

		// 4. Assign filter(s)
		treeViewer.addFilter(new RootFolderFilter(filterFolder));		
		if (foldersOnly) {			
			treeViewer.addFilter(new FolderOnlyFilter());
		}

		// 5. Assign <strike>sorter</strike> comparator
		
		// 6. Listeners

		// 7. Set input element
		treeViewer.setInput(filterFolder);
		
		// Expands tree to children of workspace
		treeViewer.expandToLevel(2);
	}
	
	//--------------------------------------------------------------------------
	// Public API - class (static)
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Public API - instance
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Overrides and implementations
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Consider toString(), equals(), hashCode()
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Getters/setters of properties
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Helper methods (mostly private)
	//--------------------------------------------------------------------------
	
}