package foldertree;

import java.util.List;

import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.equinox.app.IApplication;
import org.eclipse.equinox.app.IApplicationContext;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.PlatformUI;

import com.virtage.plexflow.system.LoginService;
import com.virtage.plexflow.system.SystemInstance;
import com.virtage.plexflow.system.SystemInstanceFactory;
import com.virtage.plexflow.system.SystemsProperties;
import com.virtage.plexflow.system.SystemsPropertiesRecord;

/**
 * This class controls all aspects of the application's execution
 */
public class Application implements IApplication {

	/* (non-Javadoc)
	 * @see org.eclipse.equinox.app.IApplication#start(org.eclipse.equinox.app.IApplicationContext)
	 */
	@Override
	public Object start(IApplicationContext context) throws Exception {
		
		// Login as SU
		List<SystemsPropertiesRecord> systems = SystemsProperties.listAvailableSystems();
		SystemInstance systemInstance = LoginService.getInstance().loginAsSu(systems.get(0));	// should be DEV		
		SystemInstanceFactory.setInstance(systemInstance);		
				
		
		// Launch workbench
		Display display = PlatformUI.createDisplay();		
		try {
			int returnCode = PlatformUI.createAndRunWorkbench(display, new ApplicationWorkbenchAdvisor());
			if (returnCode == PlatformUI.RETURN_RESTART)
				return IApplication.EXIT_RESTART;
			else
				return IApplication.EXIT_OK;
		} finally {
			display.dispose();
		}
		
	}

	/* (non-Javadoc)
	 * @see org.eclipse.equinox.app.IApplication#stop()
	 */
	@Override
	public void stop() {
		if (!PlatformUI.isWorkbenchRunning())
			return;
		final IWorkbench workbench = PlatformUI.getWorkbench();
		final Display display = workbench.getDisplay();
		display.syncExec(new Runnable() {
			@Override
			public void run() {
				if (!display.isDisposed())
					workbench.close();
			}
		});
	}
}
