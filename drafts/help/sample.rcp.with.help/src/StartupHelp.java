import org.eclipse.core.commands.Command;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.NotEnabledException;
import org.eclipse.core.commands.NotHandledException;
import org.eclipse.core.commands.common.NotDefinedException;
import org.eclipse.ui.IStartup;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.commands.ICommandService;
import org.eclipse.ui.services.IServiceLocator;

public class StartupHelp implements IStartup {

	@Override
	public void earlyStartup() {
		final IWorkbench workbench = PlatformUI.getWorkbench();
		workbench.getDisplay().asyncExec(new Runnable() {
			@Override
			public void run() {
				IWorkbenchWindow window = workbench.getActiveWorkbenchWindow();
				if (window != null) {
					
					IServiceLocator serviceLocator = workbench;					
					ICommandService commandService = (ICommandService) serviceLocator.getService(ICommandService.class);
					try {
						Command command = commandService.getCommand("org.eclipse.ui.help.helpContents");
						command.executeWithChecks(new ExecutionEvent());
						
					} catch (ExecutionException | NotDefinedException
							| NotEnabledException | NotHandledException e) {
						e.printStackTrace();
					}
					
				}
			}
		});
	}
}
