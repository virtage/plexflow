import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;


public class FileCopySpeedNioOioCompared {

	public static void main(String[] args) throws IOException {		
		
		// NIO
		long nioStart = System.currentTimeMillis();
		long nioCopiedBytes;
		{
			InputStream is = Files.newInputStream(Paths.get("sourcefile"));
			Path target = Paths.get("outputfile_nio");
			nioCopiedBytes = Files.copy(is, target, StandardCopyOption.REPLACE_EXISTING);
		}
		long nioEnd = System.currentTimeMillis();
		
		
		// OIO
		long oioStart = System.currentTimeMillis();
		long oioCopiedBytes;
		{
			oioCopiedBytes = copyStreams(new FileInputStream("sourcefile"), new FileOutputStream("outputfile_oio"));
		}
		long oioEnd = System.currentTimeMillis();
		
		
		System.out.println("nio copied bytes = " + nioCopiedBytes + ", oio copied bytes = " + oioCopiedBytes);
		System.out.println("nio = " + (nioEnd - nioStart) + " ms. oio = " + (oioEnd - oioStart) + " ms");
	}
	
	
	
	public static long copyStreams(InputStream is, OutputStream os) throws IOException {
		final int BUFFER_SIZE = 1024 * 4; // 4 kB		
		long byteCounter = 0;
		
		byte[] buffer = new byte[BUFFER_SIZE];
        int length;
        while (-1 != (length = is.read(buffer))) {
        	os.write(buffer, 0, length);
        	byteCounter += length;
        }		
		
		return byteCounter;
	}

}
