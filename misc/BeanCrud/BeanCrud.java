package com.virtage.plexflow.db;


import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.dbutils.BasicRowProcessor;
import org.apache.commons.dbutils.BeanProcessor;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.RowProcessor;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;

import org.postgresql.ds.PGPoolingDataSource;

/**
 * 
 * 
 * Limitations:<ul>
 * <li>Doesn't support composite (multi-column) primary keys</li>
 * <li>Doesn't check if only supported property class types are used.</li>
 * </ul>
 * 
 * @author libor
 *
 * @param <T> Type of managed bean
 * @param <PK> Type of bean's primary key
 */
public class BeanCrud<T, PK> implements ICrud<T, PK> {
	
	 //--------------------------------------------------------------------------
    // Class fields
    //--------------------------------------------------------------------------
		
    //--------------------------------------------------------------------------
    // Instance fields - public
    //--------------------------------------------------------------------------
		
    //--------------------------------------------------------------------------
    // Instance fields - non-public
    //--------------------------------------------------------------------------

	private Class<T> classType;
	private String tableName;
	private String pkField;
	private PK pkValue;
	private PGPoolingDataSource dataSource;
	EnhBeanHandler<T> enhancedBeanHandler;
	EnhBeanListHandler<T> enhancedBeanListHandler;
	
		
    //--------------------------------------------------------------------------
    // Constructors or factories
    //--------------------------------------------------------------------------

	public BeanCrud(Class<T> classType, String tableName, String pkField) throws CrudException {
		this.classType = classType;
		this.tableName = tableName;
		this.pkField = pkField;
		this.enhancedBeanHandler = new EnhBeanHandler<T>(classType);
		this.enhancedBeanListHandler = new EnhBeanListHandler<T>(classType);
		
		// Set DataSource
		dataSource = new PGPoolingDataSource();
		//dataSource.setDataSourceName("Plexflow DataSource");
		dataSource.setServerName("localhost");
		dataSource.setDatabaseName("pxtrunk");
		dataSource.setUser("postgres");
		dataSource.setPassword("a");
		dataSource.setMaxConnections(10);
		
		// Check for extra props in bean and cols in DB (throwing exception in case of mismatch)
		checkMatchingPropAndColumns();			
		
		// Check for supported prop types in bean and cols in DB (throwing exception in case of mismatch)
		//TODO: checkSupportedBeanAndColumnTypes();
	}
	
    //--------------------------------------------------------------------------
    // Public API
    //--------------------------------------------------------------------------
		
	
	@Override
	public T findByPk(PK pk) throws CrudException {
		T result = null;
		
		String findSql = "select * from \"" + tableName + "\" where \"" + pkField + "\" = ?";
		System.out.println(findSql);
		
		try {
			result = new QueryRunner(dataSource).query(findSql, enhancedBeanHandler, pk);
			
		} catch (SQLException e) {
			throw new CrudException("Failed to find bean by PK due to SQLException.", e);
		}
		
		return result;
	}

	@Override
	public List<T> findAll() throws CrudException {				
		List<T> result = new ArrayList<>();		
		
		String findSql = "select * from \"" + tableName + "\"";
		System.out.println(findSql);
		
		try {
			result = new QueryRunner(dataSource).query(findSql, enhancedBeanListHandler);
			
		} catch (SQLException e) {
			throw new CrudException("Failed to find bean by PK due to SQLException.", e);
		}

		return result;
	}

	@Override
	public void create(T bean) throws CrudException {
		Map<String, Object> propToValues = null; 			
		int affected = 0;
		
		StringBuffer intoPart = new StringBuffer();
		StringBuffer valuesPart = new StringBuffer();		
		
		propToValues = beanPropAndValuesAsMap(bean);
		
		// Creates "field1, field2, field3, " as insert into part
		// Creates "?, ?, ?, " as insert values part
		for (String prop : propToValues.keySet()) {
			intoPart.append("\"");
			intoPart.append(prop);
			intoPart.append("\", ");				
			valuesPart.append("?, ");
		}
		
		// Cut off last 2 chars (", ")
		intoPart = new StringBuffer(intoPart.substring(0, intoPart.length() - 2));
		valuesPart = new StringBuffer(valuesPart.substring(0, valuesPart.length() - 2));		

		String insertSql = "insert into \""
				+ tableName + "\" (" + intoPart + ") values (" + valuesPart
				+ ")";
		System.out.println(insertSql);
		
		try {
			affected = new QueryRunner(dataSource).update(insertSql, propToValues.values().toArray());
		} catch (SQLException e) {
			throw new CrudException("Failed to insert bean due to SQLException.", e);
		}
		
		if (affected != 1)
			throw new CrudException("Failed to insert bean due to unkown error. See log.");	
		
		// Store PK value. Used for SQL UPDATEs changing PK.
		refreshPkValue(bean);		
	}

	/**
	 * 
	 * If you will modify primary key object will not be saved (not mathing where PK = newPKvalue).
	 */
	@Override
	public void update(T bean) throws CrudException {
		Map<String, Object> propToValues = null; 			
		int affected = 0;
		
		StringBuffer setPart = new StringBuffer();

		propToValues = beanPropAndValuesAsMap(bean);

		
		// Creates "field1 = ?, field2 = ?, field3 = ?"
		for (String prop : propToValues.keySet()) {
			setPart.append("\"");
			setPart.append(prop);
			setPart.append("\" = ?, ");
		}
		
		// Cut off last 2 chars (", ")
		setPart = new StringBuffer(setPart.substring(0, setPart.length() - 2));
		
		String updateSql = "update \"" + tableName + "\" set " + setPart
				+ " where \"" + pkField + "\" = ?";
		System.out.println(updateSql);
		
		// Varargs to be used in QueryRunner.update(). Contains values of bean properties.
		// We need to extend array with PK as very last element (size + 1) 
		Object[] varargs = new Object[propToValues.size() + 1];
		Object[] beanProps = propToValues.values().toArray();		
		// Copy original array to new one  
		for (int i = 0; i < beanProps.length; i++) {
			varargs[i] = beanProps[i];
		}
		
		// Set last element as value of PK
		varargs[varargs.length - 1] = pkValue;
		

		try {
			affected = new QueryRunner(dataSource).update(updateSql, varargs);
		} catch (SQLException e) {
			throw new CrudException("Failed to update bean due to SQLException", e);
		}
		
		if (affected != 1)
			throw new CrudException("Failed to update bean due to unknown error.");
		
		// Update PK value.
		refreshPkValue(bean);
	}


	@Override
	public void deleteByPk(PK pk) throws CrudException {
		int affected = 0;
		
		String deleteSql = "delete from \"" + tableName + "\" where \"" + pkField + "\" = ?";
		System.out.println(deleteSql);
		
		try {
			affected = new QueryRunner(dataSource).update(deleteSql, pk);				
		} catch (SQLException e) {
			throw new CrudException("Failed to delete bean due to SQLException.", e);
		} finally {
			//DbUtils.closeQuietly(conn);
		}
		
		if (affected != 1)
			throw new CrudException("Failed to delete bean due to unknown error.");
	}
	
	@Override
	public void delete(T bean) throws CrudException {
		deleteByPk(getPkValue(bean));
	}
	
	@Override
	public void deleteAll() throws CrudException {
		String sql = "truncate table \"" + tableName + "\"";
		System.out.println(sql);
		
		try {
			new QueryRunner(dataSource).update(sql);				
		} catch (SQLException e) {
			throw new CrudException("Failed to delete all due to SQLException", e);
		}
	}

	@Override
	public boolean isSavedByPk(PK pk) {
		boolean saved = true;
		
		try {
			saved = (null != findByPk(pk));
		} catch (Exception e) {
			saved = false;
		}
		
		return saved;
	}
	
	@Override
	public boolean isSaved(T bean) {	
		boolean saved = true;
		
		try {
			saved = (null != findByPk(getPkValue(bean)));
		} catch (Exception e) {
			saved = false;
		}
		
		return saved;
	}
	
	@Override
	public PK getPkValue(T bean) throws CrudException {
		Map<String, Object> props = beanPropAndValuesAsMap(bean);
		
		@SuppressWarnings("unchecked")
		PK pk = (PK) props.get(pkField);
		
		return pk;
	}	
	
	
	//--------------------------------------------------------------------------
    // Private helper methods
    //--------------------------------------------------------------------------	

	/** Returns map of bean properties and their values */ 
	private Map<String, Object> beanPropAndValuesAsMap(final Object bean) {
        Map<String, Object> result = new HashMap<>();

        BeanInfo beanInfo;
		try {
			beanInfo = Introspector.getBeanInfo(bean.getClass());
			PropertyDescriptor[] descriptors = beanInfo.getPropertyDescriptors();
			
			for (PropertyDescriptor desc : descriptors) {
	            // Skip "class" property of every JavaBean
	            if (!"class".equals(desc.getName())) {
	                try {
						result.put(desc.getName(), desc.getReadMethod().invoke(bean));
					} catch (IllegalAccessException | IllegalArgumentException
							| InvocationTargetException e) {
						e.printStackTrace();
					}
	            }
	        }
			
			return result;
			
		} catch (IntrospectionException e1) {
			e1.printStackTrace();
			return null;			
		}        
    }
		
	
	private void refreshPkValue(T bean) {
		String sqlGetPkValue = "select " + pkField + " from \"" + tableName +
				"\" where " + pkField + " = ?";
		
		//@SuppressWarnings("unchecked")
		ResultSetHandler<PK> rsh = (ResultSetHandler<PK>) new ScalarHandler();
				
		try {
			this.pkValue = new QueryRunner(dataSource).query(sqlGetPkValue, rsh, getPkValue(bean));
			//System.out.println("pkValue = " + pkValue);
		} catch (SQLException e) {
			throw new CrudException("Unknown error.", e);
		}
	}
	
	/** Instantiates new bean from its class. */
	private T instantiateBean(Class<T> clazz) {		
		try {
			return (T) clazz.newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			throw new CrudException("Cannot instantiate new instance of JavaBean. Did you provide public no-args constructor?", e);
		}
	}
	
	
	/** Returns set of table column names */
	public Set<String> tableColumnsAsSet() {
		Set<String> cols = new HashSet<>();
		
		try {		
			DatabaseMetaData metaData = dataSource.getConnection().getMetaData();
			ResultSet columns = metaData.getColumns(dataSource.getDatabaseName(), null, "_Persons", null);
			
			while (columns.next()) {
				cols.add(columns.getString("COLUMN_NAME"));
			}
			
		} catch (SQLException e) {
			throw new CrudException("Cannot detect column names from DB table due to SQLException", e);
		}
		
		return cols;
	}
	
	
	/** Check for some extra properties in DB table exist but missing in bean. */
	private void checkMatchingPropAndColumns() {
		Set<String> beanProps = beanPropAndValuesAsMap(instantiateBean(classType)).keySet();
		Set<String> tableCols = tableColumnsAsSet();		
		
		if (beanProps.size() > tableCols.size()) {			
			if (!beanProps.equals(tableCols)) {				
				beanProps.removeAll(tableCols);		// Will contain only props not present in JavaBean
				
				throw new CrudException("Cannot instantiate because of illegal extra " + 
						"JavaBean property(ies) not corresponding to DB table: " +
						beanProps);
			}
		}
		
		if (tableCols.size() > beanProps.size()) {			
			if (!beanProps.equals(tableCols)) {				
				tableCols.removeAll(beanProps);		// Will contain only props not present in JavaBean
				
				throw new CrudException("Cannot instantiate because of illegal extra " + 
						"column(s) in DB table not corresponding to JavaBean property(ies): " +
						tableCols);
			}
		}
	}
	
	
	/** Check wheter only supported bean prop types are used. */
	//TODO: private void checkSupportedBeanAndColumnTypes() {}
    

	//--------------------------------------------------------------------------
    // Temp main() for testing
    //--------------------------------------------------------------------------
    
    public static void main(String[] args) throws Exception {;
//    	BeanDao<Person, String> personDao = new BeanDao<Person, String>(Person.class, "_Persons", "lastname");
//    	
//    	personDao.tableColumnsAsSet();
    
    System.out.println("CREATE TABLE \"_Persons\"" +
	"(" +
	"firstname character varying NOT NULL, " +
	"lastname character varying NOT NULL, " +
	"boss boolean NOT NULL DEFAULT false, " +
	"\"enteredSqlDate\" date, " +
	"\"enteredJavaDate\" date, " +
	"CONSTRAINT \"Osoby_pkey\" PRIMARY KEY (lastname) " +
	")");
    	
	}
    	
    	
//    	
//    	Person os1 = Person.DAO.findByPk("Formanova");
//    	os1.setJmeno("Květa");
//    	Person.DAO.update(os1);
//    	
    	
//    	Osoba os1 = new Osoba();
//		Osoba os2 = new Osoba();
//		
//		os1.setJmeno("Tereza");
//		os1.setPrijmeni("Formanova");
//		os1.setSef(false);
//		
//		System.out.println("PK for os1 je " + Osoba.DAO.getPk(os1));
//		
//		System.out.println("isSavedByPK = " + Osoba.DAO.isSavedByPk(Osoba.DAO.getPk(os1)));
//		System.out.println("isSaved = " + Osoba.DAO.isSaved(os1));
//		
//		os2.setJmeno("Martina");
//		os2.setPrijmeni("Procházková");
//		os2.setSef(false);
//		
//		Osoba.DAO.create(os1);
//		os1.setJmeno("Květa");
//		//Osoba.DAO.update(os1);
//		
//		System.out.println("os2.isSaved = " + Osoba.DAO.isSaved(os2));
//		
//		Osoba.DAO.create(os2);
//	}

    
    
    //--------------------------------------------------------------------------
    // Private inner classes
    //--------------------------------------------------------------------------
    
    /**
     * Enhanced bean list handler with improved type handling of column and bean property types.
     */
    private static class EnhBeanListHandler<T> extends BeanListHandler<T> {    	
		public EnhBeanListHandler(Class<T> type) {			
			super(type, new BasicRowProcessor(new EnhBeanProcessor()));
		}
    }
    
    /**
     * Enhanced bean handler with improved type handling of column and bean property types.
     */
    private static class EnhBeanHandler<T> extends BeanHandler<T> {    	
		public EnhBeanHandler(Class<T> type) {			
			super(type, new BasicRowProcessor(new EnhBeanProcessor()));
		}
    }
    
    
    /** Enhanced bean processor with improved type handling of column and bean property types.*/
	private static class EnhBeanProcessor extends BeanProcessor {
		@Override
		protected Object processColumn(ResultSet rs, int index, Class<?> propType)
				throws SQLException {
			
			if ( !propType.isPrimitive() && rs.getObject(index) == null ) {
	            return null;
	        }

	        if (propType.equals(String.class)) {
	            return rs.getString(index);

	        } else if (
	            propType.equals(Integer.TYPE) || propType.equals(Integer.class)) {
	            return Integer.valueOf(rs.getInt(index));

	        } else if (
	            propType.equals(Boolean.TYPE) || propType.equals(Boolean.class)) {
	            return Boolean.valueOf(rs.getBoolean(index));

	        } else if (propType.equals(Long.TYPE) || propType.equals(Long.class)) {
	            return Long.valueOf(rs.getLong(index));

	        } else if (
	            propType.equals(Double.TYPE) || propType.equals(Double.class)) {
	            return Double.valueOf(rs.getDouble(index));

	        } else if (
	            propType.equals(Float.TYPE) || propType.equals(Float.class)) {
	            return Float.valueOf(rs.getFloat(index));

	        } else if (
	            propType.equals(Short.TYPE) || propType.equals(Short.class)) {
	            return Short.valueOf(rs.getShort(index));

	        } else if (propType.equals(Byte.TYPE) || propType.equals(Byte.class)) {
	            return Byte.valueOf(rs.getByte(index));

	        } else if (propType.equals(java.sql.Time.class)) {
	            return rs.getTime(index);
	            
	        } else if (propType.equals(java.sql.Date.class)) {
	            return (java.sql.Date) rs.getDate(index);
	            
	        } else if (propType.equals(Timestamp.class)) {
	            return rs.getTimestamp(index);

	        } else {		        	
	        	throw new CrudException("Failed to map type of JavaBean property to ResultSet.getXY(): unsupported type " + propType.getName());
	            //return rs.getObject(index);
	        }
		}
	}
}
