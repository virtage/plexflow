package com.virtage.plexflow.db;

import java.util.List;

public interface ICrud<T, PK> {
	
	public T findByPk(PK pk) throws CrudException;

	/**
	 * 
	 * @return Never return <tt>null</tt>, thus can be safely tested as '<tt>findAll.isEmpty()</tt>'
	 * @throws CrudException
	 */
	public List<T> findAll() throws CrudException;

	public void create(T bean) throws CrudException;

	public void update(T bean) throws CrudException;
	
	public void deleteByPk(PK pk) throws CrudException;
	
	public void delete(T bean) throws CrudException;
	
	public void deleteAll() throws CrudException;
	
	public boolean isSavedByPk(PK pk);
	
	public boolean isSaved(T bean);
	
	public PK getPkValue(T bean) throws CrudException;
}

