package com.virtage.plexflow.db;

public class CrudException extends RuntimeException {

	public CrudException(String msg) {
		super(msg);
	}

	public CrudException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public CrudException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}
	
	

}
