#!/bin/bash -eu
#################################################
# Protect Eclipse run configuration (.launch)   #
#################################################
# Copyright (C) 2012 Virtage Software, Libor Jelinek

# Script finds .launch files and set them immutable using chattr command.
# Works only on (at least, I believe) ext2 filesystems!

# Other OSes equivalents:
# * Mac OS X -- similar can be done chflag command.
# * Windows -- huh, are you kidding?!

# See more on immutable flag:
# * http://www.cyberciti.biz/tips/linux-password-trick.html
# * http://en.wikipedia.org/wiki/Chattr

echo '###########################################'
echo 'Protect Eclipse run configuration (.launch)'
echo '###########################################'

# Am I root?
if [[ $UID -ne 0 ]] ; then
    read -p "You must run this script as root! Press [Enter] to exit."
    exit 1
fi

# Where to start search
find_root=../../

# To set or unset immutable flag?
# readlink -f ../../ will produce absolute path
echo "Do you want to set or unset immutable flag on all .launch files in `readlink -f $find_root` folder?"
select mode in "set" "unset"; do
	case $mode in
		set)
			command='sudo chattr +i';
			break;;

		unset)
			command='sudo chattr -i';
			break;;
	esac
done

# Find all .launch files and (un)mark them immutable
# **************************************************
# find with -print0 to separate filenames with a null character rather than
# new lines. Therefore also xargs needs to use -0.
find $find_root -type f -name *.launch -print0 | xargs --verbose -0 -I {} $command {}
