--// files, folders, files tag tables
-- Migration SQL that makes the change goes here.

-- /////////////////////////////////////////////////
-- /// A word of trashing
-- /////////////////////////////////////////////////

-- Trashed folders and files are stored in the same tables as regular ones.
-- This makes easy to view and recover them, and to use same code for manipulation
-- on both states of nodes.

-- See http://plexflow.virtage.com/wiki/User_Guide/Trash#Trashed_nodes_collision.

-- Unique must be (ensured by unique constraint) combination of PARENT, NAME, TRASHED_AT:
-- * PARENT and NAME -- can't create same node names under a folder
-- * TRASHED_AT -- can't be null because with nullable columns unique constraint doesn't work
-- (will allow duplicate values), so "Unix epoch" special value has been choosen.

-- Summary: TRASHED_AT of value 1st of Jan 1970 have meaning of "not trashed". Any other
-- value is to be considered as trashed.


--- /////////////////////////////////////////////////
--  /// Table FOLDER
--- /////////////////////////////////////////////////
CREATE TABLE FOLDER (
	ID				BIGINT NOT NULL GENERATED ALWAYS AS IDENTITY,
	NAME			VARCHAR(256) NOT NULL,			-- empty string ("") for workspace, username for home folders
	PARENT_ID		BIGINT NOT NULL,				-- itself for workspace (root)
	DESCRIPTION		VARCHAR(1024),					-- arbitrary desc
	
	CREATED_AT		TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	
	TRASHED_AT		TIMESTAMP NOT NULL DEFAULT '1970-01-01 00:00:00',	-- means "not trashed"

	CONSTRAINT 		PK_FOLDER_ID PRIMARY KEY (ID),
	CONSTRAINT 		FK_FOLDER_PARENT_ID FOREIGN KEY (PARENT_ID) REFERENCES FOLDER(ID),
	CONSTRAINT 		U_FOLDER_NOT_COLLIDE UNIQUE (NAME, PARENT_ID, TRASHED_AT)
);


-- 1) Create workspace folder (will get id 1)
INSERT INTO FOLDER (NAME, PARENT_ID) VALUES ('', 1);

-- 2) Create home folder under workspace (will get id 2)
INSERT INTO FOLDER (NAME, PARENT_ID) VALUES ('home', 1);

-- 3) Create home folder for pxsa under /home dir
INSERT INTO FOLDER (NAME, PARENT_ID) VALUES ('pxsu', 2);



--- /////////////////////////////////////////////////
--- /// Table FILE
--- /////////////////////////////////////////////////
CREATE TABLE FILE (
	ID				BIGINT NOT NULL GENERATED ALWAYS AS IDENTITY,	-- Node ID
	NAME			VARCHAR(256) NOT NULL,
	FOLDER_ID		BIGINT NOT NULL,
	DESCRIPTION		VARCHAR(1024),		-- arbitrary desc
		
	CREATED_AT		TIMESTAMP NOT NULL  DEFAULT CURRENT_TIMESTAMP,
	MODIFIED_AT		TIMESTAMP NOT NULL DEFAULT '1970-01-01 00:00:00',			-- Unix epoch when not modified yet
	ACCESSED_AT		TIMESTAMP NOT NULL DEFAULT '1970-01-01 00:00:00',			-- Unix epoch when not accessed yet	
	TRASHED_AT		TIMESTAMP NOT NULL DEFAULT '1970-01-01 00:00:00',	-- means "not trashed"
	
	CRC32			BIGINT NOT NULL DEFAULT -1,				-- 32 bit CRC
	LENGTH			BIGINT NOT NULL DEFAULT -1,				-- file length in bytes
	PUSH_IN_PROGRESS	BOOLEAN NOT NULL DEFAULT FALSE,		-- is file just being pushed?
	
	CONSTRAINT		PK_FILE_ID PRIMARY KEY (ID),
	CONSTRAINT 		FK_FILE_FOLDER_ID FOREIGN KEY (FOLDER_ID) REFERENCES FOLDER(ID),	
	CONSTRAINT 		U_FILE_NOT_COLLIDE UNIQUE (NAME, FOLDER_ID, TRASHED_AT)
);

--//@UNDO
-- SQL to undo the change goes here.


DROP TABLE FILE;
DROP TABLE FOLDER;
