--// disable null in BO created_by
-- Migration SQL that makes the change goes here.

ALTER TABLE BUSINESS_OBJECT ALTER COLUMN CREATED_BY NOT NULL;

--//@UNDO
-- SQL to undo the change goes here.

ALTER TABLE BUSINESS_OBJECT ALTER COLUMN CREATED_BY NULL;
