--// factory config
-- Migration SQL that makes the change goes here.

-- Create factory default settings in CONFIG table

INSERT INTO CONFIG (SETNAME, KEYNAME, KEYVALUE, REQUIRED, DATA_TYPE, DESCRIPTION) VALUES (
	'GLOBAL',
	'storage.datafolder', '/var/plexflow',
	TRUE,
	'path',
	'Valid absolute path to root storage datafolder where Plexflow operates to store files, versions, thumbnails etc.\n\n
	Path must be local path for/accessible (e.g. over network) to HTTP API components.
	Must not be ended with trainling ''/'' or ''/''!\n\n
	Usual location is /var/plexflow on Linux, or C:\Program Files\plexflow\data on Windows.
	'
);
INSERT INTO CONFIG (SETNAME, KEYNAME, KEYVALUE, REQUIRED, DATA_TYPE, DESCRIPTION) VALUES (
	'GLOBAL',
	'server.url', 'http://localhost:8080/plexflow-api',
	TRUE,
	'URL',
	'URL address where HTTP components resides.\n\n
	Must not be ended with trailing ''/''!
	Usual address is https://someServer:8080/plexflow-api'
);
INSERT INTO CONFIG (SETNAME, KEYNAME, KEYVALUE, REQUIRED, DATA_TYPE, DESCRIPTION) VALUES (
	'GLOBAL',
	'server.authMethod', 'BASIC',
	TRUE,
	'string',
	'HTTP authentication required for ${httpApi.url} URLs.\n\n
	Possible values: ''BASIC'' for HTTP basic authentication, ''NONE'' for no authentication.'
);


--//@UNDO
-- SQL to undo the change goes here.

TRUNCATE TABLE CONFIG;
