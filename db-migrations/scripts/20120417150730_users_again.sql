--// users again
-- Migration SQL that makes the change goes here.

-- Be aware "user" is SQL-92 reserver keyword and must be in double quotes if would used.

CREATE TABLE "USER" (
	USERNAME 		VARCHAR(60) NOT NULL,
	PASSWORD_HASH	VARCHAR(32) NOT NULL,				-- 32 chars MD5 hex hash
	SU 				BOOLEAN NOT NULL DEFAULT FALSE,		-- superuser?	
	FIRSTNAME 		VARCHAR(100),
	LASTNAME 		VARCHAR(100),
	DISPLAY_NAME 	VARCHAR(100),
		
	CONSTRAINT 		PK_USER_USERNAME PRIMARY KEY (USERNAME)
);

-- Hash of "changeme"
INSERT INTO "USER" (USERNAME, PASSWORD_HASH, SU) VALUES ('pxsu', '4cb9c8a8048fd02294477fcb1a41191a', TRUE);

--//@UNDO
-- SQL to undo the change goes here.

DROP TABLE "USER";
