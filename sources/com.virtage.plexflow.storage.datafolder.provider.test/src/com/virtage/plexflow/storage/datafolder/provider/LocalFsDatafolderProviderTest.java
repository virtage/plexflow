package com.virtage.plexflow.storage.datafolder.provider;

import static com.virtage.plexflow.testsupport.TestSupportConsts.*;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.virtage.plexflow.storage.datafolder.provider.DatafolderProviderException;
import com.virtage.plexflow.storage.datafolder.provider.impl.LocalDatafolderProvider;
import com.virtage.plexflow.utils.IoUtils;
import com.virtage.plexflow.utils.PxUtils;

public class LocalFsDatafolderProviderTest {
	
	private static LocalDatafolderProvider datafolder;
	//private Path datafolderPath;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		datafolder = new LocalDatafolderProvider();
		assertNotNull(datafolder);
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		
	}

	@After
	public void tearDown() throws Exception {
		IoUtils.deleteRecursively(datafolder.PATH_FILES_SUBFOLDER);
		IoUtils.deleteRecursively(datafolder.PATH_TMP_SUBFOLDER);
		//IoUtils.deleteRecursively(datafolder.PATH_VERS_SUBFOLDER);
		//IoUtils.deleteRecursively(datafolder.PATH_THUMBS_SUBFOLDER);
	}
	
	@Test
	public void testDelete() throws Exception {			
		long   fakeFileId = PxUtils.randomPositiveLong();
		String shardedPath = datafolder.shardPathForId(fakeFileId);
		Path target = null;
		
		try {
			// Create fake file for fake ID from test files
			Path source = Paths.get(PATH_TEST_FILES, FILENAME_TEST_10_MiB);
			target = Paths.get(datafolder.PATH_FILES_SUBFOLDER.toString(), shardedPath);		
			Files.createDirectories(target.getParent());
			Files.copy(source, target, StandardCopyOption.REPLACE_EXISTING);
			
			datafolder.delete(fakeFileId);
			
			// Cannot exists anymore
			assertTrue(Files.notExists(target));
			
		} finally {
			
		}
	}

	@Test
	public void testPull() throws IOException, DatafolderProviderException {
		InputStream isFromPull = null;
		OutputStream osForTempFile = null;
		Path fakeFile = null;
		
		try {
			long   fakeFileId = PxUtils.randomPositiveLong();
			String shardedPath = datafolder.shardPathForId(fakeFileId);
			
			// Create fake file for fake ID
			Path source = Paths.get(PATH_TEST_FILES, FILENAME_TEST_10_MiB);
			fakeFile = Paths.get(datafolder.PATH_FILES_SUBFOLDER.toString(), shardedPath);		
			Files.createDirectories(fakeFile.getParent());
			Files.copy(source, fakeFile, StandardCopyOption.REPLACE_EXISTING);
			
			// Pull it with input stream
			isFromPull = datafolder.pull(fakeFileId);
			
			// and write as temp file
			Path pulledFile = Files.createTempFile(null, null);
			osForTempFile = Files.newOutputStream(pulledFile);
			IoUtils.copyStream(isFromPull, osForTempFile, false);			
			
			assertTrue(IoUtils.compareCrc32(pulledFile, source));
			
		} finally {
			if (isFromPull != null) isFromPull.close();
			if (osForTempFile != null) osForTempFile.close();		// also deletes temp file
		}
		
	}

	@Test
	public void testPushBegin() throws Exception {
		long   fakeFileId = PxUtils.randomPositiveLong();				
		Path source = Paths.get(PATH_TEST_FILES, FILENAME_TEST_10_MiB);
		
		// Push
		datafolder.pushBegin(fakeFileId, Files.newInputStream(source), null, -1);
				
		// Compare of target with source
		assertTrue("Target corrupted", IoUtils.compareCrc32(source, Paths.get(PATH_TEST_FILES, FILENAME_TEST_10_MiB)));		
	}

	@Test
	public void testPushCommit() throws Exception {
		// Generate fake ID
		long   fakeFileId = PxUtils.randomPositiveLong();
		String shardedPath = datafolder.shardPathForId(fakeFileId);		
		
		// Create file in tmp/
		Path source = Paths.get(PATH_TEST_FILES, FILENAME_TEST_10_MiB);
		Path target = Paths.get(datafolder.PATH_TMP_SUBFOLDER.toString(), shardedPath);
		Files.createDirectories(target.getParent());
		Files.copy(source, target, StandardCopyOption.REPLACE_EXISTING);
				
		// Push commit
		datafolder.pushCommit(fakeFileId);
		
		// Verify that file is moved to files/
		assertTrue(Files.exists(Paths.get(datafolder.PATH_FILES_SUBFOLDER.toString(), shardedPath)));
	}

	@Test
	public void testPushRollback() throws Exception {
		// Generate fake ID
		long   fakeFileId = PxUtils.randomPositiveLong();
		String shardedPath = datafolder.shardPathForId(fakeFileId);		
		
		// Create file in tmp/
		Path source = Paths.get(PATH_TEST_FILES, FILENAME_TEST_10_MiB);
		Path target = Paths.get(datafolder.PATH_TMP_SUBFOLDER.toString(), shardedPath);
		Files.createDirectories(target.getParent());
		Files.copy(source, target, StandardCopyOption.REPLACE_EXISTING);
				
		// Push commit
		datafolder.pushRollback(fakeFileId);
		
		// Verify that file is deleted in tmp/
		assertTrue(Files.notExists(Paths.get(datafolder.PATH_TMP_SUBFOLDER.toString(), shardedPath)));		
	}

}
