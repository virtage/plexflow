package com.virtage.plexflow.storage.datafolder.provider;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;


@RunWith(Suite.class)
@SuiteClasses({ LocalFsDatafolderProviderTest.class })
public class AllTests {

}
