package com.virtage.plexflow.crypto;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import com.virtage.plexflow.crypto.Encrypter;

public class EncrypterTest {

	private Encrypter e = Encrypter.getInstance();
	private final String TEXT = "ultra secret text";
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGetInstance() {
		assertNotNull(Encrypter.getInstance());
	}

	@Test @Ignore
	public void testEncrypt() {
		fail("Not yet implemented");
	}

	@Test
	public void testDecrypt() {
		String encrypted = e.encrypt(TEXT);
		String decrypted = e.decrypt(encrypted);
		
		assertEquals(decrypted, TEXT);
	}

	@Test @Ignore
	public void testSerKey() {
		fail("Not yet implemented");
	}

	@Test @Ignore
	public void testDeserKey() {
		fail("Not yet implemented");
	}

	@Test @Ignore
	public void testGenKey() {
		fail("Not yet implemented");
	}
	
	public static void main(String[] args) {
		String encrypted = Encrypter.getInstance().encrypt("changeme");
		String decrypted = Encrypter.getInstance().decrypt(encrypted);
		
		System.out.println(encrypted);
		System.out.println(decrypted);
	}

}
