package com.virtage.plexflow.config;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class ConfigTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
		ConfigFactory.getGlobal().deleteAllKeys();
	}

	@Test
	public void factoriesDontReturnNull() {
		assertNotNull(ConfigFactory.getGlobal());
		assertNotNull(ConfigFactory.getDefault());
	}
	
	@Test
	public void booleanTest() throws Exception {
		ConfigFactory.getGlobal().putKey("somekey", true);
		assertTrue(ConfigFactory.getGlobal().getBoolean("somekey"));
	}
	
	@Test
	public void intTest() throws Exception {
		ConfigFactory.getGlobal().putKey("somekey", 15);
		assertEquals((Integer) 15, ConfigFactory.getGlobal().getInteger("somekey"));
	}
	
	@Test
	public void stringTest() throws Exception {
		ConfigFactory.getGlobal().putKey("somekey", "foovalue");
		assertEquals("foovalue", ConfigFactory.getGlobal().getString("somekey"));
	}

}
