package com.virtage.plexflow.system;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;


public class SystemsPropertiesRecordTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}
	
	
	@Test @Ignore
	public void testListAvailableSystemsOkay() {
		List<SystemsPropertiesRecord> actual = SystemsProperties.listAvailableSystems(
				new File("tests/com/virtage/plexflow/system/systemsOkay.properties"));
		
		List<SystemsPropertiesRecord> expected = new ArrayList<>();
		
//		SystemsPropertiesRecord s1 = new SystemsPropertiesRecord("dev", "Plexflow DEV", "jdbc:mysql://localhost:3306/pxdev", "plexflow", "4cb9c8a8048fd02294477fcb1a41191a");
//		
//		Collections.addAll(expected, s1);
//		
//		assertEquals(expected, actual);
	}

	@Test(expected=IllegalArgumentException.class)
	public void testListAvailableSystemsBad() {
		SystemsProperties.listAvailableSystems(
				new File("tests/com/virtage/plexflow/system/systemsBad.properties"));
	}
	
	public static void main(String[] args) {
		
	}
}