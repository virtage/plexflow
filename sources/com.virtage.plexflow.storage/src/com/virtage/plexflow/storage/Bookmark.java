package com.virtage.plexflow.storage;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.virtage.plexflow.storage.events.NodeEvent;
import com.virtage.plexflow.storage.events.NodeListenerAdapter;
import com.virtage.plexflow.storage.exceptions.BookmarkForbiddenException;
import com.virtage.plexflow.storage.exceptions.NodeDeletedException;
import com.virtage.plexflow.storage.exceptions.NodeTrashedException;
import com.virtage.plexflow.storage.exceptions.StorageException;
import com.virtage.plexflow.system.SystemInstanceFactory;



/**
 * Abstracts the differences between different types of bookmarks. 
 * 
 * @author libor
 *
 * BookmarkManager.createFor(IFolder) : Bookmark
 *
 */
public final class Bookmark {
	//--------------------------------------------------------------------------
    // Class fields - public
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Class fields - non-public
    //--------------------------------------------------------------------------
 
	// Holded bookmarks and their associated folders 
	private static Map<Bookmark, IFolder> toFolderMap = new HashMap<>();

	private static List<Bookmark> bookmarks = new ArrayList<>();
	
		
    //--------------------------------------------------------------------------
    // Instance fields - public
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Instance fields - non-public
    //--------------------------------------------------------------------------
		
    //--------------------------------------------------------------------------
    // Instance fields - bean properties
    //--------------------------------------------------------------------------
	
	private String displayName;
	
    //--------------------------------------------------------------------------
    // Constructors, static initializers, factories
    //--------------------------------------------------------------------------
 	
	private Bookmark(IFolder folder) throws NodeTrashedException, NodeDeletedException {		
		
		// Remove bookmark on trash
    	folder.addNodeListener(new NodeListenerAdapter() {
			@Override
			public void trashed(NodeEvent e) {
				remove();
			}
		});		
	}
	
	/**
	 * Creates bookmark for folder. Do nothing if folder is already bookmarked.
	 * 
	 * @return New bookmark for folder or existing bookmark if already bookmarked
	 * before. Never return null.
	 * @throws BookmarkForbiddenException if bookmarking home or workspace.
	 **/ 
	public static Bookmark createFrom(IFolder folder) throws BookmarkForbiddenException {
		
		// Prohibit bookmarking home and workspace
		if (folder.isHomeForCurrentUser())
			throw new BookmarkForbiddenException(NodeName.HOME_ROOT.toString());
		if (folder.isWorkspace())
			throw new BookmarkForbiddenException(NodeName.WORKSPACE.toString());
		    	
    	try {
			return new Bookmark(folder);
		} catch (NodeTrashedException e) {
			throw new BookmarkForbiddenException("trashed " + folder.getNodeName().toString());
		} catch (NodeDeletedException e) {
			throw new BookmarkForbiddenException("deleted " + folder.getNodeName().toString());
		}		
	}
	
    //--------------------------------------------------------------------------
    // Public API - class (static)
    //--------------------------------------------------------------------------
	
	public static List<Bookmark> listBookmarks() {
		return bookmarks;
	}
	
	public static boolean isBookmarked(IFolder folder) {
		return bookmarks.contains(folder);
	}
	
    //--------------------------------------------------------------------------
    // Public API - instance
    //--------------------------------------------------------------------------
 
	public void remove() {
		
	}
	
    //--------------------------------------------------------------------------
    // Overrides and implementations
    //--------------------------------------------------------------------------
 
	@Override
	public String toString() {
		return "Bookmark [displayName=" + displayName + "]";
	}
	
	
    //--------------------------------------------------------------------------
    // Getters/setters of properties
    //--------------------------------------------------------------------------
 
	public String getDisplayName() {
		return displayName;
	}	
	
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	
    //--------------------------------------------------------------------------
    // Helper methods (mostly private)
    //--------------------------------------------------------------------------
    
}
