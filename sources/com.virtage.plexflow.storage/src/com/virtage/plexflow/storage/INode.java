package com.virtage.plexflow.storage;

import java.util.Date;
import java.util.Set;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;

import com.virtage.plexflow.storage.events.NodeListener;
import com.virtage.plexflow.storage.exceptions.NodeAlreadyExistsException;
import com.virtage.plexflow.storage.exceptions.NodeDeletedException;
import com.virtage.plexflow.storage.exceptions.NodeNameForbiddenException;
import com.virtage.plexflow.storage.exceptions.NodeTrashedException;
import com.virtage.plexflow.storage.exceptions.StorageException;

/**
 * 
 * 
 * 
 */
public interface INode extends Comparable<INode>, ILockable, IAdaptable {
	
	/**
	 * 
	 * @param targetFolder
	 * @param monitor
	 * @return
	 * @throws NodeTrashedException
	 * @throws NodeDeletedException
	 * @throws NodeAlreadyExistsException
	 */
	public INode copy(IFolder targetFolder, IProgressMonitor monitor) throws NodeTrashedException, NodeDeletedException, NodeAlreadyExistsException;

	/**
	 * Identical to call {@link #copy}(targetFolder, null) (no IProgressMonitor).
	 */
	public INode copy(IFolder targetFolder) throws NodeTrashedException, NodeDeletedException, NodeAlreadyExistsException;
	
	/**
	 * 
	 * @param targetFolder
	 * @return
	 * @throws NodeTrashedException
	 * @throws NodeDeletedException
	 * @throws NodeAlreadyExistsException
	 */
	public INode move(IFolder targetFolder, IProgressMonitor monitor) throws NodeTrashedException, NodeDeletedException, NodeAlreadyExistsException;
	
	/**
	 * Identical to call {@link #move}(targetFolder, null) (no IProgressMonitor).
	 */
	public INode move(IFolder targetFolder) throws NodeTrashedException, NodeDeletedException, NodeAlreadyExistsException;
	
	/**
	 * 
	 * @param newName
	 * @return
	 * @throws NodeTrashedException
	 * @throws NodeDeletedException
	 * @throws NodeAlreadyExistsException
	 */
	public INode rename(NodeName newName) throws NodeTrashedException, NodeDeletedException, NodeAlreadyExistsException;
	
	/**
	 * 
	 * @param newName
	 * @return
	 * @throws NodeTrashedException
	 * @throws NodeDeletedException
	 * @throws NodeAlreadyExistsException
	 * @throws NodeNameForbiddenException
	 */
	public INode rename(String newName) throws NodeTrashedException, NodeDeletedException, NodeAlreadyExistsException, NodeNameForbiddenException;
	
	/** Gets ID of a node. */
	public long getId();
	
	/**
	 * Returns nice (human) name of node to be used in end-user UI. Example:
	 * <pre>
	 * String nameForUI = someNode.getNodeName().toString();
	 * </pre>
	 * 
	 * @return NodeName name of this node.
	 */
	public NodeName getNodeName();
	
	/**
	 * Note parent of workspace (root) is workspace itself
	 * (<tt>workspaceFolder.equals(getParent()) == true</tt>, not null)
	 * 
	 * @return Parent folder or folder itself for workspace (root). Never returns null.
	 */
	public IFolder getParent();
			
	/**
	 * Absolute path of the node, e.g. "/home/pxsa/foodir/foofile".
	 * 
	 * @return
	 */
	public IPath getPath();
	
	/**
	 * Gets node creation date.
	 * 
	 */
	public Date getCreatedAt();
	
	/** Natural ordering: folders sorted by name asc first, followed by files by name asc. */
	@Override
	public int compareTo(INode o);
	
	/**
	 * 
	 * @param listener
	 * @throws NodeTrashedException
	 * @throws NodeDeletedException
	 */
	public void addNodeListener(NodeListener listener) throws NodeTrashedException, NodeDeletedException;
	
	/**
	 * 
	 * @param listener
	 * @throws NodeTrashedException
	 * @throws NodeDeletedException
	 */
	public void removeNodeListener(NodeListener listener) throws NodeTrashedException, NodeDeletedException;
	
	/**
	 * 
	 * @return
	 */
	public String getDescription();
	
	/**
	 * 
	 * @param desc
	 */
	public void setDescription(String desc);
		
	/** 
	 * To get nice (human) name for end-user UI, use
	 * <tt>getNodeName().toString()</tt> instead.
	 * 
	 * @return
	 */
	@Override
	public String toString();
}
