package com.virtage.plexflow.storage;

import org.eclipse.core.runtime.IProgressMonitor;

import com.virtage.plexflow.storage.exceptions.NodeNotFoundException;
import com.virtage.plexflow.system.User;


public interface IStorage {
	
	/**
	 * Returns workspace folder (e.g. a root <tt>/</tt>).
	 * 
	 * @return
	 */
	public IFolder getWorkspace();	
	
	/**
	 * Returns home folder for current user (e.g. <tt>/home/<em>username</em>/</tt>). 
	 * 
	 * @return
	 */
	public IFolder getHomeForCurrentUser();
	
	/**
	 * Returns home folder for specified user.
	 * 
	 * @param user
	 * @return Home folder or <tt>null</tt> if there is no such user.
	 */
	public IFolder getHomeForUser(User user);
		
	
	/**
	 * Returns home root (e.g. <tt>/home/</tt>).
	 * @return
	 */
	public IFolder getHomeRoot();	
	
	public ITrash getTrash();
	
	/**
	 * 
	 * 
	 * @param fileId
	 * @return true if exists and even if it is trashed 
	 */
	public boolean existsFileWithId(long fileId);
	
	/**
	 * 
	 * @param folderId
	 * @return true if exists and even if it is trashed
	 */
	public boolean existsFolderWithId(long folderId);
	
	public IFile getFileWithId(long fileId) throws NodeNotFoundException;
	
	public IFolder getFolderWithId(long folderId) throws NodeNotFoundException;
	
	/**
	 * 
	 * @return
	 */
	public String getScheme();
	
	
	public IStorageInfo getStorageInfo(IProgressMonitor monitor);
	
	/**
	 * Permanently deletes every files, folder in storage including all
	 * version, tags, etc.
	 **/
	public void format(IProgressMonitor monitor);
	
}
