package com.virtage.plexflow.storage;

/** Denotes kind of made change */
public enum ChangeType {
	CREATION,
	DELETION,
	MODIFICATION
}
