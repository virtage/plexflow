package com.virtage.plexflow.storage;

public interface ITaggable {
	public ITags getTags();
}
