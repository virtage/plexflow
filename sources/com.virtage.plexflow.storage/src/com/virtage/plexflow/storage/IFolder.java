package com.virtage.plexflow.storage;

import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;

import com.virtage.plexflow.storage.exceptions.NodeAlreadyExistsException;
import com.virtage.plexflow.storage.exceptions.NodeDeletedException;
import com.virtage.plexflow.storage.exceptions.NodeNameForbiddenException;
import com.virtage.plexflow.storage.exceptions.NodeNotFoundException;
import com.virtage.plexflow.storage.exceptions.NodeTrashedException;
import com.virtage.plexflow.system.User;

public interface IFolder extends INode {
	
	/**
	 * Creates a subfolder at this level of specified name.
	 * 
	 * @param name
	 * @return Created folder instance.
	 * @throws NodeTrashedException
	 * @throws NodeDeletedException
	 * @throws NodeAlreadyExistsException
	 */
	public IFolder createFolder(NodeName name) throws NodeTrashedException, NodeDeletedException, NodeAlreadyExistsException;
	
	/**
	 * Identical to call <tt>createFolder(new NodeName(String))</tt>.
	 * 
	 * @see #createFolder(NodeName)
	 */
	public IFolder createFolder(String name) throws NodeTrashedException, NodeDeletedException, NodeAlreadyExistsException, NodeNameForbiddenException;
	
	/**
	 * Creates a file at this level of specified name.
	 * 
	 * @param name
	 * @return Created file instance.
	 * @throws NodeTrashedException
	 * @throws NodeDeletedException
	 * @throws NodeAlreadyExistsException
	 */
	public IFile createFile(NodeName name) throws NodeTrashedException, NodeDeletedException, NodeAlreadyExistsException;
	
	/**
	 * Identical to call <tt>createFile(new NodeName(String))</tt>.
	 * 
	 * @see #createFile(NodeName)
	 */
	public IFile createFile(String name) throws NodeTrashedException, NodeDeletedException, NodeAlreadyExistsException, NodeNameForbiddenException;
	
	/**
	 * 
	 * 
	 * (one level bellow,
	 * not recursively)
	 * 
	 * //including trashed nodes//
	 * 
	 * @return Child names in no specific order (i.e. excluding this level folder). Never returns null, use isEmpty() to detect if blank.
	 * @throws NodeDeletedException 
	 * @throws NodeTrashedException 
	 */
	public List<String> childNames(IProgressMonitor monitor) throws NodeTrashedException, NodeDeletedException;
	
	/**
	 * Identical to call <tt>childNames(null)</tt> (no IProgressMonitor).
	 * 
	 * @see #childNames(IProgressMonitor)
	 */
	public List<String> childNames() throws NodeTrashedException, NodeDeletedException;
	
	/**
	 * //including trashed nodes//
	 * 
	 * @return Child nodes in no specific order (i.e. excluding this level folder). Never returns null, use isEmpty() to detect if blank.
	 * @throws NodeDeletedException 
	 * @throws NodeTrashedException 
	 */
	public List<INode> childNodes(IProgressMonitor monitor) throws NodeTrashedException, NodeDeletedException;
	
	/**
	 * Identical to call {@link #childNodes}(null) (no IProgressMonitor).
	 */
	public List<INode> childNodes() throws NodeTrashedException, NodeDeletedException;
	
	/**
	 * //including trashed nodes//
	 * 
	 * @return Child names in no specific order (i.e. excluding this level folder). Never returns null, use isEmpty() to detect if blank.
	 * @throws NodeDeletedException 
	 * @throws NodeTrashedException 
	 */
	public List<String> childNamesRecursively(IProgressMonitor monitor) throws NodeTrashedException, NodeDeletedException;
	
	/**
	 * Identical to call {@link #childNamesRecursively}(null) (no IProgressMonitor).
	 */
	public List<String> childNamesRecursively() throws NodeTrashedException, NodeDeletedException;
	
	/**
	 * //including trashed nodes//
	 * 
	 * @return Child nodes in no specific order (i.e. excluding this level folder). Never returns null, use isEmpty() to detect if blank.
	 * @throws NodeDeletedException 
	 * @throws NodeTrashedException 
	 */
	public List<INode> childNodesRecursively(IProgressMonitor monitor) throws NodeTrashedException, NodeDeletedException;
	
	/**
	 * Identical to call {@link #childNodesRecursively}(null) (no IProgressMonitor).
	 */
	public List<INode> childNodesRecursively() throws NodeTrashedException, NodeDeletedException;
	
	
	/**
	 * Exists specified file at this level?
	 * 
	 * @param name
	 * @return
	 * @throws NodeTrashedException
	 * @throws NodeDeletedException
	 */
	public boolean existsFile(NodeName name) throws NodeTrashedException, NodeDeletedException;
	
	/**
	 * Exists specified file at this level?
	 * 
	 * @param name
	 * @return
	 * @throws NodeNameForbiddenException
	 * @throws NodeTrashedException
	 * @throws NodeDeletedException
	 */
	public boolean existsFile(String name) throws NodeNameForbiddenException, NodeTrashedException, NodeDeletedException;
	
	/**
	 * 
	 * Exists specified folder at this level?
	 * 
	 * @param name
	 * @return
	 * @throws NodeTrashedException
	 * @throws NodeDeletedException
	 */
	public boolean existsFolder(NodeName name) throws NodeTrashedException, NodeDeletedException;
	
	/**
	 * Exists specified folder at this level?
	 * 
	 * @param name
	 * @return
	 * @throws NodeTrashedException
	 * @throws NodeDeletedException
	 * @throws NodeNameForbiddenException
	 */
	public boolean existsFolder(String name) throws NodeTrashedException, NodeDeletedException, NodeNameForbiddenException;
	
	/**
	 * 
	 * @param name
	 * @return
	 * @throws NodeTrashedException
	 * @throws NodeDeletedException
	 * @throws NodeNotFoundException
	 */
	public IFile getFile(NodeName name) throws NodeTrashedException, NodeDeletedException, NodeNotFoundException;
	
	/**
	 * Returns specified file at this level.
	 * 
	 * @param name
	 * @return
	 * @throws NodeTrashedException
	 * @throws NodeDeletedException
	 * @throws NodeNotFoundException
	 * @throws NodeNameForbiddenException
	 */
	public IFile getFile(String name) throws NodeTrashedException, NodeDeletedException, NodeNotFoundException, NodeNameForbiddenException;
	
	/**
	 * Returns specified folder at this level.
	 * 	
	 * @param name
	 * @return
	 * @throws NodeTrashedException
	 * @throws NodeDeletedException
	 * @throws NodeNotFoundException
	 */
	public IFolder getFolder(NodeName name) throws NodeTrashedException, NodeDeletedException, NodeNotFoundException;
	
	/**
	 * 
	 * Returns specified folder at this level.
	 * 
	 * @param name
	 * @return
	 * @throws NodeTrashedException
	 * @throws NodeDeletedException
	 * @throws NodeNotFoundException
	 * @throws NodeNameForbiddenException
	 */
	public IFolder getFolder(String name) throws NodeTrashedException, NodeDeletedException, NodeNotFoundException, NodeNameForbiddenException;
			
	/**
	 * Tells whether this folder is a home folder for current user.
	 * 
	 * @return true if folder is home folder for current user.
	 */
	public boolean isHomeForCurrentUser();

	
	/**
	 * Tells whether this folder is a home folder for specified user.
	 * 
	 * @return true if folder is home folder for specified user.
	 */
	public boolean isHomeForUser(User user);
	
	
	/**
	 * Tells whether this folder is a home folder (for any user).
	 * Do not confuse with home root (<tt>/home/</tt>) tested with
	 * {@link #isHomeRoot()}.
	 * 
	 * @return true if folder is home folder (for any user).
	 */	
	public boolean isHome();
	
	
	/**
	 * Tells whether current folder is home root (<tt>/home/</tt>).  Do not
	 * confuse with particular user's home tested with {@link #isHomeForCurrentUser()}.
	 */
	public boolean isHomeRoot();
	
	/**
	 * 	
	 * @return
	 */
	public boolean isWorkspace();
	
	/**
	 * 
	 * TODO: To check only one child level or recursively in any level bellow this?
	 * 
	 * @param node
	 * @return
	 * @throws NodeTrashedException
	 * @throws NodeDeletedException
	 */
	public boolean isParentOf(INode node) throws NodeTrashedException, NodeDeletedException;

	
	/**
	 * Tells whether this folder has any child nodes or not (one level bellow,
	 * not recursively).
	 * 
	 * @return
	 * @throws NodeTrashedException
	 * @throws NodeDeletedException
	 */
	public boolean hasChildNodes() throws NodeTrashedException, NodeDeletedException;
	
	/**
	 * 
	 * @return
	 */
	public ITags getTags();
	
}
