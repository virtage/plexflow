package com.virtage.plexflow.storage.exceptions;

public class BookmarkForbiddenException extends StorageException {
	
	public BookmarkForbiddenException(String forbiddenNodeName) {
		super("Cannot bookmark " +  forbiddenNodeName);
	}
	
	public BookmarkForbiddenException(String forbiddenNodeName, Throwable cause) {
		super("Cannot bookmark " + forbiddenNodeName, cause);
	}
}
