/**
 * <p>Contains root {@link com.virtage.plexflow.ui.storage.StorageException} and a lot of
 * subclasses of it. StorageException itself is abstract, so Storage API is throwing its
 * concrete subclasses.</p>
 * 
 * <p>
 * If unknown or unexpected exception is detected, API will issue uncheked
 * {@link com.virtage.plexflow.RuntimeStorageException.storage.exceptions.UnknownStorageException}.
 * </p>
 */
package com.virtage.plexflow.storage.exceptions;