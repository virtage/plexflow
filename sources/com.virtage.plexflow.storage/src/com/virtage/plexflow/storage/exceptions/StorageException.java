package com.virtage.plexflow.storage.exceptions;

/**
 * Root exception class for all storage-related exceptions. This exception is abstract, Storage API
 * is throwning concrete exception subclasses with actual reason of failure. If not possible to
 * determine actual cause of problems, throw {@link RuntimeStorageException}.
 */
public abstract class StorageException extends Exception {

	private static final long serialVersionUID = 8988042592383364851L;

	private StorageException() {}
	
	public StorageException(String msg) {
		super(msg);
	}

	public StorageException(String msg, Throwable cause) {
		super(msg, cause);
	}
	
}
