package com.virtage.plexflow.storage.exceptions;

import com.virtage.plexflow.storage.NodeName;

public class NodeAlreadyExistsException extends StorageException {
	
	public NodeAlreadyExistsException(String nodeName) {
		super("There is already file or folder of the same name (" + nodeName + ").");
	}
	
	public NodeAlreadyExistsException(String nodeName, Throwable cause) {
		super("There is already file or folder of the same name (" + nodeName + ").", cause);
	}
}
