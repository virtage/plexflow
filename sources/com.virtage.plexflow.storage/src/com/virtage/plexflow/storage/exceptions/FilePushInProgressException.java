package com.virtage.plexflow.storage.exceptions;

import com.virtage.plexflow.storage.IFile;

public class FilePushInProgressException extends StorageException {

	private static final long serialVersionUID = 6965280828961300379L;
	
	public FilePushInProgressException(String nodeName, long id) {
		super("Cannot operate on a file '" + nodeName + "' (id " + id + ") " +
				"while it is push-in-progress (file is incomplete). Commit, " +
				"rollback or wait for pending transaction to finish.");
	}
	
	public FilePushInProgressException(String nodeName, long id, Throwable cause) {
		super("Cannot operate on a file '" + nodeName + "' (id " + id + ") " +
				"while it is push-in-progress (file is incomplete). Commit, " +
				"rollback or wait for pending transaction to finish.", cause);
	}

	public FilePushInProgressException(IFile file) {
		super("Cannot operate on a file '" + file.getPath() +
				"' (id " + file.getId() + ") " +
				"while it is push-in-progress (file is incomplete). Commit, " +
				"rollback or wait for pending transaction to finish.");
	}
	
	public FilePushInProgressException(IFile file, Throwable cause) {
		super("Cannot operate on a file '" + file.getPath() +
				"' (id " + file.getId() + ") " +
				"while it is push-in-progress (file is incomplete). Commit, " +
				"rollback or wait for pending transaction to finish.", cause);
	}
}
