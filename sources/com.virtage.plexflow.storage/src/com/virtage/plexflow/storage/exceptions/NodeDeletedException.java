package com.virtage.plexflow.storage.exceptions;

import com.virtage.plexflow.storage.INode;

public class NodeDeletedException extends StorageException {

	private static final long serialVersionUID = -5120590806940608445L;
	private static final String MSG = "Cannot operate on deleted folder or file: ";
	
	/** Use c-or including what node caused the exception. */
	private NodeDeletedException() {
		// Calling parent's c-or is just conforming to Java language rules
		// Never be called!
		super("This never be called!");
	}
	
	public NodeDeletedException(INode node) {
		super(MSG + node.getPath());
	}
	
	public NodeDeletedException(INode node, Throwable cause) {
		super(MSG + node.getPath(), cause);
	}
}
