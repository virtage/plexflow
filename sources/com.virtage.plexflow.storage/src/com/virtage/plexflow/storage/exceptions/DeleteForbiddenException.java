package com.virtage.plexflow.storage.exceptions;

public class DeleteForbiddenException extends StorageException {
	
	public DeleteForbiddenException(String forbiddenNodeName) {
		super("Cannot delete " +  forbiddenNodeName);
	}
	
	public DeleteForbiddenException(String forbiddenNodeName, Throwable cause) {
		super("Cannot delete " + forbiddenNodeName, cause);
	}
}
