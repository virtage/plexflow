package com.virtage.plexflow.storage.exceptions;

/**
 * Unchecked exception thrown in unrecoverable situation like record is not
 * found in table or violation of some Storage API method contract.
 * 
 * @author libor
 *
 */
public class RuntimeStorageException extends RuntimeException {
	
	private static final long serialVersionUID = -4083952596720873427L;

	public RuntimeStorageException() {
		super(msg());
	}
	
	public RuntimeStorageException(String msg) {
		super(msg(msg));
	}
	
	public RuntimeStorageException(Throwable cause) {
		super(msg(), cause);
	}
	
	public RuntimeStorageException(String msg, Throwable cause) {
		super(msg(msg), cause);
	}
	
	
	
	private static String msg() {
		return "Runtime storage exception.";
	}
	
	private static String msg(String msg) {
		return "Runtime storage exception: " + msg + ".";
	}
}
