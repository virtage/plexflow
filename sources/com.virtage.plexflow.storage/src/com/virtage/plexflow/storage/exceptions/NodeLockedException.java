package com.virtage.plexflow.storage.exceptions;

public class NodeLockedException extends StorageException {
	
	private static String msg = "Cannot operate on locked folder or file.";
	
	public NodeLockedException() {
		super(msg);
	}
	
	public NodeLockedException(Throwable cause) {
		super(msg, cause);
	}
}
