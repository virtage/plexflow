package com.virtage.plexflow.storage.exceptions;

import com.virtage.plexflow.storage.INode;

public class NodeTrashedException extends StorageException {

	private static final long serialVersionUID = 8423970066292988943L;
	private static final String MSG = "Cannot operate on trashed folder or file: ";
	
	/** Use c-or including what node caused the exception. */
	private NodeTrashedException() {
		// Calling parent's c-or is just conforming to Java language rules
		// Never be called!
		super("This never be called!");
	}
	
	public NodeTrashedException(INode node) {
		super(MSG + node.getPath());
	}
	
	public NodeTrashedException(INode node, Throwable cause) {
		super(MSG + node.getPath(), cause);
	}
}
