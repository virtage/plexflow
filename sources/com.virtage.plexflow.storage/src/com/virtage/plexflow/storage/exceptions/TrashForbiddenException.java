package com.virtage.plexflow.storage.exceptions;

public class TrashForbiddenException extends StorageException {

	public TrashForbiddenException(String forbiddenNodeName) {
		super(forbiddenNodeName + " cannot be moved to trash");
	}
	
	public TrashForbiddenException(String forbiddenNodeName, Throwable cause) {
		super(forbiddenNodeName + " cannot be moved to trash", cause);
	}
	
}
