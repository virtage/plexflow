package com.virtage.plexflow.storage.exceptions;

public class NodeNotFoundException extends StorageException {

	private static final long serialVersionUID = 1485565043808519658L;
	private static final String msg = "There is no such file or folder";

	public NodeNotFoundException(long nodeId) {
		super(msg + " (id " + nodeId + ").");
	}
	
	public NodeNotFoundException(String nodeName, long nodeId) {
		super(msg + "'" + nodeName + "' (id " + nodeId + ").");
	}
	
	public NodeNotFoundException(String nodeName, long nodeId, Throwable cause) {
		super(msg + "'" + nodeName + "' (id " + nodeId + ").", cause);
	}
}
