package com.virtage.plexflow.storage.exceptions;

public class NodeNameForbiddenException extends StorageException {
	
	public NodeNameForbiddenException(String msg) {
		super(msg);
	}
	
	public NodeNameForbiddenException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
