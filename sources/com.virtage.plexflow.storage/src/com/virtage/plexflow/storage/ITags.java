package com.virtage.plexflow.storage;

import java.util.Set;

/**
 * General collection-alike interface for managing set of string tags for a object. 
 *
 */
public interface ITags {
	
	/** Adds tag to collection. Do nothing if tag already exists. */
	public void add(String tag);
	
	/** Adds tags to collection. Do nothing if tag already exists. */
	public void add(String ... tags);
	
	/** Removes tag from collection. Do nothing if tag doesn't exist. */
	public void remove(String tag);
	
	/** Removes tags from collection. Do nothing if tags don't exist. */
	public void remove(String ... tags);
	
	/**
	 * Tells whether tags contains this tag.
	 */
	public boolean contains(String tag);
	
	/**
	 * Returns set of tags in no-specific (random) order.  
	 * 
	 */
	public Set<String> tagsSet();
	
	/**
	 * Returns sorted set of tags from the most frequenty used. 
	 * 
	 */
	public Set<String> tagsSetSortedByUsage();
}
