package com.virtage.plexflow.storage;

import java.util.Date;
import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;

import com.virtage.plexflow.storage.exceptions.DeleteForbiddenException;
import com.virtage.plexflow.storage.exceptions.StorageException;
import com.virtage.plexflow.storage.exceptions.TrashForbiddenException;

public interface ITrash {
	
	
	/**
	 * Lists trashed nodes and recursively all subnodes bellow. Return order of nodes is not
	 * specified. Sort yourself if needed.
	 * 
	 * @param folder
	 * @return
	 */
	public List<INode> getTrashedNodes(IFolder folder, IProgressMonitor monitor);
	
	/**
	 * Identical to call {@link #getTrashedNodes}(folder, null) (no IProgressMonitor).
	 */
	public List<INode> getTrashedNodes(IFolder folder);
	
	/**
	 * Moves node and recursively all subnodes into trash.
	 * 
	 * @param node
	 * @throws TrashForbiddenException 
	 */
	public void trash(INode node, IProgressMonitor monitor) throws TrashForbiddenException;
	
	/**
	 * Identical to call {@link #trash}(node, null) (no IProgressMonitor).
	 */
	public void trash(INode node) throws TrashForbiddenException;
	
	/**
	 * Restores node and recursively all subnodes from trash to original locations, possible 
	 * under new name. If node has not been trashed, it does nothing (doesn't throw an exception).
	 * 
	 * @param node
	 * @return NodeName under which node has been restored. Normally it is equals to original.
	 * In case that restoring would have caused a conflict with existing (not trashed) node, then
	 * new alternative name is returned (suffixed with " (1)", " (2)", " (3)" etc.). 
	 */
	public NodeName restore(INode node, IProgressMonitor monitor);
	
	/**
	 * Identical to call {@link #restore}(node, null) (no IProgressMonitor).
	 */
	public NodeName restore(INode node);
	
	
	/**
	 * Whether is node trashed or not.
	 * 
	 */
	public boolean contains(INode node, IProgressMonitor monitor);
	
	/**
	 * Identical to call {@link #contains}(node, null) (no IProgressMonitor).
	 */
	public boolean contains(INode node);

	/**
	 * Permanently delete node and recursively all subnodes.
	 * 
	 * @param node
	 * @throws DeleteForbiddenException 
	 */
	public void delete(INode node, IProgressMonitor monitor) throws DeleteForbiddenException;
	
	/**
	 * Identical to call {@link #delete}(node, null) (no IProgressMonitor).
	 */
	public void delete(INode node) throws DeleteForbiddenException;

		
	/**
	 * Returns date at which node has been moved to trash.
	 * 
	 * @return null if node has not been trashed or is deleted
	 */
	public Date getTrashedAt(INode node);
	
	
	/**
	 * Returns trashed nodes for particular folder.
	 * @param folder
	 * @return
	 */
	public List<INode> getTrashedNodesFor(IFolder folder);
	
	
	/**
	 * Returns deleted nodes for particular folder.
	 * @param folder
	 * @return
	 */
	public List<INode> getDeletedNodesFor(IFolder folder);
}
