package com.virtage.plexflow.storage;

import com.virtage.plexflow.storage.exceptions.NodeNameForbiddenException;
import com.virtage.plexflow.storage.exceptions.StorageException;
import com.virtage.plexflow.storage.exceptions.RuntimeStorageException;
import com.virtage.plexflow.system.User;

/**
 * <p>Name of node (folder or file).</p>
 * 
 * <p>It's simple wrapper for String name ensuring that no invalid characters are used.
 * Doesn't have to represent actual node of the storage.</p>
 * 
 * <p>Forbidden names "workspace" and "home" (home root) can't be created using constructor (it will
 * cause an exception). Use {@link #WORKSPACE} and {@link #HOME_ROOT} class constants.</p>
 * 
 * <p>Actual filename for end-user views can be obtained by <tt>toString</tt>.</p>
 * 
 * @author libor
 *
 */
public final class NodeName implements Comparable<NodeName> {	
	//--------------------------------------------------------------------------
    // Class fields - public
    //--------------------------------------------------------------------------
	
	/** Node has no extension (equals to empty string ""). */
	public static final String NO_EXTENSION = "";
	
	/** Nice name of workspace (value "Workspace" (reserved name)).*/
	public static final NodeName WORKSPACE = getWorkspaceNodeName();
	
	/** Nice name of home root (folder for users home folders). */
	public static final NodeName HOME_ROOT = getHomeRootNodeName();
	
    //--------------------------------------------------------------------------
    // Class fields - non-public
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Instance fields - public
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Instance fields - non-public
    //--------------------------------------------------------------------------
	private String name;
	
    //--------------------------------------------------------------------------
    // Instance fields - bean properties
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Constructors, static initializers, factories
    //--------------------------------------------------------------------------
	
	private NodeName() {
	}
	
	/** Creates a new NodeName with name from parameter. */
	public NodeName(NodeName nodeName) {
		this();
		this.name = nodeName.toString();
	}
	
	/**
	 * Creates NodeName from String literal.
	 * 
	 * @throws NodeNameForbiddenException if desired name containes invalid characters or reserved names.
	 * */
	public NodeName(String name) throws NodeNameForbiddenException {
		testAndThrow(name);
		
		this.name = name;
	};
	
	
    //--------------------------------------------------------------------------
    // Public API - class (static)
    //--------------------------------------------------------------------------
	
	/**
	 * <p>Tests whether name would be valid node name. Doesn't throw
	 * NodeNameForbiddenException if invalid.</p>
	 * 
	 * <p>If you need to know a reason why node name is invalid, instantiate
	 * NodeName in try-catch block as in this snippet:
	 * <pre>
String msg = "Node name is valid";
try {
    new NodeName(newText);    
} catch (NodeNameForbiddenException e) {
    msg = e.getMessage();
}
	 * </pre></p>
	 * */
	public static boolean isValid(String possibleName) {
		boolean valid = true;
		
		try {
			testAndThrow(possibleName);
		} catch (NodeNameForbiddenException e) {
			valid = false;
		}
		
		return valid;		
	}	
	
	
    //--------------------------------------------------------------------------
    // Public API - instance
    //--------------------------------------------------------------------------
	
	/**
	 * Returns node extension if any.
	 * 
	 * @return Extension or {@link NodeName.NO_EXTENSION} constant (empty string ""). Never returns
	 * null - if extension is not entered, returns empty string ("") ({@link NodeName.NO_EXTENSION}).
	 */
	public String getExtension() {
		int index = name.lastIndexOf('.');
		return (index != -1) ? name.substring(index) : NO_EXTENSION; 
	}
	
    //--------------------------------------------------------------------------
    // Overrides and implementations
    //--------------------------------------------------------------------------

	/** Case insensitive comparation. */
	@Override
	public int compareTo(NodeName o) {
		return name.compareToIgnoreCase(o.toString());
	}

	/**
	 * Node filename suitable for end-user views.
	 **/
	@Override
	public String toString() {
		return name;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	/** Case insensitive equals(). */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NodeName other = (NodeName) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equalsIgnoreCase(other.name))
			return false;
		return true;
	}	
	
    //--------------------------------------------------------------------------
    // Getters/setters of properties
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Helper methods (mostly private)
    //--------------------------------------------------------------------------
	
	/**
	 * Test name. If okay, do nothing, else throw StorageException.
	 * @see http://plexflow.virtage.com/wiki/User_Guide/Nodes#Naming_rules
	 **/
	private static void testAndThrow(String name) throws NodeNameForbiddenException {
		
		if (name == null) {
			throw new NodeNameForbiddenException("Name can't be null.");
		}		
		if (name.indexOf('/') != -1) {
			throw new NodeNameForbiddenException("Name can't contains '/' character.");
		}		
		if (name.indexOf('\\') != -1) {
			throw new NodeNameForbiddenException("Name can't contains '\\' character.");
		}
		if (name.trim().isEmpty()) {
			throw new NodeNameForbiddenException("Name can't be empty.");
		}
		if (name.equalsIgnoreCase("workspace")) {
			throw new NodeNameForbiddenException("Name can't be named 'Workspace' and its variants.");
		}
		if (name.equalsIgnoreCase("home")) {
			throw new NodeNameForbiddenException("Name can't be named 'Home' and its variants.");
		}
		if (name.equals(".")) {
			throw new NodeNameForbiddenException("Name can't be named '.' (dot).");
		}
		if (name.equals("..")) {
			throw new NodeNameForbiddenException("Name can't be named '..' (two dots).");
		}
		if (name.length() > 256) {
			throw new NodeNameForbiddenException("Name maximum lenght is 256 characters.");
		}
	}
	
	
	/** Creates node with reserved name "workspace". */
	private static NodeName getWorkspaceNodeName() {
		try {
			NodeName w = new NodeName("wworkspace");
			w.name = "Workspace";
			return w;
			
		} catch (NodeNameForbiddenException ex) {
			throw new RuntimeStorageException("Cannot create NodeName for workspace!");
		}
	}
	
	private static NodeName getHomeRootNodeName() {
		try {
			NodeName h = new NodeName("Home root");
			return h;
			
		} catch (NodeNameForbiddenException ex) {
			throw new RuntimeStorageException("Cannot create NodeName for home root!");
		}
	}
}
