package com.virtage.plexflow.storage;

import java.util.List;

public interface IVersionable {
	
	public void setVersioned(boolean versioned);
	public boolean isVersioned();
	
	public void setAutoVersioned(boolean autoVersioned);
	public boolean isAutoVersioned();
	
	public void checkOut();
	public void unCheckOut();
	public void checkIn();	
	
	public void restoreToVersion(IVersion version);
	
	public boolean isCheckedOut();
	public List<IVersion> getVersions();	
}