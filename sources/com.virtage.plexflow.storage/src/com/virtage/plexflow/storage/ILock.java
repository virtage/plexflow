package com.virtage.plexflow.storage;

import java.util.Date;

import com.virtage.plexflow.system.User;

public interface ILock {
	
	/** Denoting that lock never expires. */
	public static final long NEVER_EXPIRES = 0;
	
	
	/** User who placed this lock. */
	public User lockedBy();
	
	
	/** A time of placing this lock. */
	public Date lockedWhen();
	
	/**
	 * Says whether lock is indirect, i.e. locked node has been locked indirectly
	 * as result of locking its parent folder.
	 *  
	 * @return
	 */
	public boolean isIndirect();
	
	
	/** Lock time-out in millis, i.e. a time after lock will expire. */
	public long getTimeOut();
	
	
	/** Whether is this lock already expired or not. */
	public boolean isExpired();
	
	/**
	 * Reference to a node locked by this lock.
	 * 
	 * @return
	 */
	public INode getLockedNode();	
}
