package com.virtage.plexflow.storage;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import net.jcip.annotations.ThreadSafe;

import com.virtage.plexflow.storage.impl.LocalStorage;

/**
 * Factory for obtaining default {@link IStorage} implementation with a
 * couple of utility static methods.
 * 
 * 
 * @author libor
 */
@ThreadSafe
public class Storage {
	
	 //--------------------------------------------------------------------------
    // Class fields - public
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Class fields - non-public
    //--------------------------------------------------------------------------
    
	private static volatile IStorage instance;
 
    //--------------------------------------------------------------------------
    // Instance fields - public
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Instance fields - non-public
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Instance fields - bean properties
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Constructors, static initializers, factories
    //--------------------------------------------------------------------------
	
	private Storage() {}
	
	public static IStorage get() {
		if (instance == null) {
			synchronized (Storage.class) {
				if (instance == null) {
					instance = new LocalStorage();
				}
			}			
		}
		
		return instance;
	}
	
    //--------------------------------------------------------------------------
    // Public API - class (static)
    //--------------------------------------------------------------------------
		
	
	/**
	 * Returns path to a node as elements of array starting from workspace root.
	 * 
	 * <p>For example for file <tt>/home/pxsu/shipping/2012.xls</tt> will
	 * return array <tt>INode[] { /, pxsu/, shipping/, 2012.doc }</tt>.</p>
	 * 
	 * <p>Actual types of elements are {@link IFolder}, or {@link IFile}
	 * respectively.</p>
	 * 
	 * <p>Often used in UI to construct <tt>TreePath</tt> for
	 * selecting element in a tree viewers. Example to select
	 * <tt>subfolder/</tt> folder:
	 * <pre>
	 * INode[] folderPath = Storage.nodePathAsArray(subfolder);		
	 * treeViewer.setSelection(new TreeSelection(new TreePath(folderPath)));
	 * </pre>
	 * </p>
	 * 
	 * @param node
	 * @return Array of <tt>IFolder</tt>/<tt>IFile</tt> elements comprising a
	 * path to node.
	 */
	public static INode[] nodePathAsArray(INode node) {		
		// Start with list for easier manipulation
		List<INode> pathElements = new ArrayList<>();
		// Add end node as first element
		pathElements.add(node);
		
		IFolder workspace = Storage.get().getWorkspace();
		IFolder parent = node.getParent();
		
		// While not reached the workspace (root)
		while (!parent.equals(workspace)) {
			// Add parent
			pathElements.add(parent);
			// Ask for parent of this parent for next loop
			parent = parent.getParent();
		}
		
		// As last element, add workspace
		pathElements.add(workspace);
		
		// Now reverse from e.g. { "2012.xls", "pxsu/", "home/", "/} to
		// corrent { "/", "home/", "pxsu/", "2012.xls" } 
		Collections.reverse(pathElements);
		
		return pathElements.toArray(new INode[] {});
	}
	
	
    //--------------------------------------------------------------------------
    // Public API - instance (most utility methods)
    //--------------------------------------------------------------------------
	
    //--------------------------------------------------------------------------
    // Overrides and implementations
    //--------------------------------------------------------------------------
 
	
    //--------------------------------------------------------------------------
    // Getters/setters of properties
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Helper methods (mostly private)
    //--------------------------------------------------------------------------

}