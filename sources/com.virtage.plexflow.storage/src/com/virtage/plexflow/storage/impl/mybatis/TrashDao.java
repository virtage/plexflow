package com.virtage.plexflow.storage.impl.mybatis;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.session.SqlSession;

import com.virtage.plexflow.storage.impl.LocalFile;
import com.virtage.plexflow.storage.impl.LocalFolder;
import com.virtage.plexflow.system.SystemInstanceFactory;

public class TrashDao implements TrashMapper {

	@Override
	public void trashFile(LocalFile file) {
		SqlSession session = SystemInstanceFactory.getInstance().getSqlSessionFactory().openSession();
		try {
			session.getMapper(TrashMapper.class).trashFile(file);
			session.commit();
			
		} finally {
			session.close();
		}
	}

	@Override
	public void trashFolder(LocalFolder folder) {
		SqlSession session = SystemInstanceFactory.getInstance().getSqlSessionFactory().openSession();
		try {
			session.getMapper(TrashMapper.class).trashFolder(folder);
			session.commit();
			
		} finally {
			session.close();
		}
	}

	@Override
	public List<LocalFolder> listTrashedFolders(LocalFolder folder) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<LocalFile> listTrashedFiles(LocalFolder folder) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void deleteFolder(LocalFolder folder) {
		SqlSession session = SystemInstanceFactory.getInstance().getSqlSessionFactory().openSession();
		try {
			session.getMapper(TrashMapper.class).deleteFolder(folder);
			session.commit();
			
		} finally {
			session.close();
		}	
	}

	@Override
	public void deleteFile(LocalFile file) {
		SqlSession session = SystemInstanceFactory.getInstance().getSqlSessionFactory().openSession();
		try {
			session.getMapper(TrashMapper.class).deleteFile(file);			
			session.commit();
			
		} finally {
			session.close();
		}
		
	}

	@Override
	public boolean containsFolder(long id) {		
		SqlSession session = SystemInstanceFactory.getInstance().getSqlSessionFactory().openSession();
		try {
			return session.getMapper(TrashMapper.class).containsFolder(id);
		} finally {
			session.close();
		}		
	}

	@Override
	public boolean containsFile(long id) {
		SqlSession session = SystemInstanceFactory.getInstance().getSqlSessionFactory().openSession();
		try {
			return session.getMapper(TrashMapper.class).containsFile(id);
		} finally {
			session.close();
		}
	}

	@Override
	public void restoreFolder(long id) {
		SqlSession session = SystemInstanceFactory.getInstance().getSqlSessionFactory().openSession();		
		try {
			session.getMapper(TrashMapper.class).restoreFolder(id);
			session.commit();
		} finally {
			session.close();
		}		
	}

	@Override
	public void restoreFile(long id) {
		SqlSession session = SystemInstanceFactory.getInstance().getSqlSessionFactory().openSession();		
		try {
			session.getMapper(TrashMapper.class).restoreFile(id);
			session.commit();
		} finally {
			session.close();
		}		
	}

	@Override
	public boolean isFolderTrashed(long id) {
		SqlSession session = SystemInstanceFactory.getInstance().getSqlSessionFactory().openSession();		
		try {
			return session.getMapper(TrashMapper.class).isFolderTrashed(id);			
		} finally {
			session.close();
		}
	}

	@Override
	public boolean isFileTrashed(long id) {
		SqlSession session = SystemInstanceFactory.getInstance().getSqlSessionFactory().openSession();		
		try {
			return session.getMapper(TrashMapper.class).isFileTrashed(id);			
		} finally {
			session.close();
		}
	}

	@Override
	public Date getFolderTrashedDate(long folderId) {
		SqlSession session = SystemInstanceFactory.getInstance().getSqlSessionFactory().openSession();		
		try {
			return session.getMapper(TrashMapper.class).getFolderTrashedDate(folderId);			
		} finally {
			session.close();
		}
	}

	@Override
	public Date getFileTrashedDate(long fileId) {
		SqlSession session = SystemInstanceFactory.getInstance().getSqlSessionFactory().openSession();		
		try {
			return session.getMapper(TrashMapper.class).getFileTrashedDate(fileId);			
		} finally {
			session.close();
		}
	}
	
}
