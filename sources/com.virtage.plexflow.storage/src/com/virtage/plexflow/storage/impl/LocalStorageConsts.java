package com.virtage.plexflow.storage.impl;

import java.util.Date;
import java.util.GregorianCalendar;

public interface LocalStorageConsts {
	
	/** DB ID of workspace (/) folder in table (value {@value}). */
	long WORKSPACE_ID = 1L;
	
	/** DB ID of "/home" folder in table (value {@value}). */
	long HOME_ROOT_ID = 2L;
	
	/** Unix epoch as string for SQL queries (therefore single quoted) (value {@value}). */ 
	String UNIX_EPOCH_SQLSTR = "'1970-01-01 00:00:00'";
	
	/** Unix epoch as Java Date (value {@value}). */ 
	Date UNIX_EPOCH_DATE = new GregorianCalendar(1970, 00, 01, 00, 00, 00).getTime();
	
}
