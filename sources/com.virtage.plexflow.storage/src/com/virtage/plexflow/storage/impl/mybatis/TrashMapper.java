package com.virtage.plexflow.storage.impl.mybatis;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.virtage.plexflow.storage.impl.LocalFile;
import com.virtage.plexflow.storage.impl.LocalFolder;
import com.virtage.plexflow.storage.impl.LocalStorageConsts;

public interface TrashMapper {	
	
	/** Trashes folder except workspace, home root, and any home. */
	@Update("UPDATE FILE SET TRASHED_AT = CURRENT_TIMESTAMP WHERE ID = #{id}")
	public void trashFile(LocalFile file);
	
	@Update("UPDATE FOLDER SET TRASHED_AT = CURRENT_TIMESTAMP WHERE ID = #{id}" +
			" AND ID <> " + LocalStorageConsts.WORKSPACE_ID +
			" AND ID <> " + LocalStorageConsts.HOME_ROOT_ID +
			" AND PARENT_ID <> " + LocalStorageConsts.HOME_ROOT_ID)
	public void trashFolder(LocalFolder folder);	
	
	// TRASHED_AT <> Unix epoch = trashed
	@Select("SELECT * FROM FOLDER WHERE TRASHED_AT <> " + LocalStorageConsts.UNIX_EPOCH_SQLSTR +
			" AND PARENT_ID = #{id}")
	public List<LocalFolder> listTrashedFolders(LocalFolder folder);
	
	// TRASHED_AT <> Unix epoch = trashed
	@Select("SELECT * FROM FILE WHERE TRASHED_AT <> " + LocalStorageConsts.UNIX_EPOCH_SQLSTR +
			" AND FOLDER_ID = #{id}")
	public List<LocalFile> listTrashedFiles(LocalFolder folder);
	
	/** Deletes folder except workspace, home root, and any home. */
	@Delete("DELETE FROM FOLDER WHERE ID = #{id}" +
			" AND ID <> " + LocalStorageConsts.WORKSPACE_ID +
			" AND ID <> " + LocalStorageConsts.HOME_ROOT_ID +
			" AND PARENT_ID <> " + LocalStorageConsts.HOME_ROOT_ID)
	public void deleteFolder(LocalFolder folder);
	
	@Delete("DELETE FROM FILE WHERE ID = #{id}")
	public void deleteFile(LocalFile file);
	
	@Select("SELECT	CASE WHEN COUNT(*) = 1 THEN TRUE ELSE FALSE END " +
			"FROM FOLDER WHERE ID = #{id}")
	public boolean containsFolder(long id);
	
	@Select("SELECT	CASE WHEN COUNT(*) = 1 THEN TRUE ELSE FALSE END " +
			"FROM FILE WHERE ID = #{id}")
	public boolean containsFile(long id);
	
	@Update("UPDATE FOLDER SET TRASHED_AT = " + LocalStorageConsts.UNIX_EPOCH_SQLSTR + " WHERE ID = #{id}")
	public void restoreFolder(long id);
	
	@Update("UPDATE FILE SET TRASHED_AT = " + LocalStorageConsts.UNIX_EPOCH_SQLSTR + " WHERE ID = #{id}")
	public void restoreFile(long id);
	
	@Select("SELECT CASE WHEN COUNT(*) = 1 THEN TRUE ELSE FALSE END " +
			"FROM FOLDER WHERE ID = #{id} AND TRASHED_AT <> " + LocalStorageConsts.UNIX_EPOCH_SQLSTR)
	public boolean isFolderTrashed(long id);
	
	@Select("SELECT CASE WHEN COUNT(*) = 1 THEN TRUE ELSE FALSE END " +
			"FROM FILE WHERE ID = #{id} AND TRASHED_AT <> " + LocalStorageConsts.UNIX_EPOCH_SQLSTR)
	public boolean isFileTrashed(long id);
	
	@Select("SELECT TRASHED_AT FROM FOLDER WHERE ID = #{id} AND TRASHED_AT <> " + LocalStorageConsts.UNIX_EPOCH_SQLSTR)
	public Date getFolderTrashedDate(long folderId);
	
	@Select("SELECT TRASHED_AT FROM FILE WHERE ID = #{id} AND TRASHED_AT <> " + LocalStorageConsts.UNIX_EPOCH_SQLSTR)
	public Date getFileTrashedDate(long fileId);
}
