/**
 * Storage implementation using local files on remote server proxied throught HTTP REST-like API.
 * 
 * Package is visible to cvp.http.api only (x-friends). 
 * 
 */
package com.virtage.plexflow.storage.impl;