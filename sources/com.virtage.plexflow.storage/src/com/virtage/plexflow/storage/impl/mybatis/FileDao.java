package com.virtage.plexflow.storage.impl.mybatis;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;

import com.virtage.plexflow.storage.exceptions.NodeNotFoundException;
import com.virtage.plexflow.storage.exceptions.RuntimeStorageException;
import com.virtage.plexflow.storage.impl.LocalFile;
import com.virtage.plexflow.system.SystemInstanceFactory;
import com.virtage.plexflow.utils.PxUtils;

public class FileDao implements FileMapper {

	/** Use more convenient {@link #insertFile(String, long)}. */  
	@Override
	public void insertFile(Map<String, Object> paramMap) {
		SqlSession session = SystemInstanceFactory.getInstance().getSqlSessionFactory().openSession();
		
		try {
			FileMapper mapper = session.getMapper(FileMapper.class);
			mapper.insertFile(paramMap);			
			session.commit();
			
		} finally {
			session.close();
		}
	}
	
	
	public long insertFile(String name, long targetFolderId) {
		HashMap<String, Object> params = new HashMap<>();
		params.put(FileMapper.NAME, name);
		params.put(FileMapper.TARGET_FOLDER_ID, targetFolderId);		
				
		insertFile(params);		
		
		return PxUtils.bigDecimalToLong(params.get(FileMapper.NEW_ID));		
	}
	
	@Override
	public List<LocalFile> findAllFilesInFolder(long folderId) {
		List<LocalFile> result = new ArrayList<>();
		
		SqlSession session = SystemInstanceFactory.getInstance().getSqlSessionFactory().openSession();
		try {
			FileMapper fileMapper = session.getMapper(FileMapper.class);
			result  = fileMapper.findAllFilesInFolder(folderId);
			
		} finally {
			session.close();
		}
		
		return result;
	}

	@Override
	public LocalFile findFileById(long id) throws NodeNotFoundException {
		LocalFile f = null;
		
		SqlSession session = SystemInstanceFactory.getInstance().
				getSqlSessionFactory().openSession();
		
		try {
			f = session.getMapper(FileMapper.class).findFileById(id);
			
		} finally {
			session.close();
		}		
		
		if (f == null) {
			throw new NodeNotFoundException(id);
		}
		
		return f;
	}

	@Override
	public LocalFile findFileByName(long folderIdToSearchIn, String name) {
		LocalFile file = null;
		
		SqlSession session = SystemInstanceFactory.getInstance().getSqlSessionFactory().openSession();
		try {
			file = session.getMapper(FileMapper.class).
				findFileByName(folderIdToSearchIn, name);
			
		} finally {
			session.close();
		}
		
		return file;
	}

	@Override
	public void renameFile(long id, String newName) {
		SqlSession session = SystemInstanceFactory.getInstance().getSqlSessionFactory().openSession();
		try {
			session.getMapper(FileMapper.class).renameFile(id, newName.toString());
			session.commit();
			
		} finally {
			session.close();
		}
	}

	@Override
	public void moveFile(long sourceFileId, long targetFolderId) {
		SqlSession session = SystemInstanceFactory.getInstance().getSqlSessionFactory().openSession();
		
		try {
			FileMapper mapper = session.getMapper(FileMapper.class);
			mapper.moveFile(sourceFileId, targetFolderId);
			session.commit();
			
		} finally {
			session.close();
		}
	}

	/** Please use more convenient {@link #copyFile(long, long)} instead. */
	@Override
	public void copyFile(Map<String, Long> params) {
		SqlSession session = SystemInstanceFactory.getInstance().getSqlSessionFactory().openSession();
		
		try {
			FileMapper mapper = session.getMapper(FileMapper.class);
			mapper.copyFile(params);			
			session.commit();
			
		} finally {
			session.close();
		}
	}
	
	/**
	 * 
	 * @param sourceFileId
	 * @param targetFolderId
	 * @return ID of copied file
	 */
	public long copyFile(long sourceFileId, long targetFolderId) {
		PxUtils.iaeIfLessThen(sourceFileId, "sourceFileId", 1);
		PxUtils.iaeIfLessThen(targetFolderId, "targetFolderId", 1);		
		
		HashMap<String, Long> params = new HashMap<>();
		params.put(FileMapper.SOURCE_FILE_ID, sourceFileId);
		params.put(FileMapper.TARGET_FOLDER_ID, targetFolderId);		
				
		copyFile(params);		
		
		Long newId = params.get(FileMapper.NEW_ID);
		
		if (newId == null) {
			throw new RuntimeStorageException("Out parameter " + 
					FileMapper.NEW_ID + " was null.");
			
		} else if (newId < 1) {
			throw new RuntimeStorageException("Out parameter " + 
					FileMapper.NEW_ID + " has invalid value" + newId + ".");
		}
		
		return newId;
	}
	

	@Override
	public void setFileDescription(long folderId, String desc) {
		PxUtils.iaeIfEmptyString(desc, "description");
		PxUtils.iaeIfLongerThen(desc, "description", 1024);
		
		SqlSession session = SystemInstanceFactory.getInstance().getSqlSessionFactory().openSession();
		try {
			session.getMapper(FileMapper.class).setFileDescription(folderId, desc);
			session.commit();
			
		} finally {
			session.close();
		}

	}

	@Override
	public boolean existsFileInFolder(long folderIdToSearchIn, String name) {
		boolean result = false;
		
		SqlSession session = SystemInstanceFactory.getInstance().getSqlSessionFactory().openSession();
		try {
			result = session.getMapper(FileMapper.class).
				existsFileInFolder(folderIdToSearchIn, name);
			
		} finally {
			session.close();
		}
		
		return result;
	}

	@Override
	public boolean existsFileWithId(long id) {
		boolean exists = false;
		
		SqlSession session = SystemInstanceFactory.getInstance().getSqlSessionFactory().openSession();		
		try {		
			exists = session.getMapper(FileMapper.class).existsFileWithId(id);
			
		} finally {
			session.close();
		}
		
		return exists;
	}

	@Override
	public void setFileLength(long fileId, long length) {
		SqlSession session = SystemInstanceFactory.getInstance().getSqlSessionFactory().openSession();
		try {
			session.getMapper(FileMapper.class).setFileLength(fileId, length);
			session.commit();
			
		} finally {
			session.close();
		}
	}

	@Override
	public void setFileCrc32(long fileId, long crc32) {
		SqlSession session = SystemInstanceFactory.getInstance().getSqlSessionFactory().openSession();
		try {
			session.getMapper(FileMapper.class).setFileCrc32(fileId, crc32);
			session.commit();
			
		} finally {
			session.close();
		}
	}

	@Override
	public void setPushInProgressForFile(long id, boolean pushInProgress) {
		SqlSession session = SystemInstanceFactory.getInstance().getSqlSessionFactory().openSession();		
		try {
			session.getMapper(FileMapper.class).setPushInProgressForFile(id, pushInProgress);
			session.commit();
			
		} finally {
			session.close();
		}

	}
	//--------------------------------------------------------------------------
	// Class fields - public
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Class fields - non-public
	//--------------------------------------------------------------------------

	//private static final ISysLog SYSLOG = Logging.getSysLog(ThisClass.class);

	//--------------------------------------------------------------------------
	// Instance fields - public
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Instance fields - non-public
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Instance fields - bean properties
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Constructors, static initializers, factories
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Public API - class (static)
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Public API - instance
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Overrides and implementations
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Consider toString(), equals(), hashCode()
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Getters/setters of properties
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Helper methods (mostly private)
	//--------------------------------------------------------------------------
}
