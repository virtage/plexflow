package com.virtage.plexflow.storage.impl;

import java.util.Date;

import com.virtage.plexflow.storage.NodeName;
import com.virtage.plexflow.storage.exceptions.NodeAlreadyExistsException;
import com.virtage.plexflow.storage.impl.mybatis.FileDao;
import com.virtage.plexflow.storage.impl.mybatis.FolderDao;

/**
 * Utility class comprised from static methods only. 
 * 
 * 
 * @author libor
 *
 */
public final class LocalStorageUtils {
	//--------------------------------------------------------------------------
	// Class fields - public
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Class fields - non-public
	//--------------------------------------------------------------------------

	//private static final ISysLog SYSLOG = Logging.getSysLog(ThisClass.class);

	//--------------------------------------------------------------------------
	// Instance fields - public
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Instance fields - non-public
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Instance fields - bean properties
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Constructors, static initializers, factories
	//--------------------------------------------------------------------------

	private LocalStorageUtils() {}
	
	//--------------------------------------------------------------------------
	// Public API - class (static)
	//--------------------------------------------------------------------------

	/** Consider date as date of trashing? Answers yes if equal to Unix epoch. */
	public static boolean isDateTrashed(Date date) {
		return !date.equals(LocalStorageConsts.UNIX_EPOCH_DATE);
	}
	
	/** Consider date as date of deletion? Answers yes if equal to Unix epoch. */
	public static boolean isDateDeleted(Date date) {
		return !date.equals(LocalStorageConsts.UNIX_EPOCH_DATE);
	}
	
	
	/** Throw NodeAlreadyException if file or folder of desired name already exists in a folder. */
    public static void throwIfNodeNameAlreadyExistsInFolder(LocalFolder folderWhere, NodeName targetName)
    		throws NodeAlreadyExistsException {
    	
    	// Does exist such file already in the *same* folder"?
		boolean exists = new FileDao().existsFileInFolder(folderWhere.getId(), targetName.toString());
		if (exists) {
			throw new NodeAlreadyExistsException(targetName.toString());
		}
		
		// Does exist such folder already in the *same folder"?
		exists = new FolderDao().existsSubfolderInFolder(folderWhere.getId(), targetName.toString());
		if (exists) {
			throw new NodeAlreadyExistsException(targetName.toString());
		}
	}
	
	//--------------------------------------------------------------------------
	// Public API - instance
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Overrides and implementations
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Consider toString(), equals(), hashCode()
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Getters/setters of properties
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Helper methods (mostly private)
	//--------------------------------------------------------------------------
}
