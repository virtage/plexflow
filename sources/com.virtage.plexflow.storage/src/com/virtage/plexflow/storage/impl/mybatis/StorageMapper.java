package com.virtage.plexflow.storage.impl.mybatis;

import org.apache.ibatis.annotations.Delete;

import com.virtage.plexflow.storage.impl.LocalStorageConsts;

public interface StorageMapper {

	@Delete("DELETE FROM FOLDER WHERE " +
			"PARENT_ID <> " + LocalStorageConsts.HOME_ROOT_ID + " AND " +		// nothing under /home
			"ID <> " + LocalStorageConsts.HOME_ROOT_ID + " AND " +			// no /home
			"ID <> " + LocalStorageConsts.WORKSPACE_ID)				// no workspace (/)")
	public int delFolderTable();

	@Delete("DELETE FROM FILE")
	public int delFilesTable();
	
}
