package com.virtage.plexflow.storage.impl;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.eclipse.core.runtime.IProgressMonitor;

import com.virtage.plexflow.logging.ISysLog;
import com.virtage.plexflow.logging.Logger;
import com.virtage.plexflow.storage.IFile;
import com.virtage.plexflow.storage.IFolder;
import com.virtage.plexflow.storage.IStorage;
import com.virtage.plexflow.storage.IStorageInfo;
import com.virtage.plexflow.storage.ITrash;
import com.virtage.plexflow.storage.exceptions.NodeNotFoundException;
import com.virtage.plexflow.storage.impl.mybatis.FileDao;
import com.virtage.plexflow.storage.impl.mybatis.FolderDao;
import com.virtage.plexflow.storage.impl.mybatis.StorageDao;
import com.virtage.plexflow.system.SystemInstanceFactory;
import com.virtage.plexflow.system.User;

public final class LocalStorage implements IStorage {
	
	//--------------------------------------------------------------------------
    // Class fields - public
    //--------------------------------------------------------------------------
 
	public static final String SCHEME = "px+local";
	
	
    //--------------------------------------------------------------------------
    // Class fields - non-public
    //--------------------------------------------------------------------------
 
    private static final ISysLog SYSLOG = Logger.getSysLog(LocalStorage.class);
    
    private static final FolderDao folderDao = new FolderDao();   
    private static final FileDao fileDao = new FileDao();
    private static final StorageDao storageDao = new StorageDao();
 
    //--------------------------------------------------------------------------
    // Instance fields - public
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Instance fields - non-public
    //--------------------------------------------------------------------------
    
    /** Reusable IFolder of workspace. */ 
    private IFolder workspace;
    /** Reusable IFolder of current user home. */
    private IFolder homeForCurrentUser;
    /** Reusable IFolder of home root (/home). */
    private IFolder homeRoot;
    /** Reusable ITrash. */
    private ITrash trash;    
 
    //--------------------------------------------------------------------------
    // Instance fields - bean properties
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Constructors, static initializers, factories
    //--------------------------------------------------------------------------
         
    public LocalStorage() {
    	Path datadir = Paths.get(System.getProperty("java.io.tmpdir")
    			+ "/plexflow");
    	
    	// Does files/ exists?
    	if (!Files.exists(datadir)) {
    		try {
				Files.createDirectories(datadir);
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
    	}
	}
    
    //--------------------------------------------------------------------------
    // Public API - class (static)
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Public API - instance
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Overrides and implementations
    //--------------------------------------------------------------------------

   
    @Override
    public String getScheme() {
    	return SCHEME;
    }

	@Override
	public IFolder getWorkspace() {
		if (this.workspace == null) {			
			this.workspace = folderDao.getWorkspace();
		}
		
		return this.workspace;
	}

	@Override
	public IFolder getHomeForCurrentUser() {
		if (this.homeForCurrentUser == null) {
			this.homeForCurrentUser = folderDao.getHomeFor(
					SystemInstanceFactory.getInstance().getUser());
		}
		
		return this.homeForCurrentUser;
	}	
	
	@Override
	public IFolder getHomeForUser(User user) {
		return folderDao.getHomeFor(user);
	}	
	
	@Override
	public IFolder getHomeRoot() {
		if (this.homeRoot == null) {
			this.homeRoot = folderDao.getHomeRoot();
		}
		
		return this.homeRoot;
	}
	
	@Override
	public ITrash getTrash() {
		if (this.trash == null) {
			this.trash = new LocalTrash();
		}
		
		return this.trash;
	}
	
	@Override
	public boolean existsFileWithId(long fileId) {
		return fileDao.existsFileWithId(fileId);	
	}
	
	@Override
	public boolean existsFolderWithId(long folderId) {
		return folderDao.existsFolderWithId(folderId);		
	}
	
	@Override
	public IFile getFileWithId(long fileId) throws NodeNotFoundException {
		return fileDao.findFileById(fileId);
	}	
	
	@Override
	public IFolder getFolderWithId(long folderId) throws NodeNotFoundException {
		return folderDao.findFolderById(folderId);
	}
	
	@Override
	public IStorageInfo getStorageInfo(IProgressMonitor monitor) {
		throw new UnsupportedOperationException("Not implemented yet");
	}
	
	@Override
	public void format(IProgressMonitor monitor) {
		// Files must be first (have FK to FOLDER.ID)
		int delFiles = storageDao.delFilesTable();
		int delFolders = storageDao.delFolderTable();
		
		SYSLOG.fine("Formatting storage: deleted " + delFolders + " folders, " +
				delFiles + " files.");
	}
    
    //--------------------------------------------------------------------------
    // Getters/setters of properties
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Helper methods (mostly private)
    //--------------------------------------------------------------------------
	
	
}
