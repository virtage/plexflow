package com.virtage.plexflow.storage.impl;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.Platform;

import com.virtage.plexflow.storage.IFile;
import com.virtage.plexflow.storage.IFolder;
import com.virtage.plexflow.storage.ILock;
import com.virtage.plexflow.storage.INode;
import com.virtage.plexflow.storage.ITags;
import com.virtage.plexflow.storage.IVersion;
import com.virtage.plexflow.storage.NodeName;
import com.virtage.plexflow.storage.events.NodeListener;
import com.virtage.plexflow.storage.exceptions.FilePushInProgressException;
import com.virtage.plexflow.storage.exceptions.NodeAlreadyExistsException;
import com.virtage.plexflow.storage.exceptions.NodeDeletedException;
import com.virtage.plexflow.storage.exceptions.NodeNameForbiddenException;
import com.virtage.plexflow.storage.exceptions.NodeNotFoundException;
import com.virtage.plexflow.storage.exceptions.NodeTrashedException;
import com.virtage.plexflow.storage.exceptions.RuntimeStorageException;
import com.virtage.plexflow.storage.impl.mybatis.FileDao;
import com.virtage.plexflow.storage.impl.mybatis.FolderDao;

/*
 * 
 * 
 */
public final class LocalFile implements IFile {
	
	 //--------------------------------------------------------------------------
    // Class fields - public
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Class fields - non-public
    //--------------------------------------------------------------------------
 
    //private static final ISysLog SYSLOG = Logger.getSysLog(LocalFile.class);
	private static FileDao fileDao = new FileDao();
	private static FolderDao folderDao = new FolderDao();
 
    //--------------------------------------------------------------------------
    // Instance fields - public
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Instance fields - non-public
    //--------------------------------------------------------------------------
 
	/** Internal flag. Set by LocalTrash. */ 
    private volatile boolean trashed = false;
    /** Internal flag. Set by LocalTrash. */
    private volatile boolean deleted = false;
    
    private Set<NodeListener> nodeListeners = new HashSet<>();    
    
    //--------------------------------------------------------------------------
    // Instance fields - bean properties
    //--------------------------------------------------------------------------
 
    // Corresponds to table columns
    private long id;
	private String name;
	private long folderId;
	private String description;
	private Date createdAt;
	private Date modifiedAt;
	private Date accessedAt;
	private Date trashedAt;
	private long crc32;
	private long length;
	private boolean pushInProgress; 
    
    
    //--------------------------------------------------------------------------
    // Constructors, static initializers, factories
    //--------------------------------------------------------------------------
 
	private LocalFile() { }	
	
	public static LocalFile createFromId(long id) throws NodeNotFoundException {		
		LocalFile file = fileDao.findFileById(id);		
		
		// Initialize trashed flag		
		if (LocalStorageUtils.isDateTrashed(file.trashedAt)) {
			file.trashed = true;
		}
		
		return file;
	}
	
	//--------------------------------------------------------------------------
    // Public API - class (static)
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Public API - instance
    //-------------------------------------------------------------------------- 
	
    //--------------------------------------------------------------------------
    // Overrides and implementations
    //--------------------------------------------------------------------------
 
	@Override
	public long getId() {
		return id;
	}	
	
	@Override
	public INode copy(IFolder targetFolder) throws NodeTrashedException ,NodeDeletedException ,NodeAlreadyExistsException {
		return copy(targetFolder, null);
	};
	
	@Override
	public INode copy(IFolder targetFolder, IProgressMonitor monitor)
			throws NodeTrashedException, NodeDeletedException, NodeAlreadyExistsException {
		
		ensureNotTrashedOrDeleted();		
		LocalStorageUtils.throwIfNodeNameAlreadyExistsInFolder((LocalFolder) getParent(), getNodeName());
		
		long targetFolderId = ((LocalFolder) targetFolder).getId();		
		long newId = fileDao.copyFile(this.id, targetFolderId);		
		
		try {
			return LocalFile.createFromId(newId);
			
		} catch (NodeNotFoundException e) {
			throw new RuntimeStorageException(e);
		}
	}

	@Override
	public INode move(IFolder targetFolder) throws NodeTrashedException,
			NodeDeletedException, NodeAlreadyExistsException {
		return move(targetFolder, null);
	}
	
	
	@Override
	public INode move(IFolder targetFolder, IProgressMonitor monitor) 
			throws NodeTrashedException, NodeDeletedException, NodeAlreadyExistsException {
		
		ensureNotTrashedOrDeleted();		
		LocalStorageUtils.throwIfNodeNameAlreadyExistsInFolder((LocalFolder) getParent(), getNodeName());
		
		long sourceFileId = id;		// i.e. this file
		long targetFolderId = ((LocalFolder) targetFolder).getId();		
		fileDao.moveFile(sourceFileId, targetFolderId);
		
		try {
			return LocalFolder.createFromId(targetFolderId);
		} catch (NodeNotFoundException e) {
			throw new RuntimeStorageException(e);
		}
	}
	

	@Override
	public INode rename(String newName)
			throws NodeTrashedException, NodeDeletedException, NodeAlreadyExistsException, NodeNameForbiddenException {
		
		return rename(new NodeName(newName));
	}	
	
	@Override
	public INode rename(NodeName newName)
			throws NodeTrashedException, NodeDeletedException, NodeAlreadyExistsException {
		
		ensureNotTrashedOrDeleted();		
		LocalStorageUtils.throwIfNodeNameAlreadyExistsInFolder((LocalFolder) getParent(), newName);
		
		fileDao.renameFile(folderId, newName.toString());		
		
		try {
			return LocalFile.createFromId(this.id);
			
		} catch (NodeNotFoundException e) {
			throw new RuntimeStorageException(e);
		}
	}
	

	@Override
	public NodeName getNodeName() {
		try {
			return new NodeName(this.name);
			
		} catch (NodeNameForbiddenException e) {
			throw new RuntimeStorageException("Data inconsistency detected: name from " +
					"returned from database is invalid node name. ", e); 
		}
	}

	@Override
	public IFolder getParent() {
		try {
			return (IFolder) LocalFolder.createFromId(this.folderId);
		} catch (NodeNotFoundException e) {
			throw new RuntimeStorageException(e);
		}
	}

	@Override
	public int compareTo(INode o) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public long getLength() {
		return this.length;
	}
	
	@Override
	public Date getCreatedAt() {
		// protect against abuse modify outside class
		return new Date(createdAt.getTime());
	}

	@Override
	public Date getModifiedAt() {		
		// protect against abuse modify outside class
		return new Date(modifiedAt.getTime());
	}

	@Override
	public Date getAccessedAt() {
		// protect against abuse modify outside class
		return new Date(accessedAt.getTime());
	}
	
	
	@Override
	public void setPushInProgress(boolean pushInProgress) {		
		fileDao.setPushInProgressForFile(this.id, pushInProgress);
		
		synchronized (this) {
			this.pushInProgress = pushInProgress;
		}
	}
	
	@Override
	public boolean isPushInProgress() {
		return this.pushInProgress;
	}	

	@Override
	public void addNodeListener(NodeListener listener) throws NodeTrashedException, NodeDeletedException {
		ensureNotTrashedOrDeleted();
		
		nodeListeners.add(listener);		
	}	
	
	@Override
	public void removeNodeListener(NodeListener listener) throws NodeTrashedException, NodeDeletedException {
		ensureNotTrashedOrDeleted();
		
		nodeListeners.remove(listener);
	}	
	
	
	@Override
	public IPath getPath() {
		// Path to folder where file resides
		IPath folderPath = this.getParent().getPath();
		// + this filename
		IPath filePath = folderPath.append("/" + this.name); 
		
		return filePath;
	}
	
	@Override
	public String getDescription() {
		return this.description;
	}
	
	@Override
	public ITags getTags() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setDescription(String desc) {		
		fileDao.setFileDescription(this.id, desc);
			
		synchronized(this) {
			this.description = desc;
		}		
	}	
	
	@Override
	public long getCrc32() {
		return this.crc32;
	}
	
	@Override
	public void setCrc32(long crc32) {
		fileDao.setFileCrc32(this.id, crc32);
			
		synchronized(this) {
			this.crc32 = crc32;
		}
	}
	
	@Override
	public void setLength(long length) {
		fileDao.setFileLength(this.id, length);
		
		synchronized(this) {
			this.length = length;
		}		
	}
	
	//--------------------------------------------------------------------------
    // Locking API
    //--------------------------------------------------------------------------

	@Override
	public ILock lock() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ILock lock(long timeOutMillis) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void unLock() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean canUnlock() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isLocked() {
		// TODO Auto-generated method stub
		return false;
	}
	
	@Override
	public ILock getLock() {
		// TODO Auto-generated method stub
		return null;
	}
	
	//--------------------------------------------------------------------------
    // Versioning API
    //--------------------------------------------------------------------------

	@Override
	public void setVersioned(boolean versioned) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean isVersioned() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void setAutoVersioned(boolean autoVersioned) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean isAutoVersioned() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void checkOut() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void unCheckOut() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void checkIn() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void restoreToVersion(IVersion version) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean isCheckedOut() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public List<IVersion> getVersions() {
		// TODO Auto-generated method stub
		return null;
	}
	
	
	//--------------------------------------------------------------------------
    // toString(), equals(), hashCode()
    //--------------------------------------------------------------------------
	
	@Override
	public String toString() {
		String toString = null;		
	
		toString = getPath().toString() + " (id = " + id + ")";
		
		if (trashed)
			toString += " (trashed)";
		if (deleted)
			toString += " (deleted)";
		
		return toString;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LocalFile other = (LocalFile) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
	/** Delegates call to platform's adapter manager. */
	@Override
	public Object getAdapter(@SuppressWarnings("rawtypes") Class adapter) {
		return Platform.getAdapterManager().getAdapter(IFile.class, adapter);
	}
	
    
    //--------------------------------------------------------------------------
    // Getters/setters of properties
    //--------------------------------------------------------------------------
 	
	synchronized void setTrashed(boolean trashed) {
		this.trashed = trashed;
	}
	
	synchronized void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}	
	
	//--------------------------------------------------------------------------
    // Helper methods (mostly private)
    //--------------------------------------------------------------------------

	
	

	/** Test and throw exception if folder is trashed or deleted. */
    private void ensureNotTrashedOrDeleted() throws NodeTrashedException, NodeDeletedException {
    	if (this.trashed)
    		throw new NodeTrashedException(this);
    	if (this.deleted)
    		throw new NodeDeletedException(this);
    }
    
    
    
}
