package com.virtage.plexflow.storage.impl.mybatis;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.LongTypeHandler;

import com.virtage.plexflow.storage.exceptions.NodeNotFoundException;
import com.virtage.plexflow.storage.impl.LocalFolder;
import com.virtage.plexflow.storage.impl.LocalStorageConsts;
import com.virtage.plexflow.system.User;

public interface FolderMapper {
	
	public static final String SOURCE_FOLDER_ID = "sourceFolderId";
	public static final String TARGET_FOLDER_ID = "targetFolderId";	
	public static final String NAME = "name";
	public static final String PARENT_FOLDER_ID = "parentId";
	public static final String NEW_ID = "newId";
	
	
	@Select("SELECT * FROM FOLDER WHERE UCASE(NAME) = UCASE(#{username}) " +
			"AND PARENT_ID = " + LocalStorageConsts.HOME_ROOT_ID)
	public LocalFolder getHomeFor(User user);
	
	@Select("SELECT * FROM FOLDER WHERE ID = " + LocalStorageConsts.HOME_ROOT_ID)
	public LocalFolder getHomeRoot();
	
	@Select("SELECT * FROM FOLDER WHERE ID = " + LocalStorageConsts.WORKSPACE_ID)
	public LocalFolder getWorkspace();

	
	/**
	 * Inserts new folder into database. 
	 * 
	 * @param (in) {@link FolderMapper#NAME} -- name of new folder
	 * @param (in) {@link FolderMapper#PARENT_FOLDER_ID} -- parent id for new folder
	 * @param (out) {@link FolderMapper#NEW_ID} -- id of new folder will be stored here
	 */
	@Insert("INSERT INTO FOLDER (NAME, PARENT_ID) VALUES (#{" +
			NAME + "}, #{" + PARENT_FOLDER_ID + "})")
	@Options(useGeneratedKeys=true, keyProperty=NEW_ID)	
	// Force bigint --> long otherwise it's returned as BigDecimal
	@Results(value = {
			@Result(property="id", column="ID", javaType=Long.class, jdbcType=JdbcType.BIGINT, typeHandler=LongTypeHandler.class)
	})
	public void insertFolder(Map<String, Object> paramMap);

	
	@Select("SELECT * FROM FOLDER")
	public List<LocalFolder> findAllFolders();
	
	// "AND ID <> #{id}" excludes workspace (root) 
	@Select("SELECT * FROM FOLDER WHERE PARENT_ID = #{id} AND ID <> #{id}")
	public List<LocalFolder> findAllSubfoldersInFolder(long folderId);
		
	
	@Select("SELECT * FROM FOLDER WHERE ID = #{id}")
	public LocalFolder findFolderById(long id) throws NodeNotFoundException;		
	
	/** Search and return not trashed subfolder in specified folder by name */ 
	@Select("SELECT * FROM FOLDER WHERE PARENT_ID = #{param1} AND UCASE(NAME) = UCASE(#{param2}) " +
			"AND TRASHED_AT = " + LocalStorageConsts.UNIX_EPOCH_SQLSTR)
	public LocalFolder findSubfolderByName(long folderIdToSearchIn, String name);
	
	
	/** Search for not trashed subfolder in specified folder by name */
	@Select("SELECT CASE WHEN COUNT(*) = 1 THEN TRUE ELSE FALSE END " +
			"FROM FOLDER WHERE PARENT_ID = #{param1} AND UCASE(NAME) = UCASE(#{param2}) " +
			"AND TRASHED_AT = " + LocalStorageConsts.UNIX_EPOCH_SQLSTR)
	public boolean existsSubfolderInFolder(long folderIdToSearchIn, String name);	
	
	
	@Update("UPDATE FOLDER SET NAME = #{newName} WHERE ID = #{id}")
	public void renameFolder(@Param("id") long id, @Param("newName") String newName);
	
	
	@Update("UPDATE FOLDER SET PARENT_ID = #{target} WHERE ID = #{source}")
	public void moveFolder(
			@Param("source") long sourceFolderId,
			@Param("target") long targetFolderId);
		
	
		
	/**
	 * @param (in) SOURCE_FOLDER_ID
	 * @param (in) TARGET_FOLDER_ID
	 * @param (out) NEW_ID
	 */
	@Insert("INSERT INTO FOLDER ( " +
				"NAME,              PARENT_ID, DESCRIPTION, CREATED_AT, TRASHED_AT " +
			") SELECT " +
				"NAME, #{" + TARGET_FOLDER_ID + "} AS PARENT_ID, DESCRIPTION, CREATED_AT, TRASHED_AT " +
			"FROM " +
				"FOLDER WHERE ID = #{" + SOURCE_FOLDER_ID + "}")
	@Options(useGeneratedKeys=true, keyProperty=NEW_ID)
	@Results(value = {
			@Result(property="id", column="ID", javaType=Long.class, jdbcType=JdbcType.BIGINT)
	})
	public void copyFolder(Map<String, Long> params);
	
	
	@Select("SELECT CASE WHEN COUNT(*) = 1 THEN TRUE ELSE FALSE END " +
			"FROM FOLDER WHERE PARENT_ID = #{this} AND ID = #{parent}")
	public boolean isFolderParentOfFolder(
			@Param("this") long isThisFolderId,
			@Param("parent") long parentOfFolderId);
	
	
	@Select("SELECT CASE WHEN COUNT(*) = 1 THEN TRUE ELSE FALSE END " +
			"FROM FILE WHERE FOLDER_ID = #{parent} AND ID = #{this}")
	public boolean isFolderParentOfFile(
			@Param("this") long isThisFileId,
			@Param("parent") long parentOfFolderId);

	
	@Update("UPDATE FOLDER SET DESCRIPTION = #{desc} WHERE ID = #{id}")
	public void setFolderDescription(@Param("id") long folderId, @Param("desc") String desc);
	
	
	/** No matter if trashed or not. */
	@Select("SELECT CASE WHEN COUNT(*) = 1 THEN TRUE ELSE FALSE END " +
			"FROM FOLDER WHERE ID = #{param1}")
	public boolean existsFolderWithId(long id);
	
	@Select("SELECT CASE WHEN COUNT(*) >= 1 THEN TRUE ELSE FALSE END " +
			"FROM FOLDER WHERE PARENT_ID = #{folderId}")
	public boolean hasChildFolders(@Param("folderId") long folderId);
	
	@Select("SELECT CASE WHEN COUNT(*) >= 1 THEN TRUE ELSE FALSE END " +
			"FROM FILE WHERE FOLDER_ID = #{folderId}")
	public boolean hasChildFiles(@Param("folderId") long folderId);
}