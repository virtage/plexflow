package com.virtage.plexflow.storage.impl;

import java.util.Date;
import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;

import com.virtage.plexflow.storage.IFile;
import com.virtage.plexflow.storage.IFolder;
import com.virtage.plexflow.storage.INode;
import com.virtage.plexflow.storage.ITrash;
import com.virtage.plexflow.storage.NodeName;
import com.virtage.plexflow.storage.exceptions.DeleteForbiddenException;
import com.virtage.plexflow.storage.exceptions.NodeNameForbiddenException;
import com.virtage.plexflow.storage.exceptions.RuntimeStorageException;
import com.virtage.plexflow.storage.exceptions.TrashForbiddenException;
import com.virtage.plexflow.storage.impl.mybatis.FileDao;
import com.virtage.plexflow.storage.impl.mybatis.FolderDao;
import com.virtage.plexflow.storage.impl.mybatis.TrashDao;

public final class LocalTrash implements ITrash {
	
	//--------------------------------------------------------------------------
    // Class fields - public
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Class fields - non-public
    //--------------------------------------------------------------------------
 
    //private static final ISysLog SYSLOG = Logger.getSysLog(LocalTrash.class);
	
	private static final TrashDao trashDao = new TrashDao();
	private static final FolderDao folderDao = new FolderDao();
	private static final FileDao fileDao = new FileDao();
 
    //--------------------------------------------------------------------------
    // Instance fields - public
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Instance fields - non-public
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Instance fields - bean properties
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Constructors, static initializers, factories
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Public API - class (static)
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Public API - instance
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Overrides and implementations
    //--------------------------------------------------------------------------
 
    @Override
    public List<INode> getTrashedNodes(IFolder folder) {   	
    	return getTrashedNodes(folder, null);
    }
    
    @Override
	public List<INode> getTrashedNodes(IFolder folder, IProgressMonitor monitor) {
    	// Easy as
    	return LocalFolder.traverseRecursively((LocalFolder) folder, monitor);
    	// because we assume that everything under trashed folder must be trashed also (trash is
    	// recursive operation)
	}

    @Override
    public void trash(INode node) throws TrashForbiddenException {
    	trash(node, null);    	
    }
    
	@Override
	public void trash(INode node, IProgressMonitor monitor) throws TrashForbiddenException {
		if (node instanceof IFile) {			// node is a file
			trashFile((LocalFile) node);
			
		} else {								// or a folder...
			
			LocalFolder rootFolder = (LocalFolder) node;
			List<INode> subNodes = LocalFolder.traverseRecursively(rootFolder, monitor);

			// If some subnodes, trash it
			for (INode subNode : subNodes) {
				if (subNode instanceof IFolder) {
					trashFolder((LocalFolder) subNode);
				} else {
					trashFile((LocalFile) subNode);
				}
			}
			
			// Trash folder itself
			trashFolder(rootFolder);
		}
	}

	@Override
	public NodeName restore(INode node) {
		return restore(node, null);
	}
	
	@Override
	public NodeName restore(INode node, IProgressMonitor monitor) {
		if (node instanceof LocalFolder) {				
			return restoreFolder((LocalFolder) node);			
		} else {
			return restoreFile((LocalFile) node);
		}
	}
	

	@Override
	public void delete(INode node) throws DeleteForbiddenException {
		delete(node, null);
	}
	
	
	@Override
	public void delete(INode node, IProgressMonitor monitor) throws DeleteForbiddenException {
		if (node instanceof IFile) {			// node is a file
			deleteFile((LocalFile) node);
			
		} else {								// or a folder...
			
			LocalFolder rootFolder = (LocalFolder) node;
			List<INode> subNodes = LocalFolder.traverseRecursively(rootFolder, monitor);

			// If some subnodes, trash it
			for (INode subNode : subNodes) {
				if (subNode instanceof IFolder) {
					deleteFolder((LocalFolder) subNode);
				} else {
					deleteFile((LocalFile) subNode);
				}
			}
			
			// Trash folder itself
			deleteFolder(rootFolder);
		}
	}

	
	@Override
	public boolean contains(INode node) {
		return contains(node, null);
	}
	
	@Override
	public boolean contains(INode node, IProgressMonitor monitor) {				
		if (node instanceof LocalFolder) {
			return trashDao.containsFolder(((LocalFolder) node).getId());
		} else {
			return trashDao.containsFile(((LocalFile) node).getId());
		}
	}
	
	
	@Override
	public Date getTrashedAt(INode node) {
		if (node instanceof LocalFolder) {
			return trashDao.getFolderTrashedDate(((LocalFolder) node).getId());
		} else {
			return trashDao.getFileTrashedDate(((LocalFile) node).getId());
		}
	}
	
	
	@Override
	public List<INode> getDeletedNodesFor(IFolder folder) {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException("Not implemented yet.");
	}
	
	@Override
	public List<INode> getTrashedNodesFor(IFolder folder) {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException("Not implemented yet.");
	}
	
    
    //--------------------------------------------------------------------------
    // Getters/setters of properties
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Helper methods (mostly private)
    //--------------------------------------------------------------------------

	/** Restores and returns name under which has been restored. */
	private NodeName restoreFile(LocalFile file) {
		// Begin with same name, maybe changed later if conflicting
		NodeName afterRestoreName = new NodeName(file.getNodeName());		
			 
		// Quit if not trashed
		if (!trashDao.isFileTrashed(file.getId()))		
			return afterRestoreName;
		
		// Check for conflict with existing folder
		long folderId = ((LocalFolder) file.getParent()).getId();
		String name = file.getNodeName().toString();			
		boolean exist = fileDao.existsFileInFolder(folderId, name);
		
		if (exist) {
			// Choose new name and rename it
			afterRestoreName = suggestAlternativeNameForFile(file.getNodeName(), folderId);
			fileDao.renameFile(file.getId(), afterRestoreName.toString());
		}
		
		// Restoration itself
		trashDao.restoreFile(file.getId());
		
		file.setTrashed(false);
		
		return afterRestoreName;
	}
	
	/** Restores and returns name under which has been restored. */
	private NodeName restoreFolder(LocalFolder folder) {
		// Begin with same name, maybe changed later if conflicting
		NodeName afterRestoreName = new NodeName(folder.getNodeName());
		
		// Quit if not trashed
		if (!trashDao.isFolderTrashed(folder.getId()))		
			return afterRestoreName;
		
		// Check for conflict with existing folder
		long parentId = ((LocalFolder) folder.getParent()).getId();
		String name = folder.getNodeName().toString();			
		boolean exist = folderDao.existsSubfolderInFolder(parentId, name);
		
		if (exist) {
			// Choose new name and rename it
			afterRestoreName = suggestAlternativeNameForFolder(folder.getNodeName(), parentId);
			folderDao.renameFolder(folder.getId(), afterRestoreName.toString());
		}
		
		// Restoration itself
		trashDao.restoreFolder(folder.getId());
		
		folder.setTrashed(false);
		
		return afterRestoreName;
	}
	
	private NodeName suggestAlternativeNameForFile(NodeName currentName, long folderIdWhere) {
		NodeName trialName = null;
		
		// 2 147 483 647 trials must be enough :-)
		for (int i = 1; i < Integer.MAX_VALUE; i++) { 
			try {
				trialName = new NodeName(currentName.toString() + " (" + i + ")");
				
			} catch (NodeNameForbiddenException e) {
				throw new RuntimeStorageException("Can't build trialName NodeName.", e);
				
			}
			
			// Does it exist here yet?				
			boolean existsFile = fileDao.existsFileInFolder(
					folderIdWhere, trialName.toString());
			
			if (!existsFile)			// hurrah, doesn't exist yet!
				 break;			// exit for loop				
		}
		
		if (trialName == null)
			throw new RuntimeStorageException("Failed to find out non conflicting node name!");
		
		return trialName;
	}
	
	
	/**
	 * Finds out not conflicting name for node in particular folder.
	 * 
	 * Example:
	 * for "foodir" gets "foodir (1)", for "foodir (1)" gets "foodir (2)" etc.
	 *   
	 * @param currentName
	 * @param folderIdWhere Folder ID where to find out not conflicting name
	 */ 
	private NodeName suggestAlternativeNameForFolder(NodeName currentName, long folderIdWhere) {
		NodeName trialName = null;
		
		// 2 147 483 647 trials must be enough for everybody :-)
		for (int i = 1; i < Integer.MAX_VALUE; i++) { 
			try {
				trialName = new NodeName(currentName.toString() + " (" + i + ")");
				
			} catch (NodeNameForbiddenException e) {
				throw new RuntimeStorageException("Can't build trialName NodeName.", e);
				
			}
			
			// Does it exist here yet?			
			boolean existsFolder = folderDao.
				existsSubfolderInFolder(folderIdWhere, trialName.toString());
			
			if (!existsFolder)			// hurrah, doesn't exist yet!
				 break;					// exit from loop			
		}
		
		if (trialName == null)
			throw new RuntimeStorageException("Failed to find out non conflicting node name!");
		
		return trialName;
	}
	
	
	private void trashFolder(LocalFolder folder) throws TrashForbiddenException {
		// "Sanity check"
		if (folder.isHomeRoot())
			throw new TrashForbiddenException(NodeName.HOME_ROOT.toString());
		if (folder.isHome())
			throw new TrashForbiddenException(folder.toString());
		if (folder.isWorkspace())
			throw new TrashForbiddenException(NodeName.WORKSPACE.toString());		
		
		trashDao.trashFolder(folder);
		
		folder.setTrashed(true);
	}
	
	
	private void trashFile(LocalFile file) {
		trashDao.trashFile(file);
		
		file.setTrashed(true);
	}
	
	
	private void deleteFolder(LocalFolder folder) throws DeleteForbiddenException {
		// "Sanity check"
		if (((IFolder) folder).isHomeRoot())
			throw new DeleteForbiddenException(NodeName.HOME_ROOT.toString());
		if (((IFolder) folder).isHome())
			throw new DeleteForbiddenException(folder.getNodeName().toString());
		if (((IFolder) folder).isWorkspace())
			throw new DeleteForbiddenException(NodeName.WORKSPACE.toString());
				
		trashDao.deleteFolder(folder);			
		
		folder.setDeleted(true);
	}
	
	
	private void deleteFile(LocalFile file) {		
		trashDao.deleteFile(file);			
		
		file.setDeleted(true);
	}
}