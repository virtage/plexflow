package com.virtage.plexflow.storage.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.ibatis.session.SqlSession;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;

import com.virtage.plexflow.storage.IFile;
import com.virtage.plexflow.storage.IFolder;
import com.virtage.plexflow.storage.ILock;
import com.virtage.plexflow.storage.INode;
import com.virtage.plexflow.storage.ITags;
import com.virtage.plexflow.storage.NodeName;
import com.virtage.plexflow.storage.events.NodeListener;
import com.virtage.plexflow.storage.exceptions.NodeAlreadyExistsException;
import com.virtage.plexflow.storage.exceptions.NodeDeletedException;
import com.virtage.plexflow.storage.exceptions.NodeNameForbiddenException;
import com.virtage.plexflow.storage.exceptions.NodeNotFoundException;
import com.virtage.plexflow.storage.exceptions.NodeTrashedException;
import com.virtage.plexflow.storage.exceptions.RuntimeStorageException;
import com.virtage.plexflow.storage.impl.mybatis.FileDao;
import com.virtage.plexflow.storage.impl.mybatis.FolderDao;
import com.virtage.plexflow.storage.impl.mybatis.FolderMapper;
import com.virtage.plexflow.system.SystemInstanceFactory;
import com.virtage.plexflow.system.User;

/*
 *
 */
public final class LocalFolder implements IFolder {
	
	//--------------------------------------------------------------------------
    // Class fields - public
    //--------------------------------------------------------------------------	
	
    //--------------------------------------------------------------------------
    // Class fields - non-public
    //--------------------------------------------------------------------------
  
	private static final FolderDao folderDao = new FolderDao();
	private static final FileDao fileDao = new FileDao();
	
    //--------------------------------------------------------------------------
    // Instance fields - public
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Instance fields - non-public
    //--------------------------------------------------------------------------
			
	/** Internal flag. Set by LocalTrash. */
	private volatile boolean trashed = false;
	/** Internal flag. Set by LocalTrash. */
	private volatile boolean deleted = false;
	
	private Set<NodeListener> nodeListeners = new HashSet<>();
	
    //--------------------------------------------------------------------------
    // Instance fields - bean properties
    //--------------------------------------------------------------------------
 
	private long id;
	private String name;
	private long parentId;
	private String description;
	private Date createdAt;
	private Date trashedAt;
	
    //--------------------------------------------------------------------------
    // Constructors, static initializers, factories
    //--------------------------------------------------------------------------
	
	private LocalFolder() {}

	
	public static LocalFolder createFromId(long id) throws NodeNotFoundException {
		LocalFolder folder = folderDao.findFolderById(id);
		
		// Initialize trashed flag		
		if (LocalStorageUtils.isDateTrashed(folder.trashedAt)) {
			folder.trashed = true;
		}
		
		return folder;
	}
	
	//--------------------------------------------------------------------------
    // Public API - class (static)
    //--------------------------------------------------------------------------
	
	/**
	 * Lists everything under specified folder //including trashed nodes//. Progress monitors are
	 * unsupported yet!
	 * 
	 * @return Never return null but isEmpty() collection.
	 **/
	static List<INode> traverseRecursively(LocalFolder folder, IProgressMonitor monitor) {	
		List<INode> oneLevelchildren = oneLevelSubnodesOfFolder(folder);
		List<INode> list = new ArrayList<>();
		
		for (INode child : oneLevelchildren) {			
			if (child instanceof LocalFolder) {
				// Add folder
				list.add(child);
				// And everything under it
				list.addAll(traverseRecursively((LocalFolder) child, monitor));				
			} else {
				// Add file
				list.add(child);
			}
		}
		
		return list;
	}
	
    //--------------------------------------------------------------------------
    // Public API - instance
    //--------------------------------------------------------------------------
		
		
	//--------------------------------------------------------------------------
    // Overrides and implementations
    //--------------------------------------------------------------------------

	@Override
	public long getId() {
		return id;
	}
	
	@Override
	public IFolder createFolder(String name)
			throws NodeTrashedException, NodeDeletedException, NodeAlreadyExistsException, NodeNameForbiddenException {
		return createFolder(new NodeName(name));
	};
	
	@Override
	public IFolder createFolder(NodeName name)
			throws NodeTrashedException, NodeDeletedException, NodeAlreadyExistsException {
		
		throwIfNodeTrashedOrDeleted();		
		LocalStorageUtils.throwIfNodeNameAlreadyExistsInFolder(this, name);
		
		long newId = folderDao.insertFolder(name.toString(), this.id);		
		
		LocalFolder lf;
		try {
			lf = LocalFolder.createFromId(newId);
		} catch (NodeNotFoundException e) {
			throw new RuntimeStorageException(e);
		}
		return lf;
	}

	@Override
	public IFile createFile(String name)
			throws NodeTrashedException, NodeDeletedException, NodeAlreadyExistsException, NodeNameForbiddenException {
		return createFile(new NodeName(name));
	}
	
	@Override
	public IFile createFile(NodeName name)
			throws NodeTrashedException, NodeDeletedException, NodeAlreadyExistsException {
		throwIfNodeTrashedOrDeleted();
		LocalStorageUtils.throwIfNodeNameAlreadyExistsInFolder(this, name);		
		
		// current folder ID will be parent
		long newId = fileDao.insertFile(name.toString(), this.id);		
		
		try {
			return LocalFile.createFromId(newId);
		} catch (NodeNotFoundException e) {
			throw new RuntimeStorageException(e);
		}
	}
	
	@Override
	public INode copy(IFolder targetFolder) throws NodeTrashedException,
			NodeDeletedException, NodeAlreadyExistsException {
		return copy(targetFolder, null);
	}
	

	@Override
	public INode copy(IFolder targetFolder, IProgressMonitor monitor)
			throws NodeTrashedException, NodeDeletedException, NodeAlreadyExistsException {
		
		throwIfNodeTrashedOrDeleted();		
		LocalStorageUtils.throwIfNodeNameAlreadyExistsInFolder((LocalFolder) targetFolder, getNodeName());
		
		long targetFolderId = ((LocalFolder) targetFolder).id;		
		long newId = folderDao.copyFolder(this.id, targetFolderId);
		
		try {
			return LocalFolder.createFromId(newId);
		} catch (NodeNotFoundException e) {
			throw new RuntimeStorageException(e);
		}
	}

	
	@Override
	public INode move(IFolder targetFolder) throws NodeTrashedException,
			NodeDeletedException, NodeAlreadyExistsException {
		return move(targetFolder, null);
	}
	
	
	@Override
	public INode move(IFolder targetFolder, IProgressMonitor monitor)
			throws NodeTrashedException, NodeDeletedException, NodeAlreadyExistsException {		
		throwIfNodeTrashedOrDeleted();		
		LocalStorageUtils.throwIfNodeNameAlreadyExistsInFolder((LocalFolder) targetFolder, getNodeName());
		
		long targetFolderId = ((LocalFolder) targetFolder).id;		
		folderDao.moveFolder(this.id, targetFolderId);
		
		try {
			return LocalFolder.createFromId(targetFolderId);
		} catch (NodeNotFoundException e) {
			throw new RuntimeStorageException(e);
		}
	}
	
	@Override
	public INode rename(String newName)
			throws NodeTrashedException, NodeDeletedException, NodeAlreadyExistsException, NodeNameForbiddenException {
		return rename(new NodeName(newName));
	}

	@Override
	public INode rename(NodeName newName)
			throws NodeTrashedException, NodeDeletedException, NodeAlreadyExistsException {
		
		throwIfNodeTrashedOrDeleted();		
		LocalStorageUtils.throwIfNodeNameAlreadyExistsInFolder((LocalFolder) getParent(), newName);
		
		folderDao.renameFolder(this.id, newName.toString());
		
		try {
			return LocalFolder.createFromId(this.id);
		} catch (NodeNotFoundException e) {
			throw new RuntimeStorageException(e);
		}
	}


	@Override
	public NodeName getNodeName() {
		// This folder is workspace
		if (isWorkspace())
			return NodeName.WORKSPACE;
		
		// This folder is home root
		if (isHomeRoot())
			return NodeName.HOME_ROOT;
		
		try {		
			// This folder is root
			if (isHome()) {
				// Find for whom (folder name = username)
				User user = User.findUserByUsername(this.name);
				
				if (user == null) {
					return new NodeName(this.name + "'s home (unknown user)");
				} else {
					return new NodeName(user.getDisplayName() + "' home");
				}
			}	
		
			// This folder is an ordinary folder
			return new NodeName(name);
			
		} catch (NodeNameForbiddenException e) {
			throw new RuntimeStorageException("Data inconsistency detected: " +
					"name '" + name + "' from " +
					"returned from database is invalid node name. ", e); 
		}
	}

	@Override
	public boolean isParentOf(INode node) throws NodeTrashedException, NodeDeletedException {
		throwIfNodeTrashedOrDeleted();
		
		boolean result = false;
		
		SqlSession session = SystemInstanceFactory.getInstance().getSqlSessionFactory().openSession();
		try {
			FolderMapper mapper = session.getMapper(FolderMapper.class);
			
			if (node instanceof IFolder) {
				LocalFolder folder = (LocalFolder) node;				
				result = mapper.isFolderParentOfFolder(this.id, folder.id);				
			} else {
				LocalFile file = (LocalFile) node; 
				result = mapper.isFolderParentOfFile(file.getId(), this.id);				
			}
			
		} finally {
			session.close();
		}
		
		return result;
	}
	
	@Override
	public boolean hasChildNodes() throws NodeTrashedException, NodeDeletedException {
		if (folderDao.hasChildFolders(id) || folderDao.hasChildFiles(id)) {
			return true;
		} else {
			return false;
		}
	}

	
	@Override
	public IFolder getParent() {
		try {
			return (IFolder) LocalFolder.createFromId(parentId);
		} catch (NodeNotFoundException e) {
			throw new RuntimeStorageException(e);
		}
	}

	
	@Override
	public int compareTo(INode o) {
		
		// TODO Auto-generated method stub
		return 0;
	}

	
	@Override
	public List<INode> childNodes() throws NodeTrashedException,
			NodeDeletedException {
		return childNodes(null);
	}
		
	@Override
	public List<INode> childNodes(IProgressMonitor monitor)
			throws NodeTrashedException, NodeDeletedException {
		throwIfNodeTrashedOrDeleted();
		
		return oneLevelSubnodesOfFolder(this);
	}	
	
	@Override
	public List<INode> childNodesRecursively() throws NodeTrashedException,
			NodeDeletedException {
		return childNodesRecursively(null);
	}
	
	
	@Override
	public List<INode> childNodesRecursively(IProgressMonitor monitor)
			throws NodeTrashedException, NodeDeletedException {
		throwIfNodeTrashedOrDeleted();
		
		return traverseRecursively(this, monitor);
	}
	
	@Override
	public List<String> childNames() throws NodeTrashedException,
			NodeDeletedException {
		return childNames(null);
	}
	
	@Override
	public List<String> childNames(IProgressMonitor monitor)
			throws NodeTrashedException, NodeDeletedException {
		
		List<INode> childNodes = childNodes(monitor);
		List<String> childNames = new ArrayList<>();
		
		for (INode node : childNodes) {
			childNames.add(node.getNodeName().toString());
		}
		
		return childNames;
	}
	
	@Override
	public List<String> childNamesRecursively() throws NodeTrashedException,
			NodeDeletedException {
		return childNamesRecursively(null);
	}
	
	@Override
	public List<String> childNamesRecursively(IProgressMonitor monitor) 
			throws NodeTrashedException, NodeDeletedException {
		
		List<INode> childNodes = childNodesRecursively(monitor);
		List<String> childNames = new ArrayList<>();
		
		for (INode node : childNodes) {
			childNames.add(node.getNodeName().toString());
		}
		
		return childNames;
	}	
	
	@Override
	public boolean existsFile(String name) throws NodeNameForbiddenException, NodeTrashedException, NodeDeletedException {
		return existsFile(new NodeName(name));
	}
	
	@Override
	public boolean existsFile(NodeName name) throws NodeTrashedException, NodeDeletedException {
		throwIfNodeTrashedOrDeleted();
		
		boolean result = fileDao.existsFileInFolder(this.id, name.toString());
		
		return result;
	}

	
	@Override
	public boolean existsFolder(String name) throws NodeTrashedException, NodeDeletedException, NodeNameForbiddenException {
		return existsFolder(new NodeName(name));
	}
	
	@Override
	public boolean existsFolder(NodeName name) throws NodeTrashedException, NodeDeletedException {
		throwIfNodeTrashedOrDeleted();
		
		boolean result = folderDao.existsSubfolderInFolder(this.id, name.toString());
		
		return result;
	}

	
	@Override
	public IFile getFile(String name) throws NodeTrashedException, NodeDeletedException, NodeNotFoundException, NodeNameForbiddenException {
		return getFile(new NodeName(name));
	}
	
	@Override
	public IFile getFile(NodeName name) throws NodeTrashedException, NodeDeletedException, NodeNotFoundException {
		throwIfNodeTrashedOrDeleted();
		
		LocalFile file = fileDao.findFileByName(this.id, name.toString());
		
		if (file == null) {
			throw new NodeNotFoundException(name.toString(), this.id);
		}
		
		return file;
	}

	
	@Override
	public IFolder getFolder(String name) throws NodeTrashedException, NodeDeletedException, NodeNotFoundException, NodeNameForbiddenException {
		return getFolder(new NodeName(name));
	}
	
	@Override
	public IFolder getFolder(NodeName name) throws NodeTrashedException, NodeDeletedException, NodeNotFoundException {
		throwIfNodeTrashedOrDeleted();
		
		IFolder folder = folderDao.findSubfolderByName(this.id, name.toString());
				
		if (folder == null) {
			throw new NodeNotFoundException(name.toString(), this.id);
		}
		
		return folder;
	}

	
	@Override
	public boolean isHome() {		
		return (this.parentId == LocalStorageConsts.HOME_ROOT_ID);
	}
	
	
	@Override
	public boolean isHomeForCurrentUser() {
		String username = SystemInstanceFactory.getInstance().getUser().getUsername();
		return username.equals(this.name) ? true : false;
	}	
	
	@Override
	public boolean isHomeForUser(User user) {
		return (user.getUsername().equals(this.name)) ? true : false;
	}
	
	
	@Override
	public boolean isHomeRoot() {
		return (this.id == LocalStorageConsts.HOME_ROOT_ID);
	}
	

	@Override
	public boolean isWorkspace() {		
		return (this.id == LocalStorageConsts.WORKSPACE_ID);
	}

	
	@Override
	public void addNodeListener(NodeListener listener) throws NodeTrashedException, NodeDeletedException {
		throwIfNodeTrashedOrDeleted();
		
		nodeListeners.add(listener);
	}
	
	
	@Override
	public void removeNodeListener(NodeListener listener) throws NodeTrashedException, NodeDeletedException {
		throwIfNodeTrashedOrDeleted();
		
		nodeListeners.remove(listener);
	}

	
	@Override
	public Date getCreatedAt() {		
		// protect against abuse modify outside class
		return new Date(this.createdAt.getTime());
	}
	
	
	@Override
	public IPath getPath() {
		List<String> pathItems = new ArrayList<>();		
		
		LocalFolder current = this;
		LocalFolder parent = (LocalFolder) getParent();
		
		while (parent.id != current.id) {
			pathItems.add(current.name);
			
			current = parent;
			parent = (LocalFolder) current.getParent();
		}
		
		// Path items are from deepest to highest (e.g. somedir, foodir, pxsa, home, /), so reverse.
		Collections.reverse(pathItems);
		
		// Step by step build full abs. path
		
		StringBuffer sb = new StringBuffer("/");		
		for (String item : pathItems) {
			sb.append(item);
			sb.append("/");
		}
		
		IPath path = new Path(sb.toString());
		
		return path;
	}
	
	
	@Override
	public String getDescription() {
		return this.description;
	}
	
	@Override
	public void setDescription(String desc) {		
		folderDao.setFolderDescription(this.id, desc);
		
		synchronized (this) {
			this.description = desc;
		}	
	}

	
	@Override
	public ITags getTags() {
		return null;
	}
	

	//--------------------------------------------------------------------------
    // Locking API
    //--------------------------------------------------------------------------
	
	@Override
	public ILock lock() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ILock lock(long timeOutMillis) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void unLock() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean canUnlock() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isLocked() {
		// TODO Auto-generated method stub
		return false;
	}
	
	@Override
	public ILock getLock() {
		// TODO Auto-generated method stub
		return null;
	}	
	
	//--------------------------------------------------------------------------
    // toString, hashCode(), equals()
    //--------------------------------------------------------------------------
	
	@Override
	public String toString() {
		String toString = null;
		
		if (isHomeRoot()) {
			toString = "Home root folder (/home/)";
			
		} else if (isHome()) {
			toString = "Home folder for " + name + " (/home/" + name + "/)";
			
		} else if (isWorkspace()) {
			toString = "Workspace root (/)";
			
		} else {
			toString = getPath().toString() + " (id = " + id + ")";
			
			if (trashed)
				toString += " (trashed)";
			if (deleted)
				toString += " (deleted)";
		}
		
		return toString;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LocalFolder other = (LocalFolder) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
	/** Delegates call to platform's adapter manager. */
	@Override
	public Object getAdapter(@SuppressWarnings("rawtypes") Class adapter) {
		return Platform.getAdapterManager().getAdapter(IFile.class, adapter);
	}

    //--------------------------------------------------------------------------
    // Getters/setters of properties
    //--------------------------------------------------------------------------

	synchronized void setTrashed(boolean trashed) {
		this.trashed = trashed;
	}
	
	synchronized void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}
	
    //--------------------------------------------------------------------------
    // Helper methods (mostly private)
    //--------------------------------------------------------------------------
	
	
	/**
	 * Returns folders and files (in this order) from specified folder.
	 * @return included trashed nodes 
	 **/
	static private List<INode> oneLevelSubnodesOfFolder(LocalFolder folder) {
		List<LocalFolder> folders = folderDao.findAllSubfoldersInFolder(folder.id);
		List<LocalFile>   files = fileDao.findAllFilesInFolder(folder.id);		
		
		List<INode> result = new ArrayList<>();
		result.addAll(folders);
		result.addAll(files);
		
		return result;
	}
	

    /** Test and throw exception if folder is trashed or deleted. */
    private void throwIfNodeTrashedOrDeleted() throws NodeTrashedException, NodeDeletedException {
    	if (this.trashed)
    		throw new NodeTrashedException(this);
    	if (this.deleted)
    		throw new NodeDeletedException(this);
    }
	
		
}
