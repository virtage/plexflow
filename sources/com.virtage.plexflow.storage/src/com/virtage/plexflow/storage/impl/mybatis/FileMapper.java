package com.virtage.plexflow.storage.impl.mybatis;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.LongTypeHandler;

import com.virtage.plexflow.storage.exceptions.NodeNotFoundException;
import com.virtage.plexflow.storage.impl.LocalFile;
import com.virtage.plexflow.storage.impl.LocalStorageConsts;

public interface FileMapper {	

	// Various params for SQL queries to reduce typing error proneness
	public static final String TARGET_FOLDER_ID = "targetFolderId";
	public static final String SOURCE_FILE_ID = "sourceFileId";
	public static final String NEW_ID = "newId";		
	public static final String NAME = "name";
	
	
	/**
	 * Inserts new file into database. 
	 * 
	 * @param (in) {@link FileMapper#NAME} -- name of new file
	 * @param (in) {@link FileMapper#TARGET_FOLDER_ID} -- folder id for new file
	 * @param (out) {@link FileMapper#ID} -- id of new file will be stored here
	 */
	@Insert("INSERT INTO FILE (NAME, FOLDER_ID) VALUES (#{" + NAME + "}, #{" + TARGET_FOLDER_ID + "})")
	@Options(useGeneratedKeys=true, keyProperty=NEW_ID)	
	@Results(value = {
		@Result(property="id", column="ID", javaType=Long.class, jdbcType=JdbcType.BIGINT, typeHandler=LongTypeHandler.class),
		@Result(property="id2", column="ID2", javaType=Long.class, jdbcType=JdbcType.BIGINT, typeHandler=LongTypeHandler.class)
	})
	public void insertFile(Map<String, Object> paramMap);
	
	@Select("SELECT * FROM FILE WHERE FOLDER_ID = #{id}")
	public List<LocalFile> findAllFilesInFolder(long folderId);
	
	@Select("SELECT * FROM FILE WHERE ID = #{id}")
	public LocalFile findFileById(long id) throws NodeNotFoundException;
	
	
	/** Search and return not trashed subfolder in specified folder by name */ 
	@Select("SELECT * FROM FILE WHERE FOLDER_ID = #{param1} AND UCASE(NAME) = UCASE(#{param2}) " +
			"AND TRASHED_AT = " + LocalStorageConsts.UNIX_EPOCH_SQLSTR)
	public LocalFile findFileByName(long folderIdToSearchIn, String name);
	
	@Update("UPDATE FILE SET NAME = #{newName} WHERE ID = #{id}")
	public void renameFile(@Param("id") long id, @Param("newName") String newName);
	
	
	@Update("UPDATE FILE SET FOLDER_ID = #{target} WHERE ID = #{source}")
	public void moveFile(
			@Param("source") long sourceFileId,
			@Param("target") long targetFolderId);	
	
	
	/**
	 * Copies file to new folder, while resets CREATED_AT to current timestamp,
	 * and MODIFIED_AT and ACCESSED_AT to Unix epoch (= no yet modifier and accessed).
	 * 
	 * @param (in) {@linkplain FileMapper#SOURCE_FILE_ID}
	 * @param (in) {@linkplain FileMapper#TARGET_FOLDER_ID} 
	 * @param (out) {@linkplain FileMapper#NEW_ID} -- id assigned to new file
	 */
	@Update("INSERT INTO FILE ( " +
			"  NAME, FOLDER_ID, DESCRIPTION, CREATED_AT, MODIFIED_AT, ACCESSED_AT, TRASHED_AT, CRC32, LENGTH " +
			") SELECT " +
			"    NAME, #{" + TARGET_FOLDER_ID + "} AS FOLDER_ID, DESCRIPTION, CURRENT_TIMESTAMP AS CREATED_AT, " +
			"    '1970-01-01 00:00:00' AS MODIFIED_AT, '1970-01-01 00:00:00' AS ACCESSED_AT," +
			"    TRASHED_AT, CRC32, LENGTH " +
			"  FROM " +
			"	 FILE WHERE ID = #{" + SOURCE_FILE_ID + "}")
	@Options(useGeneratedKeys=true, keyProperty=NEW_ID)
	public void copyFile(Map<String, Long> params);
	
	@Update("UPDATE FILE SET DESCRIPTION = #{desc} WHERE ID = #{id}")
	public void setFileDescription(@Param("id") long folderId, @Param("desc") String desc);
	
	/** Search for not trashed file in specified folder by name */
	@Select("SELECT CASE WHEN COUNT(*) = 1 THEN TRUE ELSE FALSE END " +
			"FROM FILE WHERE FOLDER_ID = #{param1} AND UCASE(NAME) = UCASE(#{param2}) " +
			"AND TRASHED_AT = " + LocalStorageConsts.UNIX_EPOCH_SQLSTR)
	public boolean existsFileInFolder(long folderIdToSearchIn, String name);

	
	/** No matter if trashed or not. */
	@Select("SELECT CASE WHEN COUNT(*) = 1 THEN TRUE ELSE FALSE END " +
			"FROM FILE WHERE ID = #{param1}")
	public boolean existsFileWithId(long id);

	
	@Update("UPDATE FILE SET LENGTH = #{len} WHERE ID = #{id}")
	public void setFileLength(@Param("id") long fileId, @Param("len") long length);
	
	
	@Update("UPDATE FILE SET CRC32 = #{crc32} WHERE ID = #{id}")
	public void setFileCrc32(@Param("id") long fileId, @Param("crc32") long crc32);

	@Update("UPDATE FILE SET PUSH_IN_PROGRESS = #{push} WHERE ID = #{id}")
	public void setPushInProgressForFile(@Param("id") long id, @Param("push") boolean pushInProgress);
	
}
