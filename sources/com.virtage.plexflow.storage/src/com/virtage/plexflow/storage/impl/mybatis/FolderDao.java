package com.virtage.plexflow.storage.impl.mybatis;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;

import com.virtage.plexflow.storage.exceptions.NodeNotFoundException;
import com.virtage.plexflow.storage.impl.LocalFolder;
import com.virtage.plexflow.system.SystemInstanceFactory;
import com.virtage.plexflow.system.User;
import com.virtage.plexflow.utils.PxUtils;

public class FolderDao implements FolderMapper {
	//--------------------------------------------------------------------------
	// Class fields - non-public
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Instance fields - public
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Instance fields - non-public
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Instance fields - bean properties
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Constructors, static initializers, factories
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Public API - class (static)
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Public API - instance
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Overrides and implementations
	//--------------------------------------------------------------------------

	@Override
	public LocalFolder getHomeFor(User user) {
		SqlSession session = SystemInstanceFactory.getInstance().getSqlSessionFactory().openSession();		
		try {
			return session.getMapper(FolderMapper.class).getHomeFor(user);
			
		} finally {
			session.close();
		}
	}

	@Override
	public LocalFolder getHomeRoot() {
		SqlSession session = SystemInstanceFactory.getInstance().getSqlSessionFactory().openSession();
		try {
			return session.getMapper(FolderMapper.class).getHomeRoot();
		} finally {
			session.close();
		}
	}

	@Override
	public LocalFolder getWorkspace() {
		SqlSession session = SystemInstanceFactory.getInstance().getSqlSessionFactory().openSession();
		try {
			return session.getMapper(FolderMapper.class).getWorkspace();
		} finally {
			session.close();
		}
	}

	
	/** Use more convenient {@link #insertFolder(String, long)}. */
	@Override
	public void insertFolder(Map<String, Object> paramMap) {		
		SqlSession session = SystemInstanceFactory.getInstance().getSqlSessionFactory().openSession();
		try {
			FolderMapper mapper = session.getMapper(FolderMapper.class);
			mapper.insertFolder(paramMap);			
			session.commit();
			
		} finally {
			session.close();
		}

	}
	
	
	/**
	 * 
	 * @param folderName
	 * @param parentFolderId
	 * @return ID of created folder
	 */
	public long insertFolder(String folderName, long parentFolderId) {
		// Prepare params for LocalFolderMapper.insertFolder()
		Map<String, Object> params = new HashMap<>();
		params.put(FolderMapper.NAME, folderName);
		params.put(FolderMapper.PARENT_FOLDER_ID, parentFolderId);	// current folder ID will be parent
		
		insertFolder(params);				
		
		return PxUtils.bigDecimalToLong(params.get(FolderMapper.NEW_ID));
	}
	

	@Override
	public List<LocalFolder> findAllFolders() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<LocalFolder> findAllSubfoldersInFolder(long folderId) {
		List<LocalFolder> result = new ArrayList<>();
		
		SqlSession session = SystemInstanceFactory.getInstance().getSqlSessionFactory().openSession();
		try {
			FolderMapper folderMapper = session.getMapper(FolderMapper.class);						
			result = folderMapper.findAllSubfoldersInFolder(folderId);
			
		} finally {
			session.close();
		}
		
		return result;
	}

	@Override
	public LocalFolder findFolderById(long id) throws NodeNotFoundException {
		LocalFolder newFolder = null;
		
		SqlSession session = SystemInstanceFactory.getInstance().getSqlSessionFactory().openSession();
		try {
			newFolder = session.getMapper(FolderMapper.class).findFolderById(id);
			
		} finally {
			session.close();
		}
		
		if (newFolder == null) {
			throw new NodeNotFoundException("unknown", id);
		}
		
		return newFolder;
	}

	@Override
	public LocalFolder findSubfolderByName(long folderIdToSearchIn, String name) {
		LocalFolder folder = null;
		
		SqlSession session = SystemInstanceFactory.getInstance().getSqlSessionFactory().openSession();
		try {
			folder = session.getMapper(FolderMapper.class).
					findSubfolderByName(folderIdToSearchIn, name.toString());
		} finally {
			session.close();
		}
		
		return folder;
	}

	@Override
	public boolean existsSubfolderInFolder(long folderIdToSearchIn, String name) {
		boolean result = false;
		
		SqlSession session = SystemInstanceFactory.getInstance().getSqlSessionFactory().openSession();
		try {
			result = session.getMapper(FolderMapper.class).
				existsSubfolderInFolder(folderIdToSearchIn, name);
			
		} finally {
			session.close();
		}
		
		return result;
	}

	@Override
	public void renameFolder(long id, String newName) {
		SqlSession session = SystemInstanceFactory.getInstance().getSqlSessionFactory().openSession();
		try {
			session.getMapper(FolderMapper.class).renameFolder(id, newName);
			session.commit();
			
		} finally {
			session.close();
		}
	}

	@Override
	public void moveFolder(long sourceFolderId, long targetFolderId) {
		SqlSession session = SystemInstanceFactory.getInstance().getSqlSessionFactory().openSession();
		try {
			FolderMapper mapper = session.getMapper(FolderMapper.class);
			mapper.moveFolder(sourceFolderId, targetFolderId);
			session.commit();
			
		} finally {
			session.close();
		}
	}
	
	@Override
	public boolean isFolderParentOfFolder(long isThisFolderId,
			long parentOfFolderId) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isFolderParentOfFile(long isThisFileId, long parentOfFolderId) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void setFolderDescription(long folderId, String desc) {
		PxUtils.iaeIfEmptyString(desc, "description");
		PxUtils.iaeIfLongerThen(desc, "description", 1024);
		
		SqlSession session = SystemInstanceFactory.getInstance().getSqlSessionFactory().openSession();
		try {
			session.getMapper(FolderMapper.class).setFolderDescription(folderId, desc);
			session.commit();		
			
		} finally {
			session.close();
		}
	}

	@Override
	public boolean existsFolderWithId(long id) {
		boolean exists = false;
		
		SqlSession session = SystemInstanceFactory.getInstance().getSqlSessionFactory().openSession();		
		try {		
			exists = session.getMapper(FolderMapper.class).existsFolderWithId(id);			
			
		} finally {
			session.close();
		}
		
		return exists;
	}
	//--------------------------------------------------------------------------
	// Class fields - public
	//--------------------------------------------------------------------------

	/** Use more convenient {@link #copyFolder(long, long)}. */
	@Override
	public void copyFolder(Map<String, Long> params) {		
		SqlSession session = SystemInstanceFactory.getInstance().getSqlSessionFactory().openSession();
		try {
			session.getMapper(FolderMapper.class).copyFolder(params);
			
		} finally {
			session.close();
		}
	}
	
	public long copyFolder(long sourceFolderId, long targetFolderId) {
		HashMap<String, Long> params = new HashMap<>();
		params.put(FolderMapper.SOURCE_FOLDER_ID, sourceFolderId);
		params.put(FolderMapper.TARGET_FOLDER_ID, targetFolderId);
		
		copyFolder(params);
		
		return params.get(FolderMapper.NEW_ID);
	}
	
	@Override
	public boolean hasChildFolders(long folderId) {
		SqlSession session = SystemInstanceFactory.getInstance().getSqlSessionFactory().openSession();
		try {
			return session.getMapper(FolderMapper.class).hasChildFolders(folderId);
			
		} finally {
			session.close();
		}
	}
	
	@Override
	public boolean hasChildFiles(long folderId) {
		SqlSession session = SystemInstanceFactory.getInstance().getSqlSessionFactory().openSession();
		try {
			return session.getMapper(FolderMapper.class).hasChildFiles(folderId);
			
		} finally {
			session.close();
		}
	}
	
	//--------------------------------------------------------------------------
	// Consider toString(), equals(), hashCode()
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Getters/setters of properties
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Helper methods (mostly private)
	//--------------------------------------------------------------------------
}
