package com.virtage.plexflow.storage.impl.mybatis;

import org.apache.ibatis.session.SqlSession;

import com.virtage.plexflow.system.SystemInstanceFactory;

public class StorageDao implements StorageMapper {

	/** Caution: Table FILE must be deleted first as it contains FK to FOLDER */
	@Override
	public int delFolderTable() {
		int deleted = 0;
		
		SqlSession session = SystemInstanceFactory.getInstance().getSqlSessionFactory().openSession();		
		try {			
			deleted = session.getMapper(StorageMapper.class).delFolderTable();			
			session.commit();
			
		} finally {
			session.close();
		}
		
		return deleted;
	}

	@Override
	public int delFilesTable() {
		int deleted = 0;
		
		SqlSession session = SystemInstanceFactory.getInstance().getSqlSessionFactory().openSession();		
		try {			
			deleted = session.getMapper(StorageMapper.class).delFilesTable();			
			session.commit();
			
		} finally {
			session.close();
		}
		
		return deleted;
	}
}
