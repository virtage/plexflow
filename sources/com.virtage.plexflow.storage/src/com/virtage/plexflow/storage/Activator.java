package com.virtage.plexflow.storage;

import org.apache.ibatis.session.Configuration;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

import com.virtage.plexflow.storage.impl.mybatis.FileMapper;
import com.virtage.plexflow.storage.impl.mybatis.FolderMapper;
import com.virtage.plexflow.storage.impl.mybatis.StorageMapper;
import com.virtage.plexflow.storage.impl.mybatis.TrashMapper;
import com.virtage.plexflow.system.SystemInstanceFactory;

public class Activator implements BundleActivator {

	// The plug-in ID
	public static final String PLUGIN_ID = "com.virtage.plexflow.storage"; //$NON-NLS-1$
	
	private static BundleContext context;

	static BundleContext getContext() {
		return context;
	}

	/*
	 * (non-Javadoc)
	 * @see org.osgi.framework.BundleActivator#start(org.osgi.framework.BundleContext)
	 */
	@Override
	public void start(BundleContext bundleContext) throws Exception {
		Activator.context = bundleContext;
		
		// HERE add any plugin private MyBatis mappers
		Configuration conf = SystemInstanceFactory.getInstance().getSqlSessionFactory().getConfiguration();		
		//conf.getTypeAliasRegistry().registerAlias(LocalFolder.class);
		conf.addMapper(FolderMapper.class);
		conf.addMapper(FileMapper.class);
		conf.addMapper(TrashMapper.class);
		conf.addMapper(StorageMapper.class);
		
	}

	/*
	 * (non-Javadoc)
	 * @see org.osgi.framework.BundleActivator#stop(org.osgi.framework.BundleContext)
	 */
	@Override
	public void stop(BundleContext bundleContext) throws Exception {
		Activator.context = null;		
	}	
	

}
