package com.virtage.plexflow.storage;


public interface ILockable {
	
	public ILock lock();	
	public ILock lock(long timeOutMillis);
	
	/**
	 * Returns lock of associated node, or <tt>null</tt> if not locked.
	 * @return
	 */
	public ILock getLock();
	
	public void unLock();
	public boolean canUnlock();
	public boolean isLocked();	
	
	
}
