package com.virtage.plexflow.storage;

import java.util.Date;

import com.virtage.plexflow.system.User;

public interface IVersion {

	public IVersion getSuccessor();
	public IVersion getPredecessor();
	
	public IFile getFile();
	
	public String getCommitMessage();
	public String getDescription();
	
	public User comittedBy();	
	public Date comittedAt();
	
	public boolean isRoot();
	public boolean isHead();
}
