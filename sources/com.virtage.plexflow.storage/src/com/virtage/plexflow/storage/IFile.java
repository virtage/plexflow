package com.virtage.plexflow.storage;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Date;

import com.virtage.plexflow.storage.exceptions.FilePushInProgressException;
import com.virtage.plexflow.storage.exceptions.StorageException;

public interface IFile extends INode, IVersionable, ITaggable {
	
	/**
	 * 
	 * @return
	 */
	public long getLength();
	
	public void setLength(long length);
	
	
	public long getCrc32();
	public void setCrc32(long crc32);
	
	
	/**
	 * Set push-in-progress state of a file. File is inconsistent when being pushed.
	 *  
	 * @param pushInProgress
	 */
	public void setPushInProgress(boolean pushInProgress);
	
	/**
	 * Is pushing transaction currently in progress? File is inconsistent when being pushed.
	 * @return
	 */
	public boolean isPushInProgress();
	
	/**
	 * 
	 * @return
	 */
	public Date getModifiedAt();
	
	/**
	 * 
	 * @return
	 */
	public Date getAccessedAt();	
}
