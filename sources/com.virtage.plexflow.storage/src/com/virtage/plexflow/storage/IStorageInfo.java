package com.virtage.plexflow.storage;

public interface IStorageInfo {
	
	public long getOccupiedSize();
	
}
