package com.virtage.plexflow.storage.events;

import java.util.EventObject;

@SuppressWarnings("serial")
public abstract class VersionEvent extends EventObject {

	public VersionEvent(Object source) {
		super(source);
	}

}
