package com.virtage.plexflow.storage.events;

/**
 * Convenience "do nothing" implementation of NodeListener.
 * 
 **/
public abstract class NodeListenerAdapter implements NodeListener {

	@Override
	public void copied(NodeEvent e) {

	}

	@Override
	public void moved(NodeEvent e) {

	}

	@Override
	public void renamed(NodeEvent e) {

	}

	@Override
	public void trashed(NodeEvent e) {

	}
}
