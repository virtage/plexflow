package com.virtage.plexflow.storage.events;

import java.util.EventObject;

@SuppressWarnings("serial")
public abstract class NodeEvent extends EventObject {

	public NodeEvent(Object source) {
		super(source);
	}

}
