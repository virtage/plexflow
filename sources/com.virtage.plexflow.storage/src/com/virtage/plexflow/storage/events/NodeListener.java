package com.virtage.plexflow.storage.events;

public interface NodeListener extends java.util.EventListener {
	
	/** Node has been copied. */
	public void copied(NodeEvent e);
	
	/** Node has been moved. */
	public void moved(NodeEvent e);
	
	/** Node has been renamed. */
	public void renamed(NodeEvent e);
	
	/** Node has been trashed. */
	public void trashed(NodeEvent e);
	
}
