package com.virtage.plexflow.storage.events;

import java.util.EventObject;

@SuppressWarnings("serial")
public abstract class LockEvent extends EventObject {

	public LockEvent(Object source) {
		super(source);
	}

}
