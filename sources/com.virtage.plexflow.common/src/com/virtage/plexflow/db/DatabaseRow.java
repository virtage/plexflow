/**
 * 
 */
package com.virtage.plexflow.db;

/**
 * Tagging (no methods) interface to mark class acting as a database table row
 * delegate only.
 * 
 * <p>It's strictly recommended to implement row classes as immutable and 
 * override hashCode() and equals() to be good collection citizen.</p>
 * 
 * <p>At this moment, it doesn't have any true purpose but at least group all
 * database row objects throughout whole Plexflow codebase.</p>
 * 
 *  
 * @author libor
 *
 */
public interface DatabaseRow {

}
