package com.virtage.plexflow.config;

import net.jcip.annotations.ThreadSafe;

import com.virtage.plexflow.config.impl.DbConfig;
import com.virtage.plexflow.system.User;


/**
 * Singleton config factory class to obtain {@link IConfig} instance. 
 * 
 * @author libor
 *
 */
@ThreadSafe
public final class ConfigFactory {
	
	private static volatile IConfig globalConfig;
	private static volatile IConfig defaultConfig;
	
	
	private ConfigFactory() {}
	
	
	// All singleton access methods written with "double-checked locking"
	// http://javarevisited.blogspot.cz/2011/06/volatile-keyword-java-example-tutorial.html
	// or
	// http://jeremymanson.blogspot.cz/2008/05/double-checked-locking.html
	
	/** Returns global configuration set. */
	public static IConfig getGlobal() {
		if (globalConfig == null) {
			synchronized (ConfigFactory.class) {
				if (globalConfig == null) {
					globalConfig = new DbConfig("GLOBAL");
				}
			}
		}		 
		
		return globalConfig;
	}
	
	/** Returns default configuration set. */
	public static IConfig getDefault() {
		if (defaultConfig == null) {
			synchronized (ConfigFactory.class) {
				if (defaultConfig == null) {
					defaultConfig = new DbConfig("DEFAULT");
				}
			}
		}
		
		return defaultConfig;
	}
	
	/** Returns configuration set for specified user. */
	public static IConfig getUser(User user) {
		return new DbConfig("USER_" + user.getUsername());
	}
}
