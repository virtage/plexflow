package com.virtage.plexflow.config.impl;

import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.session.SqlSession;

import com.virtage.plexflow.config.ConfigException;
import com.virtage.plexflow.config.IConfig;
import com.virtage.plexflow.system.SystemInstanceFactory;

public class DbConfig implements IConfig {

	private final String setname;

	public DbConfig(String setname) {
		this.setname = setname;
	}

	@Override
	public Boolean getBoolean(String keyname) throws ConfigException {
		Boolean result = null;
		SqlSession session = SystemInstanceFactory.getInstance()
				.getSqlSessionFactory().openSession();

		try {
			String keyValue = session.getMapper(ConfigMapper.class)
					.getKeyValue(setname, keyname);

			if (StringUtils.isBlank(keyValue))
				throw new ConfigException(keyname, keyValue);

			result = Boolean.valueOf(keyValue);

		} finally {
			session.close();
		}

		return result;
	}

	@Override
	public Integer getInteger(String keyname) throws ConfigException {
		Integer result = null;
		SqlSession session = SystemInstanceFactory.getInstance()
				.getSqlSessionFactory().openSession();

		try {
			String keyvalue = session.getMapper(ConfigMapper.class)
					.getKeyValue(setname, keyname);

			if (StringUtils.isBlank(keyvalue)) {
				throw new ConfigException(keyname, keyvalue);
			}

			try {
				result = Integer.valueOf(keyvalue);

			} catch (NumberFormatException ex) {
				throw new ConfigException("Config key '" + keyname
						+ "' of value '" + keyvalue
						+ "' cannot be converted to integer.");
			}

		} finally {
			session.close();
		}

		return result;
	}

	@Override
	public String getString(String keyname) throws ConfigException {
		String result = null;
		SqlSession session = SystemInstanceFactory.getInstance()
				.getSqlSessionFactory().openSession();

		try {
			result = session.getMapper(ConfigMapper.class).getKeyValue(setname,
					keyname);

			if (StringUtils.isBlank(result)) {
				throw new ConfigException(keyname, result);
			}

		} finally {
			session.close();
		}

		return result;
	}

	@Override
	public void putKey(String keyname, int value) {
		SqlSession session = SystemInstanceFactory.getInstance()
				.getSqlSessionFactory().openSession();

		try {
			session.getMapper(ConfigMapper.class).putKey(setname, keyname,
					String.valueOf(value));

		} finally {
			session.close();
		}
	}

	@Override
	public void putKey(String keyname, boolean value) {
		SqlSession session = SystemInstanceFactory.getInstance()
				.getSqlSessionFactory().openSession();

		try {
			session.getMapper(ConfigMapper.class).putKey(setname, keyname,
					String.valueOf(value));

		} finally {
			session.close();
		}
	}

	@Override
	public void putKey(String keyname, String value) {
		SqlSession session = SystemInstanceFactory.getInstance()
				.getSqlSessionFactory().openSession();

		try {
			session.getMapper(ConfigMapper.class).putKey(setname, keyname,
					String.valueOf(value));

		} finally {
			session.close();
		}
	}

	@Override
	public boolean existsKey(String keyname) {
		boolean result = false;
		SqlSession session = SystemInstanceFactory.getInstance()
				.getSqlSessionFactory().openSession();

		try {
			if (session.getMapper(ConfigMapper.class).getKeyValue(setname,
					keyname) != null) {
				result = true;
			}

		} finally {
			session.close();
		}

		return result;
	}

	@Override
	public void deleteAllKeys() {
		SqlSession session = SystemInstanceFactory.getInstance()
				.getSqlSessionFactory().openSession();

		try {
			session.getMapper(ConfigMapper.class).deleteAllKeysInSetname(
					setname);

		} finally {
			session.close();
		}
	}
}
