package com.virtage.plexflow.config.impl;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

public interface ConfigMapper {
	
	@Select("SELECT KEYVALUE FROM CONFIG WHERE SETNAME = #{setname} AND KEYNAME = #{keyname}")
	String getKeyValue(
		@Param("setname") String setname,
		@Param("keyname") String keyname);
	
	@Insert("INSERT INTO CONFIG (SETNAME, KEYNAME, KEYVALUE) VALUES (#{setname}, #{keyname}, #{keyvalue}")
	void putKey(
		@Param("setname") String setname,
		@Param("keyname") String keyname,
		@Param("value") String value);
	
	
	@Select("SELECT CASE WHEN COUNT(*) > 1 THEN TRUE ELSE FALSE END " + 
			"FROM CONFIG WHERE SETNAME = #{setname}")
	boolean existsSetname(String setname);
	
	@Delete("DELETE FROM CONFIG WHERE SETNAME = #{setname}")
	void deleteAllKeysInSetname(String setname);
}
