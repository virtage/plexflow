package com.virtage.plexflow.config;

import javax.naming.ConfigurationException;

/**
 * Config keys getXY(), putXY() and a few of auxiliary methods. Heavily inspired by
 * Commons Configuration. 
 * 
 * <p>Contains a number of null-safe <tt>getXY(keyname)</tt> methods for various data types.</p>
 * 
 * <p>Methods throw {@link ConfigException} if
 * <ul>
 * <li>no such keyname exists</li>
 * <li>or is blank (null, whitespace or empty string)</li>
 * <li>or has a value that cannot be converted to result data type</li>
 * </ul>
 * Implementations must evaluate these <em>blanks</em> same as Commons's StringUtils.isBlank(): * 
 * <pre>
 * StringUtils.isBlank(null)      = true
 * StringUtils.isBlank("")        = true
 * StringUtils.isBlank(" ")       = true
 * StringUtils.isBlank("bob")     = false
 * StringUtils.isBlank("  bob  ") = false
 * </pre>
 * </p>  
 * 
 * @see http://commons.apache.org/configuration/apidocs/org/apache/commons/configuration/Configuration.html}
 */
public interface IConfig {	
	/**
	 * Returns true if and only if keyname exists and is equals to 'true'
	 * regardless of letter case.
	 * 
	 * @throws ConfigException if keyname is not found. 
	 */
	public Boolean getBoolean(String keyname)  throws ConfigException;
	
	
	/**
	 * @throws ConfigException if keyname is not found, is blank, or have cannot be
	 * converted to integer. 
	 */
	public Integer getInteger(String keyname) throws ConfigException;
	
		
	/**
	 *  
	 * @throws ConfigException if keyname is not found, or is blank. Term <em>blank</em> conforms to
	 * Commons's <tt>StringUtils.isBlank()</tt> evaluation:
	 * <pre>
	 * StringUtils.isBlank(null)      = true
	 * StringUtils.isBlank("")        = true
	 * StringUtils.isBlank(" ")       = true
	 * StringUtils.isBlank("bob")     = false
	 * StringUtils.isBlank("  bob  ") = false
	 */
	public String getString(String keyname) throws ConfigException;
	
	
	public void putKey(String keyname, int value);
	public void putKey(String keyname, boolean value);
	public void putKey(String keyname, String value);
	
	public boolean existsKey(String keyname);
	public void deleteAllKeys();
}
