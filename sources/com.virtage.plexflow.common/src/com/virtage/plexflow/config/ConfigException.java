package com.virtage.plexflow.config;

/**
 * 
 * Reports an issue with config content (i.e. not to obtain config). Possible reasons to
 * throw may include:
 * 
 * <ul>
 * <li>required key is missing</li>
 * <li>key valus is invalid</li>
 * <li></li>
 * </ul>
 *  
 * Exception are instantiated via static factory methods.
 *  
 * @author libor
 *
 */
public class ConfigException extends Exception {

	private static final long serialVersionUID = -1254023895953243508L;

	public ConfigException(String message) {
		super(message);
	}
	
	/**
	 * ConfigException with standardized message <em>"Required config key 'keyName' is missing or
	 * has an invalid value 'keyValue'."</em>.
	 * 
	 * @param keyName
	 * @param keyValue
	 */
	public ConfigException(String keyName, Object keyValue) {
		super("Required config key '" + keyName + "' is missing or has an invalid value '" +
				keyValue + "'.");
	}
	
}
