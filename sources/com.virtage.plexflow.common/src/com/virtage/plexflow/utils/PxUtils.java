package com.virtage.plexflow.utils;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.text.DecimalFormatSymbols;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Random;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.EncoderException;
import org.apache.commons.codec.net.QuotedPrintableCodec;
import org.eclipse.core.runtime.Assert;


/**
 * <p>Various utility and helper non-UI methods. UI methods goes to
 * {@link com.virtage.plexflow.ui.PxUiUtils}. Class contains solely static
 * methods.</p>
 * 
 * <p>Textual methods:
 * <ul>
 * <li>{@link #addDotToSentense(String)} --
 * adds dot (.) to the end ofsentence if missing.</li>
 * <li>{@link #capitalizeFirstLetter(String)} --
 * change first character of string to upper case.</li>
 * <li>{@code humanReadableByteCount()} group --
 * prints bytes converting to KiB/kB, MiB/MB, etc.</li>
 * <li>{@code coalesce()} group --
 * prints first non-null value</li>
 * </ul> 
 * </p>
 * 
 * <p>Java utils:
 * <ul>
 * <li>{@link #stackTraceAsString(Throwable)} --
 * returns Throwable as stack trace string</li>
 * <li>{@link #bigDecimalToLong(Object)} --
 * converts BigDecimal to Long</li>
 * <li>{@link #newProperties(String...)} --
 * simplifies code with creating Properties instance filled from vararg</li>
 * <li>{@link #newMap(Object...)} --
 * simplifies code with creating Map instance filled from vararg</li>
 * </ul> 
 * </p>
 * 
 * <p>Validations:
 * <ul>
 * <li>{@code ieaIfXY()} group --
 * throws <tt>IllegalArgumentException</tt> if ...</li>
 * </ul> 
 * </p>
 * 
 * <p>Charsets, conversion:
 * <ul>
 * <li>{@link #toQuotedPritable(String)} --
 * convert string into RFC 1521 "quoted-pritable" encoding</li>
 * <li>{@link #fromQuotedPritable(String)} --
 * convert string from RFC 1521 "quoted-pritable" encoding</li>
 * </ul>  
 * </p>
 * 
 * <p>Miscellaneous:
 * <ul>
 * <li>{@link #isBitPresent(int, int)} group --
 * tells whether style bit in present in bitwise OR-ed result.</li>
 * </ul> 
 * </p>
 * 
 * @author libor
 *
 */
public final class PxUtils {	

	//--------------------------------------------------------------------------
    // Class fields - public
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Class fields - non-public
    //--------------------------------------------------------------------------
 
    //private static final ISysLog SYSLOG = Logging.getSysLog(ThisClass.class);
 
    //--------------------------------------------------------------------------
    // Instance fields - public
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Instance fields - non-public
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Instance fields - bean properties
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Constructors, static initializers, factories
    //--------------------------------------------------------------------------
    
	private PxUtils() {};
	
    //--------------------------------------------------------------------------
    // Public API - class (static)
    //--------------------------------------------------------------------------
	
	//--------------------------------------------------------------------------
    // Lang, primitives, conversion
    //--------------------------------------------------------------------------
	
	/**
	 * Simplified code by returning Properties instance filled with key-value
	 * pairs from vararg parameters.
	 * 
	 * <p>
	 * Example:
	 * 
	 * <pre>
	 * Properties p = PxUtils.newProperties(&quot;day&quot;, &quot;Monday&quot;, &quot;weather&quot;, &quot;sunny&quot;);
	 * </pre>
	 * 
	 * </p>
	 * 
	 * @param keyValuePairs
	 *            Arbitrary number of String-String pairs
	 * @return new Properties instance containing keys and values from
	 *         parameters
	 * @throws IllegalArgumentException
	 *             If number of parameters is not even (e.g. invoked with
	 *             "key1", "value1", "key2"). OR when parameters are missing.
	 */
	public static Properties newProperties(String ... keyValuePairs) throws IllegalArgumentException {
		// Sanity check
		// empty param
		if (keyValuePairs.length == 0) {
			String msg = "Method cannot be invoked with empty parameters.";
			throw new IllegalArgumentException(msg);
		}
		
		// not even number of params 
		if ((keyValuePairs.length % 2) != 0) {
			String msg = "Method called with not even number of parameters. " +
				"Two immediate following parameters are one pair.";
			throw new IllegalArgumentException(msg);
		}
		
		Properties toReturn = new Properties();		

		for (int i = 0; i < keyValuePairs.length; i += 2) {
			String key   = keyValuePairs[i];
			String value = keyValuePairs[i+1];
			
			toReturn.setProperty(key, value);
		}
		
		return toReturn;		
	}
	
	/**
	 * Simplified code by returning HashMap instance filled with key-value pairs
	 * from vararg parameters.
	 * 
	 * <p>
	 * To get typed map, you need to cast and suppress warnings:
	 * 
	 * <pre>
	 * &#064;SuppressWarnings(&quot;unchecked&quot;)
	 * Map&lt;String, String&gt; map = (Map&lt;String, String&gt;) newMap(&quot;day&quot;, &quot;Monday&quot;,
	 * 		&quot;weather&quot;, &quot;sunny&quot;);
	 * </pre>
	 * 
	 * </p>
	 * 
	 * @param keyValuePairs
	 *            Arbitrary number of String-String pairs
	 * @return new HashMap instance containing keys and values from parameters
	 * @throws IllegalArgumentException
	 *             If number of parameters is not even (e.g. invoked with
	 *             "key1", "value1", "key2"). OR when parameters are missing.
	 */
	public static Map<?, ?> newMap(Object ... keyValuePairs) throws IllegalArgumentException {
		// Sanity check
		// empty param
		if (keyValuePairs.length == 0) {
			String msg = "Method cannot be invoked with empty parameters.";
			throw new IllegalArgumentException(msg);
		}
		
		// not even number of params 
		if ((keyValuePairs.length % 2) != 0) {
			String msg = "Method called with not even number of parameters. " +
				"Two immediate following parameters are one pair.";
			throw new IllegalArgumentException(msg);
		}
		
		Map<Object, Object> mapToReturn = new HashMap<>();		

		for (int i = 0; i < keyValuePairs.length; i += 2) {
			Object key   = keyValuePairs[i];
			Object value = keyValuePairs[i+1];
			
			mapToReturn.put(key, value);
		}
		
		return mapToReturn;	
	}
	
	/**
	 * Tells whether a bit is present in bitwise OR-ed result.
	 * @param bit style bit to test.
	 * @param result bitwise OR-ed (|) result.
	 * 
	 * @see http://www.vogella.com/blog/2011/01/25/swt-style-bits/
	 * @see http://bits.stephan-brumme.com/interactive.html
	 **/
	public static boolean isBitPresent(int bit, int result) {
		return (bit == (result & bit)) ? true : false;
	}
	
	
	/** Converts exception stack trace to single string, every step on new line. */
	public static String stackTraceAsString(Throwable t) {
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		t.printStackTrace(pw);				
		return sw.toString();
	}
	
	
	/**
	 * <p>Converts byte amount to nice human readable form like "1.7 KiB" etc.</p>
	 * 
	 * <p>Please note that if you use the standardized units, 1024 should become "1KiB" and
	 * 1024*1024 should become "1MiB".</p>
	 * 
	 * <p>Code taken from SO answer by aiobee user.</p>
	 *  
	 * @param bytes
	 * @param si SI or binary standard. For SI is base 1000 B, for binary is 1024 B.
	 * @param decimalSeparator character to seperate decimal places
	 * @return
	 * @see http://stackoverflow.com/questions/3758606/how-to-convert-byte-size-into-human-readable-format-in-java
	 * @see http://en.wikipedia.org/wiki/Binary_prefix
	 */
	public static String humanReadableByteCount(long bytes, boolean si, char decimalSeparator) {
	    int unit = si ? 1000 : 1024;
	    if (bytes < unit) return bytes + " B";
	    int exp = (int) (Math.log(bytes) / Math.log(unit));	    
	    String pre = (si ? "kMGTPE" : "KMGTPE").charAt(exp-1) + (si ? "" : "i");	    
	    String formatted = String.format("%.1f %sB", bytes / Math.pow(unit, exp), pre);
	    formatted = formatted.replace('.', decimalSeparator);
	    
	    return formatted;
	}
	
	/**
	 * Conveinece method for {@code humanReadableByteCount(long, false, locale-depend)}, i.e.
	 * human readable in binary units and base (1 KiB = 1024 B) with locale depend decimal separator.
	 */
	public static String humanReadableByteCount(long bytes) {
		char separator = DecimalFormatSymbols.getInstance().getDecimalSeparator();
	    return humanReadableByteCount(bytes, false, separator);
	}
	
	// Code from http://stackoverflow.com/a/2768198/915931
	
	/** Returns the first non-null value. */
	public static <T> T coalesce(T a, T b) {
	    return a != null ? a : b;
	}
	/** Returns the first non-null value. */
	public static <T> T coalesce(T a, T b, T c) {
	    return a != null ? a : coalesce(b,c);
	}
	/** Returns the first non-null value. */
	public static <T> T coalesce(T a, T b, T c, T d) {
	    return a != null ? a : coalesce(b,c,d);
	}
	/** Returns the first non-null value. */
	public static <T> T coalesce(T a, T b, T c, T d, T e) {
	    return a != null ? a : coalesce(b,c,d,e);
	}
	
	/** Returns the first non-null value of several variables. */
	// For efficient reasons this version is commented. 
//	public static <T> T coalesce(T ...items) {
//	    for(T i : items) if(i != null) return i;
//	    return null;
//	}
	
	
	/**
	 * Random positive (> 0) long value. Can be, for example, used to generate random file IDs and other
	 * long positive IDs.
	 * 
	 */
	public static long randomPositiveLong() {
		long positiveLong;
		// Stay in loop until a positive long is generated 
		while ((positiveLong = new Random().nextLong()) < 1) {			
		}
		
		return positiveLong;
	}
	
	
	/**
	 * Tries to convert Object from parameter to long value. <em>Used mostly
	 * as workaround for MyBatis that incorrectly maps SQL BIGINT to
	 * BigDecimal.</em>
	 *  
	 * @param bd
	 * @return
	 * @throws IllegalArgumentException is thrown when (1) conversion might lead
	 * to precision loss, (2) argument is neither BigDecimal or Long.
	 * @throws NullPointerException if argument was null. 
	 */ 
	public static long bigDecimalToLong(Object bd) {
		
		Assert.isNotNull(bd);
		
		// Column ID in table is BIGINT that should be mapped to long but
		// java.math.BigDecimal is returned instead.
		if (bd instanceof BigDecimal) {
			long longValue = ((BigDecimal) bd).longValue();
			
			// make sure that long is not different (longValue() can narrow as
			// javadoc states)
			if (!bd.equals(BigDecimal.valueOf(longValue))) {
				throw new IllegalArgumentException("Precision lost during " +
					"conversion from BigDecimal " +
					"(" + bd + ") to long (" + longValue + "): " +
					"(BigDecimal -> long) != (long -> BigDecimal).");
			}
			
			return longValue;
			
		} else if (bd instanceof Long) {
			// Already long
			return ((Long) bd).longValue();
			
		} else {
			throw new IllegalArgumentException("Parameter is neither BigDecimal or Long!"); 
		}
	}
	
	
	//--------------------------------------------------------------------------
    // Textual
    //--------------------------------------------------------------------------
		
	/**
	 * Capitalizes first letter, leaving rest of string unchanged.
	 * 
	 * E.g. "home folder" will become "Home folder", "h" will become "H".
	 * 
	 * @param str
	 * @return Capitalized first letter. When param is 1-char lenght, then this
	 * 		letter is uppercased and returned.
	 */
	public static String capitalizeFirstLetter(String str) {
		if (str.length() > 1)
			return str.substring(0, 1).toUpperCase() + str.substring(1);
		else
			return str.toUpperCase();
	}
	
	
	/**
	 * Adds dot after the last word of sentence if missing.
	 * 
	 */
	public static String addDotToSentense(String sentense) {
		// Already have dot
		if (sentense.charAt(sentense.length() -1) == '.') {
			return sentense;
			
		} else {
			return sentense + '.';
			
		}
	}
	
	
	//--------------------------------------------------------------------------
    // iaeIf*() -- valid objects and throw IllegalArgumentException if invalid 
    //--------------------------------------------------------------------------
	
	/**
	 * Throws IllegalArgumentException if param is empty String ("").
	 * 
	 * <p>Example: {@code iaeIfEmptyString(desc, "description")} will produce message
	 * "Description cannot be empty string."</p>
	 * 
	 * <p>If param will be null, then IllegalArgumentException that param can't be null will be
	 * thrown.</p>
	 * 
	 * @param param
	 * @param paramNiceName First letter will be capitalized to have nice message.
	 * @throws IllegalArgumentException
	 */
	public static void iaeIfEmptyString(String param, String paramNiceName)
			throws IllegalArgumentException {
		
		iaeIfNull(param, paramNiceName);
		
		if (param.isEmpty())
			throw new IllegalArgumentException(
				capitalizeFirstLetter(paramNiceName) + " cannot be empty string.");
	}
	
	
	/**
	 * Throws IllegalArgumentException if param is null.
	 * 
	 * <p>Example: {@code iaeIfNull(desc, "description")} will produce message
	 * "Description cannot be empty string."</p>
	 * 
	 * @param param
	 * @param paramNiceName First letter will be capitalized to have nice message.
	 * @throws IllegalArgumentException
	 */
	public static void iaeIfNull(Object param, String paramNiceName)
			throws IllegalArgumentException {
		
		if (null == param)
			throw new IllegalArgumentException(
				capitalizeFirstLetter(paramNiceName) + " cannot be null.");
	}
	
	
	/** IAE if string is longer then n chars. */
	public static void iaeIfLongerThen(String param, String paramNiceName, int maxLenght)
			throws IllegalArgumentException {
		
		iaeIfNull(param, paramNiceName);
		
		if (param.length() > maxLenght)
			throw new IllegalArgumentException(
				capitalizeFirstLetter(paramNiceName) + " cannot be longer than " +
				maxLenght + " characters.");
	}
	
	/** IAE if string is shorter then N chars. */
	public static void iaeIfShorterThen(String param, String paramNiceName, int minLenght)
			throws IllegalArgumentException {
		
		iaeIfNull(param, paramNiceName);
		
		if (param.length() > minLenght)
			throw new IllegalArgumentException(
				capitalizeFirstLetter(paramNiceName) + " cannot be shorter than " +
				minLenght + " characters.");
	}	
	
	/** IAE if string is not between N and M chars inclusive. */
	public static void iaeIfBetween(String param, String paramNiceName, int minLenght, int maxLenght)
			throws IllegalArgumentException {
		
		iaeIfNull(param, paramNiceName);
		
		if (param.length() > minLenght)
			throw new IllegalArgumentException(
				capitalizeFirstLetter(paramNiceName) + " must be between " +
				minLenght + " and " + maxLenght + " characters.");
	}
	
	/** IAE if a number is less then X. */
	public static void iaeIfLessThen(long param, String paramNiceName, long then)
			throws IllegalArgumentException {
		if (param < then) {
			throw new IllegalArgumentException("Parameter " +
					capitalizeFirstLetter(paramNiceName) +
					" cannot be less then " +
					param + ".");			
		}
		
	}
	
	/** Not implemented yet! */
	@Deprecated
	public static void iaeIfInFuture(Date param, String paramNiceName) {
		throw new UnsupportedOperationException("Not implemented yet!");
	}

	/** Not implemented yet! */
	@Deprecated
	public static void iaeIfNotInFuture(Date param, String paramNiceName)
			throws IllegalArgumentException {
		throw new UnsupportedOperationException("Not implemented yet!");
	}
	
	/** Not implemented yet! */
	@Deprecated
	public static void iaeIfInPast(Date param, String paramNiceName) {
		throw new UnsupportedOperationException("Not implemented yet!");
	}
	
	/** Not implemented yet! */
	@Deprecated
	public static void iaeIfNotInPast(Date param, String paramNiceName) {
		throw new UnsupportedOperationException("Not implemented yet!");
	}
	
	/** Not implemented yet! */
	@Deprecated
	public static void iaeIfInBetween(Date param, String paramNiceName, Date minDate, Date maxDate) {
		throw new UnsupportedOperationException("Not implemented yet!");
	}
	
	
	
	//--------------------------------------------------------------------------
    // Misc
    //--------------------------------------------------------------------------
	
	/**
	 * Converts parameter to "quoted-pritable" encoding. Used e.g. for header
	 * values containing non ISO-8859-1 characters.
	 * 
	 * <p>
	 * If parameter contains invalid data impossible to convert, a value
	 * <em>"<cannot convert to quoted-pritable>"</em> is returned instead.
	 * </p>
	 * 
	 * @param str
	 * @return
	 * 
	 * @see http://tools.ietf.org/html/rfc1521#page-18
	 */
	public static String toQuotedPritable(String str) {
		QuotedPrintableCodec codec = new QuotedPrintableCodec("UTF-8");
		
		// Try to encode, if failed "error" will be output
		String enc = "<cannot convert to quoted-pritable>";
		try {
			enc = codec.encode(str);			
		} catch (EncoderException e) {			
		}
		
		return enc;
	}
	
	
	/**
	 * Converts parameter from "quoted-pritable" encoding. Used e.g. for header
	 * values containing non ISO-8859-1 characters. 
	 * 
	 * <p>
	 * If parameter contains invalid data impossible to convert, a value
	 * <em>"<cannot convert from quoted-pritable>"</em> is returned instead.
	 * </p>
	 * 
	 * @param str
	 * @return
	 * @throws EncoderException
	 * @see http://tools.ietf.org/html/rfc1521#page-18
	 */
	public static String fromQuotedPritable(String str) {
		QuotedPrintableCodec codec = new QuotedPrintableCodec("UTF-8");
		
		// Try to decode, if failed "error" will be output
		String enc = "<cannot convert from quoted-pritable>";
		try {
			enc = codec.decode(str);			
		} catch (DecoderException e) {			
		}
		
		return enc;
	}

	
	
	
	
    //--------------------------------------------------------------------------
    // Public API - instance
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Overrides and implementations
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Getters/setters of properties
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Helper methods (mostly private)
    //--------------------------------------------------------------------------
	
	


}
