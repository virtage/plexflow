package com.virtage.plexflow.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.DirectoryNotEmptyException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.zip.CRC32;
import java.util.zip.CheckedInputStream;
import java.util.zip.CheckedOutputStream;

import org.eclipse.core.runtime.IProgressMonitor;

/**
 * Various auxiliary file I/O methods. Class contains only static methods.
 * 
 * @author libor
 *
 */
public abstract class IoUtils {
	
	//--------------------------------------------------------------------------
    // Consts
	//--------------------------------------------------------------------------
	
	/**
	 * Empirical determined optimal buffer size (value {@value}.)
	 */
	public static int BUFFER_SIZE = 8 * 1024;			// NIO.2 uses 8 KiB
	
	
	//--------------------------------------------------------------------------
    // CRC32 checksum
	//--------------------------------------------------------------------------
	
	public static long crc32(Path path) throws IOException {
		return crc32(path.toFile());
	}
	
	public static long crc32(String file) throws IOException {
		return crc32(new File(file));
	}
	
	public static long crc32(File file) throws IOException {		
		return crc32(new FileInputStream(file));
	}
	
	public static long crc32(InputStream is) throws IOException {
		byte[] buf = new byte[BUFFER_SIZE];
		CheckedInputStream cis = new CheckedInputStream(is, new CRC32());
		
		while (cis.read(buf) != -1) {};
		
		return cis.getChecksum().getValue();
	}
	
	/**
	 * Returns true whether both input stream are equal (have equal checksum).
	 * 
	 * @return true if streams are identical
	 * */ 
	public static boolean compareCrc32(InputStream is1, InputStream is2) throws IOException {
		return (crc32(is1) == crc32(is2));
	}
	
	/**
	 * Returns true whether both paths are equal (have equal checksum).
	 * Parameters must be regular files otherwise an exception is thrown.
	 * 
	 * @return true if files from path are identical
	 */
	public static boolean compareCrc32(Path path1, Path path2) throws IOException {
		return (crc32(path1) == crc32(path2));
	}
	
	/**
	 * Returns true whether both files are equal (have equal checksum).
	 * Parameters must be regular files otherwise an exception is thrown.
	 * 
	 * @return true if files from path are identical
	 * */
	public static boolean compareCrc32(String file1, String file2) throws IOException {
		return (crc32(file1) == crc32(file2));
	}
	
	
	//--------------------------------------------------------------------------
    // Filenames
	//--------------------------------------------------------------------------

	/**
	 * Produces native filesystem safe filename from unicode string.
	 * 
	 * Notably removes all accent chars, special chars, etc.
	 * 
	 * FILENAME param should contains ISO-8859-1 (US-ASCII) chars only! Unicode name must be send
	 * in additional "FILENAME*" (note asterisk) encoded in URL-style UTF-8. E.g.
	 * FILENAME "€ rates" will be "%e2%82%ac%20rates":
	 * resp.setHeader("Content-Disposition", "attachment;FILENAME=\"EUR rates\";FILENAME*=utf-8''%e2%82%ac%20rates");
	 * 
	 **/
	public static String safeFilename(String string) {
		//TODO: implement IoUtils.safeFilename()!
		return string;
	}
	
	
	//--------------------------------------------------------------------------
    // File and stream copiers
	//--------------------------------------------------------------------------
		
	/**
	 * High performance file copied internally buffering with optional progress monitoring.
	 * Overwrites target if already exists.
	 * 
	 * @param source
	 * @param target
	 * @param verify whether to verify copied file
	 * @param monitor or null if no progress reporting
	 * @return number of copied bytes
	 * @throws IOException re-thrown underlying IOException or verification failed. 
	 */
	public static final long copyFile(Path source, Path target, boolean verify, IProgressMonitor monitor) throws IOException {		
		// try-with-resources always close streams
		try (InputStream is = Files.newInputStream(source);
			 OutputStream os = Files.newOutputStream(target)) {			
			return copyStream(is, os, verify, monitor, Files.size(source));
		}
	}
		
	
	/**
	 * High performance stream copier. Already internally buffered, passing java.io.BufferedYX
	 * instances will corrupt output stream.
	 * 
	 * <p><em>Note: Closing streams is client's responsibility.</em></p>
	 * 
	 * @param is
	 * @param os
	 * @param verify whether to verify checksums of both streams
	 * @return number of copied bytes
	 * @throws IOException
	 */
	public static long copyStream(InputStream is, OutputStream os, boolean verify) throws IOException {
		return copyStream(is, os, verify, null, -1);
	}
	
	/**
	 * High performance stream copier with progress monitoring support.
	 * 
	 * <p>
	 * Already internally buffered, passing java.io.BufferedYX instances will
	 * corrupt output stream.
	 * </p>
	 * 
	 * <p>
	 * Progress is reported in 1% installment to reduce GUI progressbar repaint
	 * events.
	 * </p>
	 * 
	 * <p><em>Note:</em> Closing streams is client's responsibility.</p>
	 * 
	 * @param is
	 * @param os
	 * @param monitor
	 *            or null if no progress reporting.
	 * @param inputSize
	 *            Size in bytes of input stream. When no progress monitor (null)
	 *            is used, an arbitrary value can be passe but by convention a
	 *            -1 value is used.
	 * @param verify
	 *            Whether to verify checksum of output stream.
	 * @return number of copied bytes
	 * @throws IOException
	 * @throws IllegalArgumentException
	 *             If monitor != null and inputSize < 1.
	 */
	public static long copyStream(InputStream is, OutputStream os,
			boolean verify, IProgressMonitor monitor, long inputSize)
			throws IOException {
		long byteCounter = 0;		
		byte[] buffer = new byte[BUFFER_SIZE]; //new byte[BUFFER_SIZE];
        int readBytes;
        
        int percentDone = 0;
        
        if (monitor != null) {
        	if (inputSize < 1) {
        		throw new IllegalArgumentException("Specified input size (" +
    				inputSize + ") cannot be < 1!");
        	}
        	
        	monitor.beginTask(null, 100); 
        }
        
        if (verify) {
			is = new CheckedInputStream(is, new CRC32());
			os = new CheckedOutputStream(os, new CRC32());
		}
        
        while (-1 != (readBytes = is.read(buffer))) {
        	os.write(buffer, 0, readBytes);
        	byteCounter += readBytes;
        	
        	if (monitor != null) { 
        		// Report progress only greater then 1% to reduce GUI progressbar repaints
        		int percent = (int) (100 * byteCounter / inputSize);
        		if (percent > percentDone) {
        			monitor.worked(percent);
        			percentDone = percent;
        		}
        	}
        }		
        
        if (monitor != null) {
        	monitor.done();
        }
        
        if (verify) {
        	long isChecksum = ((CheckedInputStream) is).getChecksum().getValue();
        	long osChecksum = ((CheckedOutputStream) os).getChecksum().getValue();
        	
        	is.close();
        	os.close();
        	
        	if (isChecksum != osChecksum) {
        		throw new IOException("Checksum mismatched. Target corrupted.");
        	}
        }
		
		return byteCounter;
	}
	
	
	
	//--------------------------------------------------------------------------
    // Deletion
	//--------------------------------------------------------------------------
	
	
	/**
	 * Recursively deletes path elements.
	 * 
	 * <p>Folders are deleted only if they are empty. On first not empty
	 * folder is found, method quits.</p> 
	 * 
	 * <p>Uses NIO2.</p>
	 * 
	 * @param path
	 * @param whereStop Deletion will be stopped at this path element (this path element
	 * 		will be preserved). If complete path should be traversed for deletion,
	 * 		pass <tt>null</tt>.
	 * @throws IOException only when thrown by underlying I/O API.
	 */
	public static void deleteRecursivelyEmptyOnly(Path path, Path whereStop) throws IOException {
		// Stop on this element?
		if (whereStop != null) {
			if (path.equals(whereStop)) {
				return;
			}
		}
		
		try {
			Files.delete(path);			
		} catch (DirectoryNotEmptyException e) {
			// Quit if non empty folder found.
			return;
		}
		
		// Otherwise if root already reach, recursively call itself
		if (path.getParent() != null) {
			deleteRecursivelyEmptyOnly(path.getParent(), whereStop);
		}
	}
	
	
	/**
	 * Deletes recursively all files and folder even if they are not empty.
	 * 
	 * @param p Path to delete. Do nothing if path doesn't exist.
	 * @throws IOException re-thrown underlying exception
	 */
	public static void deleteRecursively(Path p) throws IOException {
		deleteRecursively(p.toFile());
	}
	
	// Little modified code from http://stackoverflow.com/a/779529/915931
	private static void deleteRecursively(File f) throws IOException {		
		if (f.isDirectory()) {
			for (File c : f.listFiles())
				deleteRecursively(c);
		}
		
		// OIO f.delete() doesn't throw a reason on failure as NIO.2 Files.delete() do so
		java.nio.file.Files.deleteIfExists(f.toPath());
	}
	
	
}
