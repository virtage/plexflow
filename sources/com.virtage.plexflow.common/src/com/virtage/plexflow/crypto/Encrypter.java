package com.virtage.plexflow.crypto;

import java.io.File;

import javax.crypto.SecretKey;

import com.virtage.plexflow.PxConstants;

/**
 * Encrypt and decrypt utility class.
 * 
 * @author libor
 */
public abstract class Encrypter {
	
	//--------------------------------------------------------------------------
    // Class fields - public
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Class fields - non-public
    //--------------------------------------------------------------------------
 
	private static volatile Encrypter impl;
	
    //--------------------------------------------------------------------------
    // Instance fields - public
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Instance fields - non-public
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Instance fields - bean properties
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Constructors or factories
    //--------------------------------------------------------------------------
 
	public static Encrypter getInstance() {
		if (impl == null) {
			synchronized (Encrypter.class) {
				if (impl == null) {

					// *** Try to locate seckey ***
					File seckeyFilePath = null;

					// a) at first in user-wide folder
					seckeyFilePath = new File(PxConstants.PATH_PX_HOME_FOLDER
							+ PxConstants.FILENAME_SECKEY);
					if (!seckeyFilePath.exists()) {

						// b) secondly in system-wide folder
						seckeyFilePath = new File(
								PxConstants.PATH_PX_ETC_FOLDER
										+ PxConstants.FILENAME_SECKEY);
						if (!seckeyFilePath.exists()) {
							// Nowhere
							throw new RuntimeException("Cannot find '"
									+ PxConstants.FILENAME_SECKEY
									+ "' in neither in '"
									+ PxConstants.PATH_PX_HOME_FOLDER
									+ "' or '"
									+ PxConstants.PATH_PX_ETC_FOLDER
									+ "' folder.");
						}
					}

					impl = new DesEncrypter(seckeyFilePath);
				}
			}
		}

		return impl;
	}
	
    //--------------------------------------------------------------------------
    // Public API - class (static)
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Public API - instance
    //--------------------------------------------------------------------------

	public abstract String encrypt(String str);

	public abstract String decrypt(String str);
	
	/** Serialize key to specified file. */
	public abstract void serKey(SecretKey key, File pathToKey);
	
	/** Serialize key to specified file. */
	public abstract SecretKey deserKey(File pathToKey);
	
	/** Serialize key to specified file. */
	public abstract SecretKey genKey();
	
	/** Returns hash of string (algorithm implementation depends). */
	public abstract String hash(String str);
	
    //--------------------------------------------------------------------------
    // Overrides and implementations
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Getters/setters of properties
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Helper methods (mostly private)
    //--------------------------------------------------------------------------

}
