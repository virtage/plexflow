package com.virtage.plexflow.crypto;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;


//**************************************************************************
// DES encryption implementation
//**************************************************************************	
/** Impl using DES. */
class DesEncrypter extends Encrypter {
	
	//--------------------------------------------------------------------------
    // Class fields - public
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Class fields - non-public
    //--------------------------------------------------------------------------
			
    //--------------------------------------------------------------------------
    // Instance fields - public
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Instance fields - non-public
    //--------------------------------------------------------------------------
 
	private Cipher ecipher;
	private Cipher dcipher;
	
    //--------------------------------------------------------------------------
    // Instance fields - bean properties
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Constructors or factories
    //--------------------------------------------------------------------------
 
	/** Deserialize key from file and create encrypted using DES algorithm */
	DesEncrypter(File seckeyFilePath) {			
		SecretKey key = deserKey(seckeyFilePath);
	
		try {
			ecipher = Cipher.getInstance("DES");
			ecipher.init(Cipher.ENCRYPT_MODE, key);
			dcipher = Cipher.getInstance("DES");
			dcipher.init(Cipher.DECRYPT_MODE, key);
			
		} catch (NoSuchAlgorithmException | NoSuchPaddingException
				| InvalidKeyException ex) {
			
			throw new RuntimeException(ex);
		}
	}
	
    //--------------------------------------------------------------------------
    // Public API - class (static)
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Public API - instance
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Overrides and implementations
    //--------------------------------------------------------------------------

	@Override
	public String encrypt(String str) {
		byte[] utf8;
		byte[] enc = null;

		try {
			utf8 = str.getBytes("UTF8");
			enc = ecipher.doFinal(utf8);
		} catch (UnsupportedEncodingException | IllegalBlockSizeException
				| BadPaddingException ex) {
			
			throw new RuntimeException(ex);
		}

		// return new sun.misc.BASE64Encoder().encode(enc);
		return new String(Hex.encodeHex(enc));
	}

	@Override
	public String decrypt(String str) {
		byte[] dec;
		byte[] utf8;
		String decrypted = null;

		try {
			// dec = new sun.misc.BASE64Decoder().decodeBuffer(str);
			dec = Hex.decodeHex(str.toCharArray());
			utf8 = dcipher.doFinal(dec);
			decrypted = new String(utf8, "UTF8");
		} catch (IOException | IllegalBlockSizeException
				| BadPaddingException | DecoderException ex) {

			throw new RuntimeException(ex);
		}

		return decrypted;
	}

	/** Serialize key to specified file. */
	@Override
	public void serKey(SecretKey key, File pathToKey) {
		try {
			ObjectOutputStream s = new ObjectOutputStream(
					new FileOutputStream(pathToKey));
			s.writeObject(key);
		} catch (IOException ex) {

			throw new RuntimeException(ex);
		}
	}

	/** Deserialize key from specified file. */
	@Override
	public SecretKey deserKey(File pathToKey) {
		SecretKey key = null;

		try {
			ObjectInputStream inputStream = new ObjectInputStream(
					new FileInputStream(pathToKey));
			key = (SecretKey) inputStream.readObject();
		} catch (IOException | ClassNotFoundException ex) {

			throw new RuntimeException(
					"Failed to deserialize key from file '" + pathToKey.getAbsolutePath() + "'",
					ex);
		}

		return key;
	}

	/** Generate new secret key. */
	@Override
	public SecretKey genKey() {
		SecretKey key = null;

		try {
			key = KeyGenerator.getInstance("DES").generateKey();
		} catch (NoSuchAlgorithmException ex) {

			throw new RuntimeException(ex);
		}

		return key;
	}
	
	@Override
	public String hash(String str) {
		return org.apache.commons.codec.digest.DigestUtils.md5Hex(str);
	}
	
    //--------------------------------------------------------------------------
    // Getters/setters of properties
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Helper methods (mostly private)
    //--------------------------------------------------------------------------
	
}