package com.virtage.plexflow.logging;


/*
*** Eclipse logging ***
http://wiki.eclipse.org/FAQ_How_do_I_use_the_platform_logging_facility%3F

Status status= new Status(IStatus.INFO, Activator.PLUGIN_ID, msg);
StatusManager.getManager().handle(status, StatusManager.LOG);	


Podle http://www.softwarepassion.com/eclipse-rcp-simple-logging-mechanism/:
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;

public class LogUtil {

    public static void logError(String msg){
        IStatus st = new Status(IStatus.ERROR,Activator.PLUGIN_ID, msg);
        Activator.getDefault().getLog().log(st);
    }
    public static void logInfo(String msg){
        IStatus st = new Status(IStatus.INFO,Activator.PLUGIN_ID, msg);
        Activator.getDefault().getLog().log(st);
    }
    public static void logWarning(String msg){
        IStatus st = new Status(IStatus.WARNING,Activator.PLUGIN_ID, msg);
        Activator.getDefault().getLog().log(st);
    }
}
*/


/**
 * Implementes ISysLog using Eclipse logging facility.
 * 
 * @author libor 
 */
@Deprecated
abstract class EclipseSysLog implements ISysLog {

	public EclipseSysLog() {
		throw new UnsupportedOperationException("Not implemented yet");
	}


}