package com.virtage.plexflow.logging;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.ConsoleHandler;
import java.util.logging.Formatter;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

@Deprecated
public class JdkSysLog implements ISysLog {
	
	private final Logger logger;
	private final String NO_MSG = "<no message>"; 
	
	public JdkSysLog(String loggerName, Level level) {
		this.logger = java.util.logging.Logger.getLogger(loggerName);
		this.logger.setUseParentHandlers(false);
		this.logger.setLevel(level);		
	
		ConsoleHandler handler = new ConsoleHandler();
		handler.setFormatter(new SysLogFormatter());
		handler.setLevel(level);		

		this.logger.addHandler(handler);
	}
	

	@Override
	public void fine(String msg) {
		this.logger.log(Level.FINE, msg);		
	}
	
	@Override
	public void fine(String msg, Throwable cause) {
		this.logger.log(Level.FINE, msg, cause);		
	}
	
	@Override
	public void fine(Throwable cause) {
		this.logger.log(Level.FINE, NO_MSG, cause);
	}

	@Override
	public void info(String msg) {
		this.logger.log(Level.INFO, msg);
	}
	
	@Override
	public void info(String msg, Throwable cause) {
		this.logger.log(Level.INFO, msg, cause);
	}
	
	@Override
	public void info(Throwable cause) {
		this.logger.log(Level.INFO, NO_MSG, cause);
	}

	@Override
	public void warn(String msg) {
		this.logger.log(Level.WARNING, msg);
	}
	
	@Override
	public void warn(String msg, Throwable cause) {
		this.logger.log(Level.WARNING, msg, cause);
	}
	
	@Override
	public void warn(Throwable cause) {
		this.logger.log(Level.WARNING, NO_MSG, cause);
	}

	@Override
	public void severe(String msg) {
		this.logger.log(Level.SEVERE, msg);
	}
	
	@Override
	public void severe(String msg, Throwable cause) {
		this.logger.log(Level.SEVERE, msg, cause);
	}
	
	@Override
	public void severe(Throwable cause) {
		this.logger.log(Level.SEVERE, NO_MSG, cause);
	}
	
	
	
	/**
	 * Formats as "[time][level][logger]: msg", time field is ISO 8601 format (yyyy-mm-dd hh:mm:ss).
	 */
	private static class SysLogFormatter extends Formatter {		
		@Override
		public String format(LogRecord record) {
			StringBuffer sb = new StringBuffer();

			SimpleDateFormat sdf = new SimpleDateFormat("yyy-MM-dd HH:mm:ss");
			String time = sdf.format(new Date(record.getMillis()));

			sb.append("[");
			sb.append(time);
			sb.append("][");
			sb.append(record.getLevel().getName());
			sb.append("][");
			sb.append(record.getLoggerName());
			sb.append("][");
			sb.append(record.getMessage());
			sb.append("]");
			
			if (record.getThrown() != null) {
				// Trick to output printStackTrace() but to string
				StringWriter sw = new StringWriter();
				PrintWriter pw = new PrintWriter(sw);
				record.getThrown().printStackTrace(pw);
				// Now available as sw.toString()				
				sb.append("[Caused by " + sw.toString() + "]");
			}
			
			sb.append(System.getProperty("line.separator")); // OS-independend new line

			return sb.toString();
		}		
	}
}