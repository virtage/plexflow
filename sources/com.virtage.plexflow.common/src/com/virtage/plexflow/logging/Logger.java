package com.virtage.plexflow.logging;

import java.util.logging.Level;

/**
 * Deprecated! Use <tt>org.slf4j.Logger</tt> instead! Example:
 * 
 * <pre>
 * private final static Logger SYSLOG = LoggerFactory.getLogger(SomeClass.class);
 * </pre>
 * 
 * @author libor
 *
 */
@Deprecated
public class Logger {	
	
	private Logger() {};
	
	/** Convenience method calling <tt>getSysLog(SomeClass.class.getName())</tt>. */
	public static ISysLog getSysLog(Class<?> forClass) {
		return getSysLog(forClass.getName());
	}
	
	public static ISysLog getSysLog(String loggerName) {
		return new JdkSysLog(loggerName, Level.ALL);
	}
	
//	public static IActivityLog getActivityLog(Object object) {
//		throw new UnsupportedOperationException("Not supported yet");
//	}
	
}
