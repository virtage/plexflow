package com.virtage.plexflow.logging;

/**
 * PxApplication log.
 * 
 * @author libor
 *
 */
public interface IActivityLog {
	public void logCreation(Object createdObject);
	public void logDeletion(Object deletedObject);
	
	/**
	 * 
	 * @param difference Textual human-readable description of change
	 */
	public void logChange(String difference);
}