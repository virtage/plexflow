package com.virtage.plexflow.logging;

/**
 * DEPRECATED! Use SLF4J instead!
 * 
 * System logger.
 * 
 * Usage:
 * <pre>
 * ISysLog log = Logging.getSysLog(TestHandler.class); * 
 * log.logSevere("toto je chyba");
 * log.logInfo("toto je info");
 * </pre>
 */
@Deprecated
public interface ISysLog {
	
	public void fine(String msg);
	public void fine(String msg, Throwable cause);
	public void fine(Throwable cause);
	
	public void info(String msg);
	public void info(String msg, Throwable cause);
	public void info(Throwable cause);
	
	public void warn(String msg);
	public void warn(String msg, Throwable cause);
	public void warn(Throwable cause);
	
	public void severe(String msg);
	public void severe(String msg, Throwable cause);
	public void severe(Throwable cause);
	
}