package com.virtage.plexflow.system;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import com.virtage.plexflow.PxConstants;

/**
 * Utility static methods to locate and read <tt>systems.properties</tt> file.
 * 
 * @author libor
 *
 */
public final class SystemsProperties {
	
	private SystemsProperties() {}

	/**
	 * Lists enabled systems from best-guessed systems.properties file. It is only shorthand for
	 * <tt>listAvailableSystems(locateFile())</tt>.
	 **/
	public static List<SystemsPropertiesRecord> listAvailableSystems() throws IllegalArgumentException {
		File file = SystemsProperties.locateFile();
		
		return SystemsProperties.listAvailableSystems(file);
	}

	/**
	 * <p>Tries to locate best guessed <tt>systems.properties</tt> from system property, or
	 * user-wide or system-wide file.</p>
	 * 
	 * <p>Search order is the following:</p>
	 * 
	 * <ol>
	 * <li>System property (-Dsomething to JVM) "com.virtage.plexflow.systemsProperties.path" (or
	 * value of {@link PxConstants#SYSPROP_SYSTEMS_PROPERTIES_PATH}).</li>
	 * <li>User-wide folder (OS-depend, e.g. /home/joe/.plexflow/systems.properties)</li>
	 * <li>System-wide folder (OS-depend, e.g. /opt/plexflow/plexflow/etc)</li>
	 * </ol>
	 * 
	 * @return found java.io.File to systems.properties
	 * @throws IllegalArgumentException if any problem found (illegal path, inaccessible etc.) 
	 */
	public static File locateFile()
			throws IllegalArgumentException {
		String systemPropertiesPath = System.getProperty(PxConstants.SYSPROP_SYSTEMS_PROPERTIES_PATH);
		File file = null;
		
		// (A) Sysprop not specified
		if (systemPropertiesPath == null) {
			// First try user-wide location
			file = new File(PxConstants.PATH_PX_HOME_FOLDER + PxConstants.FILENAME_SYSTEMS_PROPERTIES);
			if (!file.exists()) {						
				// Then try user-wide location
				file = new File(PxConstants.PATH_PX_ETC_FOLDER + PxConstants.FILENAME_SYSTEMS_PROPERTIES);
				if (!file.exists()) {						
					// If still not found
					throw new IllegalArgumentException("Required '" +
						PxConstants.FILENAME_SYSTEMS_PROPERTIES +
						"' file is not found '" +
						PxConstants.PATH_PX_HOME_FOLDER + PxConstants.FILENAME_SYSTEMS_PROPERTIES + "' or '" +
						PxConstants.PATH_PX_ETC_FOLDER + PxConstants.FILENAME_SYSTEMS_PROPERTIES + "'. " +
						"Specify full path in '" + PxConstants.SYSPROP_SYSTEMS_PROPERTIES_PATH +
						"' system property.");
				}
			}			
			
		// (B) Sysprop specified but empty
		} else if (systemPropertiesPath.isEmpty()) {
			throw new IllegalArgumentException("System property '" +
				PxConstants.SYSPROP_SYSTEMS_PROPERTIES_PATH + "' cannot be empty.");
			
		// (C) Sysprop specified and not empty
		} else {
			file = new File(systemPropertiesPath);
		
			if (!file.exists()) {
				throw new IllegalArgumentException("Required file '" + file.getAbsolutePath() +
					"' specified as value of system property '" + PxConstants.SYSPROP_SYSTEMS_PROPERTIES_PATH +
					"' cannot be found or is inaccessible.");
			}
		}
		return file;
	}

	/**
	 * Lists enabled systems from specified file.
	 *
	 * @return list of SystemsPropertiesRecord. <em>Never return null</em>. If no systems exist
	 * 		empty (<tt>List.isEmpty()</tt>) list is returned instead.
	 *
	 * @throws IllegalArgumentException if system.properties cannot be loaded/
	 * 		doesn't exists/has invalid content.
	 * 
	 * @see listAvailableSystems if you want to parse explicitly specified file.
	 **/
	public static List<SystemsPropertiesRecord> listAvailableSystems(File sysFile)
			throws IllegalArgumentException {		
	
		Properties props = new Properties();
	
		try {
			props.load(new FileInputStream(sysFile));
	
		} catch (IOException e) {
			throw new IllegalArgumentException("Failed to load properties from '" +
					sysFile.getAbsolutePath() + "' due to exception " +
					"(error code 4).", e);
		}
	
		/*
	    * Get defined systems. E.g.
	    *      systems=dev:test:prod
	    * so we will get
	    *      { "dev", "test", "prod" }
	    */
	    String rawNames = props.getProperty(SystemsPropertiesKeys.SYSTEMS_ID_LIST, null);
	    if (rawNames == null) {		// Quit, no systems defined
	    	throw new IllegalArgumentException("File '" + sysFile.getAbsolutePath() + "'" +
					"doesn't define any systems or is missing property '" +
					SystemsPropertiesKeys.SYSTEMS_ID_LIST + "' " +
					"(error code 5)");
	    }
	    String[] names = rawNames.split(SystemsPropertiesKeys.SYSTEMS_ID_SEPARATOR);
	
	    List<SystemsPropertiesRecord> sysInfos = new ArrayList<>();
	    // Text of exception
	    String iaePrefix = "Error during validation '" + sysFile.getAbsolutePath() + "': property '";
	    String iaeSuffix = "' is missing or is invalid.";
	
	    // Loop throught system names
	    for (String name : names) {
	
	    	// Keys must looks like "system.nameOfSystem.someProperty", e.g.
	    	// "system.dev.displayName = Plexflow DEV system"
	
	    	String propKey = SystemsPropertiesKeys.SYSTEM_PREFIX + "." + name + "." +
	    			SystemsPropertiesKeys.SYSTEM_DISPLAY_NAME;
	        String displayName = props.getProperty(propKey);
	        if (displayName == null)
	        	throw new IllegalArgumentException(iaePrefix + propKey + iaeSuffix);
	
	        propKey = SystemsPropertiesKeys.SYSTEM_PREFIX  + "." + name + "." +
	        		SystemsPropertiesKeys.SYSTEM_JDBC_URL;
	        String jdbcUrl = props.getProperty(propKey);
	        if (jdbcUrl == null)
	        	throw new IllegalArgumentException(iaePrefix + propKey + iaeSuffix);
	
	        propKey = SystemsPropertiesKeys.SYSTEM_PREFIX  + "." + name + "." +
	        		SystemsPropertiesKeys.SYSTEM_JDBC_USER;
	        String jdbcUser = props.getProperty(propKey);
	        if (jdbcUser == null)
	        	throw new IllegalArgumentException(iaePrefix + propKey + iaeSuffix);
	
	        propKey = SystemsPropertiesKeys.SYSTEM_PREFIX  + "." + name + "." +
	        		SystemsPropertiesKeys.SYSTEM_JDBC_KEY;
	        String jdbcKey = props.getProperty(propKey);
	        if (jdbcKey == null)
	        	throw new IllegalArgumentException(iaePrefix + propKey + iaeSuffix);
	
	        propKey = SystemsPropertiesKeys.SYSTEM_PREFIX  + "." + name + "." +
	        		SystemsPropertiesKeys.SYSTEM_DISABLED;
	        boolean disabled = Boolean.valueOf(props.getProperty(propKey));
	
	        if (!disabled) {
	        	// name is systemID
	        	sysInfos.add(new SystemsPropertiesRecord(name, displayName, jdbcUrl, jdbcUser, jdbcKey));
	        }
	    }
	
		return sysInfos;
	}

}
