package com.virtage.plexflow.system;

import java.io.IOException;
import java.io.Reader;
import java.util.Properties;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import com.virtage.plexflow.crypto.Encrypter;

/**
 * Simple structure to hold record in <tt>systems.properties</tt> file. In addition to getters it
 * provides {@link #createSqlSessionFactory()} method building MyBatis SqlSessionFactory from this
 * instance system ID.
 *
 * @author libor
 */
public class SystemsPropertiesRecord {

	//--------------------------------------------------------------------------
    // Class fields - public
    //--------------------------------------------------------------------------

    //--------------------------------------------------------------------------
    // Class fields - non-public
    //--------------------------------------------------------------------------

    //--------------------------------------------------------------------------
    // Instance fields - public
    //--------------------------------------------------------------------------

    //--------------------------------------------------------------------------
    // Instance fields - non-public
    //--------------------------------------------------------------------------

	private final String systemId, displayName, jdbcUrl, jdbcUser, jdbcKey;

    //--------------------------------------------------------------------------
    // Instance fields - bean properties
    //--------------------------------------------------------------------------

    //--------------------------------------------------------------------------
    // Constructors, static initializers, factories
    //--------------------------------------------------------------------------

	SystemsPropertiesRecord(String systemId, String displayName, String jdbcUrl, String jdbcUser, String jdbcKey) {
		this.systemId = systemId;
		this.displayName = displayName;
		this.jdbcUrl = jdbcUrl;
		this.jdbcUser = jdbcUser;
		this.jdbcKey = jdbcKey;
	}

    //--------------------------------------------------------------------------
    // Public API - class (static)
    //--------------------------------------------------------------------------

	

	//--------------------------------------------------------------------------
    // Public API - instance
    //--------------------------------------------------------------------------

	/**
	 * Builds MyBatis's SqlSessionFactory from this system info's database information.
	 * 
	 * @throws IOException if creating SqlSessionFactoryBuilder from XML file failed
	 * 		(file doesn't exists etc.).  
	 * 
	 */
	public SqlSessionFactory createSqlSessionFactory() throws IOException {
		Properties props = new Properties();
		
		props.setProperty("url", this.jdbcUrl);
		props.setProperty("username", this.jdbcUser);
		props.setProperty("password", Encrypter.getInstance().decrypt(this.jdbcKey));		
		
		SqlSessionFactory factory = null;	// factory to return will be here
		
		/*
        -------------------------------------------------------------------
        Locating and loading mybatis-config.xml
        -------------------------------------------------------------------

        Normally in non-Eclipse app MyBatis config XML is loaded as easy as:

                String resource = "resources/mybatis-config.xml";
                Reader reader = Resources.getResourceAsReader(resource);
                factory = new SqlSessionFactoryBuilder().build(reader, props);

        Where resources is folder in project root. In Eclipse plug-in world this cannot be
        used and causes ClassNotFoundException because different classloader manage
        resources dir and Java folder package hierarchy dirs.
        
        Solution 1) ------------------------------------------------------------

        Thus it's necessary to use appropriate classloader. Hack bellow is inspired from
        http://www.eclipsezone.com/eclipse/forums/t61831.html post.
        
	        Thread currentThread = Thread.currentThread();
			ClassLoader oldLoader = currentThread.getContextClassLoader();
	
			try {
				currentThread.setContextClassLoader(
						SystemsPropertiesRecord.class.getClassLoader());
	        
	        	// Library client code here - START
	        	 
	         	String resource = "resources/mybatis-config.xml";
				Reader reader = Resources.getResourceAsReader(resource);
				factory = new SqlSessionFactoryBuilder().build(reader, props);
				
				// Library client code here - END
	        
	        } finally {
				currentThread.setContextClassLoader(oldLoader);
			}
        
        
        
        Solution 2) ------------------------------------------------------------
        ! Observed that no solution: !
        	MyBatis cannot find any referred mapper interfaces (ClassNotFoundException).
        	Probably something with classloaders.        
        
        Update by Libor, 2012-06-29: Investigate using FileLocator:
        
        Example how to print out a file from bundle:------------------------
        
	        URL confURL = bundleContext.getBundle().getEntry("OSGI-INF/component.xml");
			FileLocator.toFileURL(confURL).getFile();			
			
			try (BufferedReader reader = new BufferedReader(new InputStreamReader(
					FileLocator.openStream(bundleContext.getBundle(), new Path("OSGI-INF/component.xml"), false)));) {
				
				String line;
				while ((line = reader.readLine()) != null) {
					System.out.println(line);
				}
			}
			
		Example how to load MyBatis config:
			
			String xmlLocation = "resources/mybatis-config.xml";		
			SqlSessionFactory sqlSessionFactory = null;
			try (InputStream is = FileLocator.openStream(context.getBundle(), new Path(xmlLocation), false)) {		
				sqlSessionFactory = new SqlSessionFactoryBuilder().build(is);        
			}	
			
        */	

		
		Thread currentThread = Thread.currentThread();
		ClassLoader oldLoader = currentThread.getContextClassLoader();

		try {
			currentThread.setContextClassLoader(
					SystemsPropertiesRecord.class.getClassLoader());
        
        	// Library client code here - START
        	 
         	String resource = "resources/mybatis-config.xml";
			Reader reader = Resources.getResourceAsReader(resource);
			factory = new SqlSessionFactoryBuilder().build(reader, props);
			
			// Library client code here - END
        
        } finally {
			currentThread.setContextClassLoader(oldLoader);
		}
		
		if (factory == null) {
			throw new IOException("Factory cannot be built");
		}
		
		return factory;
	}

    //--------------------------------------------------------------------------
    // Overrides and implementations
    //--------------------------------------------------------------------------

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((systemId == null) ? 0 : systemId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SystemsPropertiesRecord other = (SystemsPropertiesRecord) obj;
		if (systemId == null) {
			if (other.systemId != null)
				return false;
		} else if (!systemId.equals(other.systemId))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "SystemsPropertiesRecord [systemId=" + systemId + ", displayName=" + displayName
				+ ", jdbcUrl=" + jdbcUrl + ", jdbcUser=" + jdbcUser
				+ ", jdbcKey=" + jdbcKey + "]";
	}


    //--------------------------------------------------------------------------
    // Getters/setters of properties
    //--------------------------------------------------------------------------

	public String getSystemId() {
		return systemId;
	}

	public String getDisplayName() {
		return displayName;
	}
	
	public String getJdbcUrl() {
		return jdbcUrl;
	}

	public String getJdbcUser() {
		return jdbcUser;
	}

	public String getJdbcKey() {
		return jdbcKey;
	}

    //--------------------------------------------------------------------------
    // Helper methods (mostly private)
    //--------------------------------------------------------------------------
	
}