package com.virtage.plexflow.system;

import java.io.IOException;
import java.util.Date;
import java.util.GregorianCalendar;

import net.jcip.annotations.Immutable;

import org.apache.ibatis.session.SqlSessionFactory;

/**
 * Simple immutable structure to hold information about authenticated and logged system returned
 * from {@link ILoginService#login(SystemsPropertiesRecord, User)} method.
 *
 */
@Immutable
public final class SystemInstance {
	
	//--------------------------------------------------------------------------
    // Class fields - public
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Class fields - non-public
    //--------------------------------------------------------------------------
  
    //--------------------------------------------------------------------------
    // Instance fields - public
    //-------------------------------------------------------------------------- 
       
    //--------------------------------------------------------------------------
    // Instance fields - non-public
    //--------------------------------------------------------------------------
	
    private final SystemsPropertiesRecord systemsPropertiesRecord;
    
    private final SqlSessionFactory sqlSessionFactory;
    
    private final User user;
    
    private final String cleartextPassword;
    
    private final Date logonTime;
    
    //--------------------------------------------------------------------------
    // Instance fields - bean properties
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Constructors, static initializers, factories
    //--------------------------------------------------------------------------
 
    public SystemInstance(SystemsPropertiesRecord systemsPropertiesRecord, User user)
    				throws IOException {
    	this(systemsPropertiesRecord, user, null);
	}
    
    public SystemInstance(
    		SystemsPropertiesRecord systemsPropertiesRecord, User user, String cleartextPassword)
    				throws IOException {
		this.systemsPropertiesRecord = systemsPropertiesRecord;
		this.sqlSessionFactory = systemsPropertiesRecord.createSqlSessionFactory();
		this.user = user;
		this.cleartextPassword = cleartextPassword;
		this.logonTime = GregorianCalendar.getInstance().getTime();
	}
    
    //--------------------------------------------------------------------------
    // Public API - class (static)
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Public API - instance
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Overrides and implementations
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Getters/setters of properties
    //--------------------------------------------------------------------------
 
    /** Returns detail of currently system instance from systems.properties. */
	public SystemsPropertiesRecord getSystemsPropertiesRecord() {
		return systemsPropertiesRecord;
	}	
	
	/** Returns MyBatis SqlSessionFactory constructed for this system instance. */
	public SqlSessionFactory getSqlSessionFactory() {
		return sqlSessionFactory;
	}

	/** User that is authenticated and currently working with this system instance. */
	public User getUser() {
		return user;
	}
	
	/**
	 * Gets current user's password as cleartext (not hashed). Used for special cases when
	 * not hashed password is needed (notably server client).
	 * 
	 * @throws IllegalStateException if asked when running using builtin super-user account.
	 */
	public String getUserCleartextPassword() {
		if (cleartextPassword == null)
			throw new IllegalStateException("Builtin super-user password as clear-text can't be provided due to security restriction.");
		
		return cleartextPassword;
	}

	/** Time when a user logged in this system instance. */
	public Date getLogonTime() {
		return logonTime;
	}
    
    //--------------------------------------------------------------------------
    // Helper methods (mostly private)
    //--------------------------------------------------------------------------
}
