package com.virtage.plexflow.system.impl;

import org.apache.ibatis.session.SqlSession;

import com.virtage.plexflow.system.SystemInstanceFactory;
import com.virtage.plexflow.system.User;

public class UserDao implements UserMapper {

	//--------------------------------------------------------------------------
	// Class fields - public
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Class fields - non-public
	//--------------------------------------------------------------------------

	//private static final Logger SYSLOG = LoggerFactory.getLogger(ThisClass.class);

	//--------------------------------------------------------------------------
	// Instance fields - public
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Instance fields - non-public
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Instance fields - bean properties
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Constructors, static initializers, factories
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Public API - class (static)
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Public API - instance
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Overrides and implementations
	//--------------------------------------------------------------------------

	@Override
	public User findUserByUsername(String username) {		
		SqlSession session = SystemInstanceFactory.getInstance().getSqlSessionFactory().openSession();		
		
		User user = null;
		try {
			user = session.getMapper(UserMapper.class).findUserByUsername(username);
			
		} finally {
			session.close();
		}
		
		return user;
	}

	@Override
	public User getSu() {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException("Not implemented yet.");
	}

	@Override
	public boolean existsUser(String username, String passwordHash) {
		boolean result = false;
		SqlSession session = SystemInstanceFactory.getInstance().getSqlSessionFactory().openSession();		
		
		try {
			result = session.getMapper(UserMapper.class).existsUser(username, passwordHash);
			
		} finally {
			session.close();
		}
		
		return result;
	}
	
	//--------------------------------------------------------------------------
	// Consider toString(), equals(), hashCode()
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Getters/setters of properties
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Helper methods (mostly private)
	//--------------------------------------------------------------------------
}
