package com.virtage.plexflow.system.impl;

import java.io.IOException;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.ibatis.session.SqlSession;

import com.virtage.plexflow.system.BadUsernameOrPasswordException;
import com.virtage.plexflow.system.ILoginService;
import com.virtage.plexflow.system.LoginException;
import com.virtage.plexflow.system.SystemInstance;
import com.virtage.plexflow.system.SystemsPropertiesRecord;
import com.virtage.plexflow.system.User;

/** Implements internal authentication mechanism with database table with passwords. */
public class InternalLoginService implements ILoginService {

	//--------------------------------------------------------------------------
    // Class fields - public
    //--------------------------------------------------------------------------

    //--------------------------------------------------------------------------
    // Class fields - non-public
    //--------------------------------------------------------------------------

    //--------------------------------------------------------------------------
    // Instance fields - public
    //--------------------------------------------------------------------------

    //--------------------------------------------------------------------------
    // Instance fields - non-public
    //--------------------------------------------------------------------------

    //--------------------------------------------------------------------------
    // Instance fields - bean properties
    //--------------------------------------------------------------------------

    //--------------------------------------------------------------------------
    // Constructors, static initializers, factories
    //--------------------------------------------------------------------------

    //--------------------------------------------------------------------------
    // Public API - class (static)
    //--------------------------------------------------------------------------

    //--------------------------------------------------------------------------
    // Public API - instance
    //--------------------------------------------------------------------------

    //--------------------------------------------------------------------------
    // Overrides and implementations
    //--------------------------------------------------------------------------
	
	@Override
	public SystemInstance login(SystemsPropertiesRecord spRecord, String username, String cleartextPassword)
			throws BadUsernameOrPasswordException, LoginException {	
		
		SqlSession session = null;
		boolean logged = false;
		User user = null;
		
		String hashedPassword = DigestUtils.md5Hex(cleartextPassword);
		
		try {
			session = spRecord.createSqlSessionFactory().openSession();
			logged = session.getMapper(UserMapper.class).existsUser(username, hashedPassword);
			user = session.getMapper(UserMapper.class).findUserByUsername(username);		
			
		} catch (Exception e) {
			throw new LoginException("Cannot login due to underlying database " +
					"exception (error code 1).", e);

		} finally {
			if (session != null) session.close();
		}

		if (!logged) {
			throw new BadUsernameOrPasswordException();
		}

		// Hurrah! If here, we're authenticated, thus set-up environment to return
		try {
			return new SystemInstance(spRecord, user, cleartextPassword);
		} catch (IOException e) {
			throw new LoginException("Failed to instantiate SystemInstance", e);
		}
	}
	
	
	@Override
	public SystemInstance loginAsSu(SystemsPropertiesRecord spRecord)
			throws LoginException {
	
		SqlSession session = null;
		User user = null;
		
		try {
			session = spRecord.createSqlSessionFactory().openSession();
			user = session.getMapper(UserMapper.class).getSu();
			
		} catch (Exception e) {
			throw new LoginException("Cannot login due to underlying database " +
					"exception (error code 1).", e);

		} finally {
			if (session != null) session.close();
		}		

		// Set-up environment to return
		try {
			return new SystemInstance(spRecord, user);
		} catch (IOException e) {
			throw new LoginException("Failed to instantiate SystemInstance", e);
		}
	}
	
    //--------------------------------------------------------------------------
    // Getters/setters of properties
    //--------------------------------------------------------------------------

    //--------------------------------------------------------------------------
    // Helper methods (mostly private)
    //--------------------------------------------------------------------------

}
