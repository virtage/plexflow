package com.virtage.plexflow.system.impl;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.virtage.plexflow.system.User;

public interface UserMapper {
	// USER is SQL keyword, so it's needed to double quote it for Derby DB
	
	@Select("SELECT * FROM \"USER\" WHERE USERNAME = #{username}")
	public User findUserByUsername(@Param("username") String username);
	
	@Select("SELECT * FROM \"USER\" WHERE USERNAME = 'pxsu'")
	public User getSu();

	/** True if password hash for particular username matches value in DB table. */
	@Select("SELECT CASE WHEN COUNT(*) = 1 THEN TRUE ELSE FALSE END " + 
			"FROM \"USER\" WHERE USERNAME = #{username} AND PASSWORD_HASH = #{hash}")
	public boolean existsUser(@Param("username") String username, @Param("hash") String passwordHash);		
}