package com.virtage.plexflow.system;


/** Set of constants for key names used in systems.properties file. */
interface SystemsPropertiesKeys {	
	public final String  SYSTEMS_ID_LIST = "systems";
	public final String  SYSTEMS_ID_SEPARATOR = ":";
	public final String  SYSTEM_PREFIX = "system";
	public final String  SYSTEM_DISPLAY_NAME = "displayName";
	public final String  SYSTEM_JDBC_URL = "jdbcUrl";
	public final String  SYSTEM_JDBC_USER = "jdbcUser";
	public final String  SYSTEM_JDBC_KEY = "jdbcKey";
	public final String  SYSTEM_DISABLED = "disabled";
	public final boolean SYSTEM_DISABLED_DEFAULT = false;
}
