package com.virtage.plexflow.system;

/**
 * Signals failure during authentication like database or IO error. Just bad username or password
 * is reported by {@link BadUsernameOrPasswordException} instead!
 */
public class LoginException extends Exception {

	private static final long serialVersionUID = -4591017689311730638L;

	public LoginException(String message, Throwable cause) {
		super(message, cause);
	}
	
	

}
