package com.virtage.plexflow.system;

import net.jcip.annotations.ThreadSafe;

import com.virtage.plexflow.system.impl.InternalLoginService;

@ThreadSafe
public class LoginService implements ILoginService {

	private static volatile ILoginService instance;
	
	
	public static ILoginService getInstance() {
		if (instance == null) {
			synchronized (LoginService.class) {
				if (instance == null) {
					// MAYBE: Later implementation will be contributed from extension point
					instance = new InternalLoginService();
				}
			}			
		}
		
		return instance;
	}
	
	@Override
	public SystemInstance login(SystemsPropertiesRecord sysinfo, String username, String notHashedPassword)
			throws BadUsernameOrPasswordException, LoginException {		
		return instance.login(sysinfo, username, notHashedPassword);
	}
	
	@Override
	public SystemInstance loginAsSu(SystemsPropertiesRecord spRecord)
			throws LoginException {
		return instance.loginAsSu(spRecord);
	}

}
