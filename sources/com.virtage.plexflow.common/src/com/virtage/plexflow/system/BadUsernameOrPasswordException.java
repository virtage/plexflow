package com.virtage.plexflow.system;

public class BadUsernameOrPasswordException extends Exception {

	private static final long serialVersionUID = 224049365565683233L;

	public BadUsernameOrPasswordException() {
		super("Username or password is not valid.");
	}

}
