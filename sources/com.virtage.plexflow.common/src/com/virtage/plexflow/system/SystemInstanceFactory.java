package com.virtage.plexflow.system;

import net.jcip.annotations.ThreadSafe;

@ThreadSafe
public final class SystemInstanceFactory {
	
	private static volatile SystemInstance instance;
	
	
	private SystemInstanceFactory() {}
	

	public static void setInstance(SystemInstance i) {
		if (i == null) {
			throw new IllegalArgumentException("Cannot set nullable instance.");
		}
		
		synchronized (SystemInstanceFactory.class) {
			instance = i; 
		}
	}
	
	public static synchronized SystemInstance getInstance() {
		if (instance == null) {
			throw new IllegalStateException("Cannot return SystemInstance because it has not " +
				"been set yet.");
		}
		
		return instance;
	}
}
