package com.virtage.plexflow.system;


/**
 * Simple interface with single {@link #login(SystemsPropertiesRecord, User)} method.
 * 
 * This package ships with internal login service (USERS db table).
 * 
 * Additional implementations can be set through JDK6+ ServiceLoader facility. Read e.g.
 * http://www.javaspecialists.co.za/archive/newsletter.do?issue=139
 * 
 */
public interface ILoginService {
	
	/**
	 * Authenticate supplied user against suppllied system.
	 * 
	 * @param spRecord choosen system from systems.properties file
	 * @param username 
	 * @param notHashedPassword cleartext password. Will be internally hashed or encrypted using
	 * 		correct algorithm.
	 * @return SystemInstance. Never returns null (exception is thrown in case of error).
	 * @throws BadUsernameOrPasswordException if credentials were incorrect
	 * @throws LoginException if some exception occured (database, IO, etc.)
	 */
	public SystemInstance login(SystemsPropertiesRecord spRecord, String username, String notHashedPassword)
			throws BadUsernameOrPasswordException, LoginException;
	
	/**
	 * Login with builtin superuser account. Does not require username and password
	 * however trying to obtain password with {@link SystemInstance#getUserCleartextPassword()}
	 * will throw security exception. 
	 * 
	 * @return SystemInstance. Never returns null (exception is thrown in case of error). 
	 * @throws LoginException if something went wrong (database, IO, etc.)
	 */
	public SystemInstance loginAsSu(SystemsPropertiesRecord spRecord) throws LoginException;
	
}