package com.virtage.plexflow.system;

import org.apache.ibatis.session.SqlSession;

import com.virtage.plexflow.system.impl.UserDao;
import com.virtage.plexflow.system.impl.UserMapper;

/**
 * User object representation.
 * 
 * If you wish to check credentials of some user call static {@link #authenticate(String, String)}.
 * 
 * @author libor
 *
 */
public class User {

	//--------------------------------------------------------------------------
    // Class fields - public
    //--------------------------------------------------------------------------

    //--------------------------------------------------------------------------
    // Class fields - non-public
    //--------------------------------------------------------------------------

	private static final long serialVersionUID = -707268290359888693L;
	
	private static final UserDao userDao = new UserDao();

    //--------------------------------------------------------------------------
    // Instance fields - public
    //--------------------------------------------------------------------------

    //--------------------------------------------------------------------------
    // Instance fields - non-public
    //--------------------------------------------------------------------------

    //--------------------------------------------------------------------------
    // Instance fields - bean properties
    //--------------------------------------------------------------------------

	private String username;
	private String passwordHash;
	private boolean su;
	private String firstname;
	private String lastname;
	private String displayName;

    //--------------------------------------------------------------------------
    // Constructors or factories
    //--------------------------------------------------------------------------

	private User() {}
	
	public User(
			String username,
			String passwordHash,
			boolean su,
			String firstname,
			String lastname,
			String displayName) {		
		this.username = username;
		this.passwordHash = passwordHash;
		this.su = su;
		this.firstname = firstname;
		this.lastname = lastname;
		this.displayName = displayName;
	}

    //--------------------------------------------------------------------------
    // Public API - class (static)
    //--------------------------------------------------------------------------
		

	/** Returns true if username and password hash match. */
	public static boolean authenticate(String username, String passwordHash) {				
		return userDao.existsUser(username, passwordHash);
	}
	
	
	/**
	 * Lookups user by his/her username. 
	 * 
	 * @param username Searched username.
	 * @return User or <tt>null</tt> if no such user exists.
	 */
	public static User findUserByUsername(String username) {
		return userDao.findUserByUsername(username);
	}
	
    //--------------------------------------------------------------------------
    // Public API - instance
    //--------------------------------------------------------------------------

    //--------------------------------------------------------------------------
    // Overrides and implementations
    //--------------------------------------------------------------------------
	
	@Override
	public String toString() {
		return "User [username=" + username + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((username == null) ? 0 : username.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (username == null) {
			if (other.username != null)
				return false;
		} else if (!username.equalsIgnoreCase(other.username))
			return false;
		return true;
	}

    //--------------------------------------------------------------------------
    // Getters/setters of properties
    //--------------------------------------------------------------------------

	public String getUsername() {
		return username;		
	}
	
	public String getPasswordHash() {
		return passwordHash;
	}
	/** Is superuser */
	public boolean isSu() {
		return su;
	}
	public String getFirstname() {
		return firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public String getDisplayName() {
		if (displayName == null)
			return username;
			
		return displayName;
	}

	// Private setters for MyBatis
	
	private void setUsername(String username) {
		this.username = username;
	}

	private void setPasswordHash(String passwordHash) {
		this.passwordHash = passwordHash;
	}

	private void setSu(boolean su) {
		this.su = su;
	}

	private void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	private void setLastname(String lastname) {
		this.lastname = lastname;
	}

	private void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	
	
    //--------------------------------------------------------------------------
    // Helper methods (mostly private)
    //--------------------------------------------------------------------------

}
