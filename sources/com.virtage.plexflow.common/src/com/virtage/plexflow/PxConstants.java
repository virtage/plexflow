package com.virtage.plexflow;

import org.apache.commons.lang3.SystemUtils;

public interface PxConstants {


	//--------------------------------------------------------------------------
    // Misc
    //--------------------------------------------------------------------------
	
	
	//--------------------------------------------------------------------------
    // Standard paths and filenames.
	// 
	// Notation:
	// 		* if it is path to file or folder, use "PATH_" prefix.
	// 		* if it is filename, use "FILENAME_" prefix.
    //--------------------------------------------------------------------------
	
	/**
	 * User-wide Plexflow folder (with trailing /). Value is
	 * {@code System.getProperty("user.home") + ".plexflow/"}.	
	 **/
	public final String PATH_PX_HOME_FOLDER = System.getProperty("user.home") + "/.plexflow/";
	
	/**
	 * System-wide Plexflow configuration folder (with trailing /). Value for Unix OSes is
	 * <tt>/opt/plexflow/etc</tt>, for Windows OSes is <tt>C:\Program Files\plexflow\etc</tt>.
	 */
	public final String PATH_PX_ETC_FOLDER =
			SystemUtils.IS_OS_UNIX ? "/opt/plexflow/etc/" : "C:/Program Files/plexflow/etc/";
		
	/**
	 * Filename of seckey (value {@value}). 
	 */
	public final String FILENAME_SECKEY = "seckey";	
	
	/**
	 * 
	 */	
	
	/** system.properties default filename (value {@value}). */
	public final String FILENAME_SYSTEMS_PROPERTIES = "systems.properties";    
    
	
	//--------------------------------------------------------------------------
    // System properties key names (for System.getProperty())
	// 
	// Notation:
	// 		* "SYSPROP_" prefix.
    //--------------------------------------------------------------------------
	
	/**
	 * Absolute path to file used as system.properties file. 
	 **/
	public final String SYSPROP_SYSTEMS_PROPERTIES_PATH = "com.virtage.plexflow.systemsProperties.path";
	
}
