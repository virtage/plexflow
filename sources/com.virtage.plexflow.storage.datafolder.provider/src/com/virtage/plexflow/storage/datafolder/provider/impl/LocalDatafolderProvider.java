package com.virtage.plexflow.storage.datafolder.provider.impl;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.core.runtime.IProgressMonitor;

import com.virtage.plexflow.config.ConfigException;
import com.virtage.plexflow.config.ConfigFactory;
import com.virtage.plexflow.storage.datafolder.provider.DatafolderProviderException;
import com.virtage.plexflow.storage.datafolder.provider.IDatafolderProvider;
import com.virtage.plexflow.utils.IoUtils;

/**
 * Pure local filesystem layer {@link IDatafolderProvider} implementation.
 * 
 * Intended to be wrapped into logic enforcing object that honor lock state,
 * concurrent access and so on.
 * 
 * @author libor
 * 
 */
public class LocalDatafolderProvider implements IDatafolderProvider {
	
	//--------------------------------------------------------------------------
	// Class fields - public
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Class fields - non-public
	//--------------------------------------------------------------------------

	//private static final ISysLog SYSLOG = Logging.getSysLog(ThisClass.class);

	//--------------------------------------------------------------------------
	// Instance fields - public
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Instance fields - non-public
	//--------------------------------------------------------------------------
	public final Path PATH_DATAFOLDER_ROOT;
 	public final Path PATH_FILES_SUBFOLDER;
 	public final Path PATH_TMP_SUBFOLDER;
 	public final Path PATH_VERS_SUBFOLDER;
 	public final Path PATH_THUMBS_SUBFOLDER;
 	
	//--------------------------------------------------------------------------
	// Instance fields - bean properties
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Constructors, static initializers, factories
	//--------------------------------------------------------------------------

	public LocalDatafolderProvider() throws DatafolderProviderException {
		Path datafolderPath;
		
		try {
			// *** Verify datafolder config key and existence of path *** 
			String datafolderString = ConfigFactory.getGlobal().getString(
					DatafolderConfigKeys.DATAFOLDER);
			if (StringUtils.isBlank(datafolderString)) {
				throw new ConfigException(DatafolderConfigKeys.DATAFOLDER,
						datafolderString);
			}

			datafolderPath = Paths.get(datafolderString);
			if (Files.notExists(datafolderPath)) {
				throw new ConfigException("Config key '"
						+ DatafolderConfigKeys.DATAFOLDER
						+ "' refers to invalid or inaccessible path '"
						+ datafolderString + "'.");
			}
		} catch (ConfigException e) {
			throw new DatafolderProviderException(e);
		}		
		
		// *** Prepare paths to datafolder subfolders ***
		// Package visibility due to tests
		PATH_DATAFOLDER_ROOT  = datafolderPath;
		PATH_FILES_SUBFOLDER  = datafolderPath.resolve(DatafolderConsts.SUBFOLDER_FILES);
		PATH_TMP_SUBFOLDER    = datafolderPath.resolve(DatafolderConsts.SUBFOLDER_TMP);
		PATH_VERS_SUBFOLDER   = datafolderPath.resolve(DatafolderConsts.SUBFOLDER_VERS);
		PATH_THUMBS_SUBFOLDER = datafolderPath.resolve(DatafolderConsts.SUBFOLDER_THUMBS);		
	}
	
	//--------------------------------------------------------------------------
	// Public API - class (static)
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Public API - instance
	//--------------------------------------------------------------------------

	/**
     * Constructs relative sharded path from ID. For example ID 9223372036854775807 will
     * become 92/23/37/20/36/85/47/75/80/7 path.
     **/
	public String shardPathForId(long id) {
    	String idString = String.valueOf(id);
    	StringBuffer path = new StringBuffer(19);	// Long.MAX_VALUE is 19 digits length
    	
    	int i = 0;
    	while (i < (idString.length() - 2)) {
    		path.append(idString.substring(i, i + 2));
    		path.append("/");
    		i = i + 2;
    	}

    	if (i < idString.length())
    		path.append(idString.substring(i, idString.length()));
    	
    	return path.toString();
    }
	
	//--------------------------------------------------------------------------
	// Overrides and implementations
	//--------------------------------------------------------------------------

	@Override
	public void delete(long fileId) throws DatafolderProviderException {
		try {
			deleteHandler(fileId);
		} catch (IOException e) {
			throw new DatafolderProviderException(e);
		}
	}

	@Override
	public InputStream pull(long fileId) throws DatafolderProviderException {		
		try {
			return pullHandler(fileId);
		} catch (IOException e) {
			throw new DatafolderProviderException(e);
		}
	}

	@Override
	public long pushBegin(long fileId, InputStream is,
			IProgressMonitor monitor, long inputSize) throws DatafolderProviderException {
		try {
			return pushBeginHandler(fileId, is, monitor, inputSize);
		} catch (IOException e) {
			throw new DatafolderProviderException(e);
		}
	}

	@Override
	public void pushCommit(long fileId) throws DatafolderProviderException {
		try {
			pushCommitHandler(fileId);
		} catch (IOException e) {
			throw new DatafolderProviderException(e);
		}
	}

	@Override
	public void pushRollback(long fileId) throws DatafolderProviderException {
		try {
			pushRollbackHandler(fileId);
		} catch (IOException e) {
			throw new DatafolderProviderException(e);
		}
	}
	
	//--------------------------------------------------------------------------
    // Query methods
    //--------------------------------------------------------------------------

	/**
	 * Counts CRC32 checksum of file in files/.
	 *  
	 **/
	@Override
	public long countCRC32inFiles(long fileId) throws DatafolderProviderException {
		// 0. Sharded path for file ID e.g. 1589252 is 15/89/25/2 
		String shardedPath = shardPathForId(fileId);
		
		// 1. Get tmp/ path, e.g. /var/plexflow-data/tmp/12/58/6
		Path filesPath = PATH_FILES_SUBFOLDER.resolve(shardedPath);
		
		try {
			return IoUtils.crc32(filesPath);
		} catch (IOException e) {
			throw new DatafolderProviderException(e);
		}
	}
	
	/**
	 * Counts CRC32 checksum of file in tmp/.
	 *  
	 **/
	@Override
	public long countCRC32inTmp(long fileId) throws DatafolderProviderException {	
		// 0. Sharded path for file ID e.g. 1589252 is 15/89/25/2 
		String shardedPath = shardPathForId(fileId);
		
		// 1. Get tmp/ path, e.g. /var/plexflow-data/tmp/12/58/6
		Path tmpPath = PATH_TMP_SUBFOLDER.resolve(shardedPath);
		
		try {
			return IoUtils.crc32(tmpPath);
		} catch (IOException e) {
			throw new DatafolderProviderException(e);
		}
	}
	
	
	@Override
	public long countLengthInFiles(long fileId) throws DatafolderProviderException {
		// 0. Sharded path for file ID e.g. 1589252 is 15/89/25/2 
		String shardedPath = shardPathForId(fileId);
		
		// 1. Get tmp/ path, e.g. /var/plexflow-data/tmp/12/58/6
		Path filesPath = PATH_FILES_SUBFOLDER.resolve(shardedPath);
		
		try {
			return Files.size(filesPath);
		} catch (IOException e) {
			throw new DatafolderProviderException(e);
		}
	}
	
	@Override
	public long countLengthInTmp(long fileId) throws DatafolderProviderException {
		// 0. Sharded path for file ID e.g. 1589252 is 15/89/25/2 
		String shardedPath = shardPathForId(fileId);
		
		// 1. Get tmp/ path, e.g. /var/plexflow-data/tmp/12/58/6
		Path tmpPath = PATH_TMP_SUBFOLDER.resolve(shardedPath);
		
		try {
			return Files.size(tmpPath);
		} catch (IOException e) {
			throw new DatafolderProviderException(e);
		}
	}
	
	//--------------------------------------------------------------------------
	// Consider toString(), equals(), hashCode()
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Getters/setters of properties
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Helper methods (mostly private)
	//--------------------------------------------------------------------------
	
	private void deleteHandler(long fileId) throws IOException {
		// Sharded path for file ID e.g. 1589252 is 15/89/25/2 
		String sharded = shardPathForId(fileId);
		Path filesPath = PATH_FILES_SUBFOLDER.resolve(sharded);
		
		// Delete sharded path as possible, but stop at files/ folder. 
		IoUtils.deleteRecursivelyEmptyOnly(filesPath, PATH_FILES_SUBFOLDER);
	}

	private InputStream pullHandler(long fileId) throws IOException {		
		// Sharded path for file ID e.g. 1589252 is 15/89/25/2 
		String shardedPath = shardPathForId(fileId);			
		
		// Delete
		Path filesPath = PATH_FILES_SUBFOLDER.resolve(shardedPath);
		
		return Files.newInputStream(filesPath);
	}

	private long pushBeginHandler(long fileId, InputStream is,
			IProgressMonitor monitor, long inputSize) throws IOException {
		// Sharded path for file ID e.g. 1589252 is 15/89/25/2 
		String shardedPath = shardPathForId(fileId);
		
		// Get tmp/ sharded path (e.g. ${storage.datafolder}/tmp/12/06/1 for file ID 12061)
		Path tmpPath = PATH_TMP_SUBFOLDER.resolve(shardedPath); 
		
		// Create parent folders (e.g. ${storage.datafolder}/tmp/12/06/)
		Files.createDirectories(tmpPath.getParent());		
		
		// Store input stream as a file (JDK7's "try-with-resources")
		// And return read bytes
		try (OutputStream os = Files.newOutputStream(tmpPath)) {
			return IoUtils.copyStream(is, os, true, monitor, inputSize);
		}
	}

	private void pushCommitHandler(long fileId) throws IOException {
		// 0. Sharded path for file ID e.g. 1589252 is 15/89/25/2 
		String shardedPath = shardPathForId(fileId);
		
		// 1. Get tmp/ path, e.g. /var/plexflow-data/tmp/15/89/25/2
		Path tmpPath = PATH_TMP_SUBFOLDER.resolve(shardedPath);
		
		// 2. Get files/ path, e.g. /var/plexflow-data/files/15/89/25/2
		Path filesPath = PATH_FILES_SUBFOLDER.resolve(shardedPath);
		
		// 3. Create parent folders in files/ as needed  
		Files.createDirectories(filesPath.getParent());
		
		// 4. Atomic move from tmp/ to files/
		Files.move(tmpPath, filesPath, StandardCopyOption.REPLACE_EXISTING,
				StandardCopyOption.ATOMIC_MOVE);
	}
	
	private void pushRollbackHandler(long fileId) throws IOException {
		// 0. Sharded path for file ID e.g. 1589252 is 15/89/25/2 
		String shardedPath = shardPathForId(fileId);
		
		// 1. Get tmp/ path, e.g. /var/plexflow-data/tmp/12/58/6
		Path tmpPath = PATH_TMP_SUBFOLDER.resolve(shardedPath);
		
		// 2. Delete tmp/ path
		Files.delete(tmpPath);
	}
	
}
