package com.virtage.plexflow.storage.datafolder.provider.impl;

import java.io.InputStream;
import java.text.NumberFormat;

import org.eclipse.core.runtime.IProgressMonitor;

import com.virtage.plexflow.storage.IFile;
import com.virtage.plexflow.storage.Storage;
import com.virtage.plexflow.storage.datafolder.provider.DatafolderProviderException;
import com.virtage.plexflow.storage.datafolder.provider.IDatafolderProvider;
import com.virtage.plexflow.storage.exceptions.FilePushInProgressException;
import com.virtage.plexflow.storage.exceptions.NodeNotFoundException;

/**
 * Logic enforcing layer {@link IDatafolderProvider} implementation ensuring
 * lock states, concurrent access and so on.
 * 
 * @author libor
 * 
 */
public class LogicDatafolderProvider implements IDatafolderProvider {
	
	//--------------------------------------------------------------------------
	// Class fields - public
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Class fields - non-public
	//--------------------------------------------------------------------------

	//private static final ISysLog SYSLOG = Logging.getSysLog(ThisClass.class);
	
	//--------------------------------------------------------------------------
	// Instance fields - public
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Instance fields - non-public
	//--------------------------------------------------------------------------
	
	private final LocalDatafolderProvider localProvider;
	
	//--------------------------------------------------------------------------
	// Instance fields - bean properties
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Constructors, static initializers, factories
	//--------------------------------------------------------------------------

	public LogicDatafolderProvider(LocalDatafolderProvider provider) {
		this.localProvider = provider; 
	}
	
	//--------------------------------------------------------------------------
	// Public API - class (static)
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Public API - instance
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Overrides and implementations
	//--------------------------------------------------------------------------

	@Override
	public void delete(long fileId) throws DatafolderProviderException {
		try {
			handleDelete(fileId);
			
		} catch (NodeNotFoundException | FilePushInProgressException e) {
			throw new DatafolderProviderException(e);
		}
	}

	

	@Override
	public InputStream pull(long fileId) throws DatafolderProviderException {
		try {			
			return handlePull(fileId);
			
		} catch (Exception e) {
			// If e is already DF exception, re-throw it
			if (e instanceof DatafolderProviderException) {
				throw (DatafolderProviderException) e;
			} else {
				// else wrap any other exception into DF ex
				throw new DatafolderProviderException(e);
			}
		}
	}

	

	@Override
	public long pushBegin(long fileId, InputStream is,
			IProgressMonitor monitor, long inputSize) throws DatafolderProviderException {			
		try {
			return handlePushBegin(fileId, is, monitor, inputSize);
			
		} catch (NodeNotFoundException | FilePushInProgressException e) {
			throw new DatafolderProviderException(e);
		}
	}

	

	@Override
	public void pushCommit(long fileId) throws DatafolderProviderException {
		try {
			handlePushCommit(fileId);
			
		} catch (NodeNotFoundException e) {
			throw new DatafolderProviderException(e);
		}
	}

	

	@Override
	public void pushRollback(long fileId) throws DatafolderProviderException {
		try {
			handlePushRollback(fileId);
			
		} catch (NodeNotFoundException e) {
			throw new DatafolderProviderException(e);
		}
	}

	
	
	@Override
	public long countCRC32inFiles(long fileId) throws DatafolderProviderException {
		try {
			throwIfInPIP(fileId);
			return localProvider.countCRC32inFiles(fileId);
			
		} catch (NodeNotFoundException | FilePushInProgressException e) {
			throw new DatafolderProviderException(e);
		}		
	}

	
	@Override
	public long countCRC32inTmp(long fileId) throws DatafolderProviderException {
		try {
			throwIfNotInPIP(fileId);
			return localProvider.countCRC32inTmp(fileId);
			
		} catch (NodeNotFoundException e) {
			throw new DatafolderProviderException(e);
		}
	}

	
	
	@Override
	public long countLengthInFiles(long fileId) throws DatafolderProviderException {
		try {
			throwIfInPIP(fileId);
			return localProvider.countLengthInFiles(fileId);
			
		} catch (NodeNotFoundException | FilePushInProgressException e) {
			throw new DatafolderProviderException(e);
		}
	}
	
	
	@Override
	public long countLengthInTmp(long fileId) throws DatafolderProviderException {
		try {
			throwIfNotInPIP(fileId);
			return localProvider.countLengthInTmp(fileId);
			
		} catch (NodeNotFoundException e) {
			throw new DatafolderProviderException(e);
		}
	}
	
	//--------------------------------------------------------------------------
	// Consider toString(), equals(), hashCode()
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Getters/setters of properties
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Helper methods (mostly private)
	//--------------------------------------------------------------------------
	
	/**
	 * @param fileId
	 * @throws NodeNotFoundException
	 * @throws FilePushInProgressException 
	 * @throws DatafolderProviderException
	 */
	private void handleDelete(long fileId) throws NodeNotFoundException, FilePushInProgressException, DatafolderProviderException {
		IFile file = Storage.get().getFileWithId(fileId);		
		// Forbid if PIP
		if (file.isPushInProgress()) {
			throw new FilePushInProgressException(file);
		}
		
		localProvider.delete(fileId);
		
		// TODO: notify listeners about deletion
	}
	
	
	/**
	 * @param fileId
	 * @return
	 * @throws NodeNotFoundException
	 * @throws DatafolderProviderException
	 * @throws FilePushInProgressException 
	 */
	private InputStream handlePull(long fileId) throws NodeNotFoundException,
			DatafolderProviderException, FilePushInProgressException {
		IFile file = Storage.get().getFileWithId(fileId);			
		
		// Forbid if PIP
		if (file.isPushInProgress()) {
			throw new FilePushInProgressException(file);
		}
		
		// Compare checksum of actual file and DB
		long crcFile = localProvider.countCRC32inFiles(fileId);
		long crcDB = file.getCrc32();
		if (crcFile != crcDB) {
			throw new DatafolderProviderException("Inconsistency detected " +
					"on file '" + file.getPath() + " (id " + fileId + "). " +
					"Actual file checksum (" + crcFile + ") is different " +
					"from value in database (" + crcDB + ").");
		}
		
		// Compare length of actual file and DB
		long lengthFile = localProvider.countLengthInFiles(fileId);
		long lengthDB = file.getLength();
		if (lengthFile != lengthDB) {
			throw new DatafolderProviderException("Inconsistency detected "
					+ "on file '" + file.getPath() + "' (id " + fileId + "). "
					+ "Actual file length ("
					+ NumberFormat.getInstance().format(lengthFile)
					+ " bytes) is different from value in database ("
					+ NumberFormat.getInstance().format(lengthDB) + " bytes).");
		}
		
		return localProvider.pull(fileId);
	}
	
	/**
	 * @param fileId
	 * @param is
	 * @param monitor
	 * @param inputSize
	 * @return
	 * @throws NodeNotFoundException
	 * @throws DatafolderProviderException
	 * @throws FilePushInProgressException 
	 */
	private long handlePushBegin(long fileId, InputStream is,
			IProgressMonitor monitor, long inputSize)
			throws NodeNotFoundException, DatafolderProviderException, FilePushInProgressException {
		IFile file = Storage.get().getFileWithId(fileId);		
		if (file.isPushInProgress()) {
			throw new FilePushInProgressException(file);
		}
		
		file.setPushInProgress(true);
		
		return localProvider.pushBegin(fileId, is, monitor, inputSize);
	}
	
	/**
	 * @param fileId
	 * @throws NodeNotFoundException
	 * @throws DatafolderProviderException
	 */
	private void handlePushCommit(long fileId) throws NodeNotFoundException,
			DatafolderProviderException {
		IFile file = Storage.get().getFileWithId(fileId);		
		if (!file.isPushInProgress()) {
			throw new DatafolderProviderException("PUSH-COMMIT on '" +
					file.getPath() + "' (id " + file.getId() + ") is forbidden " +
					"since this file is not in push-in-progress.");
		}
		
		localProvider.pushCommit(fileId);
		
		// Set new CRC and length to DB
		file.setCrc32(localProvider.countCRC32inFiles(fileId));
		file.setLength(localProvider.countLengthInFiles(fileId));
		
		// Clear PIP
		file.setPushInProgress(false);
		
		// TODO: Notify listeners about commit
	}
	
	/**
	 * @param fileId
	 * @throws NodeNotFoundException
	 * @throws DatafolderProviderException
	 */
	private void handlePushRollback(long fileId) throws NodeNotFoundException,
			DatafolderProviderException {
		IFile file = Storage.get().getFileWithId(fileId);		
		if (!file.isPushInProgress()) {
			throw new DatafolderProviderException("PUSH-ROLLBACK on '" +
					file.getPath() + "' (id " + file.getId() + ") is forbidden " +
					"since this file is not in push-in-progress.");
		}
		
		localProvider.pushRollback(fileId);
		
		// Clear PIP
		file.setPushInProgress(false);
	}
	
	
	/** Check existence of a file and throws exception if it IS NOT in push-in-progress. */
	private void throwIfNotInPIP(long fileId) throws NodeNotFoundException, DatafolderProviderException {
		// Forbid if not PIP
		IFile file = Storage.get().getFileWithId(fileId);
		if (!file.isPushInProgress()) {
			throw new DatafolderProviderException("File id " + fileId + " is not in push-in-progress.");
		}
	}
	
	/** Check existence of a file and throws exception if it IS in push-in-progress. */
	private void throwIfInPIP(long fileId) throws NodeNotFoundException, FilePushInProgressException {
		// Forbid if not PIP
		IFile file = Storage.get().getFileWithId(fileId);
		if (file.isPushInProgress()) {
			throw new FilePushInProgressException(file);
		}
	}
}
