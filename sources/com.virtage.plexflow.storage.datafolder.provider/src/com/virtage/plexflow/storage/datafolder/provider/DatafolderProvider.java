package com.virtage.plexflow.storage.datafolder.provider;

import com.virtage.plexflow.storage.datafolder.provider.impl.LocalDatafolderProvider;
import com.virtage.plexflow.storage.datafolder.provider.impl.LogicDatafolderProvider;


public final class DatafolderProvider {
	//--------------------------------------------------------------------------
	// Class fields - public
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Class fields - non-public
	//--------------------------------------------------------------------------

	//private static final ISysLog SYSLOG = Logging.getSysLog(ThisClass.class);
	
	private static volatile IDatafolderProvider instance;

	//--------------------------------------------------------------------------
	// Instance fields - public
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Instance fields - non-public
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Instance fields - bean properties
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Constructors, static initializers, factories
	//--------------------------------------------------------------------------

	private DatafolderProvider() {}
	
	/**
	 * Mixes up and returns IDatafolder operating on local filesystem with 
	 * additional business logic.
	 * 
	 * @return
	 * @throws DatafolderProviderException
	 */
	public static IDatafolderProvider getInstance() throws DatafolderProviderException {
		if (instance == null) {
			synchronized (DatafolderProvider.class) {
				if (instance == null) {
					// Decorator pattern
					instance = new LogicDatafolderProvider(new LocalDatafolderProvider());
				}
			}
		}
		
		return instance;
	}	
	
	//--------------------------------------------------------------------------
	// Public API - class (static)
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Public API - instance
	//--------------------------------------------------------------------------
	
	//--------------------------------------------------------------------------
	// Overrides and implementations
	//--------------------------------------------------------------------------
	
	//--------------------------------------------------------------------------
	// Consider toString(), equals(), hashCode()
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Getters/setters of properties
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Helper methods (mostly private)
	//--------------------------------------------------------------------------
}
