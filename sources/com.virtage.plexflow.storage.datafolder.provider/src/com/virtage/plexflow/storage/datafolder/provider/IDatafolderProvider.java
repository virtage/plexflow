package com.virtage.plexflow.storage.datafolder.provider;

import java.io.InputStream;

import org.eclipse.core.runtime.IProgressMonitor;

/**
 * Datafolder provider interface. There is number of low-level commands
 * (push-begin, push-commit, ...) and a few of query methods to count CRC32 and
 * length of files in files/ and tmp/ subfolders.
 * 
 * @author libor
 */
public interface IDatafolderProvider {
	
	/**
	 * 
	 * @param fileId
	 * @throws DatafolderProviderException 
	 */
	public void delete(long fileId) throws DatafolderProviderException;
	
	
	/**
	 * 
	 * @param fileId
	 * @return
	 * @throws DatafolderProviderException
	 */
	public InputStream pull(long fileId) throws DatafolderProviderException;
	
	
	/**
	 * 
	 * @param fileId
	 * @param is
	 * @param monitor or null if no progress monitor. 
	 * @param inputSize size of input stream in bytes or -1 when progress monitor is not used.
	 * @return read bytes from InputStream
	 * @throws DatafolderProviderException -- re-thrown underlying IO exception or push checksum mismatch
	 */
	public long pushBegin(long fileId, InputStream is, IProgressMonitor monitor,
			long inputSize) throws DatafolderProviderException;	
	
	
	/**
	 * 
	 * @param fileId
	 * @throws DatafolderProviderException
	 */
	public void pushCommit(long fileId) throws DatafolderProviderException;
	
	
	/**
	 * 
	 * @param fileId
	 * @throws DatafolderProviderException
	 */
	public void pushRollback(long fileId) throws DatafolderProviderException;


	/**
	 * Counts CRC32 checksum of file in tmp/.
	 *  
	 **/
	public long countCRC32inTmp(long fileId) throws DatafolderProviderException;

	
	/**
	 * Counts CRC32 checksum of file in files/.
	 *  
	 **/
	public long countCRC32inFiles(long fileId) throws DatafolderProviderException;
		
	public long countLengthInFiles(long fileId) throws DatafolderProviderException;
	
	public long countLengthInTmp(long fileId) throws DatafolderProviderException;
	
}
