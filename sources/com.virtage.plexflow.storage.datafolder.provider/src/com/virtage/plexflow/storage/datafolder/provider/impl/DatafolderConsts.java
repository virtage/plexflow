package com.virtage.plexflow.storage.datafolder.provider.impl;

/**
 * Subfolder names of local datafolder.
 * 
 * @author libor
 *
 */
public interface DatafolderConsts {
	
	/** Subfolder under storage.datafolder for sharded files (value {@value}). */
	String SUBFOLDER_FILES = "files";
	
	/** Subfolder under storage.datafolder for temp files (value {@value}). */
	String SUBFOLDER_TMP = "tmp";
	
	/** Subfolder under storage.datafolder for version deltas (value {@value}). */
	String SUBFOLDER_VERS = "vers";
	
	/** Subfolder under storage.datafolder for thumbnails (value {@value}). */
	String SUBFOLDER_THUMBS = "thumbs";
	
}
