package com.virtage.plexflow.storage.datafolder.provider;

/**
 * Outermost (or "encapsulating", "unifying") exception to reduce amount of
 * various exceptions that might be thrown by datafolder API (IOException,
 * ConfigException, ...).
 * 
 * @author libor
 * 
 */
public class DatafolderProviderException extends Exception {

	//--------------------------------------------------------------------------
	// Class fields - public
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Class fields - non-public
	//--------------------------------------------------------------------------

	private static final long serialVersionUID = -8249594996387651095L;
	//private static final ISysLog SYSLOG = Logging.getSysLog(ThisClass.class);

	//--------------------------------------------------------------------------
	// Instance fields - public
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Instance fields - non-public
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Instance fields - bean properties
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Constructors, static initializers, factories
	//--------------------------------------------------------------------------
		

	public DatafolderProviderException(String message) {
		super(message);
	}

	public DatafolderProviderException(Throwable cause) {
		super(cause);
	}

	public DatafolderProviderException(String message, Throwable cause) {
		super(message, cause);
	}
	
	//--------------------------------------------------------------------------
	// Public API - class (static)
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Public API - instance
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Overrides and implementations
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Consider toString(), equals(), hashCode()
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Getters/setters of properties
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Helper methods (mostly private)
	//--------------------------------------------------------------------------
}
