package com.virtage.plexflow.storage.datafolder.provider.impl;

public interface DatafolderConfigKeys {
	/** Datafolder root. Contains files/, thumbs/ etc. (value {@value}).  */
	String DATAFOLDER = "storage.datafolder";
}
