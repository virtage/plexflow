================================================================================
Plexflow Server target README
================================================================================

This folder constitutes Plexflow Server target

* plexflow-server.target --  target platform definition file
* vcs-ignored/* -- binary content

NEVER PUT LARGE BINARIS UNDER VCS! =============================================

They should always come to vcs-ignored/ folder that is, as name implies, on VCS
ignore list.

Target is comprised from features and plugins is placed outside VCS in folder:

	~/Dropbox/Internal/Plexflow/dev/PDE_targets/com.virtage.plexflow.server.target/

Target definition expects its content is under under vcs-ignore/ folder.

STEPS TO PREPARE TARGET IN YOUR COPY ===========================================

1. Cd to this folder
   $ cd /to/your/com.virtage.plexflow.server.target/
   
2. Create a symlink named vcs-ignored/ referring to Dropbox
   $ ln -s ~/Dropbox/Internal/Plexflow/dev/PDE_targets/com.virtage.plexflow.server.target/ vcs-ignored