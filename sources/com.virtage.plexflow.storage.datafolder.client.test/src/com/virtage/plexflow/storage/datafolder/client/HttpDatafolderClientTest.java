package com.virtage.plexflow.storage.datafolder.client;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.eclipse.core.runtime.NullProgressMonitor;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.virtage.plexflow.server.api.ServerConsts;
import com.virtage.plexflow.server.commands.CommandUtils;
import com.virtage.plexflow.storage.IFile;
import com.virtage.plexflow.storage.Storage;
import com.virtage.plexflow.utils.IoUtils;

import static com.virtage.plexflow.testsupport.TestSupportConsts.*;

public class HttpDatafolderClientTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
		Storage.get().format(null);
	}

	@Test
	public void constructorNotThrowsException() throws Exception {
		new JdkHttpDatafolderClient();
	}

	@Test
	public void testPushPull() throws Exception {
		JdkHttpDatafolderClient client = new JdkHttpDatafolderClient();
		IFile file = Storage.get().getHomeForCurrentUser().createFile("foofile");		
		Path testfile = Paths.get(PATH_TEST_FILES, FILENAME_TEST_1_MiB);
		
		client.push(file, testfile, null);
		
		Path tempFile = null;
		try {
			tempFile = Files.createTempFile(null, null);
			
			client.pull(file, tempFile, null);
			
			assertTrue(IoUtils.compareCrc32(testfile, tempFile));
			
		} finally {
			Files.deleteIfExists(tempFile);
		}	
	}

	@Test
	public void testDelete() throws Exception {
		JdkHttpDatafolderClient client = new JdkHttpDatafolderClient();
		IFile file = Storage.get().getHomeForCurrentUser().createFile("foofile");
				
		client.push(file, Paths.get("/tmp/plexflow-testfiles/1MiB.bin"), new NullProgressMonitor());
		
		client.delete(file);
	}
	
	
	@Test
	public void testPushNotExisting() throws Exception {
		JdkHttpDatafolderClient client = new JdkHttpDatafolderClient();
		IFile file = Storage.get().getHomeForCurrentUser().createFile("foofile");
				
		client.push(file, Paths.get("/tmp/plexflow-testfiles/not-existing-file.bin"), new NullProgressMonitor());
		
	}	

	@Test
	public void testVersion() throws Exception {
		assertEquals(CommandUtils.getVersionInfo(), new JdkHttpDatafolderClient().version());
	}

}
