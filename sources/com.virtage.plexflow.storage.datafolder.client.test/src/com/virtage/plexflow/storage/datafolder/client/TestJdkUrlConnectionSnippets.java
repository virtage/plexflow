package com.virtage.plexflow.storage.datafolder.client;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.commons.codec.binary.Base64;

import com.virtage.plexflow.server.api.ServerConsts;
import com.virtage.plexflow.utils.IoUtils;
import com.virtage.plexflow.utils.PxUtils;

public class TestJdkUrlConnectionSnippets {
	
	/*
	 * JDK HttpURLConnection extremely good tut by BaluC:
	 * http://stackoverflow.com/questions/2793150/how-to-use-java-net-urlconnection-to-fire-and-handle-http-requests
	 * 
	 * http://www.avajava.com/tutorials/lessons/how-do-i-connect-to-a-url-using-basic-authentication.html
	 * http://en.wikipedia.org/wiki/Basic_access_authentication
	 * http://www.ask.com/wiki/List_of_HTTP_status_codes
	 */
	
	public static void main(String[] args) throws IOException, BadResponseReceivedException, MalformedResponseReceivedException {
		// Params:
		String url = "http://localhost:8080/plexflow-api//version";
		String username="pxsu";
		String password="changeme";
		
		HttpURLConnection conn = (HttpURLConnection) new URL(url).openConnection();		

		// if to authenticate:
		String authStr = username + ":" + password;
		String base64 = Base64.encodeBase64String(authStr.getBytes());
		//System.out.println(base64);
		conn.setRequestProperty("Authorization", "Basic " + base64);
		
		// Distinguish OK or BAD response
		String response = conn.getHeaderField(ServerConsts.HEADER_RESPONSE_TYPE);		
		
		if (ServerConsts.HEADER_RESPONSE_TYPE_OK.equals(response)) {
			System.out.println("OK resp");
			
		} else if (ServerConsts.HEADER_RESPONSE_TYPE_BAD.equals(response)) {
			throw new BadResponseReceivedException();

		} else {
			throw new MalformedResponseReceivedException("Required '" + ServerConsts.HEADER_RESPONSE_TYPE +
					"' header is missing or has unexpected value ('" + response + "').", conn);			
		}				
		
		// Uložení do souboru:
//		System.out.println(conn.getHeaderField("Content-Disposition"));
//		Path targetFile = Paths.get("vystup");		
//		OutputStream os = Files.newOutputStream(targetFile);		
//		InputStream is = conn.getInputStream();
//		IoUtils.copyStreams(is, os);
//		System.out.println("crc32 = " + IoUtils.crc32(targetFile));
//		System.out.println("size = " + Files.size(targetFile));
		

		// Čtení:
		

		
		// TODO: If HTTP 400 (e.g. not existing fileId), read body to get detail
		// ...
	}
	
	public static String readBodyAsText(HttpURLConnection conn) throws IOException {
		BufferedReader is = new BufferedReader(new InputStreamReader(conn.getInputStream()));
		StringBuffer sb = new StringBuffer();
		String str;
		
		while ((str = is.readLine()) != null) {
			sb.append(str);
		}
		
		return sb.toString();
	}
}
