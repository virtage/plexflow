===============================================================================
How to prepare PDE Target for RCP app?
===============================================================================

NEVER PUT LARGE BINARIES UNDER VCS CONTROL!
Always should come under vcs-ignored/ folder that is, as name implies,
on VCS ignore list.

TARGET CONTENT IS STORED OUTSIDE
Target comprised from features and plugins is placed outside VCS in folder:

	~/Dropbox/Internal/Plexflow/dev/PDE_targets/com.virtage.plexflow.ui.target/

Target definition expects its content directly under vcs-ignore/ folder.

STEPS TO RECREATE TARGET CONTENT IN YOUR COPY

1. Cd to this folder
   $ cd /to/your/com.virtage.plexflow.ui.target/
   
2. Create a symlink named vcs-ignored/ referring to Dropbox
   $ ln -s ~/Dropbox/Internal/Plexflow/dev/PDE_targets/com.virtage.plexflow.ui.target/ vcs-ignored