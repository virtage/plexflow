package com.virtage.plexflow.storage.datafolder.client.impl;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.nio.file.StandardOpenOption;
import java.text.NumberFormat;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.Header;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.SubMonitor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.virtage.plexflow.config.ConfigException;
import com.virtage.plexflow.config.ConfigFactory;
import com.virtage.plexflow.server.api.ServerConfigKeys;
import com.virtage.plexflow.server.api.ServerConsts;
import com.virtage.plexflow.server.commands.DeleteCommand;
import com.virtage.plexflow.server.commands.PullCommand;
import com.virtage.plexflow.server.commands.PushBeginCommand;
import com.virtage.plexflow.server.commands.PushCommitCommand;
import com.virtage.plexflow.server.commands.PushRollbackCommand;
import com.virtage.plexflow.server.commands.VersionCommand;
import com.virtage.plexflow.storage.IFile;
import com.virtage.plexflow.storage.Storage;
import com.virtage.plexflow.storage.datafolder.client.Activator;
import com.virtage.plexflow.storage.datafolder.client.BadResponseReceivedException;
import com.virtage.plexflow.storage.datafolder.client.DatafolderClientException;
import com.virtage.plexflow.storage.datafolder.client.IDatafolderClient;
import com.virtage.plexflow.storage.datafolder.client.MalformedResponseReceivedException;
import com.virtage.plexflow.system.SystemInstanceFactory;
import com.virtage.plexflow.utils.IoUtils;

/**
 * Implementation using Apache HttpClient.
 * 
 * @author libor
 *
 */
public class ApacheHttpDatafolderClient implements IDatafolderClient {
		
	//--------------------------------------------------------------------------
	// Class fields - public
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Class fields - non-public
	//--------------------------------------------------------------------------

	private static final Logger SYSLOG = LoggerFactory.getLogger(ApacheHttpDatafolderClient.class);

	//--------------------------------------------------------------------------
	// Instance fields - public
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Instance fields - non-public
	//--------------------------------------------------------------------------
	/** HTTP API's base URL (e.g. http://liam:5890/plexflow-api). */
	private final String HTTP_API_URL;
	
	/** Shared HttpClient to use by all methods. */
	private final DefaultHttpClient httpCli; 
	
	//--------------------------------------------------------------------------
	// Instance fields - bean properties
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Constructors, static initializers, factories
	//--------------------------------------------------------------------------

	public ApacheHttpDatafolderClient() throws DatafolderClientException {
		try {
			this.HTTP_API_URL = ConfigFactory.getGlobal().getString(ServerConfigKeys.URL);
			
			/** == Acquire HttpClient ==
			 * Why this indirection?! {@link Activator#getHttpClient()} throws
			 * {@link ConfigException}. Handling this exception in *every*
			 * access to HttpClient would pollute rest of code.   
			 */
			this.httpCli = Activator.getHttpClient();
			
		} catch (ConfigException e) {
			throw new DatafolderClientException(e);
		}
	}
	
	//--------------------------------------------------------------------------
	// Public API - class (static)
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Public API - instance
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Overrides and implementations
	//--------------------------------------------------------------------------

	@Override
	public void pull(IFile file, Path path, IProgressMonitor monitor) throws DatafolderClientException {
		HttpGet req = new HttpGet(HTTP_API_URL
				+ new PullCommand().getCommandURI() + "?"
				+ ServerConsts.PARAM_FILE_ID + "=" + file.getId());		
		
		try {
			HttpResponse resp = httpCli.execute(req);
			resolveResponse(resp);			
			
			// Download to temp file
			Path tempFile = Files.createTempFile(null, null);
			
			try (InputStream is = resp.getEntity().getContent();
				 OutputStream os = Files.newOutputStream(tempFile, StandardOpenOption.DELETE_ON_CLOSE)) {
				
				IoUtils.copyStream(is, os, true, monitor, file.getLength());
			}
			
			// Compare CRC
			long crc32tmp = IoUtils.crc32(tempFile);
			long crc32db  = file.getCrc32();
			if (crc32tmp != crc32db) {
				throw new IOException("Checksum mismatched. Pulled file " +
					"checksum (" + crc32tmp + ") is different from " +
					"database (" + crc32db + ").");
			}
			
			// Compare length
			long lengthTmp = Files.size(tempFile);
			long lengthDb  = file.getLength();
			if (lengthTmp != lengthDb) {
				throw new IOException("Length mismatched. Pulled file length " +
					"(" + NumberFormat.getInstance().format(lengthTmp) +
					" bytes) is different from database " +
					"(" + NumberFormat.getInstance().format(lengthDb) +
					" bytes).");
			}
			
			// Move temp file to actual file
			Files.move(tempFile, path, StandardCopyOption.REPLACE_EXISTING, StandardCopyOption.ATOMIC_MOVE);
			
			// Delete temp file
			Files.delete(tempFile);
			
		} catch (IOException | MalformedResponseReceivedException | BadResponseReceivedException e) {
			throw new DatafolderClientException(e);
			
		}
	}
	

	@Override	
	public void push(IFile file, Path path, IProgressMonitor monitor) throws DatafolderClientException {
		SubMonitor subMonitor = SubMonitor.convert(monitor, 100);
		
		// Push-begin (uploading) is considered as 95% of operation
		tryPushBeginOrRollback(file, path, subMonitor.newChild(95));

		// Committing as the remaining 5%
		tryPushCommit(file, path);
		
		subMonitor.done();
	}	

	
	@Override
	public void delete(IFile file) throws DatafolderClientException {
		HttpGet req = new HttpGet(HTTP_API_URL + new DeleteCommand().getCommandURI() +
				"?" + ServerConsts.PARAM_FILE_ID + "=" + file.getId());
		
		try {
			HttpResponse resp = httpCli.execute(req);			
			resolveResponse(resp);
			
		} catch (Exception e) {
			throw new DatafolderClientException(e);
		}		
	}
	

	@Override
	public String version() throws DatafolderClientException {		
		HttpGet req = new HttpGet(HTTP_API_URL + new VersionCommand().getCommandURI());		
		
		try {
			HttpResponse resp = httpCli.execute(req);
			resolveResponse(resp);			
			
			Header version = resp.getFirstHeader(ServerConsts.HEADER_SERVER_VERSION);			
			if (version != null) {
				return version.getValue();
				
			} else {
				// Should never occur -- passing version header is required by
				// Plexflow protocol.
				return "<server don't provide version>";
			}

		} catch (IOException | MalformedResponseReceivedException | BadResponseReceivedException e) {
			throw new DatafolderClientException(e);
		}
	}
	
	
	//--------------------------------------------------------------------------
	// Consider toString(), equals(), hashCode()
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Getters/setters of properties
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Helper methods (mostly private)
	//--------------------------------------------------------------------------
	
	/**
	 * Tries to PUSH-BEGIN, followed by issuing PUSH-ROLLBACK on failure.
	 * 
	 * @param file
	 * @param path
	 * @param monitor
	 * @throws DatafolderClientException
	 */
	private void tryPushBeginOrRollback(IFile file, Path path,
			IProgressMonitor monitor) throws DatafolderClientException {		
		try {
			httpPushBegin(file, path, monitor);
			
		} catch (Exception beginEx) {
			try {
				httpPushRollback(file, path);
				
			} catch (Exception rollbackEx) {
				DatafolderClientException dfEx = 
						new DatafolderClientException("Both PUSH-BEGIN " +
						"command (root exception) and PUSH-ROLLBACK " +
						"command (supressed exception) failed.", beginEx);
				dfEx.addSuppressed(rollbackEx);			// JDK7 only
				
				throw dfEx;
			}
			
			throw new DatafolderClientException(
					"PUSH-BEGIN command failed.", beginEx);
		}
	}
	
	
	/**
	 * Tries to PUSH-COMMIT.
	 * 
	 * @param file
	 * @param path
	 * @throws DatafolderClientException
	 */
	private void tryPushCommit(IFile file, Path path)
			throws DatafolderClientException {
		try {
			httpPushCommit(file, path);
			
		} catch (Exception commitEx) {
			throw new DatafolderClientException(
					"PUSH-COMMIT command failed.", commitEx);
		}
	}
	
	
	private void httpPushBegin(IFile file, Path path, IProgressMonitor monitor) throws MalformedURLException, IOException, BadResponseReceivedException, MalformedResponseReceivedException, ConfigException  {
	HttpURLConnection conn = null;
		
		try {
			// e.g. "/push-begin?fileId=5589"
			String commandURI = new PushBeginCommand().getCommandURI();
			conn = makeConnection(
				 commandURI + "?" + ServerConsts.PARAM_FILE_ID + "=" + file.getId());
			conn.setRequestMethod("PUT");
			// Otherwise cannot getOutputStream() and read to it
			conn.setDoOutput(true);
			
			// JDK 7's new "try-with-resources" statement
			// http://docs.oracle.com/javase/tutorial/essential/exceptions/tryResourceClose.html
			try (InputStream is = Files.newInputStream(path);
				 OutputStream os = conn.getOutputStream()) {
				IoUtils.copyStream(is, os, false, monitor, Files.size(path));
			}
			
			// Resolve response 
			resolveResponse(conn);
						
			// Compare CRC32s from response and original
			long respCrc32 = Long.valueOf(conn.getHeaderField(ServerConsts.HEADER_FILE_CRC32));
			long origCrc32 = IoUtils.crc32(path);			
			if (respCrc32 != origCrc32) {
				throw new IOException("Mismatched CRC32 checksums of original (" +
						origCrc32 + ") and transfered file (" + respCrc32 + ").");
			}
			
			// Compare file lengths from response and original
			long respLength = Long.valueOf(conn.getHeaderField(ServerConsts.HEADER_FILE_LENGTH));			
			long origLength = Files.size(path);			
			if (respLength != origLength) {
				throw new IOException("Mismatched lengths of original (" +
						origLength + ") and transfered file (" + respLength + ").");
			}
			
		} finally {
			cleanupConnection(conn);
		}
		
	}
	
	
	private void httpPushRollback(IFile file, Path path) throws MalformedURLException, ConfigException, IOException, BadResponseReceivedException, MalformedResponseReceivedException {
		HttpURLConnection conn = null;
		
		try {
			// e.g. "/push-begin?fileId=5589"
			String commandURI = new PushRollbackCommand().getCommandURI();
			conn = makeAndResolveConnection(
				 commandURI + "?" + ServerConsts.PARAM_FILE_ID + "=" + file.getId());
			
			// Resolve response 
			resolveResponse(conn);
				
			// Other response then OK
			if (!isResponseOk(conn)) {
				throw new BadResponseReceivedException(conn);
			}
			
			
		} finally {
			cleanupConnection(conn);
		}
		
		
	}
	
	
	private void httpPushCommit(IFile file, Path path) throws MalformedURLException, ConfigException, IOException, BadResponseReceivedException, MalformedResponseReceivedException {
		HttpURLConnection conn = null;
		
		try {
			String commandURI = new PushCommitCommand().getCommandURI();
			conn = makeAndResolveConnection(
				 commandURI + "?" + ServerConsts.PARAM_FILE_ID + "=" + file.getId());
				
			// Other response then OK
			if (!isResponseOk(conn)) {
				throw new BadResponseReceivedException(conn);
			}			
			
		} finally {
			cleanupConnection(conn);
		}
	}
	
	
	
	
	
	/**
	 * Resolves a response. Purpose of method is to throw
	 * {@link MalformedResponseReceivedException} if response is malformed (in
	 * terms of Plexflow HTTP API protocol or even HTTP protocol) or to throw
	 * {@link BadResponseReceivedException} on BAD response.
	 * 
	 * @param resp
	 * @throws MalformedResponseReceivedException on malformed response
	 * @throws BadResponseReceivedException on BAD response
	 */
	private void resolveResponse(HttpResponse resp)	throws
		MalformedResponseReceivedException, BadResponseReceivedException {
		// Response must be 200 OK
		if (resp.getStatusLine().getStatusCode() != 200) {
			String msg = "Response status code other then 200 OK received ('"
					+ resp.getStatusLine().getStatusCode() + " "
					+ resp.getStatusLine().getReasonPhrase() + "')."; 
			
			throw new MalformedResponseReceivedException(msg, resp);
		}
		
		// Response must contain non-blank "X-Plexflow-Server-Version"
		Header version = resp.getFirstHeader(ServerConsts.HEADER_SERVER_VERSION);
		if ((version == null) || StringUtils.isBlank(version.getValue())) {
			String msg = "Missing '" + ServerConsts.HEADER_SERVER_VERSION +
				"' header in response or this value is blank ('" +
				version.getValue() + "').";
			
			throw new MalformedResponseReceivedException(msg, resp);
		}
		
		// Response must contain header "X-Plexflow-Response-Type"
		Header responseType = resp.getFirstHeader(ServerConsts.HEADER_RESPONSE_TYPE);
		if (responseType == null) {		// no such header in response			
			String msg = "Missing '" + ServerConsts.HEADER_RESPONSE_TYPE +
					"' header.";
			
			throw new MalformedResponseReceivedException(msg, resp);
		}
		
		// of value "OK" or "BAD"
		if ((!responseType.equals(ServerConsts.HEADER_RESPONSE_TYPE_OK)) && 
			(!responseType.equals(ServerConsts.HEADER_RESPONSE_TYPE_BAD))) {
			String msg = "Unexpected value of '"
					+ ServerConsts.HEADER_RESPONSE_TYPE + "' header ('"
					+ responseType.getValue() + "').";
			
			throw new MalformedResponseReceivedException(msg, resp);
		}
		
		// BAD response 
		if (responseType.getValue().equals(ServerConsts.HEADER_RESPONSE_TYPE_OK)) {
			throw new BadResponseReceivedException(resp);
		}
	}
	
	
	/** Tells whether is response type OK.
	 * 
	 * @deprecated probably unnecessary 
	 * */
	@Deprecated
	private boolean isResponseOK(HttpResponse resp) {
		Header responseType = resp.getFirstHeader(ServerConsts.HEADER_RESPONSE_TYPE);
		if ((responseType != null) &&
			(responseType.getValue().equals(ServerConsts.HEADER_RESPONSE_TYPE_OK)))
			return true;
		else
			return false;
	}
	
	@Deprecated
	private boolean isResponseOk2(HttpURLConnection conn) {
		String responseType = conn.getHeaderField(ServerConsts.HEADER_RESPONSE_TYPE);
		
		if (ServerConsts.HEADER_RESPONSE_TYPE_OK.equals(responseType)) {
			return true;
		} else {
			return false;
		}
	}
}
