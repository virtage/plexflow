package com.virtage.plexflow.storage.datafolder.client;

import java.net.HttpURLConnection;




/**
 * Reports "BAD" response received from HTTP API components. 
 *  
 * <p>Don't confuse with {@linkplain com.virtage.plexflow.server.api.BadRequestException} class.</p> 
 */
public class BadResponseReceivedException extends Exception {

	private static final long serialVersionUID = -6677886352275035517L;
	
	private static final String MSG = "Bad response received.";

	public BadResponseReceivedException() {
		super(MSG);
	}

	public BadResponseReceivedException(HttpURLConnection conn) {
		super(MSG + " See attached cause for details. " + HttpClientUtils.dumpResponse(conn));
	}

	public BadResponseReceivedException(String msg) {
		super(msg);
	}
	
}
