package com.virtage.plexflow.storage.datafolder.client.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.util.EntityUtils;

import com.virtage.plexflow.utils.PxUtils;

public final class HttpUtils {


	//--------------------------------------------------------------------------
	// Class fields - public
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Class fields - non-public
	//--------------------------------------------------------------------------

	//private static final ISysLog SYSLOG = Logging.getSysLog(ThisClass.class);

	//--------------------------------------------------------------------------
	// Instance fields - public
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Instance fields - non-public
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Instance fields - bean properties
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Constructors, static initializers, factories
	//--------------------------------------------------------------------------
	private HttpUtils() {}
	
	//--------------------------------------------------------------------------
	// Public API - class (static)
	//--------------------------------------------------------------------------

	/**
	 * Returns body of response as String. Never returns null or throw an exception - 
	 * returned string will contain an error, if any.
	 */
	public static String readBodyAsText(HttpResponse resp) {
		HttpEntity entity = resp.getEntity();
		
		// Is there some body? 
		if (entity == null) {
			return "<empty body returned>";
		}
		
		// Print out only if it's text MIME
		String mimeType = EntityUtils.getContentMimeType(entity);
		if (mimeType == null) {
			return "<Content-Type is missing, body will not be dumped>";
			
		} else if (!mimeType.startsWith("text/")) {
			return "<non-textual body (" + mimeType + ")>";
			
		} else {
			try {
				return EntityUtils.toString(entity);
				
			} catch (ParseException | IOException e) {
				return "<" + e.getClass() + " exception>"; 
			}
			
		}
	}
	
	
	/**
	 * @deprecated JDK HttpURLConnection has memory leaks and performance issues,
	 * used Apache HttpClient instead. Use {@link  #readBodyAsText(HttpResponse)}.
	 */
	@Deprecated
	public static String readBodyAsText(HttpURLConnection conn) throws IOException {
		BufferedReader is = new BufferedReader(new InputStreamReader(conn.getInputStream()));
		StringBuffer sb = new StringBuffer();
		String str;
		
		while ((str = is.readLine()) != null) {
			sb.append(str);
		}
		
		return sb.toString();
	}
	
	/**
	 * Returns status code, headers and body as one string.
	 * 
	 * @param resp
	 * @return
	 */
	public static String dumpResponse(HttpResponse resp) {
		StringBuffer sb = new StringBuffer();
		
		sb.append("Full HTTP response dump = { ");
		
		// Response status code
		sb.append("Status = ");
		sb.append(resp.getStatusLine().getStatusCode());
		sb.append(" ");
		sb.append(resp.getStatusLine().getReasonPhrase());
		sb.append(". ");
		
		// Headers
		//sb.append(SystemUtils.LINE_SEPARATOR);
		sb.append("Headers = { ");
		
		Header[] headers = resp.getAllHeaders();
		for (Header header : headers) {
			sb.append(header.getName());
			sb.append(" = ");
			sb.append(PxUtils.coalesce(header.getValue(), "<null>"));
		}
		
		sb.append(" }. ");
		
		// Body (if any)
		//sb.append(SystemUtils.LINE_SEPARATOR);
		sb.append("Body = { ");
		sb.append(HttpUtils.readBodyAsText(resp));
		sb.append(" }");
		sb.append(" }.");
		
		return sb.toString();
	}
	
	
	/**
	 * Returns status code, headers and body as one string.
	 * 
	 * @deprecated JDK HttpURLConnection has memory leaks and performance issues,
	 * used Apache HttpClient instead. Use {@link  #dumpResponse(HttpResponse)}.
	 */
	@Deprecated
	public static String dumpResponse(HttpURLConnection conn) {
		StringBuffer sb = new StringBuffer();
		
		try {
			sb.append("Full HTTP response dump follows: { ");
			
			// Response status code
			sb.append("Status: ");
			sb.append(conn.getResponseCode());
			sb.append(" ");
			sb.append(conn.getResponseMessage());
			sb.append(". ");
			
			// Headers
			//sb.append(SystemUtils.LINE_SEPARATOR);
			sb.append("Headers: { ");
			
			for (int i = 1; ; i++) {
				String headerFieldKey = conn.getHeaderFieldKey(i);
				String headerField = conn.getHeaderField(i);
				
				if (headerFieldKey == null)
					break;
				
				sb.append(headerFieldKey);
				sb.append(": ");
				sb.append(headerField);
				sb.append(", ");
			}
			
			sb.append(" }. ");
			
			// Body (if any)
			//sb.append(SystemUtils.LINE_SEPARATOR);
			sb.append("Body: { ");
			sb.append(HttpUtils.readBodyAsText(conn));
			sb.append(" }");
			sb.append(" }.");
			
		} catch (IOException e) {
			sb.append(" Detail cannot be provided due to IOException: " +
					PxUtils.stackTraceAsString(e));
		}
		
		return sb.toString();
	}
	
	//--------------------------------------------------------------------------
	// Public API - instance
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Overrides and implementations
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Consider toString(), equals(), hashCode()
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Getters/setters of properties
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Helper methods (mostly private)
	//--------------------------------------------------------------------------
}
