package com.virtage.plexflow.storage.datafolder.client;

import java.net.HttpURLConnection;

import org.apache.commons.lang3.SystemUtils;

import com.virtage.plexflow.logging.ISysLog;
import com.virtage.plexflow.logging.Logger;

/**
 * Reports malformed or incomplete response received from HTTP API components.
 * 
 * <p>Response, for example, doesn't contain required response headers or response is in unexpected
 * format (e.g. binary stream when no body expected).</p>
 * 
 * <p>Offers a number of constructors depending on desirable
 * <tt><i>exceptionInstance</i>.getMessage()</tt> value. Whenever possible, <tt>getMessage()</tt>
 * will prints response headers and body to ease troubleshooting.</p>
 * 
 * <p>This dumping HTTP response dumping feature is implemented in
 * {@link HttpClientUtils#readBodyAsText(HttpURLConnection)} static method visible for others within Java
 * package access.</p>
 * 
 * @author libor
 *
 */
public class MalformedResponseReceivedException extends Exception {
	
	//--------------------------------------------------------------------------
    // Class fields - public
    //--------------------------------------------------------------------------
 
	
    //--------------------------------------------------------------------------
    // Class fields - non-public
    //--------------------------------------------------------------------------
 
	private static final long serialVersionUID = -707630406228595943L;
	
	protected static final String BASE_MSG = "Malformed response received.";
	protected static final String NO_DETAIL = " No details available.";
	
	//private static final ISysLog SYSLOG = Logger.getSysLog(BadResponseReceivedException.class);

 
    //--------------------------------------------------------------------------
    // Instance fields - public
    //--------------------------------------------------------------------------
 
	/** Holds message for every getMessage() invocation. */
	protected String msg;
	
    //--------------------------------------------------------------------------
    // Instance fields - non-public
    //--------------------------------------------------------------------------
	
    //--------------------------------------------------------------------------
    // Instance fields - bean properties
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Constructors, static initializers, factories
    //--------------------------------------------------------------------------
	
	/**
	 * getMessage() will look like "yourMessage httpResponseDump". 
	 */
	public MalformedResponseReceivedException(String yourMessage, HttpURLConnection conn) {
		this.msg = yourMessage + SystemUtils.LINE_SEPARATOR + HttpClientUtils.dumpResponse(conn);
	}
	
	
	/**
	 * getMessage() will look like "BASE_MSG httpResponseDump".
	 */
	public MalformedResponseReceivedException(HttpURLConnection conn) {
		this.msg = BASE_MSG + SystemUtils.LINE_SEPARATOR + HttpClientUtils.dumpResponse(conn);
	}
	
	
	/**
	 * getMessage() will look like "yourMessage".
	 */
	public MalformedResponseReceivedException(String yourMessage) {
		this.msg = yourMessage;
	}
	
	/**
	 * getMessage() will look like "BASE_MSG".
	 */
	public MalformedResponseReceivedException() {
		this.msg = BASE_MSG;
	}	
	
    //--------------------------------------------------------------------------
    // Public API - class (static)
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Public API - instance
    //--------------------------------------------------------------------------
	
    //--------------------------------------------------------------------------
    // Overrides and implementations
    //--------------------------------------------------------------------------
	
	@Override
	public String getMessage() {
		return msg;
	}
	
    //--------------------------------------------------------------------------
    // Getters/setters of properties
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Helper methods (mostly private)
    //--------------------------------------------------------------------------


}
