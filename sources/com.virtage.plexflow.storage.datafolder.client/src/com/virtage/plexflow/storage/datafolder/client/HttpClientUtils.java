package com.virtage.plexflow.storage.datafolder.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;

import com.virtage.plexflow.utils.PxUtils;

final class HttpClientUtils {


	//--------------------------------------------------------------------------
	// Class fields - public
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Class fields - non-public
	//--------------------------------------------------------------------------

	//private static final ISysLog SYSLOG = Logging.getSysLog(ThisClass.class);

	//--------------------------------------------------------------------------
	// Instance fields - public
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Instance fields - non-public
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Instance fields - bean properties
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Constructors, static initializers, factories
	//--------------------------------------------------------------------------
	private HttpClientUtils() {}
	
	//--------------------------------------------------------------------------
	// Public API - class (static)
	//--------------------------------------------------------------------------

	public static String readBodyAsText(HttpURLConnection conn) throws IOException {
		BufferedReader is = new BufferedReader(new InputStreamReader(conn.getInputStream()));
		StringBuffer sb = new StringBuffer();
		String str;
		
		while ((str = is.readLine()) != null) {
			sb.append(str);
		}
		
		return sb.toString();
	}
	
	/** Returns status code, headers and body as one string. */
	public static String dumpResponse(HttpURLConnection conn) {
		StringBuffer sb = new StringBuffer();
		
		try {
			sb.append("Full HTTP response dump follows: { ");
			
			// Response status code
			sb.append("Status: ");
			sb.append(conn.getResponseCode());
			sb.append(" ");
			sb.append(conn.getResponseMessage());
			sb.append(". ");
			
			// Headers
			//sb.append(SystemUtils.LINE_SEPARATOR);
			sb.append("Headers: { ");
			
			for (int i = 1; ; i++) {
				String headerFieldKey = conn.getHeaderFieldKey(i);
				String headerField = conn.getHeaderField(i);
				
				if (headerFieldKey == null)
					break;
				
				sb.append(headerFieldKey);
				sb.append(": ");
				sb.append(headerField);
				sb.append(", ");
			}
			
			sb.append(" }. ");
			
			// Body (if any)
			//sb.append(SystemUtils.LINE_SEPARATOR);
			sb.append("Body: { ");
			sb.append(HttpClientUtils.readBodyAsText(conn));
			sb.append(" }");
			sb.append(" }.");
			
		} catch (IOException e) {
			sb.append(" Detail cannot be provided due to IOException: " +
					PxUtils.stackTraceAsString(e));
		}
		
		return sb.toString();
	}
	
	//--------------------------------------------------------------------------
	// Public API - instance
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Overrides and implementations
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Consider toString(), equals(), hashCode()
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Getters/setters of properties
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Helper methods (mostly private)
	//--------------------------------------------------------------------------
}
