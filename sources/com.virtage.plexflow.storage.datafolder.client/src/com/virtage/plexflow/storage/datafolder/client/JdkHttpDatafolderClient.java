package com.virtage.plexflow.storage.datafolder.client;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.nio.file.StandardOpenOption;

import org.apache.commons.codec.binary.Base64;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.SubMonitor;

import com.virtage.plexflow.config.ConfigException;
import com.virtage.plexflow.config.ConfigFactory;
import com.virtage.plexflow.server.api.ServerConfigKeys;
import com.virtage.plexflow.server.api.ServerConsts;
import com.virtage.plexflow.server.commands.DeleteCommand;
import com.virtage.plexflow.server.commands.PullCommand;
import com.virtage.plexflow.server.commands.PushBeginCommand;
import com.virtage.plexflow.server.commands.PushCommitCommand;
import com.virtage.plexflow.server.commands.PushRollbackCommand;
import com.virtage.plexflow.server.commands.VersionCommand;
import com.virtage.plexflow.storage.IFile;
import com.virtage.plexflow.storage.Storage;
import com.virtage.plexflow.system.SystemInstanceFactory;
import com.virtage.plexflow.utils.IoUtils;

/**
 * Implementation using standard JDK HttpURLConnection only.
 * 
 * @author libor
 *
 */
/*
 * JDK HttpURLConnection extremely good tut by BaluC:
 * http://stackoverflow.com/questions/2793150/how-to-use-java-net-urlconnection-to-fire-and-handle-http-requests
 * 
 * http://www.avajava.com/tutorials/lessons/how-do-i-connect-to-a-url-using-basic-authentication.html
 * http://en.wikipedia.org/wiki/Basic_access_authentication
 * http://www.ask.com/wiki/List_of_HTTP_status_codes
 */
class JdkHttpDatafolderClient implements IDatafolderClient {
		
	//--------------------------------------------------------------------------
	// Class fields - public
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Class fields - non-public
	//--------------------------------------------------------------------------

	//private static final ISysLog SYSLOG = Logging.getSysLog(ThisClass.class);

	//--------------------------------------------------------------------------
	// Instance fields - public
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Instance fields - non-public
	//--------------------------------------------------------------------------
	/** HTTP API's base URL. */
	private final String HTTP_API_URL;
	
	/** If authentication needed, base64 encoded string for reuse in futher requests. */
	private String authStrBase64Encoded;
	
	//--------------------------------------------------------------------------
	// Instance fields - bean properties
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Constructors, static initializers, factories
	//--------------------------------------------------------------------------

	public JdkHttpDatafolderClient() throws DatafolderClientException {
		try {
			this.HTTP_API_URL = ConfigFactory.getGlobal().getString(ServerConfigKeys.URL);
			
		} catch (ConfigException e) {
			throw new DatafolderClientException(e);
		}
	}
	
	//--------------------------------------------------------------------------
	// Public API - class (static)
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Public API - instance
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Overrides and implementations
	//--------------------------------------------------------------------------

	@Override
	public void pull(IFile file, Path path, IProgressMonitor monitor) throws DatafolderClientException {
		HttpURLConnection conn = null;
		try {
			conn = makeAndResolveConnection(new PullCommand().getCommandURI() +
				"?" + ServerConsts.PARAM_FILE_ID + "=" + file.getId());
			
			if (!isResponseOk(conn))
				throw new BadResponseReceivedException();
			
			// Download to temp file
			Path tempFile = Files.createTempFile(null, null);
			
			try (InputStream is = conn.getInputStream();
				 OutputStream os = Files.newOutputStream(tempFile, StandardOpenOption.DELETE_ON_CLOSE)) {
				IoUtils.copyStream(is, os, true, monitor, file.getLength());
			}
			
			// Compare CRC
			long crc32tmp = IoUtils.crc32(tempFile);
			long crc32db  = file.getCrc32();
			if (crc32tmp != crc32db) {
				throw new IOException("Checksum mismatched. Pulled file checksum is different from database.");
			}
			
			// Compare length
			long lengthTmp = Files.size(tempFile);
			long lengthDb  = file.getLength();
			if (lengthTmp != lengthDb) {
				throw new IOException("Length mismatched. Pulled file length is different from database.");
			}
			
			// Move temp file to actual file
			Files.move(tempFile, path, StandardCopyOption.REPLACE_EXISTING, StandardCopyOption.ATOMIC_MOVE);
			
			// Delete temp file
			Files.delete(tempFile);
			
		} catch (Exception e) {
			throw new DatafolderClientException(e);
			
		} finally {
			cleanupConnection(conn);
		}
	}

	@Override	
	public void push(IFile file, Path path, IProgressMonitor monitor) throws DatafolderClientException {
		SubMonitor subMonitor = SubMonitor.convert(monitor, 100);
		
		// Push-begin (uploading) is considered as 95% of operation
		tryPushBeginOrRollback(file, path, subMonitor.newChild(95));
				
		tryPushCommit(file, path);
		
		subMonitor.done();
	}	

	@Override
	public void delete(IFile file) throws DatafolderClientException {
		HttpURLConnection conn = null;
		try {
			conn = makeAndResolveConnection(new DeleteCommand().getCommandURI() +
				"?" + ServerConsts.PARAM_FILE_ID + "=" + file.getId());
			
			if (!isResponseOk(conn))
				throw new BadResponseReceivedException();
			
		} catch (Exception e) {
			throw new DatafolderClientException(e);
			
		} finally {
			cleanupConnection(conn);
		}
		
	}

	@Override
	public String version() throws DatafolderClientException {
		String version = null;
		HttpURLConnection conn = null;		

		try {
			conn = makeAndResolveConnection(
					new VersionCommand().getCommandURI());
			version = conn.getHeaderField(ServerConsts.HEADER_RESPONSE_MESSAGE);
			
		} catch (Exception e) {
			// Can throw IOException, BadResponseReceivedException,
			// MalformedResponseReceivedException, or ConfigException
			throw new DatafolderClientException(e);
			
		} finally {
			cleanupConnection(conn);
		}
		
		if (version == null) {
			throw new DatafolderClientException(
					new MalformedResponseReceivedException(
					"VERSION command responsed with null string."));
		}
		
		return version;
	}
	
	
	//--------------------------------------------------------------------------
	// Consider toString(), equals(), hashCode()
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Getters/setters of properties
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Helper methods (mostly private)
	//--------------------------------------------------------------------------
	
	/**
	 * Tries to PUSH-BEGIN, followed by issuing PUSH-ROLLBACK on failure.
	 * 
	 * @param file
	 * @param path
	 * @param monitor
	 * @throws DatafolderClientException
	 */
	private void tryPushBeginOrRollback(IFile file, Path path,
			IProgressMonitor monitor) throws DatafolderClientException {		
		try {
			httpPushBegin(file, path, monitor);
			
		} catch (Exception beginEx) {
			try {
				httpPushRollback(file, path);
				
			} catch (Exception rollbackEx) {
				DatafolderClientException dfEx = 
						new DatafolderClientException("Both PUSH-BEGIN " +
						"command (root exception) and PUSH-ROLLBACK " +
						"command (supressed exception) failed.", beginEx);
				dfEx.addSuppressed(rollbackEx);			// JDK7 only
				
				throw dfEx;
			}
			
			throw new DatafolderClientException(
					"PUSH-BEGIN command failed.", beginEx);
		}
	}
	
	
	/**
	 * Tries to PUSH-COMMIT.
	 * 
	 * @param file
	 * @param path
	 * @throws DatafolderClientException
	 */
	private void tryPushCommit(IFile file, Path path)
			throws DatafolderClientException {
		try {
			httpPushCommit(file, path);
			
		} catch (Exception commitEx) {
			throw new DatafolderClientException(
					"PUSH-COMMIT command failed.", commitEx);
		}
	}
	
	
	private void httpPushBegin(IFile file, Path path, IProgressMonitor monitor) throws MalformedURLException, IOException, BadResponseReceivedException, MalformedResponseReceivedException, ConfigException  {
	HttpURLConnection conn = null;
		
		try {
			// e.g. "/push-begin?fileId=5589"
			String commandURI = new PushBeginCommand().getCommandURI();
			conn = makeConnection(
				 commandURI + "?" + ServerConsts.PARAM_FILE_ID + "=" + file.getId());
			conn.setRequestMethod("PUT");
			// Otherwise cannot getOutputStream() and read to it
			conn.setDoOutput(true);
			
			// JDK 7's new "try-with-resources" statement
			// http://docs.oracle.com/javase/tutorial/essential/exceptions/tryResourceClose.html
			try (InputStream is = Files.newInputStream(path);
				 OutputStream os = conn.getOutputStream()) {
				IoUtils.copyStream(is, os, false, monitor, Files.size(path));
			}
			
			// Resolve response 
			resolveResponse(conn);
						
			// Compare CRC32s from response and original
			long respCrc32 = Long.valueOf(conn.getHeaderField(ServerConsts.HEADER_FILE_CRC32));
			long origCrc32 = IoUtils.crc32(path);			
			if (respCrc32 != origCrc32) {
				throw new IOException("Mismatched CRC32 checksums of original (" +
						origCrc32 + ") and transfered file (" + respCrc32 + ").");
			}
			
			// Compare file lengths from response and original
			long respLength = Long.valueOf(conn.getHeaderField(ServerConsts.HEADER_FILE_LENGTH));			
			long origLength = Files.size(path);			
			if (respLength != origLength) {
				throw new IOException("Mismatched lengths of original (" +
						origLength + ") and transfered file (" + respLength + ").");
			}
			
		} finally {
			cleanupConnection(conn);
		}
		
	}
	
	
	private void httpPushRollback(IFile file, Path path) throws MalformedURLException, ConfigException, IOException, BadResponseReceivedException, MalformedResponseReceivedException {
		HttpURLConnection conn = null;
		
		try {
			// e.g. "/push-begin?fileId=5589"
			String commandURI = new PushRollbackCommand().getCommandURI();
			conn = makeAndResolveConnection(
				 commandURI + "?" + ServerConsts.PARAM_FILE_ID + "=" + file.getId());
			
			// Resolve response 
			resolveResponse(conn);
				
			// Other response then OK
			if (!isResponseOk(conn)) {
				throw new BadResponseReceivedException(conn);
			}
			
			
		} finally {
			cleanupConnection(conn);
		}
		
		
	}
	
	
	private void httpPushCommit(IFile file, Path path) throws MalformedURLException, ConfigException, IOException, BadResponseReceivedException, MalformedResponseReceivedException {
		HttpURLConnection conn = null;
		
		try {
			String commandURI = new PushCommitCommand().getCommandURI();
			conn = makeAndResolveConnection(
				 commandURI + "?" + ServerConsts.PARAM_FILE_ID + "=" + file.getId());
				
			// Other response then OK
			if (!isResponseOk(conn)) {
				throw new BadResponseReceivedException(conn);
			}			
			
		} finally {
			cleanupConnection(conn);
		}
	}
	
	
	/** Shorthand for <tt>resolveResponse(makeConnection(String))</tt>. */
	private HttpURLConnection makeAndResolveConnection(String commandURI)
	throws MalformedURLException, IOException, BadResponseReceivedException,
		   MalformedResponseReceivedException, ConfigException {
		
		return resolveResponse(makeConnection(commandURI)); 
	}
	
	
	/**
	 * Returns a new, and authenticated (if needed), HttpURLConnection ready
	 * to use.
	 * 
	 * @param commandURI Command URI added to HTTP API base URI. E.g. base URI is "http://rodger:8080/plexflow-api/"
	 * and commandURI is "version", result will be "http://rodger:8080/plexflow-api/version"
	 * @throws ConfigException 
	 * @throws IOException 
	 * @throws MalformedURLException 
	 *
	 **/
	private HttpURLConnection makeConnection(String commandURI)
	throws ConfigException, MalformedURLException, IOException {
		
		// Base URL + command (e.g. http://somewhere/plexflow-api + /push-commit)
		HttpURLConnection conn = (HttpURLConnection) 
				new URL(HTTP_API_URL + commandURI).openConnection();

		// *** Choose whether to use authentication ***
		String authStyle = ConfigFactory.getGlobal().getString(ServerConfigKeys.AUTH_METHOD);
		
		// BASIC authentication
		if (ServerConfigKeys.AUTH_METHOD_BASIC.equals(authStyle)) {
			String username = SystemInstanceFactory.getInstance().getUser().getUsername();
			String password = SystemInstanceFactory.getInstance().getUserCleartextPassword();			
			String authStr = username + ":" + password;
			
			if (this.authStrBase64Encoded == null) {
				this.authStrBase64Encoded = Base64.encodeBase64String(authStr.getBytes());
			}
			
			conn.setRequestProperty("Authorization", "Basic " + authStrBase64Encoded);

		// NONE authentication
		} else if (ServerConfigKeys.AUTH_METHOD_NONE.equals(authStyle)) {
			// no authentication, do nothing
			
		// Unknown value
		} else {
			throw new ConfigException(ServerConfigKeys.AUTH_METHOD, authStyle);

		}
		
		return conn;
	}
	
	
	private void cleanupConnection(HttpURLConnection conn) {
		if (conn != null) conn.disconnect();
		conn = null;
	}
	
	
	/**
	 * Resolves a response from connection. Don't invoke this method before
	 * having a prepared response. Purpose of method is to throw appropriate
	 * exception if response is malformed (in terms of Plexflow HTTP API
	 * protocol or even HTTP protocol) or server response with BAD response
	 * type.
	 * 
	 * @param conn
	 * @throws IOException
	 * @throws BadResponseReceivedException
	 * @throws MalformedResponseReceivedException
	 */
	private HttpURLConnection resolveResponse(HttpURLConnection conn)
	throws IOException, BadResponseReceivedException, MalformedResponseReceivedException {
		
		if (conn.getResponseCode() != 200) {
			throw new MalformedResponseReceivedException("Response code other then 200 OK received: '" +
					conn.getResponseCode() + " " + conn.getResponseMessage() + "'.");
		}
		
		String responseType = conn.getHeaderField(ServerConsts.HEADER_RESPONSE_TYPE);
		if (responseType == null) {
			throw new MalformedResponseReceivedException("Missing response " +
					"header '" + ServerConsts.HEADER_RESPONSE_TYPE + "'.",
					conn);
		}
		
		if ((!responseType.equals(ServerConsts.HEADER_RESPONSE_TYPE_OK)) && 
			(!responseType.equals(ServerConsts.HEADER_RESPONSE_TYPE_BAD))) {			
			throw new MalformedResponseReceivedException("Not recognized" +
					" value of header '" + ServerConsts.HEADER_RESPONSE_TYPE +
					"'.", conn);
		}
		
		return conn;		
	}
	
	private boolean isResponseOk(HttpURLConnection conn) {
		String responseType = conn.getHeaderField(ServerConsts.HEADER_RESPONSE_TYPE);
		
		if (ServerConsts.HEADER_RESPONSE_TYPE_OK.equals(responseType)) {
			return true;
		} else {
			return false;
		}
	}
}
