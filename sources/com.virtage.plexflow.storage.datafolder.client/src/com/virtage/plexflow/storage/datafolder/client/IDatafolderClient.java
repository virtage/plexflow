package com.virtage.plexflow.storage.datafolder.client;

import java.nio.file.Path;

import org.eclipse.core.runtime.IProgressMonitor;

import com.virtage.plexflow.storage.IFile;

public interface IDatafolderClient {
	//--------------------------------------------------------------------------
    // Class fields - public
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Class fields - non-public
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Instance fields - public
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Instance fields - non-public
    //--------------------------------------------------------------------------
	
    //--------------------------------------------------------------------------
    // Instance fields - bean properties
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Constructors, static initializers, factories
    //--------------------------------------------------------------------------
 	
    //--------------------------------------------------------------------------
    // Public API - class (static)
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Public API - instance
    //-------------------------------------------------------------------------- 
	
	/**
	 * Performs "PULL" command and stores output as specified path.
	 * 
	 * @param file Plexflow file being requested
	 * @param path Path to physical file where Plexflow file will be saved. File
	 * will be overwritten if it exists.
	 * @param monitor the progress monitor to use for reporting progress to the user. It is the caller's responsibility
        to call done() on the given monitor. Accepts null, indicating that no progress should be
        reported and that the operation cannot be cancelled.
	 */
	public void pull(IFile file, Path path, IProgressMonitor monitor) throws DatafolderClientException;
	
	
    /**
	 * Performs sequence of "PUSH-BEGIN" and "PUSH-COMMIT" (ideal case), or "PUSH-BEGIN" and
	 * "PUSH-ROLLBACK" (worse case) commands.
	 * 
	 * @param file Plexflow file that should be linked with being pushed 
	 * physical file.
	 * @param path Valid filesystem Path to file that will be pushed
	 * @param monitor the progress monitor to use for reporting progress to the user. It is the caller's responsibility
        to call done() on the given monitor. Accepts null, indicating that no progress should be
        reported and that the operation cannot be cancelled.
	 */
	public void push(IFile file, Path path, IProgressMonitor monitor) throws DatafolderClientException;

	/**
	 * Performs "DELETE" command.
	 * 
	 * @return
	 */
	public void delete(IFile file) throws DatafolderClientException;

	/**
	 * Performs "VERSION" command.
	 * 
	 * @return
	 * @throws HttpApiClientException 
	 */
	public String version() throws DatafolderClientException;
    
    //--------------------------------------------------------------------------
    // Overrides and implementations
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Getters/setters of properties
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Helper methods (mostly private)
    //--------------------------------------------------------------------------
}
