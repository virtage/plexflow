package com.virtage.plexflow.ui.explorer;

import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.ListViewer;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.part.ViewPart;

import com.virtage.plexflow.storage.Bookmark;
import com.virtage.plexflow.storage.INode;
import com.virtage.plexflow.storage.Storage;
import com.virtage.plexflow.storage.exceptions.StorageException;
import com.virtage.plexflow.logging.ISysLog;
import com.virtage.plexflow.logging.Logger;


public class ExplorerView extends ViewPart {
	
    //--------------------------------------------------------------------------
    // Class fields - public
    //--------------------------------------------------------------------------
	public static final String ID = "com.virtage.plexflow.ui.ui.explorer.ExplorerView"; //$NON-NLS-1$
	
    //--------------------------------------------------------------------------
    // Class fields - non-public
    //-------------------------------------------------------------------------- 
    private static final ISysLog SYSLOG = Logger.getSysLog(ExplorerView.class);
 
    //--------------------------------------------------------------------------
    // Instance fields - public
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Instance fields - non-public
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Instance fields - bean properties
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Constructors, static initializers, factories
    //--------------------------------------------------------------------------
    public ExplorerView() {
	}
    
    //--------------------------------------------------------------------------
    // Public API - class (static)
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Public API - instance
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Overrides and implementations
    //--------------------------------------------------------------------------
    
    /**
	 * Create contents of the view part.
	 * @param parent
	 */
	@Override
	public void createPartControl(Composite parent) {
		
		ListViewer viewer = new ListViewer(parent, SWT.SINGLE);
		
		
		
		viewer.setLabelProvider(new ContainerLabelProvider());
		viewer.setContentProvider(new ArrayContentProvider());
		
		//listViewer.setInput(new String[] { "jedna", "dva", "tři" });
		viewer.setInput(loadInput());
		
		viewer.addSelectionChangedListener(new ISelectionChangedListener() {			
			@Override
			public void selectionChanged(SelectionChangedEvent event) {
				System.out.println("selectionChanged()");				
			}
		});
		
		
//		Composite container = new Composite(parent, SWT.NONE);
//		{
//			Label lblNewLabel = new Label(container, SWT.NONE);
//			lblNewLabel.setBounds(10, 10, 164, 15);
//			lblNewLabel.setText("Hello, I am Explorer view!");
//		}
//		createActions();
//		initializeToolBar();
//		initializeMenu();
	}
	
	
	@Override
	public void setFocus() {
		// Set the focus
		
		//IProject project = ResourcesPlugin.getWorkspace().getRoot().getProject("com.virtage.plexflow.ui.ui.explorer");
		//IEditorInput input = new ExplorerEditorInput(project);
//		
//		try {
//			getViewSite().getPage().openEditor(input, ExplorerEditor.ID);
//			
//		} catch (PartInitException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
	}
    
    //--------------------------------------------------------------------------
    // Getters/setters of properties
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Helper methods (mostly private)
    //--------------------------------------------------------------------------	

	/** Array of hard folders followed by bookmarks */
	private Object[] loadInput() {
//		List<Object> result = new ArrayList<>();
//		
//		result.add(Storage.getHome());
//		result.add(Storage.getWorkspace());
//		result.addAll(Bookmark.listBookmarks());
//						
//		return result.toArray();
		return new Object[] { "ahoj", "jak se", "vede?" }; 
	}
	
	//--------------------------------------------------------------------------
    // Helper nested classes
    //--------------------------------------------------------------------------


	private final class ContainerLabelProvider extends LabelProvider {
		@Override
		public String getText(Object el) {
			if (el instanceof INode)
				return ((INode) el).getNodeName().toString();
			
			if (el instanceof Bookmark)
				return ((Bookmark) el).getDisplayName();
			
			return "<CAN'T GET LABEL>";
		}

		@Override
		public Image getImage(Object el) {
			return StorageUiUtils.getIconFor(el);
		}
	}
	
}
