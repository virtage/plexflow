package com.virtage.plexflow.ui.explorer;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormAttachment;

public class BookmarkManagerDialog2 extends Dialog {
	private Table table;

	/**
	 * Create the dialog.
	 * @param parentShell
	 */
	public BookmarkManagerDialog2(Shell parentShell) {
		super(parentShell);
	}

	/**
	 * Create contents of the dialog.
	 * @param parent
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		Composite container = (Composite) super.createDialogArea(parent);
		container.setLayout(new FormLayout());
		
		TableViewer tableViewer = new TableViewer(container, SWT.BORDER | SWT.FULL_SELECTION);
		table = tableViewer.getTable();
		FormData fd_table = new FormData();
		fd_table.bottom = new FormAttachment(0, 388);
		fd_table.right = new FormAttachment(0, 366);
		fd_table.top = new FormAttachment(0, 62);
		fd_table.left = new FormAttachment(0, 11);
		table.setLayoutData(fd_table);
		
		Button btnNewButton = new Button(container, SWT.NONE);
		FormData fd_btnNewButton = new FormData();
		fd_btnNewButton.left = new FormAttachment(table, 6);
		fd_btnNewButton.right = new FormAttachment(100, -10);
		fd_btnNewButton.top = new FormAttachment(table, 0, SWT.TOP);
		btnNewButton.setLayoutData(fd_btnNewButton);
		btnNewButton.setText("Add new...");
		
		Button btnEdit = new Button(container, SWT.NONE);
		FormData fd_btnEdit = new FormData();
		fd_btnEdit.top = new FormAttachment(btnNewButton, 6);
		fd_btnEdit.right = new FormAttachment(100, -10);
		fd_btnEdit.left = new FormAttachment(table, 6);
		btnEdit.setLayoutData(fd_btnEdit);
		btnEdit.setText("Edit...");
		
		Button btnNewButton_1 = new Button(container, SWT.NONE);
		FormData fd_btnNewButton_1 = new FormData();
		fd_btnNewButton_1.top = new FormAttachment(btnEdit, 6);
		fd_btnNewButton_1.right = new FormAttachment(100, -10);
		fd_btnNewButton_1.left = new FormAttachment(table, 6);
		btnNewButton_1.setLayoutData(fd_btnNewButton_1);
		btnNewButton_1.setText("Delete");
		
		Button btnNewButton_2 = new Button(container, SWT.NONE);
		FormData fd_btnNewButton_2 = new FormData();
		fd_btnNewButton_2.top = new FormAttachment(btnNewButton_1, 41);
		fd_btnNewButton_2.left = new FormAttachment(table, 6);
		fd_btnNewButton_2.right = new FormAttachment(btnNewButton, 0, SWT.RIGHT);
		btnNewButton_2.setLayoutData(fd_btnNewButton_2);
		btnNewButton_2.setText("Move up");
		
		Button btnNewButton_3 = new Button(container, SWT.NONE);
		FormData fd_btnNewButton_3 = new FormData();
		fd_btnNewButton_3.left = new FormAttachment(table, 6);
		fd_btnNewButton_3.right = new FormAttachment(btnNewButton, 0, SWT.RIGHT);
		fd_btnNewButton_3.top = new FormAttachment(btnNewButton_2, 6);
		btnNewButton_3.setLayoutData(fd_btnNewButton_3);
		btnNewButton_3.setText("Move down");

		return container;
		//return null;
	}

	/**
	 * Create contents of the button bar.
	 * @param parent
	 */
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL,
				true);
		createButton(parent, IDialogConstants.CANCEL_ID,
				IDialogConstants.CANCEL_LABEL, false);
	}

	/**
	 * Return the initial size of the dialog.
	 */
	@Override
	protected Point getInitialSize() {
		return new Point(494, 483);
	}
}
