package com.virtage.plexflow.ui.explorer;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.part.EditorPart;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.widgets.ToolItem;
import org.eclipse.wb.swt.ResourceManager;
import org.eclipse.swt.layout.GridData;
import org.eclipse.wb.swt.SWTResourceManager;


public class ExplorerEditor extends EditorPart {

	public static final String ID = "com.virtage.plexflow.ui.ui.explorer.ExplorerEditor"; //$NON-NLS-1$
	private ExplorerEditorInput input;

	public ExplorerEditor() {
	}

	/**
	 * Create contents of the editor part.
	 * @param parent
	 */
	@Override
	public void createPartControl(Composite parent) {
		Composite container = new Composite(parent, SWT.NONE);
		container.setLayout(new GridLayout(1, false));

		ToolBar toolBar = new ToolBar(container, SWT.FLAT | SWT.RIGHT);
		toolBar.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));

		ToolItem tltmGoParent = new ToolItem(toolBar, SWT.NONE);
		tltmGoParent.setToolTipText("Go parent folder");
		tltmGoParent.setImage(ResourceManager.getPluginImage("com.virtage.plexflow.ui.ui.explorer", "icons/arrow_up.png"));

				ToolItem tltmRefresh = new ToolItem(toolBar, SWT.NONE);
				tltmRefresh.setToolTipText("Refresh content");
				tltmRefresh.setImage(ResourceManager.getPluginImage("com.virtage.plexflow.ui.ui.explorer", "icons/arrow_refresh.png"));

		ToolItem tltmSearch = new ToolItem(toolBar, SWT.NONE);
		tltmSearch.setToolTipText("Search");
		tltmSearch.setImage(ResourceManager.getPluginImage("com.virtage.plexflow.ui.ui.explorer", "icons/magnifier.png"));

		ToolItem tltmDropdownShowAs = new ToolItem(toolBar, SWT.DROP_DOWN);
		tltmDropdownShowAs.setText("View as list");

		ScrolledComposite scrolledComposite = new ScrolledComposite(container, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);
		scrolledComposite.setBackground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		scrolledComposite.setAlwaysShowScrollBars(true);
		scrolledComposite.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, true, true, 1, 1));
		scrolledComposite.setExpandHorizontal(true);
		scrolledComposite.setExpandVertical(true);

		Label lblNewLabel = new Label(scrolledComposite, SWT.NONE);
		lblNewLabel.setImage(ResourceManager.getPluginImage("com.virtage.plexflow.ui.ui.explorer", "icons/page.png"));
		lblNewLabel.setText("file1.doc");
		scrolledComposite.setContent(lblNewLabel);
		scrolledComposite.setMinSize(lblNewLabel.computeSize(SWT.DEFAULT, SWT.DEFAULT));

	}

	@Override
	public void setFocus() {
		// Set the focus
	}

	@Override
	public void doSave(IProgressMonitor monitor) {
		// Do the Save operation
	}

	@Override
	public void doSaveAs() {
		// Do the Save As operation
	}

	@Override
	public void init(IEditorSite site, IEditorInput input)
			throws PartInitException {
		// Initialize the editor part
		
		if (!(input instanceof ExplorerEditorInput)) {
			throw new RuntimeException("Wrong input");
		}
		
		this.input = (ExplorerEditorInput) input;
		
		setSite(site);
		setInput(input);
	}

	@Override
	public boolean isDirty() {
		return false;
	}

	@Override
	public boolean isSaveAsAllowed() {
		return false;
	}
}
