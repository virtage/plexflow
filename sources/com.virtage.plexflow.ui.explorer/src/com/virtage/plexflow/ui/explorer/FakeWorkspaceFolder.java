package com.virtage.plexflow.ui.explorer;

import java.util.Date;
import java.util.List;

import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.ui.model.IWorkbenchAdapter;

import com.virtage.plexflow.storage.IFile;
import com.virtage.plexflow.storage.IFolder;
import com.virtage.plexflow.storage.ILock;
import com.virtage.plexflow.storage.INode;
import com.virtage.plexflow.storage.ITags;
import com.virtage.plexflow.storage.NodeName;
import com.virtage.plexflow.storage.events.NodeListener;
import com.virtage.plexflow.storage.exceptions.NodeAlreadyExistsException;
import com.virtage.plexflow.storage.exceptions.NodeDeletedException;
import com.virtage.plexflow.storage.exceptions.NodeNameForbiddenException;
import com.virtage.plexflow.storage.exceptions.NodeNotFoundException;
import com.virtage.plexflow.storage.exceptions.NodeTrashedException;
import com.virtage.plexflow.system.User;

/**
 * Fake workspace folder to render workspace as a only root element in JFace
 * tree viewers.
 * 
 * <p>Workspace folder is regular folder, therefore its <tt>childNodes()</tt>
 * returns workspace subfolders as top-level tree elements:
 * <pre>
 * + home/
 *   + pxsu/
 *   + joe/
 * + public/ 
 * </pre></p>
 * 
 * <p>Use this fake workspace {@link IFolder} implementation to achieve tree
 * looking like
 * <pre>
 * + Workspace
 *   + home/
 *     + pxsu/
 *     + joe/
 *   + public/
 * </pre></p>
 * 
 * <p>Class simply delegates most of its methods to actual
 * {@link IFolder} implementation supplied to {@link #FakeWorkspaceFolder(IFolder)}
 * constructor. The {@link #isWorkspace()} always returns false to prevent
 * recursive infinite loop.</p>
 * 
 * <p>Here is excerpt from {@link IWorkbenchAdapter#getChildren(Object)} body.
 * Note that if parent element is workspace, a fake workspace is returned.
 * In next invocation of method, a fake workspace responds to
 * {@link #isWorkspace()} with <tt>false</tt> thus proceeding to fetching
 * actual children of workspace but having fake workspace folder as parent.   
 * <pre>
 * public Object[] getChildren(Object o) {
 *    IFolder parent = (IFolder) o;
 *
 *    <b>if (parent.isWorkspace()) {
 *       return new IFolder[] { new FakeWorkspaceFolder(parent) };
 *    }</b>
 *
 *    List&lt;INode&gt; childNodes = null;
 *    try {
 *       childNodes = parent.childNodes();			
 *    } catch (NodeTrashedException | NodeDeletedException e) {
 *       ...			
 * }
 * </pre></p>
 * 
 * <p>All methods are simply delegating call to managed workspace folder
 * implementation except the following:
 * <ul>
 * <li>{@link #isWorkspace()} -- always responds false to prevent invocation
 * recursive loop.</li>
 * <li>{@link #toString()} -- informs that this is fake workspace folder.</li>
 * <li>{@link #hashCode()} and {@link #equals(Object)} --
 * <tt>hashCode()</tt> delegates to workspace implementation, however
 * <tt>equals()</tt> is rewritten (see methods javadoc).</li>
 * </ul>
 * </p>
 * 
 * @author libor
 * @see https://bugs.eclipse.org/bugs/show_bug.cgi?id=9262
 * @see http://manuelselva.wordpress.com/2009/01/16/bug-9262/
 */
public class FakeWorkspaceFolder implements IFolder {
	
	//--------------------------------------------------------------------------
	// Class fields - public
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Class fields - non-public
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Instance fields - public
	//--------------------------------------------------------------------------
	
	/**
	 * Managed workspace folder implementation.
	 */
	private IFolder workspaceImpl;
	
	//--------------------------------------------------------------------------
	// Instance fields - non-public
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Instance fields - bean properties
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Constructors, static initializers, factories
	//--------------------------------------------------------------------------

	/**
	 * Instantiate fake workspace delegating to specified actual workspace
	 * folder.
	 * 
	 * @param workspace Actual workspace folder instance.
	 * @throws IllegalArgumentException If argument is not workspace folder. 
	 */
	public FakeWorkspaceFolder(IFolder workspace) throws IllegalArgumentException {
		if (!workspace.isWorkspace())
			throw new IllegalArgumentException("Supplied folder '" +
				workspace.getPath().toString() + "' isn't a workspace!");
		
		this.workspaceImpl = workspace;
	}
	
	//--------------------------------------------------------------------------
	// Public API - class (static)
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Public API - instance
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Overrides and implementations
	//--------------------------------------------------------------------------

	/**
	 * Always responds <tt>false</tt> to prevent invocation recursive loop.
	 * See {@link FakeWorkspaceFolder} class javadoc for reasons. 
	 */
	@Override
	public boolean isWorkspace() {
		return false;		// always false
	}
	
	/**
	 * Informs this is fake workspace folder for what managed workspace
	 * folder implementation. 
	 */
	@Override
	public String toString() {
		return this.getClass().getSimpleName() + " for " + workspaceImpl;
	}	
	
	/** Delegates to managed workspace folder implementation. */
	@Override
	public int hashCode() {
		return workspaceImpl.hashCode();
	}	

	/**
	 * Customized <tt>equals()</tt>. Rules are as the following:
	 * <ul>
	 * <li>If compared object is also {@link FakeWorkspaceFolder}, compares
	 * managed workspace folder implementations.</li>
	 * <li>If compared object is regular {@link IFolder}, delegates to
	 * <tt>equal()</tt> of managed workspace folder implementation.</li>
	 * <li>In any other case, objects are not equal.</li>
	 * </ul>
	 */
	@Override
	public boolean equals(Object other) {
		if (this == other)
			return true;
		if (other == null)
			return false;
		if ((other instanceof IFolder) == false)
			return false;
		else {
			// It's also fake workspace, compare managed folders
			if (other instanceof FakeWorkspaceFolder)
				return this.workspaceImpl.equals(((FakeWorkspaceFolder) other).workspaceImpl);
			else
				// It's regular IFolder, compare managed folder with "other"
				return this.workspaceImpl.equals((IFolder) other);
		}
	}
	
	//--------------------------------------------------------------------------
	// Rest are just delegates
	//--------------------------------------------------------------------------
	
	/** Delegates to managed workspace folder implementation. */
	@Override
	public ILock lock() {
		return workspaceImpl.lock();
	}

	/** Delegates to managed workspace folder implementation. */
	@Override
	public ILock lock(long timeOutMillis) {
		return workspaceImpl.lock(timeOutMillis);
	}

	/** Delegates to managed workspace folder implementation. */
	@Override
	public ILock getLock() {
		return workspaceImpl.getLock();
	}

	/** Delegates to managed workspace folder implementation. */
	@Override
	public void unLock() {
		workspaceImpl.unLock();
	}

	/** Delegates to managed workspace folder implementation. */
	@Override
	public boolean canUnlock() {
		return workspaceImpl.canUnlock();
	}

	/** Delegates to managed workspace folder implementation. */
	@Override
	public boolean isLocked() {
		return workspaceImpl.isLocked();
	}

	/** Delegates to managed workspace folder implementation. */
	@Override
	public IFolder createFolder(NodeName name) throws NodeTrashedException,
			NodeDeletedException, NodeAlreadyExistsException {
		return workspaceImpl.createFolder(name);
	}

	/** Delegates to managed workspace folder implementation. */
	@Override
	public INode copy(IFolder targetFolder, IProgressMonitor monitor)
			throws NodeTrashedException, NodeDeletedException,
			NodeAlreadyExistsException {
		return workspaceImpl.copy(targetFolder, monitor);
	}

	/** Delegates to managed workspace folder implementation. */
	@Override
	public IFolder createFolder(String name) throws NodeTrashedException,
			NodeDeletedException, NodeAlreadyExistsException,
			NodeNameForbiddenException {
		return workspaceImpl.createFolder(name);
	}

	/** Delegates to managed workspace folder implementation. */
	@Override
	public INode copy(IFolder targetFolder) throws NodeTrashedException,
			NodeDeletedException, NodeAlreadyExistsException {
		return workspaceImpl.copy(targetFolder);
	}

	/** Delegates to managed workspace folder implementation. */
	@Override
	public IFile createFile(NodeName name) throws NodeTrashedException,
			NodeDeletedException, NodeAlreadyExistsException {
		return workspaceImpl.createFile(name);
	}

	/** Delegates to managed workspace folder implementation. */
	@Override
	public INode move(IFolder targetFolder, IProgressMonitor monitor)
			throws NodeTrashedException, NodeDeletedException,
			NodeAlreadyExistsException {
		return workspaceImpl.move(targetFolder, monitor);
	}

	/** Delegates to managed workspace folder implementation. */
	@Override
	public Object getAdapter(@SuppressWarnings("rawtypes") Class adapter) {
		return workspaceImpl.getAdapter(adapter);
	}

	/** Delegates to managed workspace folder implementation. */
	@Override
	public INode move(IFolder targetFolder) throws NodeTrashedException,
			NodeDeletedException, NodeAlreadyExistsException {
		return workspaceImpl.move(targetFolder);
	}

	/** Delegates to managed workspace folder implementation. */
	@Override
	public IFile createFile(String name) throws NodeTrashedException,
			NodeDeletedException, NodeAlreadyExistsException,
			NodeNameForbiddenException {
		return workspaceImpl.createFile(name);
	}

	/** Delegates to managed workspace folder implementation. */
	@Override
	public INode rename(NodeName newName) throws NodeTrashedException,
			NodeDeletedException, NodeAlreadyExistsException {
		return workspaceImpl.rename(newName);
	}

	/** Delegates to managed workspace folder implementation. */
	@Override
	public List<String> childNames(IProgressMonitor monitor)
			throws NodeTrashedException, NodeDeletedException {
		return workspaceImpl.childNames(monitor);
	}

	/** Delegates to managed workspace folder implementation. */
	@Override
	public INode rename(String newName) throws NodeTrashedException,
			NodeDeletedException, NodeAlreadyExistsException,
			NodeNameForbiddenException {
		return workspaceImpl.rename(newName);
	}

	/** Delegates to managed workspace folder implementation. */
	@Override
	public List<String> childNames() throws NodeTrashedException,
			NodeDeletedException {
		return workspaceImpl.childNames();
	}

	/** Delegates to managed workspace folder implementation. */
	@Override
	public long getId() {
		return workspaceImpl.getId();
	}

	/** Delegates to managed workspace folder implementation. */
	@Override
	public NodeName getNodeName() {
		return workspaceImpl.getNodeName();
	}

	/** Delegates to managed workspace folder implementation. */
	@Override
	public List<INode> childNodes(IProgressMonitor monitor)
			throws NodeTrashedException, NodeDeletedException {
		return workspaceImpl.childNodes(monitor);
	}

	/** Delegates to managed workspace folder implementation. */
	@Override
	public IFolder getParent() {
		return workspaceImpl.getParent();
	}

	/** Delegates to managed workspace folder implementation. */
	@Override
	public IPath getPath() {
		return workspaceImpl.getPath();
	}

	/** Delegates to managed workspace folder implementation. */
	@Override
	public List<INode> childNodes() throws NodeTrashedException,
			NodeDeletedException {
		return workspaceImpl.childNodes();
	}

	/** Delegates to managed workspace folder implementation. */
	@Override
	public Date getCreatedAt() {
		return workspaceImpl.getCreatedAt();
	}

	/** Delegates to managed workspace folder implementation. */
	@Override
	public int compareTo(INode o) {
		return workspaceImpl.compareTo(o);
	}

	/** Delegates to managed workspace folder implementation. */
	@Override
	public List<String> childNamesRecursively(IProgressMonitor monitor)
			throws NodeTrashedException, NodeDeletedException {
		return workspaceImpl.childNamesRecursively(monitor);
	}

	/** Delegates to managed workspace folder implementation. */
	@Override
	public void addNodeListener(NodeListener listener)
			throws NodeTrashedException, NodeDeletedException {
		workspaceImpl.addNodeListener(listener);
	}

	/** Delegates to managed workspace folder implementation. */
	@Override
	public void removeNodeListener(NodeListener listener)
			throws NodeTrashedException, NodeDeletedException {
		workspaceImpl.removeNodeListener(listener);
	}

	/** Delegates to managed workspace folder implementation. */
	@Override
	public List<String> childNamesRecursively() throws NodeTrashedException,
			NodeDeletedException {
		return workspaceImpl.childNamesRecursively();
	}

	/** Delegates to managed workspace folder implementation. */
	@Override
	public String getDescription() {
		return workspaceImpl.getDescription();
	}

	/** Delegates to managed workspace folder implementation. */
	@Override
	public void setDescription(String desc) {
		workspaceImpl.setDescription(desc);
	}

	/** Delegates to managed workspace folder implementation. */
	@Override
	public List<INode> childNodesRecursively(IProgressMonitor monitor)
			throws NodeTrashedException, NodeDeletedException {
		return workspaceImpl.childNodesRecursively(monitor);
	}

	/** Delegates to managed workspace folder implementation. */
	@Override
	public List<INode> childNodesRecursively() throws NodeTrashedException,
			NodeDeletedException {
		return workspaceImpl.childNodesRecursively();
	}

	/** Delegates to managed workspace folder implementation. */
	@Override
	public boolean existsFile(NodeName name) throws NodeTrashedException,
			NodeDeletedException {
		return workspaceImpl.existsFile(name);
	}

	/** Delegates to managed workspace folder implementation. */
	@Override
	public boolean existsFile(String name) throws NodeNameForbiddenException,
			NodeTrashedException, NodeDeletedException {
		return workspaceImpl.existsFile(name);
	}

	/** Delegates to managed workspace folder implementation. */
	@Override
	public boolean existsFolder(NodeName name) throws NodeTrashedException,
			NodeDeletedException {
		return workspaceImpl.existsFolder(name);
	}

	/** Delegates to managed workspace folder implementation. */
	@Override
	public boolean existsFolder(String name) throws NodeTrashedException,
			NodeDeletedException, NodeNameForbiddenException {
		return workspaceImpl.existsFolder(name);
	}

	/** Delegates to managed workspace folder implementation. */
	@Override
	public IFile getFile(NodeName name) throws NodeTrashedException,
			NodeDeletedException, NodeNotFoundException {
		return workspaceImpl.getFile(name);
	}

	/** Delegates to managed workspace folder implementation. */
	@Override
	public IFile getFile(String name) throws NodeTrashedException,
			NodeDeletedException, NodeNotFoundException,
			NodeNameForbiddenException {
		return workspaceImpl.getFile(name);
	}
	
	/** Delegates to managed workspace folder implementation. */
	@Override
	public IFolder getFolder(NodeName name) throws NodeTrashedException,
			NodeDeletedException, NodeNotFoundException {
		return workspaceImpl.getFolder(name);
	}

	/** Delegates to managed workspace folder implementation. */
	@Override
	public IFolder getFolder(String name) throws NodeTrashedException,
			NodeDeletedException, NodeNotFoundException,
			NodeNameForbiddenException {
		return workspaceImpl.getFolder(name);
	}

	/** Delegates to managed workspace folder implementation. */
	@Override
	public boolean isHomeForCurrentUser() {
		return workspaceImpl.isHomeForCurrentUser();
	}

	/** Delegates to managed workspace folder implementation. */
	@Override
	public boolean isHomeForUser(User user) {
		return workspaceImpl.isHomeForUser(user);
	}

	/** Delegates to managed workspace folder implementation. */
	@Override
	public boolean isHome() {
		return workspaceImpl.isHome();
	}

	/** Delegates to managed workspace folder implementation. */
	@Override
	public boolean isHomeRoot() {
		return workspaceImpl.isHomeRoot();
	}	

	/** Delegates to managed workspace folder implementation. */
	@Override
	public boolean isParentOf(INode node) throws NodeTrashedException,
			NodeDeletedException {
		return workspaceImpl.isParentOf(node);
	}

	/** Delegates to managed workspace folder implementation. */
	@Override
	public boolean hasChildNodes() throws NodeTrashedException,
			NodeDeletedException {
		return workspaceImpl.hasChildNodes();
	}

	/** Delegates to managed workspace folder implementation. */
	@Override
	public ITags getTags() {
		return workspaceImpl.getTags();
	}
	
	//--------------------------------------------------------------------------
	// Consider toString(), equals(), hashCode()
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Getters/setters of properties
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Helper methods (mostly private)
	//--------------------------------------------------------------------------
}
