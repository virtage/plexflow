package com.virtage.plexflow.ui.explorer.dialogs;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TreePath;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.model.BaseWorkbenchContentProvider;
import org.eclipse.ui.model.WorkbenchLabelProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.virtage.plexflow.storage.IFile;
import com.virtage.plexflow.storage.IFolder;
import com.virtage.plexflow.storage.INode;
import com.virtage.plexflow.storage.NodeName;
import com.virtage.plexflow.storage.Storage;
import com.virtage.plexflow.storage.exceptions.NodeAlreadyExistsException;
import com.virtage.plexflow.storage.exceptions.NodeDeletedException;
import com.virtage.plexflow.storage.exceptions.NodeTrashedException;
import com.virtage.plexflow.ui.explorer.filters.FolderOnlyFilter;
import com.virtage.plexflow.ui.explorer.filters.RootFolderFilter;
import com.virtage.plexflow.utils.PxUtils;


/**
 * <p>Dialog to choose file or folder. Dialog is conceptually
 * similar to SWT <tt>FileDialog</tt> and <tt>DirectoryDialog</tt>.
 * Select mode is controlled by style bits.</p>
 *  
 * <p>Supported style bits are:
 * <ul>
 * <li>{@link #STYLE_FOLDER} -- select folder mode .</li>
 * <li>{@link #STYLE_NEW_FOLDER_BUTTON} -- dialog will allow to create
 * new folders via "Create new folder" button. Applicable only in
 * conjunction with {@link #STYLE_FOLDER}.</li>
 * <li>{@link #STYLE_FOLDER_WITH_NEW_BUTTON} -- shortcut for two above</li>
 * <li>{@link #STYLE_FILE} -- select file mode</li> 
 * </ul>
 * </p>
 * 
 * <p>Worth to mention methods are:
 * <ul>
 * <li><tt>{@link #openAndReturn()}</tt> -- use this instead of JFace Dialog's
 * <tt>open()</tt>. It will open dialog, block execution
 * until node is chosen. This node is return value.</li>
 * <li><tt>{@link #setText(String)}</tt> -- sets dialog title bar.</li>
 * <li><tt>{@link #setFilterFolder(IFolder)}</tt> -- limits choosing to
 * particular folder and its child nodes.</li>
 * </ul> 
 * </p>
 * 
 * @author libor
 *
 */
public class StorageTreeDialog extends Dialog {	

	//--------------------------------------------------------------------------
	// Class fields - public
	//--------------------------------------------------------------------------

	/**
	 * Style bit for folder selection mode.
	 */
	public static final int STYLE_FOLDER = 2;
	
	/**
	 * Style bit forcing to show a "Create new folder" button. Applicable only
	 * when used in conjunction with {@link #STYLE_FOLDER}, otherwise is
	 * ignored.
	 */
	public static final int STYLE_NEW_FOLDER_BUTTON = 4;	
	
	/**
	 * Convenience style combining {@link #STYLE_FOLDER} and
	 * {@link #STYLE_NEW_FOLDER_BUTTON} in one style.
	 */
	public static final int STYLE_FOLDER_WITH_NEW_BUTTON = STYLE_FOLDER | STYLE_NEW_FOLDER_BUTTON;
		
	/**
	 * Style bit for file selection mode. 
	 */
	public static final int STYLE_FILE = 8;

	//--------------------------------------------------------------------------
	// Class fields - non-public
	//--------------------------------------------------------------------------
	
	private static final Logger SYSLOG = LoggerFactory.getLogger(StorageTreeDialog.class);
	
	/** Default dialog title text for folder browse. */
	private static final String TITLE_FOLDER = "Browse for Plexflow folder";
	
	/** Default dialog title text for file browse. */
	private static final String TITLE_FILE = "Browse for Plexflow file";
	
	/** "Create new folder" button ID. */ 
	private static final int NEW_FOLDER_BUTTON_ID = IDialogConstants.CLIENT_ID + 1;

	//--------------------------------------------------------------------------
	// Instance fields - public
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Instance fields - non-public
	//--------------------------------------------------------------------------
	
	/**
	 * Dialog title to be used. Set in c-or according to style, later maybe
	 * changed from {@link #setText(String)}.
	 **/
	private String title;
	
	/** Style bits set from c-or. */
	private int style;
	
	/** Folder to be used as filter for StorageTreeComposite. */
	private IFolder filterFolder = null;

	private Button btnNewFolder;
	private Button btnOk;
	
	/** Currently selected node (regularly updated by tree viewer listener). */
	private INode selectedNode;

	/** Reference to TreeViewer managed by inner StorageTreeComposite. */
	private TreeViewer treeViewer;

	//--------------------------------------------------------------------------
	// Instance fields - bean properties
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Constructors, static initializers, factories
	//--------------------------------------------------------------------------

	/**
	 * Creates new FolderDialog.
	 * 
	 * @param parentShell
	 * @param style 
	 */
	private StorageTreeDialog(Shell parentShell, int style) {
		super(parentShell);
		
		setShellStyle(SWT.DIALOG_TRIM | SWT.RESIZE);
		
		this.style = style;		
				
		// Check for common mistake to "Create new folder" style without
		// corresponding "FOLDER_SELECT" style
		if (isStyleNewFolderButton() && (!isStyleFolderSelect())) {
			throw new IllegalArgumentException("Ignored to show 'Create new folder' button in " +
				"file selection mode (corresponding folder selection mode " +
				"style bit has not been set).");
		}
		
		// Set dialog title
		if (isStyleFolderSelect()) {
			this.title = TITLE_FOLDER;
		} else if (isStyleFileSelect()) {
			this.title = TITLE_FILE;
		} else {
			throw new IllegalArgumentException("Illegal style set (" + style + ")");
		}
	}
	
	//--------------------------------------------------------------------------
	// Public API - class (static)
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Public API - instance
	//--------------------------------------------------------------------------

	/**
	 * Opens, blocks and returns chosen {@link INode} or null if dialog has been
	 * closed by Cancel or X. Easier to use then standard JFace <tt>open()</tt>.
	 * 
	 * @return Chosen {@link INode} or <tt>null</tt> when dialog has been closed by Cancel or X.
	 * @throws IllegalArgumentException if invalid style bits set
	 */
	public static INode openAndReturn(Shell parentShell, int style)
	throws IllegalArgumentException {
		StorageTreeDialog dg = new StorageTreeDialog(parentShell, style);
		dg.setBlockOnOpen(true);
		
		// Cancel or X pressed
		if (dg.open() != IDialogConstants.OK_ID) {
			return null;
		}
	
		return dg.selectedNode;
	}
	
	
	/**
	 * Sets custom dialog title. <b>Must be set before
	 * {@link #openAndReturn()}.</b>
	 * 
	 * @param title
	 */
	public void setText(String title) {
		this.title = title;
	}
	
	
	/**
	 * Limits visible (and thus selectable) path. Only this path or its children
	 * can be selected by user. <b>Must be set before
	 * {@link #openAndReturn()}.</b>
	 * 
	 * @param path. Null is ignored.
	 */
	public void setFilterFolder(IFolder filterFolder) {
		this.filterFolder = filterFolder;
	}
	
	//--------------------------------------------------------------------------
	// Overrides and implementations
	//--------------------------------------------------------------------------
	
	@Override
	protected Control createDialogArea(Composite parent) {
		Composite container = (Composite) super.createDialogArea(parent);
		container.setLayout(new FillLayout(SWT.HORIZONTAL));

		// This composite draws a tree
		TreeComposite treeComposite = new TreeComposite(				
				container, SWT.NONE, filterFolder, isStyleFolderSelect());
		
		this.treeViewer = treeComposite.treeViewer;
		
		// Selection listener		
		// WHY NOT addPostSelectionListener (IPostSelectionProvider)?
		// Post selections are delayed when triggered by keyboard event and
		// therefore may, in rare case, allow to user to press "OK" that is not
		// yet disabled despite inappropriate node type is already selected.
		this.treeViewer.addSelectionChangedListener(new ISelectionChangedListener() {
			@Override
			public void selectionChanged(SelectionChangedEvent event) {				
				IStructuredSelection ss = (IStructuredSelection) event.getSelection();
				
				if (ss.isEmpty())
					return;
				
				selectedNode = (INode) ss.getFirstElement();
				
				if (selectedNode instanceof IFolder) {
					// "Ok" enable only for folder selected
					if (isStyleFolderSelect()) {
						btnOk.setEnabled(true);
					} else {
						btnOk.setEnabled(false);
					}
										
					if (isStyleNewFolderButton()) {
						btnNewFolder.setEnabled(true);
					}
					
				} else if (selectedNode instanceof IFile) {
					// "Ok" enable only for file selected
					if (!isStyleFolderSelect()) {
						btnOk.setEnabled(true);
					} else {
						btnOk.setEnabled(false);
					}					
				}
			}
		});		

		return container;
	}

	
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		// "Create new folder" button only in also in folder select mode
		if (isStyleNewFolderButton()) {
			btnNewFolder = createButton(parent, NEW_FOLDER_BUTTON_ID, "Create new folder",
					false);
			// Initially set disabled until a parent folder is selected
			btnNewFolder.setEnabled(false);
		}
		
		btnOk = createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL,
				true);
		// Initially disabled to OK until a folder is selected
		btnOk.setEnabled(false);
		
		// "Cancel" button
		createButton(parent, IDialogConstants.CANCEL_ID, IDialogConstants.CANCEL_LABEL, false);
	}
	
	@Override
	protected void buttonPressed(int buttonId) {
		// Handles OK (super.okPressed()) and
		// Cancel (super.cancelPressed() buttons)
		super.buttonPressed(buttonId);
		
		if (isStyleNewFolderButton() && (buttonId == NEW_FOLDER_BUTTON_ID)) {
			handleNewFolder();
		}
	}

	@Override
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		
		newShell.setText(title);
	}
	
	/** Use {@link #openAndReturn(Shell, int)} instead. */
	@Override
	public int open() {
		return super.open();
	}	
	
	@Override
	protected Point getInitialSize() {
		return new Point(400, 450);
	}
	
	@Override
	protected boolean isResizable() {
		return true;
	}		
	
	//--------------------------------------------------------------------------
	// Consider toString(), equals(), hashCode()
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Getters/setters of properties
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Helper methods (mostly private)
	//--------------------------------------------------------------------------
		
	/** Handles pressing "Create new folder" button. */
	private void handleNewFolder() {
		IFolder parentFolder = (IFolder) selectedNode;		
		NodeName subfolderName = NodeNameInputDialog.openAndReturn(
				getShell(),
				NodeNameInputDialog.STYLE_FOLDER | NodeNameInputDialog.STYLE_NEW,
				null); 

		SYSLOG.debug("Creating new subfolder '{}' in folder '{}'",
			subfolderName, parentFolder);
		
		IFolder subfolder = null;
		try {
			subfolder = parentFolder.createFolder(subfolderName);
			
		} catch (NodeTrashedException e) {
			MessageDialog.openError(getShell(), "Failed to create folder",
				"Parent folder has been trashed meanwhile.");
			treeViewer.refresh(parentFolder.getParent());
			
		} catch (NodeDeletedException e) {
			MessageDialog.openError(getShell(), "Failed to create folder",
				"Parent folder has been deleted meanwhile.");
			treeViewer.refresh();
			
		} catch (NodeAlreadyExistsException e) {
			MessageDialog.openError(getShell(), "Folder already exists",
				"There is already folder of such name.");
		}
	
		if (subfolder == null) {
			return;
		}
		
		// Add subfolder and expand tree to make it visible
		this.treeViewer.add(parentFolder, subfolder);
		this.treeViewer.expandToLevel(parentFolder, 1);
		
		// Select newly created subfolder
		INode[] folderPath = Storage.nodePathAsArray(subfolder);		
		this.treeViewer
				.setSelection(new TreeSelection(new TreePath(folderPath)));
	}	

	private boolean isStyleFolderSelect() {		
		return PxUtils.isBitPresent(STYLE_FOLDER, style);
	}
	
	private boolean isStyleFileSelect() {
		return PxUtils.isBitPresent(STYLE_FILE, style);
	}	
	
	/** Reads from style whether "Create new folder" button should be on. */
	private boolean isStyleNewFolderButton() {
		return PxUtils.isBitPresent(STYLE_NEW_FOLDER_BUTTON, style);
	}
	
	
	
	//--------------------------------------------------------------------------
	// Nested class Composite
	//--------------------------------------------------------------------------
		
	/**
	 * <p>Composite showing storage tree viewer. Designed to be used from
	 * {@link StorageTreeDialog} but others may also make use of it.</p>
	 * 
	 * <p>Tree viewer managed by this composite is achievable as public field
	 * {@link #treeViewer}.</p>
	 * 
	 * @author libor
	 *
	 */
	static class TreeComposite extends Composite {
		
		/** Tree viewer managed by this composite. */ 
		public TreeViewer treeViewer;
		
		/**
		 * Create folder tree composite limiting folders to select from.
		 * 
		 * @param parent
		 * @param style
		 * @param filterFolder
		 *            or <tt>null</tt> to workspace as a filter (which
		 *            effectively means no filter).
		 * @param foldersOnly whether to limit content to folders only
		 */
		public TreeComposite(Composite parent, int style, IFolder filterFolder, boolean foldersOnly) {
			super(parent, style);
	
			// Set filter folder to workspace if null
			if (filterFolder == null) {
				filterFolder = Storage.get().getWorkspace();
			} // otherwise left unchanged
			
			setLayout(new FillLayout(SWT.HORIZONTAL));
	
			treeViewer = new TreeViewer(this, SWT.SINGLE);
	
			// 2. Assign content provider
			treeViewer.setContentProvider(new BaseWorkbenchContentProvider());
	
			// 3. Assign label provider
			treeViewer.setLabelProvider(new WorkbenchLabelProvider());
	
			// 4. Assign filter(s)
			treeViewer.addFilter(new RootFolderFilter(filterFolder));		
			if (foldersOnly) {			
				treeViewer.addFilter(new FolderOnlyFilter());
			}
	
			// 5. Assign <strike>sorter</strike> comparator
			
			// 6. Listeners
	
			// 7. Set input element
			treeViewer.setInput(filterFolder);
			
			// Expands tree to children of workspace
			treeViewer.expandToLevel(2);
		}
	}
}
