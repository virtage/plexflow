package com.virtage.plexflow.ui.explorer;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IPersistableElement;

import com.virtage.plexflow.logging.ISysLog;
import com.virtage.plexflow.logging.Logger;

public class ExplorerEditorInput implements IEditorInput {

	//--------------------------------------------------------------------------
    // Class fields - public
    //--------------------------------------------------------------------------

    //--------------------------------------------------------------------------
    // Class fields - non-public
    //--------------------------------------------------------------------------

    private static final ISysLog SYSLOG = Logger.getSysLog(ExplorerEditorInput.class);

    //--------------------------------------------------------------------------
    // Instance fields - public
    //--------------------------------------------------------------------------

    //--------------------------------------------------------------------------
    // Instance fields - non-public
    //--------------------------------------------------------------------------

    //private final IContainer container;

    //--------------------------------------------------------------------------
    // Instance fields - bean properties
    //--------------------------------------------------------------------------

    //--------------------------------------------------------------------------
    // Constructors, static initializers, factories
    //--------------------------------------------------------------------------

//    public ExplorerEditorInput(IContainer container) {
//		this.container = container;
//	}

    //--------------------------------------------------------------------------
    // Public API - class (static)
    //--------------------------------------------------------------------------

    //--------------------------------------------------------------------------
    // Public API - instance
    //--------------------------------------------------------------------------
//
//    public IContainer getContainer() {
//		return container;
//	}

    //--------------------------------------------------------------------------
    // Overrides and implementations
    //--------------------------------------------------------------------------

	@Override
	public Object getAdapter(Class adapter) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean exists() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public ImageDescriptor getImageDescriptor() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getName() {
		return "jméno";
		//return container.getName();
	}

	@Override
	public IPersistableElement getPersistable() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getToolTipText() {
		return "nějaký tooltip";
		//return container.getFullPath().toString();
	}

//	@Override
//	public int hashCode() {
//		final int prime = 31;
//		int result = 1;
//		result = prime * result
//				+ ((container == null) ? 0 : container.hashCode());
//		return result;
//	}

//	@Override
//	public boolean equals(Object obj) {
//		if (this == obj)
//			return true;
//		if (obj == null)
//			return false;
//		if (getClass() != obj.getClass())
//			return false;
//		ExplorerEditorInput other = (ExplorerEditorInput) obj;
//		if (container == null) {
//			if (other.container != null)
//				return false;
//		} else if (!container.equals(other.container))
//			return false;
//		return true;
//	}

    //--------------------------------------------------------------------------
    // Getters/setters of properties
    //--------------------------------------------------------------------------

    //--------------------------------------------------------------------------
    // Helper methods (mostly private)
    //--------------------------------------------------------------------------


}
