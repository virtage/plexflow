package com.virtage.plexflow.ui.explorer;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.swt.widgets.Button;

public class BookmarkManagerDialog extends TitleAreaDialog {
	private Table table;

	/**
	 * Create the dialog.
	 * @param parentShell
	 */
	public BookmarkManagerDialog(Shell parentShell) {
		super(parentShell);
	}

	/**
	 * Create contents of the dialog.
	 * @param parent
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		setTitle("Bookmark Manager");
		setMessage("Edit, create, sort your bookmarks to folders.");
		Composite area = (Composite) super.createDialogArea(parent);
		Composite container = new Composite(area, SWT.NONE);
		container.setLayoutData(new GridData(GridData.FILL_BOTH));
		
		TableViewer tableViewer = new TableViewer(container, SWT.BORDER | SWT.FULL_SELECTION);
		table = tableViewer.getTable();
		table.setBounds(10, 10, 337, 323);
		
		Button btnNew = new Button(container, SWT.NONE);
		btnNew.setBounds(353, 10, 114, 28);
		btnNew.setText("Add new...");
		
		Button btnEdit = new Button(container, SWT.NONE);
		btnEdit.setBounds(353, 44, 114, 28);
		btnEdit.setText("Edit...");
		
		Button btnDelete = new Button(container, SWT.NONE);
		btnDelete.setBounds(353, 78, 114, 28);
		btnDelete.setText("Delete");
		
		Button btnUp = new Button(container, SWT.ARROW | SWT.UP);
		btnUp.setBounds(353, 125, 114, 28);
		btnUp.setText("New Button");
		
		Button btnDown = new Button(container, SWT.ARROW | SWT.DOWN);
		btnDown.setBounds(353, 159, 114, 28);
		btnDown.setText("New Button");

		return area;
	}

	/**
	 * Create contents of the button bar.
	 * @param parent
	 */
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL,
				true);
		createButton(parent, IDialogConstants.CANCEL_ID,
				IDialogConstants.CANCEL_LABEL, false);
	}

	/**
	 * Return the initial size of the dialog.
	 */
	@Override
	protected Point getInitialSize() {
		return new Point(479, 492);
	}
}
