package com.virtage.plexflow.ui.explorer.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.ui.handlers.HandlerUtil;

import com.virtage.plexflow.ui.explorer.BookmarkManagerDialog2;

public class ManageBookmarks extends AbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		BookmarkManagerDialog2 d =
				new BookmarkManagerDialog2(HandlerUtil.getActiveShell(event));
		d.open();
		
		return null;
	}

}
