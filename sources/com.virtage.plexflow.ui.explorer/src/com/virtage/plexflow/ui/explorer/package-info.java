/**
 * Main classes of Content Explorer plug-in.
 * 
 * Views:
 * <ul>
 * <li>
 * <b>{@link FolderTreeView}</b> -- folder tree view
 * </li>
 * <li>
 * <b>{@link PlacesView}</b> -- places view
 * </ul>
 * 
 * Editors:
 * <ul>
 * <li>
 * <b>{@link ExplorerAsIcons}</b> --
 * </li>
 * <li>
 * <b>{@link ExplorerAsList}</b> --
 * </li>
 * <li>
 * <b>{@link ExplorerAsTable}</b> -- 
 * </li>
 * </ul>
 * 
 * Components:
 * <ul>
 * <li>
 * <b>{@link StorageTreeComposite}</b> -- used by
 * {@link com.virtage.plexflow.ui.explorer.dialogs.StorageTreeDialog} and
 * {@link com.virtage.plexflow.ui.explorer.FolderTreeView}.
 * </li>
 * </ul>
 * 
 * Dialogs:
 * <ul>
 * <li>
 * <b>{@link com.virtage.plexflow.ui.explorer.dialogs.StorageTreeDialog}</b> --
 * Allows to select file or folder from storage.
 * </li>
 * <b>{@link com.virtage.plexflow.ui.explorer.dialogs.NodeNameInputDialog}</b> --
 * to enter folder name validating input using
 * {@link com.virtage.plexflow.ui.explorer.dialogs.NodeNameInputValidator}.
 * </li>
 * </ul>
 * 
 */
package com.virtage.plexflow.ui.explorer;