package com.virtage.plexflow.ui.explorer;

import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.PlatformUI;

import com.virtage.plexflow.storage.Bookmark;
import com.virtage.plexflow.storage.IFile;
import com.virtage.plexflow.storage.IFolder;

public class StorageUiUtils {
	
	private static final ISharedImages PLATFORM_IMAGES = PlatformUI.getWorkbench().getSharedImages();
	

	/**
	 * Gets standard icon for type.
	 * 
	 * @param object Can be a node, folder, home, workspace, bookmark, ...
	 * @return Image for type or null if no icon is known or this type of
	 * objects is not supported (like passing non-sense HashMap class etc.)
	 */
	public static Image getIconFor(Object obj) {
		
		// Workspace, home or plain folder
		if (obj instanceof IFolder) {
			if (((IFolder) obj).isWorkspace()) {
				return null;
			}
			
			if (((IFolder) obj).isHome()) {
				return null;
			}
			
			return PLATFORM_IMAGES.getImage(ISharedImages.IMG_OBJ_FOLDER);
		}
		
		// File
		if (obj instanceof IFile) {
			return PLATFORM_IMAGES.getImage(ISharedImages.IMG_OBJ_FILE);
		}
		
		// Bookmark
		if (obj instanceof Bookmark) {
			return PLATFORM_IMAGES.getImage(ISharedImages.IMG_OBJS_BKMRK_TSK);
		}
		
		return null;
	}
	
}
