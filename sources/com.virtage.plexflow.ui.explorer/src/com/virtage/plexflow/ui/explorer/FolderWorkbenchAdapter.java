package com.virtage.plexflow.ui.explorer;

import java.util.List;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.StyledString;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.model.WorkbenchAdapter;

import com.virtage.plexflow.storage.IFolder;
import com.virtage.plexflow.storage.INode;
import com.virtage.plexflow.storage.Storage;
import com.virtage.plexflow.storage.exceptions.NodeDeletedException;
import com.virtage.plexflow.storage.exceptions.NodeTrashedException;
import com.virtage.plexflow.storage.exceptions.RuntimeStorageException;
import com.virtage.plexflow.storage.impl.LocalFolder;

public class FolderWorkbenchAdapter extends WorkbenchAdapter {

	//--------------------------------------------------------------------------
	// Class fields - public
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Class fields - non-public
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Instance fields - public
	//--------------------------------------------------------------------------
	
	//--------------------------------------------------------------------------
	// Instance fields - non-public
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Instance fields - bean properties
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Constructors, static initializers, factories
	//--------------------------------------------------------------------------
	
	//--------------------------------------------------------------------------
	// Public API - class (static)
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Public API - instance
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Overrides and implementations
	//--------------------------------------------------------------------------

	@Override
	public Object[] getChildren(Object o) {
		IFolder parent = (IFolder) o;
		
		if (parent.isWorkspace()) {
			return new IFolder[] { new FakeWorkspaceFolder(parent) };
		}
		
		List<INode> childNodes = null;
		try {
			childNodes = parent.childNodes();			
		} catch (NodeTrashedException | NodeDeletedException e) {
			new RuntimeStorageException("Failed to get child nodes.", e);			
		}
		
		if (childNodes == null) {
			return NO_CHILDREN;
		} else {
			return childNodes.toArray(new INode[] {});			
		}
	}

	@Override
	public ImageDescriptor getImageDescriptor(Object object) {
		return PlatformUI.getWorkbench().getSharedImages()
				.getImageDescriptor(ISharedImages.IMG_OBJ_FOLDER);
	}

	@Override
	public String getLabel(Object o) {
		return ((IFolder) o).getNodeName().toString();
	}

	@Override
	public Object getParent(Object o) {
		return ((IFolder) o).getParent();
	}
	
	//--------------------------------------------------------------------------
	// Consider toString(), equals(), hashCode()
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Getters/setters of properties
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Helper methods (mostly private)
	//--------------------------------------------------------------------------
}
