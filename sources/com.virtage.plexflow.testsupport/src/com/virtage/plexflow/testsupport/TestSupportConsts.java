package com.virtage.plexflow.testsupport;

public interface TestSupportConsts {
	/** Absolute path to test files folder created by create-test-files.sh. */
	String PATH_TEST_FILES = "/var/plexflow-testfiles/";
	
	String FILENAME_TEST_1_KiB   = "1KiB.bin";
	String FILENAME_TEST_1_MiB   = "1MiB.bin";
	String FILENAME_TEST_10_MiB  = "10MiB.bin";
	String FILENAME_TEST_50_MiB  = "10MiB.bin";
	String FILENAME_TEST_100_MiB = "100MiB.bin";
	String FILENAME_TEST_500_MiB = "500MiB.bin";
	String FILENAME_TEST_1_GiB   = "1GiB.bin";
	String FILENAME_TEST_2_GiB   = "2GiB.bin";
	String FILENAME_TEST_4_GiB   = "4GiB.bin";
}
