/**
 * <ul>
 * <li>Above all, this plug-in exports Mockito framework.</li>  
 * <li>Provides constants for automated testing with location of test files, etc.</li>
 * <li></li>
 * </ul>
 */
package com.virtage.plexflow.testsupport;