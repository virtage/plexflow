#!/bin/bash

# Creates files with random content here
# Maybe needed to run as root
FOLDER=/var/plexflow-testfiles

mkdir $FOLDER

echo ">>> Generating 1 KiB random file.... <<<"
dd if=/dev/urandom of=$FOLDER/1KiB.bin bs=1K count=1

echo ">>> Generating 1 MB random file.... <<<"
dd if=/dev/urandom of=$FOLDER/1MiB.bin bs=1M count=1

echo ">>> Generating 10 MB random file.... <<<"
dd if=/dev/urandom of=$FOLDER/10MiB.bin bs=1M count=10

echo ">>> Generating 50 MB random file.... <<<"
dd if=/dev/urandom of=$FOLDER/50MiB.bin bs=1M count=50

echo ">>> Generating 100 MB random file.... <<<"
dd if=/dev/urandom of=$FOLDER/100MiB.bin bs=1M count=100

echo ">>> Generating 500 MB random file.... <<<"
dd if=/dev/urandom of=$FOLDER/500MiB.bin bs=1M count=500

echo ">>> Generating 1 GB random file.... <<<"
dd if=/dev/urandom of=$FOLDER/1GiB.bin bs=1G count=1

echo ">>> Generating 2 GB random file.... <<<"
dd if=/dev/urandom of=$FOLDER/2GiB.bin bs=1G count=2

echo ">>> Generating 4 GB random file.... <<<"
dd if=/dev/urandom of=$FOLDER/4GiB.bin bs=1G count=4
