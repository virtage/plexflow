package com.virtage.plexflow.storage;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({
	INodeTest.class,
	NodeNameTest.class,
//	BookmarkTest.class,
//	LockingTest.class,
	IStorageTest.class,
	ITrashTest.class,
	IFolderTest.class,
	IFileTest.class,
//	LockingTest.class,
//	VersioningTest.class,
	})
public class SuiteAllTests {

}
