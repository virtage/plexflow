package com.virtage.plexflow.storage;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.virtage.plexflow.storage.exceptions.NodeNameForbiddenException;
import com.virtage.plexflow.storage.exceptions.StorageException;

public class NodeNameTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}
	
	@Test(expected=NodeNameForbiddenException.class)
	public void disallowEmptyName() throws StorageException {
		new NodeName("");		
	}
	
	@Test(expected=NodeNameForbiddenException.class)
	public void disallowEmptyName2() throws StorageException {
		new NodeName("   ");		
	}
	
	@Test(expected=NodeNameForbiddenException.class)
	public void disallowNullName() throws StorageException {
		new NodeName((String) null);
	}	
	
	@Test(expected=NodeNameForbiddenException.class)
	public void disallowedCharsFordwardSlash() throws StorageException {
		new NodeName("contains/forwardslash");
	}
	
	@Test(expected=NodeNameForbiddenException.class)
	public void disallowedCharsBackwardSlash() throws StorageException {
		new NodeName("contains\\backwardslash");
	}
	
	@Test
	public void getExtension() throws StorageException {
		assertEquals(
				NodeName.NO_EXTENSION,
				new NodeName("somethingWithoutExt").getExtension());
		
		assertEquals(
				".ext",
				new NodeName("somethingWith.ext").getExtension());
	}
	
	@Test
	public void ignoringCaseTest() throws StorageException {
		assertEquals(
				new NodeName("fooBar"),
				new NodeName("FOOBaR"));
	}
	
	@Test(expected=NodeNameForbiddenException.class)
	public void reservedNameHomeUsed() throws StorageException {
		new NodeName("home");
	}
	
	@Test(expected=NodeNameForbiddenException.class)
	public void reservedNameWorkspaceUsed() throws StorageException {
		new NodeName("workspace");
	}
	
	@Test(expected=NodeNameForbiddenException.class)
	public void tooLongName() throws Exception {
		// 256 long (allowed 256)		
		new NodeName("6jA29deSuA6ooOUDLLHF6y4jmOnXCFyrB3edIuAW3foaqbCyxVVyEUhVROFhPbI17b2olCzH1O1lt571IN8XXcqCovGp7D1GAR7m1dhGGD0SEwx4UhTluReVp7ZRlsRPqAH0i3SMeT7TSAXv0Z8h9gl0ljB4upx2bayo7B3LalWmXcRko0A1sB6kf2sq3Yll361ktCslmFB4EkCDW7rImw0EmsdbF9dXVwwY4qL3gT86SFbOrgEGNgvby2H21mjj");
		// 257 long
		new NodeName("6jA29deSuA6ooOUDLLHF6y4jmOnXCFyrB3edIuAW3foaqbCyxVVyEUhVROFhPbI17b2olCzH1O1lt571IN8XXcqCovGp7D1GAR7m1dhGGD0SEwx4UhTluReVp7ZRlsRPqAH0i3SMeT7TSAXv0Z8h9gl0ljB4upx2bayo7B3LalWmXcRko0A1sB6kf2sq3Yll361ktCslmFB4EkCDW7rImw0EmsdbF9dXVwwY4qL3gT86SFbOrgEGNgvby2H21mjjx");
	}
	
	@Test(expected=NodeNameForbiddenException.class)
	public void reservedNameDot() throws Exception {
		new NodeName(".");
	}
	
	@Test(expected=NodeNameForbiddenException.class)
	public void reservedNameTwoDot() throws Exception {
		new NodeName("..");
	}
	

}
