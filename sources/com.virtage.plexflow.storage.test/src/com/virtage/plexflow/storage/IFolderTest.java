package com.virtage.plexflow.storage;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import com.virtage.plexflow.storage.exceptions.NodeDeletedException;
import com.virtage.plexflow.storage.exceptions.NodeTrashedException;
import com.virtage.plexflow.storage.exceptions.StorageException;

import static org.junit.Assert.*;


public class IFolderTest {
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		Storage.get().format(null);
	}	

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
		Storage.get().format(null);
	}
	
	
	//--------------------------------------------------------------------------
    // INodes method tests
    //--------------------------------------------------------------------------

	
	
	
	
	
    //--------------------------------------------------------------------------
    // IFolder method tests
    //--------------------------------------------------------------------------
	
	@Test(expected=NodeTrashedException.class)
	public void childXYonTrashedFolderCausesException() throws Exception {
		IFolder foodir = Storage.get().getHomeForCurrentUser().createFolder("foodir55");		
		Storage.get().getTrash().trash(foodir);
		
		foodir.childNodes();
	}
	
	@Test(expected=NodeDeletedException.class)
	public void childXYonDeletedFolderCausesException() throws Exception {
		IFolder foodir = Storage.get().getHomeForCurrentUser().createFolder("foodir55");		
		Storage.get().getTrash().delete(foodir);
		
		foodir.childNodes();
	}
	
	@Test
	public void childXYdoesntIncludeTrashedNodes() throws Exception {
		IFolder root = Storage.get().getHomeForCurrentUser();
		
		IFolder subdir1 = root.createFolder("subdir1");
			IFile subdir1file1 = subdir1.createFile("subdir1file1");
			IFile subdir1file2 = subdir1.createFile("subdir1file2");
		
		IFolder subdir2 = root.createFolder("subdir2");
			IFile subdir2file1 = subdir1.createFile("subdir2file1");
			IFile subdir2file2 = subdir1.createFile("subdir2file2");		
			
		// Subdir1 and complete subnodes are trashed
		Storage.get().getTrash().trash(subdir1);
		Storage.get().getTrash().trash(subdir2file1);
		
		// childNodesRecursively()
		List<INode> expectedRecursively = new ArrayList<>();
		Collections.addAll(expectedRecursively, subdir2, subdir2file2);
		List<INode> actualRecursively = root.childNodesRecursively();				
		
		assertTrue(actualRecursively.containsAll(expectedRecursively));
		
		
		// childNodes()
		List<INode> expectedPlain = new ArrayList<>();
		Collections.addAll(expectedPlain, subdir2);
		List<INode> actualPlain = root.childNodes();
		
		assertTrue(actualPlain.containsAll(expectedPlain));
	}
	
	@Test
	public void childXYdoesntIncludeDeletedNodes() throws Exception {
		IFolder root = Storage.get().getHomeForCurrentUser();
		
		IFolder subdir1 = root.createFolder("subdir1");
			IFile subdir1file1 = subdir1.createFile("subdir1file1");
			IFile subdir1file2 = subdir1.createFile("subdir1file2");
		
		IFolder subdir2 = root.createFolder("subdir2");
			IFile subdir2file1 = subdir1.createFile("subdir2file1");
			IFile subdir2file2 = subdir1.createFile("subdir2file2");		
			
		// Subdir1 and complete subnodes are deleted
		Storage.get().getTrash().delete(subdir1);
		Storage.get().getTrash().delete(subdir2file1);
		
		// childNodesRecursively()
		List<INode> expectedRecursively = new ArrayList<>();
		Collections.addAll(expectedRecursively, subdir2, subdir2file2);
		List<INode> actualRecursively = root.childNodesRecursively();				
		
		assertTrue(actualRecursively.containsAll(expectedRecursively));
		
		
		// childNodes()
		List<INode> expectedPlain = new ArrayList<>();
		Collections.addAll(expectedPlain, subdir2);
		List<INode> actualPlain = root.childNodes();
		
		assertTrue(actualPlain.containsAll(expectedPlain));
	}
	
	
	@Test
	public void childNames() throws StorageException {
		Storage.get().getHomeForCurrentUser().createFolder(new NodeName("foodir1"));
		Storage.get().getHomeForCurrentUser().createFolder(new NodeName("foodir2"));
		Storage.get().getHomeForCurrentUser().createFolder(new NodeName("foodir3"));
		Storage.get().getHomeForCurrentUser().createFolder(new NodeName("foofile1"));
		Storage.get().getHomeForCurrentUser().createFolder(new NodeName("foofile2"));
		Storage.get().getHomeForCurrentUser().createFolder(new NodeName("foofile3"));
		
		List<String> expected = new ArrayList<>();
		Collections.addAll(expected, "foodir1", "foodir2", "foodir3", "foofile1", "foofile2",
				"foofile3");
		
		assertEquals(expected, Storage.get().getHomeForCurrentUser().childNames(null));
	}
	
	@Test
	public void childNamesRecursively() throws StorageException {
		IFolder root = Storage.get().getHomeForCurrentUser();
		
		IFolder subdir1 = root.createFolder(new NodeName("subdir1"));
		IFolder subdir2 = root.createFolder(new NodeName("subdir2"));
		IFile file1subdir1 = subdir1.createFile(new NodeName("file1subdir1"));
		IFile file2subdir1 = subdir1.createFile(new NodeName("file2subdir1"));
		IFile file1subdir2 = subdir2.createFile(new NodeName("file1subdir2"));
		IFile file2subdir2 = subdir2.createFile(new NodeName("file2subdir2"));
		IFile file1root = root.createFile(new NodeName("file1root"));
		IFile file2root = root.createFile(new NodeName("file2root"));
		
		List<String> expected = new ArrayList<>();
		Collections.addAll(expected, "subdir1", "subdir2", "file1subdir1", "file2subdir1",
				"file1subdir2", "file2subdir2", "file1root", "file2root");
		
		List<String> actual = root.childNamesRecursively(null);
		
		assertTrue(actual.containsAll(expected));
	}
	
	@Test
	public void childNodes() throws StorageException {
		IFolder folder1 = Storage.get().getHomeForCurrentUser().createFolder(new NodeName("foodir1"));
		IFolder folder2 = Storage.get().getHomeForCurrentUser().createFolder(new NodeName("foodir2"));
		IFolder folder3 = Storage.get().getHomeForCurrentUser().createFolder(new NodeName("foodir3"));
		IFolder file1   = Storage.get().getHomeForCurrentUser().createFolder(new NodeName("foofile1"));
		IFolder file2   = Storage.get().getHomeForCurrentUser().createFolder(new NodeName("foofile2"));
		IFolder file3   = Storage.get().getHomeForCurrentUser().createFolder(new NodeName("foofile3"));
		
		List<INode> expected = new ArrayList<>();
		Collections.addAll(expected, folder1, folder2, folder3, file1, file2, file3);
		
		assertEquals(expected, Storage.get().getHomeForCurrentUser().childNodes(null));
	}
	
	@Test
	public void childNodesRecursively() throws StorageException {
		IFolder root = Storage.get().getHomeForCurrentUser();
		
		IFolder subdir1 = root.createFolder(new NodeName("subdir1"));
		IFolder subdir2 = root.createFolder(new NodeName("subdir2"));
		IFile file1subdir1 = subdir1.createFile(new NodeName("file1subdir1"));
		IFile file2subdir1 = subdir1.createFile(new NodeName("file2subdir1"));
		IFile file1subdir2 = subdir2.createFile(new NodeName("file1subdir2"));
		IFile file2subdir2 = subdir2.createFile(new NodeName("file2subdir2"));
		IFile file1root = root.createFile(new NodeName("file1root"));
		IFile file2root = root.createFile(new NodeName("file2root"));
		
		List<INode> expected = new ArrayList<>();
		Collections.addAll(expected, subdir1, subdir2, file1subdir1, file2subdir1,
				file1subdir2, file2subdir2, file1root, file2root);
		
		List<INode> actual = root.childNodesRecursively(null);
		
		assertTrue(actual.containsAll(expected));
	}
	
	
	@Test
	public void childXxxNeverReturnsNullButEmptyList() throws StorageException {
		IFolder root = Storage.get().getHomeForCurrentUser().createFolder("root");
		
		assertTrue(root.childNames(null).isEmpty());
		assertTrue(root.childNamesRecursively(null).isEmpty());
		assertTrue(root.childNodes(null).isEmpty());
		assertTrue(root.childNodesRecursively(null).isEmpty());
	}
	
	
	@Test
	public void childNamesAndNodesEquality() throws StorageException {
		IFolder root = Storage.get().getHomeForCurrentUser().createFolder("root");
		
		// childNodes() == childNames
		IFolder subdir1 = root.createFolder(new NodeName("subdir1"));
		IFolder subdir2 = root.createFolder(new NodeName("subdir2"));
		IFile file1subdir1 = subdir1.createFile(new NodeName("file1subdir1"));
		IFile file2subdir1 = subdir1.createFile(new NodeName("file2subdir1"));
		IFile file1subdir2 = subdir2.createFile(new NodeName("file1subdir2"));
		IFile file2subdir2 = subdir2.createFile(new NodeName("file2subdir2"));
		IFile file1root = root.createFile(new NodeName("file1root"));
		IFile file2root = root.createFile(new NodeName("file2root"));
		
		List<INode> childNodes = root.childNodes(null);
		List<String> childNames = root.childNames(null);
		
		assertEquals(childNodes.size(), childNames.size());
		
		// childNodesRecursively() == childNamesRecursively()
		List<INode> childNodesRecursively = root.childNodesRecursively(null);
		List<String> childNamesRecursively = root.childNamesRecursively(null);
		
		assertEquals(childNodesRecursively.size(), childNamesRecursively.size());
	}
	
	
	@Test
	public void existsFile() throws StorageException {
		NodeName name = new NodeName("foofile");
		Storage.get().getHomeForCurrentUser().createFile(name);
		
		assertTrue(Storage.get().getHomeForCurrentUser().existsFile(name));		
	}
	
	@Test
	public void existsFolder() throws StorageException {
		NodeName name = new NodeName("foodir");
		Storage.get().getHomeForCurrentUser().createFolder(name);
		
		assertTrue(Storage.get().getHomeForCurrentUser().existsFolder(name));
	}
	
	@Test
	public void getFile() throws StorageException {
		NodeName name = new NodeName("foofile");
		IFile expected = Storage.get().getHomeForCurrentUser().createFile(name);		
		IFile actual = Storage.get().getHomeForCurrentUser().getFile(name);
		
		assertEquals(expected, actual);		
	}
	
	@Test
	public void getFolder() throws StorageException {
		NodeName name = new NodeName("foodir");
		IFolder expected = Storage.get().getHomeForCurrentUser().createFolder(name);		
		IFolder actual = Storage.get().getHomeForCurrentUser().getFolder(name);
		
		assertEquals(expected, actual);		
	}
	
	@Test
	public void isHome() {
		assertTrue(Storage.get().getHomeForCurrentUser().isHomeForCurrentUser());
	}
	
	@Test
	public void isHomeRoot() throws StorageException {
		IFolder homeRoot = Storage.get().getWorkspace().getFolder(NodeName.HOME_ROOT);
		assertNotNull(homeRoot);
		assertTrue(homeRoot.isHomeRoot());
	}
	
	@Test
	public void isWorkspace() {
		assertTrue(Storage.get().getWorkspace().isWorkspace());
	}
	
	@Test
	public void isParentOf() throws StorageException {
		IFolder folder = Storage.get().getHomeForCurrentUser().createFolder("foodir");
		IFile file     = Storage.get().getHomeForCurrentUser().createFile("foofile");
		
		assertTrue(Storage.get().getHomeForCurrentUser().isParentOf(folder));
		assertTrue(Storage.get().getHomeForCurrentUser().isParentOf(file));
	}
	
	@Test
	public void testHasChildNodes() throws Exception {
		IFolder root = Storage.get().getHomeForCurrentUser().createFolder("root");
		
		IFolder subdir1 = root.createFolder("subdir1");
		IFile subdir1file1 = subdir1.createFile("subdir1file1");
		IFile subdir1file2 = subdir1.createFile("subdir1file2");
	
		IFolder subdir2 = root.createFolder("subdir2");
		
		
		assertTrue(root.hasChildNodes());
		assertTrue(subdir1.hasChildNodes());
		assertFalse(subdir2.hasChildNodes());			
	}
	
}