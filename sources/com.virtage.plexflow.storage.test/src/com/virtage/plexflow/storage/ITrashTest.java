package com.virtage.plexflow.storage;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import com.virtage.plexflow.storage.exceptions.DeleteForbiddenException;
import com.virtage.plexflow.storage.exceptions.NodeNameForbiddenException;
import com.virtage.plexflow.storage.exceptions.NodeNotFoundException;
import com.virtage.plexflow.storage.exceptions.NodeTrashedException;
import com.virtage.plexflow.storage.exceptions.StorageException;
import com.virtage.plexflow.storage.exceptions.TrashForbiddenException;

public class ITrashTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
		Storage.get().format(null);
	}
	
	@Test
	public void getTrash() throws Exception {
		assertNotNull(Storage.get().getTrash());
		assertTrue(Storage.get().getTrash() instanceof ITrash);
	}	
	
	@Test
	public void deleteFile() throws StorageException {	
		IFile file = Storage.get().getHomeForCurrentUser().createFile("foofile");		
		Storage.get().getTrash().delete(file);
		
		assertFalse(Storage.get().getTrash().contains(file));
	}
	
	@Test
	public void deleteFolder() throws StorageException {
		IFolder folder = Storage.get().getHomeForCurrentUser().createFolder("foodir");		
		Storage.get().getTrash().delete(folder);
		
		assertFalse(Storage.get().getTrash().contains(folder));
	}
	
	@Test
	public void restoreFile() throws StorageException {	
		IFile file = Storage.get().getHomeForCurrentUser().createFile("foofile");		
		Storage.get().getTrash().trash(file);
		
		// Restore (must return same nodename)
		assertEquals(new NodeName("foofile"), Storage.get().getTrash().restore(file));
		
		// Must be identical to original
		assertEquals(file, Storage.get().getHomeForCurrentUser().getFile(file.getNodeName()));
	}
	
	@Test
	public void restoreFolder() throws StorageException {
		NodeName foodir = new NodeName("foodir");		
		IFolder folder = Storage.get().getHomeForCurrentUser().createFolder(foodir);		
		Storage.get().getTrash().trash(folder);		
		
		// Restore (must return same nodename)
		assertEquals(foodir, Storage.get().getTrash().restore(folder));
		
		// Must be identical to original
		assertEquals(folder, Storage.get().getHomeForCurrentUser().getFolder(foodir));
	}
	
	@Test
	public void restoreNotTrashedFile() throws StorageException {
		NodeName foofile = new NodeName("foofile");		
		IFolder file = Storage.get().getHomeForCurrentUser().createFolder(foofile);
		
		// Omit to trash
		//Storage.getInstance().getTrash().trash(file);
		
		// Does nothing
		NodeName restoredName = Storage.get().getTrash().restore(file);
		
		assertEquals(foofile, restoredName);
	}
	
	@Test
	public void restoreNotTrashedFolder() throws StorageException {
		NodeName foodir = new NodeName("foodir");
		
		IFolder folder = Storage.get().getHomeForCurrentUser().createFolder(foodir);
		
		// Omit to trash
		//Storage.getInstance().getTrash().trash(folder);
		
		NodeName restoredName = Storage.get().getTrash().restore(folder);
		
		assertEquals(foodir, restoredName);
	}
	
	@Test(expected=NodeNotFoundException.class)
	public void trashFileAndGetIt() throws Exception {
		IFile file = Storage.get().getHomeForCurrentUser().createFile("foofile");		
		Storage.get().getTrash().trash(file);	
		Storage.get().getHomeForCurrentUser().getFile("foofile");
	}
	
	@Test(expected=NodeNotFoundException.class)
	public void trashFolderAndGetIt() throws StorageException {
		IFolder folder = Storage.get().getHomeForCurrentUser().createFolder("foodir");		
		Storage.get().getTrash().trash(folder);	
		Storage.get().getHomeForCurrentUser().getFile("foodir");		
	}	
	
	/**
	 *  It's possible to have multiple trashed files of same name in a folder.
	 */
	@Test
	public void multipleTrashedFilesOfSameName() throws StorageException {
		IFile file1 = Storage.get().getHomeForCurrentUser().createFile("foofile");
		Storage.get().getTrash().trash(file1);
		
		IFile file2 = Storage.get().getHomeForCurrentUser().createFile("foofile");
		Storage.get().getTrash().trash(file2);
	}
	
	/**
	 *  It's possible to have multiple trashed subfolder of same name in a folder.
	 */
	@Test
	public void multipleTrashedFolderOfSameName() throws StorageException {
		IFolder folder1 = Storage.get().getHomeForCurrentUser().createFolder("foodir");
		Storage.get().getTrash().trash(folder1);
		
		IFolder folder2 = Storage.get().getHomeForCurrentUser().createFolder("foodir");
		Storage.get().getTrash().trash(folder2);
	}
	
	
	/**
	 * Trying to restore trashed folder, but there is already a new different one
	 * folder of the same name.
	 */
	@Test
	public void restoreFolderConflictAddsSuffix() throws StorageException {
		NodeName name = new NodeName("foobar");
		
		// 1. Create three "foobar" and delete them
		IFolder oldest, older, old;
		
		oldest = Storage.get().getWorkspace().createFolder(name);		
		Storage.get().getTrash().trash(oldest);
		
		older = Storage.get().getWorkspace().createFolder(name);
		Storage.get().getTrash().trash(older);
		
		old = Storage.get().getWorkspace().createFolder(name);
		Storage.get().getTrash().trash(old);
		
		// 2. Create new "foobar"
		Storage.get().getWorkspace().createFolder(name);

		// 3. Restore under different names		
		assertEquals("foobar (1)", Storage.get().getTrash().restore(oldest).toString());
		assertEquals("foobar (2)", Storage.get().getTrash().restore(older).toString());
		assertEquals("foobar (3)", Storage.get().getTrash().restore(old).toString());
	}
	
	/**
	 * Trying to restore trashed file, but there is already a new different one
	 * file of the same name.
	 */
	@Test
	public void restoreFilerConflictAddsSuffix() throws StorageException {
		NodeName name = new NodeName("foofile");
		
		// 1. Create three "foofiles" and delete them
		IFile oldest, older, old;
		
		oldest = Storage.get().getWorkspace().createFile(name);		
		Storage.get().getTrash().trash(oldest);
		
		older = Storage.get().getWorkspace().createFile(name);
		Storage.get().getTrash().trash(older);
		
		old = Storage.get().getWorkspace().createFile(name);
		Storage.get().getTrash().trash(old);
		
		// 2. Create new "foofile"
		Storage.get().getWorkspace().createFile(name);

		// 3. Restore under different names		
		assertEquals("foofile (1)", Storage.get().getTrash().restore(oldest).toString());
		assertEquals("foofile (2)", Storage.get().getTrash().restore(older).toString());
		assertEquals("foofile (3)", Storage.get().getTrash().restore(old).toString());
	}
	
	/**
	 * Creating folder with name of trashed one in the same folder **doest not cause conflict**. 
	 */
	@Test
	public void trashedAndNewFolderConflict() throws StorageException {
		NodeName n = new NodeName("foobardir");
		IFolder f1 = Storage.get().getHomeForCurrentUser().createFolder(n);
		Storage.get().getTrash().trash(f1);
		
		IFolder f2 = Storage.get().getHomeForCurrentUser().createFolder(n);
		assertEquals(f2, Storage.get().getHomeForCurrentUser().getFolder(n));
	}
	
	/**
	 * Creating file with name of different trashed one in the same folder **doest not cause conflict**. 
	 */
	@Test
	public void trashedAndNewFileConflict() throws StorageException {
		NodeName n = new NodeName("foobar.file");
		IFile f1 = Storage.get().getHomeForCurrentUser().createFile(n);
		Storage.get().getTrash().trash(f1);
		
		IFile f2 = Storage.get().getHomeForCurrentUser().createFile(n);
		assertEquals(f2, Storage.get().getHomeForCurrentUser().getFile(n));
	}		
	
	@Test(expected=NodeTrashedException.class)
	public void operateOnTrashedFolder() throws StorageException {
		IFolder folder = Storage.get().getHomeForCurrentUser().createFolder(new NodeName("parent"));
		Storage.get().getTrash().trash(folder);
		folder.rename(new NodeName("attempt to rename trashed folder"));
	}
	
	@Test(expected=NodeTrashedException.class)
	public void operateOnTrashedFile() throws StorageException {
		IFile file = Storage.get().getHomeForCurrentUser().createFile(new NodeName("parent"));
		Storage.get().getTrash().trash(file);
		file.rename(new NodeName("attempt to rename trashed folder"));
	}
	
	@Test(expected=TrashForbiddenException.class)
	public void trashHome() throws StorageException {
		Storage.get().getTrash().trash(Storage.get().getHomeForCurrentUser());
	}
	
	@Test(expected=TrashForbiddenException.class)
	public void trashHomeRoot() throws StorageException {
		Storage.get().getTrash().trash(Storage.get().getHomeRoot());
	}	
	
	@Test(expected=TrashForbiddenException.class)
	public void trashWorkspace() throws StorageException {
		Storage.get().getTrash().trash(Storage.get().getWorkspace());		
	}
	
	@Test(expected=DeleteForbiddenException.class)
	public void deleteHome() throws StorageException {
		Storage.get().getTrash().delete(Storage.get().getHomeForCurrentUser());		
	}
	
	@Test(expected=DeleteForbiddenException.class)
	public void deleteHomeRoot() throws StorageException {
		Storage.get().getTrash().delete(Storage.get().getHomeRoot());
	}
	
	@Test(expected=DeleteForbiddenException.class)
	public void deleteWorkspace() throws StorageException {
		Storage.get().getTrash().delete(Storage.get().getWorkspace());
	}
	
	@Test
	public void getTrashedAt() throws StorageException {
		// returns null if node has not been trashed or is deleted
		
		IFolder folder = Storage.get().getHomeForCurrentUser().createFolder("foodir");
		IFile   file   = Storage.get().getHomeForCurrentUser().createFile("foofile");
		
		Date folderTrashedAt, fileTrashedAt;
		
		// Not trashed yet, must be null
		folderTrashedAt = Storage.get().getTrash().getTrashedAt(folder);
		fileTrashedAt   = Storage.get().getTrash().getTrashedAt(file);
		
		assertNull(folderTrashedAt);
		assertNull(fileTrashedAt);
		
		// Trash
		Storage.get().getTrash().trash(folder);
		Storage.get().getTrash().trash(file);		
		
		folderTrashedAt = Storage.get().getTrash().getTrashedAt(folder);
		fileTrashedAt   = Storage.get().getTrash().getTrashedAt(file);
		
		// Trashed, can't be null
		assertNotNull(folderTrashedAt);
		assertNotNull(fileTrashedAt);
		
		Storage.get().getTrash().delete(folder);
		Storage.get().getTrash().delete(file);
		
		// Already deleted, must be null
		folderTrashedAt = Storage.get().getTrash().getTrashedAt(folder);
		fileTrashedAt   = Storage.get().getTrash().getTrashedAt(file);
		
		assertNull(folderTrashedAt);
		assertNull(fileTrashedAt);		
	}
	
	@Test
	public void recursiveTrashAndContains() throws Exception {
		IFolder root = Storage.get().getHomeForCurrentUser().createFolder("testingdir");
		IFolder subRoot = root.createFolder("willBeTrashed");
		
		IFolder subdir1 = subRoot.createFolder(new NodeName("subdir1"));
		IFolder subdir2 = subRoot.createFolder(new NodeName("subdir2"));
		IFile file1subdir1 = subdir1.createFile(new NodeName("file1subdir1"));
		IFile file2subdir1 = subdir1.createFile(new NodeName("file2subdir1"));
		IFile file1subdir2 = subdir2.createFile(new NodeName("file1subdir2"));
		IFile file2subdir2 = subdir2.createFile(new NodeName("file2subdir2"));
		IFile file1root = subRoot.createFile(new NodeName("file1root"));
		IFile file2root = subRoot.createFile(new NodeName("file2root"));
		
		Storage.get().getTrash().trash(subRoot);
		
		assertTrue(Storage.get().getTrash().contains(subRoot));
		assertTrue(Storage.get().getTrash().contains(subdir1));
		assertTrue(Storage.get().getTrash().contains(subdir2));
		assertTrue(Storage.get().getTrash().contains(file1subdir1));
		assertTrue(Storage.get().getTrash().contains(file2subdir1));
		assertTrue(Storage.get().getTrash().contains(file1subdir2));
		assertTrue(Storage.get().getTrash().contains(file2subdir2));
		assertTrue(Storage.get().getTrash().contains(file1subdir2));
		assertTrue(Storage.get().getTrash().contains(file1root));
		assertTrue(Storage.get().getTrash().contains(file2root));
		
		// child**() includes trashed nodes
		assertFalse(root.childNodesRecursively(null).isEmpty());		
	}
	
	
	@Test
	public void recursiveDeleteAndContains() throws Exception {
		IFolder root = Storage.get().getHomeForCurrentUser().createFolder("testingdir");
		IFolder subRoot = root.createFolder("willBeDeleted");
		
		IFolder subdir1 = subRoot.createFolder(new NodeName("subdir1"));
		IFolder subdir2 = subRoot.createFolder(new NodeName("subdir2"));
		IFile file1subdir1 = subdir1.createFile(new NodeName("file1subdir1"));
		IFile file2subdir1 = subdir1.createFile(new NodeName("file2subdir1"));
		IFile file1subdir2 = subdir2.createFile(new NodeName("file1subdir2"));
		IFile file2subdir2 = subdir2.createFile(new NodeName("file2subdir2"));
		IFile file1root = subRoot.createFile(new NodeName("file1root"));
		IFile file2root = subRoot.createFile(new NodeName("file2root"));
		
		Storage.get().getTrash().trash(subRoot);
		
		assertTrue(Storage.get().getTrash().contains(subRoot));
		assertTrue(Storage.get().getTrash().contains(subdir1));
		assertTrue(Storage.get().getTrash().contains(subdir2));
		assertTrue(Storage.get().getTrash().contains(file1subdir1));
		assertTrue(Storage.get().getTrash().contains(file2subdir1));
		assertTrue(Storage.get().getTrash().contains(file1subdir2));
		assertTrue(Storage.get().getTrash().contains(file2subdir2));
		assertTrue(Storage.get().getTrash().contains(file1subdir2));
		assertTrue(Storage.get().getTrash().contains(file1root));
		assertTrue(Storage.get().getTrash().contains(file2root));
		
		// child**() includes trashed nodes
		assertFalse(root.childNodesRecursively(null).isEmpty());
	}
	
	@Test
	public void recursiveGetTrashedNodes() throws Exception {
		IFolder root = Storage.get().getHomeForCurrentUser().createFolder("testingdir");
		IFolder subRoot = root.createFolder("willBeTrashed");
		
		IFolder subdir1 = subRoot.createFolder(new NodeName("subdir1"));
		IFolder subdir2 = subRoot.createFolder(new NodeName("subdir2"));
		IFile file1subdir1 = subdir1.createFile(new NodeName("file1subdir1"));
		IFile file2subdir1 = subdir1.createFile(new NodeName("file2subdir1"));
		IFile file1subdir2 = subdir2.createFile(new NodeName("file1subdir2"));
		IFile file2subdir2 = subdir2.createFile(new NodeName("file2subdir2"));
		IFile file1root = subRoot.createFile(new NodeName("file1root"));
		IFile file2root = subRoot.createFile(new NodeName("file2root"));
		
		Storage.get().getTrash().trash(subRoot);
		
		List<INode> expected = new ArrayList<>();
		Collections.addAll(expected, subdir1, subdir2, file1subdir1, file2subdir1, file1subdir2,
				file2subdir2, file1root, file2root);
				
		List<INode> actual = Storage.get().getTrash().getTrashedNodes(subRoot);
		
		assertTrue(actual.containsAll(expected));
	}
	
}
