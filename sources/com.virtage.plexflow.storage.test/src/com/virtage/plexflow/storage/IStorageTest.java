package com.virtage.plexflow.storage;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.virtage.plexflow.storage.exceptions.StorageException;

public class IStorageTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		Storage.get().format(null);
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
		Storage.get().format(null);
	}
	
	@Test
	public void getHome() throws StorageException {
		IFolder home = Storage.get().getHomeForCurrentUser();		
		
		assertNotNull("Home can't be null.", home);		
		assertTrue(home.isHomeForCurrentUser());
	}
	
	@Test
	public void getHomeRoot() throws StorageException {
		IFolder homeRoot = Storage.get().getHomeRoot();		
		
		assertNotNull(homeRoot);		
		assertTrue(homeRoot.isHomeRoot());
	}
	
	@Test
	public void getWorkspace() throws StorageException {
		IFolder ws = Storage.get().getWorkspace();
		// Name of workspace is always "" (empty string). Not null!
		assertNotNull(ws);
		assertEquals(NodeName.WORKSPACE, ws.getNodeName());
		assertTrue(ws.isWorkspace());
	}

}
