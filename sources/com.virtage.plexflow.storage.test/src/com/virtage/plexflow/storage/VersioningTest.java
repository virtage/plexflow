package com.virtage.plexflow.storage;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.virtage.plexflow.storage.exceptions.StorageException;

import static org.junit.Assert.*;


public class VersioningTest {
	
	private static IFolder root;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		Storage.get().format(null);
	}	

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		
	}

	@Before
	public void setUp() throws Exception {
		root = Storage.get().getHomeForCurrentUser().createFolder(new NodeName("testdir"));
	}

	@After
	public void tearDown() throws Exception {
		Storage.get().format(null);
	}
}
