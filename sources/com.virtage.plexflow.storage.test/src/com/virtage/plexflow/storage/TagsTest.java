package com.virtage.plexflow.storage;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import com.virtage.plexflow.storage.exceptions.StorageException;

import static org.junit.Assert.*;


public class TagsTest {
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		Storage.get().format(null);
	}	

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
		Storage.get().format(null);
	}	
		
	@Test @Ignore
	public void dummyTest() throws StorageException {

	}
}

