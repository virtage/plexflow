package com.virtage.plexflow.storage;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.virtage.plexflow.storage.exceptions.StorageException;

import static org.junit.Assert.*;


public class LockingTest {
	
	private static IFolder root;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		Storage.get().format(null);
	}	

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		
	}

	@Before
	public void setUp() throws Exception {
		root = Storage.get().getHomeForCurrentUser().createFolder(new NodeName("testdir"));
	}

	@After
	public void tearDown() throws Exception {
		Storage.get().format(null);
	}
	
	@Test
	public void lockFile() throws StorageException {
		IFile file = root.createFile(new NodeName("test.file"));
		assertTrue(file.isLocked());
		assertTrue(file.getLock().isIndirect());
		assertEquals(file, file.getLock().getLockedNode());
	}
	
	
	@Test
	public void lockFolderAndChild() throws StorageException {
		NodeName subdir1 = new NodeName("subdir1");
		NodeName subdir2 = new NodeName("subdir2");
		root.createFolder(subdir1);
		root.createFolder(subdir2);
		root.getFolder(subdir1).createFile(new NodeName("file1_in_subdir1"));
		root.getFolder(subdir1).createFile(new NodeName("file2_in_subdir1"));
		root.getFolder(subdir2).createFile(new NodeName("file1_in_subdir2"));
		root.getFolder(subdir2).createFile(new NodeName("file2_in_subdir2"));
		root.createFile(new NodeName("file1_in_root"));
		root.lock(); 
		
		for (INode child : root.childNodesRecursively(null)) {
			// Locking folder should lock all child
			assertTrue(child.isLocked());
			// Lock should be type of indirect on child
			assertTrue(child.getLock().isIndirect());
			// Lock should refer back to child itself
			assertEquals(child, child.getLock().getLockedNode());
		}
		
		// Later added nodes into locked folder have to become locked also
		IFile later = root.createFile(new NodeName("file2_in_root"));
		assertTrue(later.isLocked());
		assertTrue(later.getLock().isIndirect());
		assertEquals(later, later.getLock().getLockedNode());
	}
	
	
	@Test
	public void unlockFileWhenMovedOutside() throws StorageException {
		// File moved outside locked folder is not locked anymore
		
		IFolder subdir1 = root.createFolder(new NodeName("subdir1"));
		IFolder subdir2 = root.createFolder(new NodeName("subdir2"));		
		subdir1.lock();
		
		IFile file = subdir1.createFile(new NodeName("foofile"));		
		assertTrue(file.isLocked());
		
		file.move(subdir2, null);		
		assertFalse(file.isLocked());		
	}
	
	@Test
	public void unlockCopiesOfLockedNodes() throws StorageException {
		// Copy of locked node is not locked no matter whether or not origin was locked
		
		IFolder subdir1 = root.createFolder(new NodeName("subdir1"));
		IFolder subdir2 = root.createFolder(new NodeName("subdir2"));
		subdir1.lock();
		
		IFile file = subdir1.createFile(new NodeName("foofile"));
		assertTrue(file.isLocked());
		
		IFile copy = (IFile) file.copy(subdir2, null);
		assertFalse(copy.isLocked());
	}
	
	
	@Test
	public void canUnlock() throws StorageException {
		IFile file = root.createFile(new NodeName("test.file"));
		assertFalse("Not locked file cannot return true from canUnlock()", file.canUnlock());
		
		file.lock(ILock.NEVER_EXPIRES);
		assertFalse(file.canUnlock());
		
		file.unLock();
		assertTrue(file.canUnlock());
	}
	
	
	@Test
	public void canUnCheckOut() throws StorageException {
		IFile file = root.createFile(new NodeName("test.file"));
		file.unCheckOut();		// Should do nothing on not check-outed file
		assertFalse("Not checked-out file cannot return true from isCheckOut()", file.isCheckedOut());
		
	}
	

}
