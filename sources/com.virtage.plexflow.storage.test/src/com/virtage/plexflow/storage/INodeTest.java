package com.virtage.plexflow.storage;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import com.virtage.plexflow.storage.exceptions.NodeAlreadyExistsException;
import com.virtage.plexflow.storage.exceptions.NodeDeletedException;
import com.virtage.plexflow.storage.exceptions.NodeNameForbiddenException;
import com.virtage.plexflow.storage.exceptions.NodeNotFoundException;
import com.virtage.plexflow.storage.exceptions.NodeTrashedException;
import com.virtage.plexflow.storage.exceptions.StorageException;
import com.virtage.plexflow.system.SystemInstanceFactory;

public class INodeTest {
	
	private static IFolder root;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		Storage.get().format(null);
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		
	}

	@Before
	public void setUp() throws Exception {
		root = Storage.get().getHomeForCurrentUser().createFolder(new NodeName("testdir"));
	}

	@After
	public void tearDown() throws Exception {
		Storage.get().format(null);
	}
	
	
	@Test(expected=NodeNotFoundException.class)
	public void getNotExistingFolder() throws StorageException {
		root.getFolder(new NodeName("notexisting"));		
	}
	
	@Test(expected=NodeNotFoundException.class)
	public void getNotExistingFile() throws StorageException {
		root.getFile(new NodeName("notexisting"));
	}
		
	@Test
	public void createFolder() throws StorageException {
		NodeName name = new NodeName("foobardir");
		IFolder expected = root.createFolder(name);
		IFolder actual = root.getFolder(name);
		
		assertNotNull(expected);
		assertNotNull(actual);
		assertEquals(expected, actual);
	}
	
	@Test
	public void createFile() throws StorageException {
		NodeName name = new NodeName("foobar.file");
		IFolder expected = root.createFolder(name);
		IFolder actual = root.getFolder(name);
		
		assertNotNull(expected);
		assertNotNull(actual);
		assertEquals(expected, actual);
	}
	
	@Test(expected=NodeAlreadyExistsException.class)
	public void fileAlreadyExists() throws StorageException {		
		NodeName name = new NodeName("foobar.file");		
		root.createFile(name);
		root.createFile(name);
	}
	
	@Test(expected=NodeAlreadyExistsException.class)
	public void folderAlreadyExists() throws StorageException {
		NodeName name = new NodeName("foobardir");		
		root.createFolder(name);
		root.createFolder(name);
	}
	
	@Test(expected=NodeAlreadyExistsException.class)
	public void folderAndFileNameConfict() throws StorageException {		
		IFolder parent = root.createFolder(new NodeName("parent"));
		parent.createFile(new NodeName("foobar"));
		parent.createFolder(new NodeName("foobar"));
	}	
	
	
	

	@Test
	public void newFileHas0Size() throws StorageException {
		IFile file = root.createFile(new NodeName("dummyfile"));
		assertEquals(0, file.getLength());		
	}
	
	@Test(expected=NodeAlreadyExistsException.class)
	public void renameFileToExistingName() throws Exception {
		// Desired name passed to rename() is already in use
		root.createFile("some file");
		root.createFile("another file").rename("some file");
	}
	
	@Test(expected=NodeAlreadyExistsException.class)
	public void renameFolderToExistingName() throws Exception {
		// Desired name passed to rename() is already in use
		root.createFolder("some folder");
		root.createFolder("another folder").rename("some folder");
	}
	
	@Test(expected=NodeAlreadyExistsException.class)
	public void createFolderWithExistingName() throws Exception {
		root.createFolder("foodir");
		root.createFolder("foodir");
	}
	
	@Test(expected=NodeAlreadyExistsException.class)
	public void createFileWithExistingName() throws Exception {
		root.createFile("foofile");
		root.createFile("foofile");
	}
		
	@Test(expected=NodeAlreadyExistsException.class)
	public void copyFileToWhereNameExist() throws Exception {
		root.createFile("foofile");
		root.createFolder("foodir").createFile("foofile").copy(root, null);		
	}
	
	
	@Test(expected=NodeAlreadyExistsException.class)
	public void copyFolderToWhereNameExist() throws Exception {
		root.createFolder("foodir1");
		root.createFolder("foodir2").createFolder("foodir1").copy(root, null);
	}
	
	
	@Test(expected=NodeAlreadyExistsException.class)
	public void moveFileToWhereNameExist() throws Exception {
		root.createFile("foofile");
		root.createFolder("foodir").createFile("foofile").move(root, null);
	}
	
	@Test(expected=NodeAlreadyExistsException.class)
	public void moveFolderToWhereNameExist() throws Exception {
		root.createFolder("foodir1");
		root.createFolder("foodir2").createFolder("foodir1").move(root, null);
	}
	
	
	@Test
	public void getParentOnFile() throws StorageException {
		NodeName parentName = new NodeName("root");
		NodeName childName = new NodeName("child");
		
		IFolder parent = Storage.get().getHomeForCurrentUser().createFolder(parentName);
		IFile child = parent.createFile(childName);
		
		assertEquals(parent, child.getParent());
	}
	

	@Test
	public void getParentOnFolder() throws StorageException {
		NodeName parentName = new NodeName("root");
		NodeName childName = new NodeName("child");
		
		IFolder parent = Storage.get().getHomeForCurrentUser().createFolder(parentName);
		IFolder child = parent.createFolder(childName);
		
		assertEquals(parent, child.getParent());
	}
	
	@Test
	public void getPathOnFile() throws StorageException {
		IPath actual = Storage.get().getHomeForCurrentUser().
				createFolder("some").
				createFolder("very").
				createFolder("long").
				createFolder("path").
				createFolder("to").
				createFile("file").getPath();
		
		IPath expected = new Path("/home/" + SystemInstanceFactory.getInstance().getUser().getUsername() +
				"/some/very/long/path/to/file");
		
		assertEquals(expected, actual);
	}
	
	@Test
	public void getPathOnFolder() throws StorageException {
		IPath actual = Storage.get().getHomeForCurrentUser().
				createFolder("some").
				createFolder("very").
				createFolder("long").
				createFolder("path").
				createFolder("to").
				createFolder("folder").getPath();
		
		IPath expected = new Path("/home/" + SystemInstanceFactory.getInstance().getUser().getUsername() +
				"/some/very/long/path/to/folder");
		
		assertEquals(expected, actual);
	}
	
	@Test
	public void setGetDescriptionOnFile() throws StorageException {
		String desc = "I am some description of a node..."; 
		NodeName name = new NodeName("foobar");
		
		Storage.get().getWorkspace().createFile(name).setDescription(desc);
		assertEquals(desc, Storage.get().getWorkspace().getFile(name).getDescription());		
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void tooLongSetDescriptionOnFile() throws StorageException {
		// 1025 char (max 1024)
		String desc = "h5CceFJFsDKp0ZjNVEBozNLvTwVRn3BkJkfJ8wNDHcPgeUWcYm7dF3C7ryJZqfzP3Q7rCdiD3llY2sMS81014XzHAcKVOPxpla0iXRlWFh5jJEMTm7Kqt2BmuXKwSWRk1qr8O1UckWhkup9LGqVarDRDZECcZOXvJwQMW2iSx3UIWMJxvaT2e3r8wcOVpAQ5Fuvyl6Wv1jv6vC9f8hdngRpJ39DsWRLd92cFRrkY8HHed6VJPWz9cxOMLIvKhvsPF7shg83fgY7XzLrA5eCaMwNItM3yUqpxLrR2F5aYFlrfs5RBKAum5dMSscIzgytmhEtJNjSYpd2I2zkvJvkDvlGeulhfYa7WEec0g0x20n8BZEqdlN6tEYIitaSTFeutWv8rCjQr9pEopIVwjjp89M4rDiJED8WdfkCVRJV6HLGWYKxkUVzFENwo4le3jIo8OWyxvsnEvVmY4uSRmn88hUOWJ56QcnTuONZt3zdlFx4Rkexvi9lgZs5WMI96fF7mXyvLKxxYecxw7zsqI0hmMvdemOvlDlR8yS6I8CxBy1fZsoDhwWfLCfp9xo8K9lGM3MEhYPsJ2jBsmHlHr13EAlwTFyQZNM1ZqRj9uEH01GvpYuqHAnRBMFcAq5N6rULmljCoFH5E8vtPfpCuafopciRsFPVCwOeF7GE0typLqYUJEez8l6sVCsHnbA3Zhwk1ChIjSt4hgEmJJF2dcjDxSyE7QmOHuc6VZgwWm2bOGd8PMBOLrqkqAWJOkmU5WbNLph263SrkIsq7kiLHVvmF4FjyhWQHi8DQJXUHSePdcBxMOL6krgtarWDOF8B61gYlqiV8ALtgIzddVkjbyfjKvewjbArZHDJzyElsPBmNgSBlx1UzT44xmZODIjvW7hfm5igWHACAx5mb2EcFZD1mW23IoXA424Frp4nRLjvzUFenauq1ADddeoY1M3Qoq9a6aqD2NKFNexwLQ4jiKkRvtIHAJo2FUWNH5bixFlUN7Vd73lsPXICcUXICLTMWIaPsS"; 
		NodeName name = new NodeName("foobar");
		
		Storage.get().getWorkspace().createFile(name).setDescription(desc);
		assertEquals(desc, Storage.get().getWorkspace().getFile(name).getDescription());		
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void emptySetDescriptionOnFile() throws StorageException {
		String desc = ""; 	// empty str disallowed
		NodeName name = new NodeName("foobar");
		
		Storage.get().getWorkspace().createFile(name).setDescription(desc);
		assertEquals(desc, Storage.get().getWorkspace().getFile(name).getDescription());		
	}
	
	@Test
	public void setGetDescriptionOnFolder() throws StorageException {
		String desc = "I am some description of a node..."; 
		NodeName name = new NodeName("foobar");
		
		Storage.get().getWorkspace().createFolder(name).setDescription(desc);
		assertEquals(desc, Storage.get().getWorkspace().getFolder(name).getDescription());		
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void tooLongSetDescriptionOnFolder() throws StorageException {
		// 1025 char (max 1024)
		String desc = "h5CceFJFsDKp0ZjNVEBozNLvTwVRn3BkJkfJ8wNDHcPgeUWcYm7dF3C7ryJZqfzP3Q7rCdiD3llY2sMS81014XzHAcKVOPxpla0iXRlWFh5jJEMTm7Kqt2BmuXKwSWRk1qr8O1UckWhkup9LGqVarDRDZECcZOXvJwQMW2iSx3UIWMJxvaT2e3r8wcOVpAQ5Fuvyl6Wv1jv6vC9f8hdngRpJ39DsWRLd92cFRrkY8HHed6VJPWz9cxOMLIvKhvsPF7shg83fgY7XzLrA5eCaMwNItM3yUqpxLrR2F5aYFlrfs5RBKAum5dMSscIzgytmhEtJNjSYpd2I2zkvJvkDvlGeulhfYa7WEec0g0x20n8BZEqdlN6tEYIitaSTFeutWv8rCjQr9pEopIVwjjp89M4rDiJED8WdfkCVRJV6HLGWYKxkUVzFENwo4le3jIo8OWyxvsnEvVmY4uSRmn88hUOWJ56QcnTuONZt3zdlFx4Rkexvi9lgZs5WMI96fF7mXyvLKxxYecxw7zsqI0hmMvdemOvlDlR8yS6I8CxBy1fZsoDhwWfLCfp9xo8K9lGM3MEhYPsJ2jBsmHlHr13EAlwTFyQZNM1ZqRj9uEH01GvpYuqHAnRBMFcAq5N6rULmljCoFH5E8vtPfpCuafopciRsFPVCwOeF7GE0typLqYUJEez8l6sVCsHnbA3Zhwk1ChIjSt4hgEmJJF2dcjDxSyE7QmOHuc6VZgwWm2bOGd8PMBOLrqkqAWJOkmU5WbNLph263SrkIsq7kiLHVvmF4FjyhWQHi8DQJXUHSePdcBxMOL6krgtarWDOF8B61gYlqiV8ALtgIzddVkjbyfjKvewjbArZHDJzyElsPBmNgSBlx1UzT44xmZODIjvW7hfm5igWHACAx5mb2EcFZD1mW23IoXA424Frp4nRLjvzUFenauq1ADddeoY1M3Qoq9a6aqD2NKFNexwLQ4jiKkRvtIHAJo2FUWNH5bixFlUN7Vd73lsPXICcUXICLTMWIaPsS"; 
		NodeName name = new NodeName("foobar");
		
		Storage.get().getWorkspace().createFolder(name).setDescription(desc);
		assertEquals(desc, Storage.get().getWorkspace().getFolder(name).getDescription());		
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void emptySetDescriptionOnFolder() throws StorageException {
		String desc = ""; 
		NodeName name = new NodeName("foobar");
		
		Storage.get().getWorkspace().createFolder(name).setDescription(desc);
		assertEquals(desc, Storage.get().getWorkspace().getFolder(name).getDescription());		
	}
}

