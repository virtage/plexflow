package com.virtage.plexflow.storage;

import static org.junit.Assert.assertEquals;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.zip.CRC32;
import java.util.zip.CheckedInputStream;

import org.apache.commons.codec.digest.DigestUtils;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import static com.virtage.plexflow.testsupport.TestSupportConsts.*;

public class ChecksumSpeedTest {
	private static long start, end;
	private static String checksum1mb, checksum10mb, checksum50mb, checksum100mb, checksum500mb, checksum1gb;
	private static long time1mb, time10mb, time50mb, time100mb, time500mb, time1gb;
	
	private static final String FILE1MiB   = PATH_TEST_FILES + FILENAME_TEST_1_MiB; 
	private static final String FILE10MiB  = PATH_TEST_FILES + FILENAME_TEST_10_MiB;
	private static final String FILE50MiB  = PATH_TEST_FILES + FILENAME_TEST_50_MiB; 
	private static final String FILE100MiB = PATH_TEST_FILES + FILENAME_TEST_100_MiB;
	private static final String FILE500MiB = PATH_TEST_FILES + FILENAME_TEST_500_MiB;
	private static final String FILE1GiB   = PATH_TEST_FILES + FILENAME_TEST_1_GiB;
	

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		start = System.currentTimeMillis();
	}

	@After
	public void tearDown() throws Exception {
		end = System.currentTimeMillis();		
				
		System.out.println("1 MB   checksum: " + checksum1mb + ", counted within " + time1mb + " (ms)");
		System.out.println("10 MB  checksum: " + checksum10mb + ", counted within " + time10mb + " (ms)");
		System.out.println("50 MB  checksum: " + checksum50mb + ", counted within " + time50mb + " (ms)");
		System.out.println("100 MB checksum: " + checksum100mb + ", counted within " + time100mb + " (ms)");
		System.out.println("500 MB checksum: " + checksum500mb + ", counted within " + time500mb + " (ms)");
		System.out.println("1 GB   checksum: " + checksum1gb + ", counted within " + time1gb + " (ms)");
		System.out.println("Total time: " + (end - start) + " (ms)");
	}
	
	@Test
	public void sha1() throws FileNotFoundException, IOException {
		System.out.println("=== SHA1 test ===================================");
		
		start = System.currentTimeMillis();				
		checksum1mb = DigestUtils.shaHex(new FileInputStream(FILE1MiB));
		end = System.currentTimeMillis();
		time1mb = end - start;
		
		start = System.currentTimeMillis();	
		checksum10mb = DigestUtils.shaHex(new FileInputStream(FILE10MiB));
		end = System.currentTimeMillis();
		time10mb = end - start;
		
		start = System.currentTimeMillis();
		checksum50mb = DigestUtils.shaHex(new FileInputStream(FILE50MiB));
		end = System.currentTimeMillis();
		time50mb = end - start;
		
		start = System.currentTimeMillis();
		checksum100mb = DigestUtils.shaHex(new FileInputStream(FILE100MiB));
		end = System.currentTimeMillis();
		time100mb = end - start;
		
		start = System.currentTimeMillis();
		checksum500mb = DigestUtils.shaHex(new FileInputStream(FILE500MiB));
		end = System.currentTimeMillis();
		time500mb = end - start;

		start = System.currentTimeMillis();
		checksum1gb = DigestUtils.shaHex(new FileInputStream(FILE1GiB));
		end = System.currentTimeMillis();
		time1gb = end - start;
		
	}
	
	@Test
	public void crc32() throws IOException {		
		System.out.println("=== CRC32 test ==================================");
		
		byte[] buf = new byte[3072];	// 3 kB
		CheckedInputStream cis = null;
		long start, end;		
		
		start = System.currentTimeMillis();
		cis = new CheckedInputStream(new FileInputStream(FILE1MiB), new CRC32());		
		while (cis.read(buf) != -1) {};		
		checksum1mb = String.valueOf(cis.getChecksum().getValue());
		end = System.currentTimeMillis();
		time1mb = end - start;
		
		start = System.currentTimeMillis();
		cis = new CheckedInputStream(new FileInputStream(FILE10MiB), new CRC32());
		while (cis.read(buf) != -1) {};		
		checksum10mb = String.valueOf(cis.getChecksum().getValue());
		end = System.currentTimeMillis();
		time10mb = end - start;
		
		start = System.currentTimeMillis();
		cis = new CheckedInputStream(new FileInputStream(FILE50MiB), new CRC32());
		while (cis.read(buf) != -1) {};		
		checksum50mb = String.valueOf(cis.getChecksum().getValue());
		end = System.currentTimeMillis();
		time50mb = end - start;
		
		start = System.currentTimeMillis();
		cis = new CheckedInputStream(new FileInputStream(FILE100MiB), new CRC32());
		while (cis.read(buf) != -1) {};		
		checksum100mb = String.valueOf(cis.getChecksum().getValue());
		end = System.currentTimeMillis();
		time100mb = end - start;
		
		start = System.currentTimeMillis();
		cis = new CheckedInputStream(new FileInputStream(FILE500MiB), new CRC32());
		while (cis.read(buf) != -1) {};		
		checksum500mb = String.valueOf(cis.getChecksum().getValue());
		end = System.currentTimeMillis();
		time500mb = end - start;

		start = System.currentTimeMillis();
		cis = new CheckedInputStream(new FileInputStream(FILE1GiB), new CRC32());
		while (cis.read(buf) != -1) {};		
		checksum1gb = String.valueOf(cis.getChecksum().getValue());
		end = System.currentTimeMillis();
		time1gb = end - start;
	}
	
	@Test
	public void md5() throws FileNotFoundException, IOException {
		System.out.println("=== MD5 test ====================================");
		
		start = System.currentTimeMillis();				
		checksum1mb = DigestUtils.md5Hex(new FileInputStream(FILE1MiB));
		end = System.currentTimeMillis();
		time1mb = end - start;
		
		start = System.currentTimeMillis();	
		checksum10mb = DigestUtils.md5Hex(new FileInputStream(FILE10MiB));
		end = System.currentTimeMillis();
		time10mb = end - start;
		
		start = System.currentTimeMillis();
		checksum50mb = DigestUtils.md5Hex(new FileInputStream(FILE50MiB));
		end = System.currentTimeMillis();
		time50mb = end - start;
		
		start = System.currentTimeMillis();
		checksum100mb = DigestUtils.md5Hex(new FileInputStream(FILE100MiB));
		end = System.currentTimeMillis();
		time100mb = end - start;
		
		start = System.currentTimeMillis();
		checksum500mb = DigestUtils.md5Hex(new FileInputStream(FILE500MiB));
		end = System.currentTimeMillis();
		time500mb = end - start;

		start = System.currentTimeMillis();
		checksum1gb = DigestUtils.md5Hex(new FileInputStream(FILE1GiB));
		end = System.currentTimeMillis();
		time1gb = end - start;
	}

}

