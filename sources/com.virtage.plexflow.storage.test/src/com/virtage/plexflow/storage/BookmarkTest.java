package com.virtage.plexflow.storage;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.virtage.plexflow.storage.exceptions.BookmarkForbiddenException;
import com.virtage.plexflow.storage.exceptions.StorageException;


public class BookmarkTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		Storage.get().format(null);
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
		Storage.get().format(null);
	}
	
	@Test
	public void createAndKeepInsertionOrder() throws StorageException {
		IFolder home = Storage.get().getHomeForCurrentUser();
		IFolder[] folders = new IFolder[20];
		
		// Create folders and bookmarks for them
		for (int i = 0; i < 20; i++) {
			IFolder folder = home.createFolder(new NodeName("folder" + i));
			Bookmark.createFrom(folder);
			folders[i] = folder;
		}
		
		// Test that bookmarked successfully
		for (int i = 0; i < 20; i++) {
			assertTrue("Folder is not bookmarked but should be!",
					Bookmark.isBookmarked(folders[i]));
		}
		
		// Read in the same order as inserted
		Bookmark[] bookmarks = Bookmark.listBookmarks().toArray(new Bookmark[] {});		
		for (int i = 0; i < 20; i++) {
			assertEquals(folders[i].getNodeName(), bookmarks[i].getDisplayName());
		}
	}
	
	@Test
	public void bookmarkSameTwice() throws StorageException {
		Bookmark b1 = Bookmark.createFrom(
				Storage.get().getHomeForCurrentUser().createFolder(new NodeName("foobar")));
		Bookmark b2 = Bookmark.createFrom(
				Storage.get().getHomeForCurrentUser().createFolder(new NodeName("foobar")));
		
		assertNotNull("Bookmarking already bookmarked have to return that " +
				"bookmark again, not null!", b2);
		assertEquals("Bookmarking already bookmarked have to return that " +
				"bookmark again", b1, b2);		
	}
	
	@Test(expected=BookmarkForbiddenException.class)
	public void bookmarkHome() throws StorageException {
		Bookmark.createFrom(Storage.get().getHomeForCurrentUser());
	}
	
	@Test(expected=BookmarkForbiddenException.class)
	public void bookmarkWorkspace() throws StorageException {		
		Bookmark.createFrom(Storage.get().getWorkspace());
	}	
		
	@Test(expected=BookmarkForbiddenException.class)
	public void bookmarkTrashedFolder() throws StorageException {		
		IFolder folder = Storage.get().getWorkspace().createFolder("foodir");
		Storage.get().getTrash().trash(folder);
		
		Bookmark.createFrom(folder);
	}	
		
	@Test(expected=BookmarkForbiddenException.class)
	public void bookmarkDeletedFolder() throws StorageException {		
		IFolder folder = Storage.get().getWorkspace().createFolder("foodir");
		Storage.get().getTrash().delete(folder);
		
		Bookmark.createFrom(folder);
	}

}
