package com.virtage.plexflow.ui;

import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.Platform;
import org.eclipse.equinox.app.IApplication;
import org.eclipse.equinox.app.IApplicationContext;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Device;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.PlatformUI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.virtage.plexflow.system.BadUsernameOrPasswordException;
import com.virtage.plexflow.system.LoginException;
import com.virtage.plexflow.system.LoginService;
import com.virtage.plexflow.system.SystemInstance;
import com.virtage.plexflow.system.SystemInstanceFactory;
import com.virtage.plexflow.system.SystemsProperties;
import com.virtage.plexflow.system.SystemsPropertiesRecord;

/**
 * This class controls all aspects of the application's execution
 */
public class PxApplication implements IApplication {

	//--------------------------------------------------------------------------
    // Class fields - public
    //--------------------------------------------------------------------------

	/** Empty progress monitor for fast jobs (value <tt>null</tt>). */
	public static final IProgressMonitor NO_PROGRESS_MONITOR = null;	
	
    //--------------------------------------------------------------------------
    // Class fields - non-public
    //--------------------------------------------------------------------------
	private static final Logger SYSLOG = LoggerFactory.getLogger(PxApplication.class);

	private static boolean dontAsk;

    //--------------------------------------------------------------------------
    // Instance fields - public
    //--------------------------------------------------------------------------

    //--------------------------------------------------------------------------
    // Instance fields - non-public
    //--------------------------------------------------------------------------

	// If auto-login, values from from cmdline args will be placed here
	private String autologinUsername;
	private String autologinPassword;
	private String autologinSystemID;


    //--------------------------------------------------------------------------
    // Instance fields - bean properties
    //--------------------------------------------------------------------------

    //--------------------------------------------------------------------------
    // Constructors, static initializers, factories
    //--------------------------------------------------------------------------

	
	 //--------------------------------------------------------------------------
    // Public API - class (static)
    //--------------------------------------------------------------------------

		
	/**
	 * Whether application should don't ask question during execution and go
	 * ahead with default option. For example whether to ask to confirm quitting
	 * the application. It is set by commandline option.
	 * 
	 * @see http://plexflow.virtage.com/wiki/Admin_Guide/Commandline_options. 
	 */
	public static boolean isDontAskSet() {
		return PxApplication.dontAsk;
	}

    //--------------------------------------------------------------------------
    // Public API - instance
    //--------------------------------------------------------------------------	
	
    //--------------------------------------------------------------------------
    // Overrides and implementations
    //--------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see org.eclipse.equinox.app.IApplication#start(org.eclipse.equinox.app.IApplicationContext)
	 */
	@Override
	public Object start(IApplicationContext context) {
		Display display = PlatformUI.createDisplay();
		
		try {

			//------------------------------------------------------------------
			// *** Plexflow start-up sequence BEGINS here ***
			//------------------------------------------------------------------

			// Create and store commonly used fonts to FontRegtistry
			PxUiUtils.createAndSaveCommonFonts(display);
			
			// Sign-in sequence
			
			try {
				
				// -------------------------------------------------------------
				// (1) RUN FIRST TIME WIZARD?
				// -------------------------------------------------------------
				// TODO First time wizard
				
				// -------------------------------------------------------------
				// (2) AUTHENTICATE AND SET SystemInstanceFactory
				// -------------------------------------------------------------
				SystemInstance systemInstance = null;

				// Auto-login (sign-in from commandline supplied credentials)
				if (shouldAutologin()) {
					SYSLOG.info("Authenticating with autologin (skipping login dialog)");
					systemInstance = loginWithAutostart();
					
				} else {
					SYSLOG.info("Authenticating with login dialog");
					systemInstance = loginWithDialog();
					
				}

				if (systemInstance == null) {
					// Unsuccessfull login or Cancel/X pressed
					SYSLOG.warn("Exiting due to unsuccessful authentication.");
					return PxUiConstants.RT_AUTH_FAILED;
				}

				// Set static fields
				// ... (nothing now)
				
				// Set appropriate SystemInstanceFactory to be used by the rest of RCP code
				SystemInstanceFactory.setInstance(systemInstance);
				

			} catch (Throwable t) {
				String msg = "Serious exception occured during launching. PxApplication " +
						"will be terminated.";
				
				// Any throwable occurred is handled in the unified way here
				PxUiUtils.errorDialogWithStackTrace(msg,
					t	/* <-- stack trace will be in detail area */);
				SYSLOG.error(msg, t);
				
				return PxUiConstants.RT_EXCEPTION;
			}
			
			// Don't ask?
			PxApplication.dontAsk = dontAsk();					

			
			//------------------------------------------------------------------
			//*** Plexflow start-up sequence ENDS here ***
			//------------------------------------------------------------------

			//  |  This has to be last op. Indicates that  
			// \|/ custom init phase has complete.
			context.applicationRunning();

			int returnCode = PlatformUI.createAndRunWorkbench(display, new ApplicationWorkbenchAdvisor());
			
			if (returnCode == PlatformUI.RETURN_RESTART) {
				return IApplication.EXIT_RESTART;
			}

			return IApplication.EXIT_OK;
			
		} finally {
			display.dispose();
			
		}
	}
	

	/* (non-Javadoc)
	 * @see org.eclipse.equinox.app.IApplication#stop()
	 */
	@Override
	public void stop() {
		if (!PlatformUI.isWorkbenchRunning())
			return;
		final IWorkbench workbench = PlatformUI.getWorkbench();
		final Display display = workbench.getDisplay();
		display.syncExec(new Runnable() {
			@Override
			public void run() {
				if (!display.isDisposed())
					workbench.close();
			}
		});
	}


    //--------------------------------------------------------------------------
    // Getters/setters of properties
    //--------------------------------------------------------------------------

    //--------------------------------------------------------------------------
    // Helper methods (mostly private)
    //--------------------------------------------------------------------------
	
	/** Does the cmdline specify to "dont ask" argument? */
	private boolean dontAsk() {
		//Traverse app's cmdline arguments
		for (String arg : Platform.getCommandLineArgs()) {
			if (arg.startsWith(PxUiConstants.CMDARG_DONT_ASK)) {
				SYSLOG.info("Commandline option '{}' used. Go ahead with default options.", PxUiConstants.CMDARG_DONT_ASK);				
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * Parse autostart cmdline args and return true/false whether to do autostart.
	 *
	 * @throws IllegalArgumentException if some arg(s) is missing or invalid
	 * */
	private boolean shouldAutologin() {
		boolean autologin = false;		// to return

		// Traverse app's cmdline arguments
		for (String arg : Platform.getCommandLineArgs()) {

			// Will contain argument value or null if malformed arg passed
			String argValue =
					(arg.indexOf("=") == -1) ? null : arg.substring(arg.indexOf("=") + 1);

			if (arg.startsWith(PxUiConstants.CMDARG_AUTOLOGIN)) {
				autologin = true;

				if (argValue != null)
					this.autologinUsername = argValue;
			}

			if (arg.startsWith(PxUiConstants.CMDARG_PASSWORD)) {
				if (argValue != null)
					this.autologinPassword = argValue;
			}

			if (arg.startsWith(PxUiConstants.CMDARG_SYSTEMID)) {
				if (argValue != null)
					this.autologinSystemID = argValue;
			}

		}

		// If autostart, then validate all params
		if (autologin) {
			if ((this.autologinUsername != null) &&
				(this.autologinPassword != null) &&
				(this.autologinSystemID != null)) {
				autologin = true;

			} else {
				throw new IllegalArgumentException("You did not supply all required " +
						"arguments or their values necessary for autologin! Please see " +
						"http://plexflow.virtage.com/wiki/Commandline_options#Autologin for details. " +
						"(error code 6).");
			}
		}

		return autologin;
	}
	
	
	/** Whether cmdline tells to start bulk import wizard. */
	private boolean shouldBulkImport() {
		//Traverse app's cmdline arguments
		for (String arg : Platform.getCommandLineArgs()) {
			if (arg.startsWith(PxUiConstants.CMDARG_BULK_IMPORT)) {
				return true;
			}
		}
		
		return false;
	}


	private SystemInstance loginWithDialog() {
		LoginDialog loginDialog = new LoginDialog();

									// this method manages everything
		SystemInstance loggedSystem = loginDialog.doTheJob();

		return loggedSystem;
	}


	private SystemInstance loginWithAutostart()
			throws BadUsernameOrPasswordException, LoginException {

		List<SystemsPropertiesRecord> systems = SystemsProperties.listAvailableSystems();

		if (systems.isEmpty()) {
			throw new IllegalArgumentException("No systems available to login. " +
					"Make sure that systems.properties file " +
					"exists and contains at least one enabled system (error code 7).");
		}

		// Find system ID from argument in available systems
		SystemsPropertiesRecord foundSys = null;

		for (SystemsPropertiesRecord sys : systems) {
			if (this.autologinSystemID.equals(sys.getSystemId())) {
				foundSys = sys;
				break;
			}
		}

		if (foundSys == null) {
			throw new IllegalArgumentException("Specified system ID doesn't exist. " +
				"Double check for typo in '" + PxUiConstants.CMDARG_SYSTEMID +
				"' argument value (error code 8).");
		}

		SystemInstance systemInstance = LoginService.getInstance().
				login(foundSys, autologinUsername, autologinPassword);

		return systemInstance;
	}
}
