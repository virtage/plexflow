package com.virtage.plexflow.ui;

import org.eclipse.swt.graphics.Point;
import org.eclipse.ui.IWorkbenchPreferenceConstants;
import org.eclipse.ui.application.ActionBarAdvisor;
import org.eclipse.ui.application.IActionBarConfigurer;
import org.eclipse.ui.application.IWorkbenchWindowConfigurer;
import org.eclipse.ui.application.WorkbenchWindowAdvisor;
import org.eclipse.ui.internal.WorkbenchWindow;

import com.virtage.plexflow.system.SystemInstanceFactory;

public class ApplicationWorkbenchWindowAdvisor extends WorkbenchWindowAdvisor {

	public ApplicationWorkbenchWindowAdvisor(IWorkbenchWindowConfigurer configurer) {
		super(configurer);
	}

	@Override
	public ActionBarAdvisor createActionBarAdvisor(IActionBarConfigurer configurer) {
		return new ApplicationActionBarAdvisor(configurer);
	}

	@Override
	public void preWindowOpen() {
		IWorkbenchWindowConfigurer configurer = getWindowConfigurer();
		configurer.setInitialSize(new Point(950, 750));
		configurer.setShowCoolBar(true);
		configurer.setShowStatusLine(true);
		// Not sure if it is meant Progress view or progress in status line?!
		configurer.setShowProgressIndicator(true);
		configurer.setShowMenuBar(true);
	}
	
	@Override
	public void postWindowOpen() {
		//IStatusLineManager statusLine = 
		getWindowConfigurer().getActionBarConfigurer().getStatusLineManager().
		setMessage("Logged user: " + SystemInstanceFactory.getInstance().getUser().getDisplayName());
	}

}
