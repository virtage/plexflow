package com.virtage.plexflow.ui;

import org.eclipse.jface.action.IContributionItem;
import org.eclipse.jface.action.ICoolBarManager;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.ui.IWorkbenchActionConstants;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.actions.ActionFactory;
import org.eclipse.ui.actions.ActionFactory.IWorkbenchAction;
import org.eclipse.ui.actions.ContributionItemFactory;
import org.eclipse.ui.application.ActionBarAdvisor;
import org.eclipse.ui.application.IActionBarConfigurer;

/**
 * Basic ActionBarAdvisor defining
 * 
 * <ul>
 * <li>main menu and toolbar skeleton,</li>
 * <li>placeholders for other plug-ins to place their declaratively made
 * contribution.</li>
 * </ul>
 * 
 * <p>
 * Some actions don't have command equivalent, thus full menu/toolbar cannot be
 * build solely declaratively. Also main menubar itself cannot be created
 * declaratively. That's the reason for still have some code here.
 * </p>
 * 
 * <p>
 * In other words, main role of this class is to define placeholder where other
 * plug-ins will place their declarative contributions.
 * </p>
 * 
 * <p>
 * Placeholders bellow looks like:
 * </p>
 * 
 * <pre>
 * MenuManager file = new MenuManager(&quot;&amp;File&quot;, PxUiConstants.MB_FILE);
 * file.add(newWizardShortList);
 * file.add(new Separator());
 * file.add(close);
 * file.add(closeAll);
 * file.add(new Separator());
 * // --&gt; Declaratively contributions will be placed here &lt;--
 * file.add(new Separator(IWorkbenchActionConstants.MB_ADDITIONS));
 * </pre>
 * 
 * <p>
 * Value of MB_ADDIONTS is "additions".
 * </p>
 * 
 * <p>
 * <em>Example of filling main menubar and toolbar in declaratively fashion using
 * <tt>org.eclipse.ui.menus</tt> extension point:</em>
 * </p>
 * 
 * <pre>
 * &lt;extension point=&quot;org.eclipse.ui.menus&quot;&gt;
 * 	&lt;menuContribution
 *             locationURI=&quot;menu:<b>help</b>?after=<b>additions</b>&quot;&gt;
 *          &lt;command
 *                commandId=&quot;com.virtage.plexflow.ui.help.openPlexflowWebsite&quot;
 *                label=&quot;Open product website&quot;
 *                style=&quot;push&quot;&gt;
 *          &lt;/command&gt;
 *   &lt;/menuContribution&gt;
 * &lt;/extension&gt;
 * </pre>
 * 
 * <p>
 * where
 * </p>
 * 
 * <ul>
 * <li><tt>help</tt> is ID of toplevel menu Help defined in this class. This
 * constant is stored in {@link PxUiConstants#MB_HELP}.</li>
 * <li><tt>additions</tt> denotes that commands will be placed in the additions
 * placeholder. Some menus (like Help) define more placeholders (welcome,
 * commands to show help, p2 actions like update, about box). Again, constants
 * are in {@link PxUiConstants}.</li>
 * </ul>
 * 
 */
public class ApplicationActionBarAdvisor extends ActionBarAdvisor {
	
	//--------------------------------------------------------------------------
	// Class fields - public
	//--------------------------------------------------------------------------	
	
	//--------------------------------------------------------------------------
	// Class fields - non-public
	//--------------------------------------------------------------------------
	
	//--------------------------------------------------------------------------
	// Instance fields - public
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Instance fields - non-public
	//--------------------------------------------------------------------------
	
	private IContributionItem newWizardShortList;
	private IWorkbenchAction close;
	private IWorkbenchAction closeAll;
	private IWorkbenchAction quit;
	private IWorkbenchAction properties;
	private IWorkbenchAction preferences;
	private IWorkbenchAction about;
	private IContributionItem viewsShortlist;
	
	//--------------------------------------------------------------------------
	// Constructors, static initializers, factories
	//--------------------------------------------------------------------------

	public ApplicationActionBarAdvisor(IActionBarConfigurer configurer) {
		super(configurer);
	}
	
	//--------------------------------------------------------------------------
	// Public API - class (static)
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Public API - instance
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Overrides and implementations
	//--------------------------------------------------------------------------

	@Override
	protected void makeActions(IWorkbenchWindow window) {
		/*
		// Example only of programmatically way
		preferencesAction = ActionFactory.PREFERENCES.create(window);
		register(preferencesAction);
		*/
		
		// File menu
		newWizardShortList = ContributionItemFactory.NEW_WIZARD_SHORTLIST.create(window);
		close = ActionFactory.CLOSE.create(window);
		closeAll = ActionFactory.CLOSE_ALL.create(window);
		quit = ActionFactory.QUIT.create(window);
		properties = ActionFactory.PROPERTIES.create(window);
		
		// Window menu
		viewsShortlist = ContributionItemFactory.VIEWS_SHORTLIST.create(window);
		preferences = ActionFactory.PREFERENCES.create(window);		
		
		// Help menu
		about = ActionFactory.ABOUT.create(window);		
	}
	
	@Override
	protected void fillMenuBar(IMenuManager menuBar) {
		/*
		// Example only of programmatically way
		MenuManager file = new MenuManager("&File", "file");
		file.add(preferencesAction);		
		menuBar.add(file);
		*/		
		
		MenuManager file = new MenuManager("&File", PxUiConstants.MB_FILE);
		file.add(newWizardShortList);
		file.add(new Separator());
		file.add(close);
		file.add(closeAll);
		file.add(new Separator());
		// --> Declaratively contributions will be placed here <--
		file.add(new Separator(IWorkbenchActionConstants.MB_ADDITIONS));		
		file.add(properties);
		file.add(new Separator());
		file.add(quit);		
		
		MenuManager edit = new MenuManager("&Edit", PxUiConstants.MB_EDIT);
		// --> Declaratively contributions will be placed here <--
		edit.add(new Separator(IWorkbenchActionConstants.MB_ADDITIONS));
		
		MenuManager view = new MenuManager("&View", PxUiConstants.MB_VIEW);
		// --> Declaratively contributions will be placed here <--
		view.add(new Separator(IWorkbenchActionConstants.MB_ADDITIONS));
		
		MenuManager window = new MenuManager("&Window", PxUiConstants.MB_WINDOW);
		window.add(viewsShortlist);
		// --> Declaratively contributions will be placed here <--
		window.add(new Separator(IWorkbenchActionConstants.MB_ADDITIONS));
		window.add(new Separator());
		window.add(preferences);		
		
		/* Help menu is contributed by multiple plug-ins, therefore multiple
		 * placeholders.
		 */
		MenuManager help = new MenuManager("&Help", PxUiConstants.MB_HELP);
		help.add(new Separator(PxUiConstants.MB_HELP_WELCOME));
		help.add(new Separator(PxUiConstants.MB_HELP_HELP));
		help.add(new Separator(PxUiConstants.MB_HELP_P2));
		// --> Declaratively contributions will be placed here <--
		help.add(new Separator(IWorkbenchActionConstants.MB_ADDITIONS));
		help.add(new Separator(PxUiConstants.MB_HELP_ABOUT));
		
		menuBar.add(file);
		menuBar.add(edit);
		menuBar.add(view);
		menuBar.add(window);
		menuBar.add(help);
	}
	
	@Override
	protected void fillCoolBar(ICoolBarManager coolBar) {
		/*
		// Example only of programmatically way:
		IToolBarManager toolbar = new ToolBarManager(coolBar.getStyle());
		coolBar.add(toolbar);
		toolbar.add(preferencesAction);
		*/
	}
	
}
