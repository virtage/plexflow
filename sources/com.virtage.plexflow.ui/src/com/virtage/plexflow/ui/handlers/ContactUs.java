package com.virtage.plexflow.ui.handlers;

import java.awt.Desktop;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.core.commands.IHandlerListener;

/** Opens web browser with contact info */
public class ContactUs extends AbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		try {
			Desktop.getDesktop().browse(new URI("http://virtage.com/contact"));
		} catch (IOException | URISyntaxException e) {
			new RuntimeException("Failed to open website", e);
		}
		
		return null;
	}
	
}