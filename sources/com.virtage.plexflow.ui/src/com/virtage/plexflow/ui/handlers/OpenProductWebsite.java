package com.virtage.plexflow.ui.handlers;
import java.awt.Desktop;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Display;

public class OpenProductWebsite extends AbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		try {
			Desktop.getDesktop().browse(new URI("http://plexflow.virtage.com/"));
		} catch (IOException | URISyntaxException e) {
			new RuntimeException("Failed to open website", e);
		}
		
		return null;
	}

}
