package com.virtage.plexflow.ui;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.application.IWorkbenchConfigurer;
import org.eclipse.ui.application.IWorkbenchWindowConfigurer;
import org.eclipse.ui.application.WorkbenchAdvisor;
import org.eclipse.ui.application.WorkbenchWindowAdvisor;

import com.virtage.plexflow.logging.ISysLog;
import com.virtage.plexflow.logging.Logger;

public class ApplicationWorkbenchAdvisor extends WorkbenchAdvisor {

	private static final String DEFAULT_PERSPECTIVE_ID =
			"com.virtage.plexflow.ui.defaultPerspective";
	
	private static final ISysLog SYSLOG = Logger.getSysLog(ApplicationWorkbenchAdvisor.class);

	@Override
	public WorkbenchWindowAdvisor createWorkbenchWindowAdvisor(
			IWorkbenchWindowConfigurer configurer) {
		return new ApplicationWorkbenchWindowAdvisor(configurer);
	}

	@Override
	public String getInitialWindowPerspectiveId() {
		return DEFAULT_PERSPECTIVE_ID;
	}

	@Override
	public void initialize(IWorkbenchConfigurer configurer) {
		// Save and restore workbench parts (views and editors) state
		configurer.setSaveAndRestore(true);
	}
	
	@Override
	public boolean preShutdown() {	
		if (getWorkbenchConfigurer().emergencyClosing())
			return true;
		
		// Just quit if "don't ask" has been set
		if (PxApplication.isDontAskSet())
			return true;
		
		// Otherwise ask for confirmation
		return MessageDialog.openQuestion(
				PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(),
				PxUiConstants.DIALOG_TITLE,
				"Do you really want to exit the application?");
	}
	
	@Override
	public void eventLoopException(Throwable exception) {
		SYSLOG.severe("Unhandled event loop exception", exception);
		PxUiUtils.errorDialogWithStackTrace("Unhandled event loop exception. " +
				"Please file a bug report about this serious exception state to " +
				"the Virtage Support.", exception);
	}
	
	@Override
	public void eventLoopIdle(Display display) {
		// Workbench is idle right now...
		// TODO: index storage, defragment repository, whatever...
	}

}
