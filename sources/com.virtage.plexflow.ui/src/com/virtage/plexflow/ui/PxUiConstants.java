package com.virtage.plexflow.ui;

import org.eclipse.swt.layout.RowData;
import org.eclipse.ui.IWorkbenchActionConstants;

public interface PxUiConstants {

	// --------------------------------------------------------------------------
	// Misc
	// --------------------------------------------------------------------------

	/** Default title bar text. */
	String DIALOG_TITLE = "Virtage Plexflow";

	/** Recommended wizard dialog width for <tt>WizardDialog.setPageSize()</tt>. */
	int WIZARD_WIDTH = 550;

	/**
	 * Recommended wizard dialog height for <tt>WizardDialog.setPageSize()</tt>.
	 */
	int WIZARD_HEIGHT = 400;

	/**
	 * RowLayout's RowData instance limiting maximum wide to
	 * {@link #WIZARD_WIDTH}.
	 * 
	 * <p>
	 * Used for very wide widgets (like long text <tt>Label</tt>s) that need
	 * some width limit otherwise it causes wizard page to be as wide as
	 * possible as screen resolution.
	 * </p>
	 */
	RowData ROW_DATA_MAX_WIDTH = new RowData(PxUiConstants.WIZARD_WIDTH, -1);

	// --------------------------------------------------------------------------
	// Menu bar contributions placeholder IDs
	// --------------------------------------------------------------------------

	// There are constants IWorkbenchActionConstants.M_FILE, M_EDIT,
	// M_HELP, ... with values "file", "edit", etc. but many of these are
	// IDE-specific and likely will be deprecated in future Eclipse versions.

	/**
	 * ID of menu section "File" for declarative contributions (value {@value}
	 * ).
	 */
	String MB_FILE = "file";

	/**
	 * ID of menu section "Edit" for declarative contributions (value {@value}
	 * ).
	 */
	String MB_EDIT = "edit";

	/**
	 * ID of menu section "View" for declarative contributions (value {@value}
	 * ).
	 */
	// E.g. IWorkbenchActionConstants.M_VIEW is @since 3.0 deprecated
	String MB_VIEW = "view";

	/**
	 * ID of menu section "Window" for declarative contributions (value {@value}
	 * ).
	 */
	String MB_WINDOW = "window";

	/**
	 * ID of menu section "Help" for declarative contributions (value {@value}
	 * ).
	 */
	String MB_HELP = "help";

	/**
	 * ID of placeholder for declarative contributions for "welcome" command of
	 * "Help" menu (value {@value} ).
	 */
	String MB_HELP_WELCOME = "help.welcome";
	
	/**
	 * ID of placeholder for declarative contributions for "help" command (Help
	 * Contents, Search, ...) of "Help" menu (value {@value} ).
	 */
	String MB_HELP_HELP = "help.help";
	
	/**
	 * ID of placeholder for declarative contributions for p2 (update...)
	 * command(s) of "Help" menu (value {@value} ).
	 */
	String MB_HELP_P2 = "help.p2";
	
	/**
	 * ID of placeholder for declarative contributions for "about" command(s) of
	 * "Help" menu (value {@value} ).
	 */
	String MB_HELP_ABOUT = "help.about";

	// --------------------------------------------------------------------------
	// Font names for JFace FontRegistry
	// --------------------------------------------------------------------------
	// To FontRegistry are added frequently used fonts with the following keys.
	// Usage patterns is akin to:
	// Font font = JFaceResources.getFont(PxUiConstants.FONT_DIALOG_BOLD);

	/**
	 * Symbolic name for standard dialog font modified to bold.
	 * 
	 * <p>
	 * It's placed by UI startup code to to JFace <tt>FontRegistry</tt> and
	 * achievable with code akin to:
	 * 
	 * <pre>
	 * Font font = JFaceResources.getFont(PxUiConstants.FONT_xy);
	 * </pre>
	 * 
	 * </p>
	 **/
	String FONT_DIALOG_BOLD = "PX_FONT_DIALOG_BOLD";

	/**
	 * Symbolic name for standard dialog font modified to italic.
	 * 
	 * <p>
	 * It's placed by UI startup code to to JFace <tt>FontRegistry</tt> and
	 * achievable with code akin to:
	 * 
	 * <pre>
	 * Font font = JFaceResources.getFont(PxUiConstants.FONT_xy);
	 * </pre>
	 * 
	 * </p>
	 **/
	String FONT_DIALOG_ITALIC = "PX_FONT_DIALOG_ITALIC";

	/**
	 * Plexflow custom fonts available in JFace <tt>FontRegistry</tt>.
	 * 
	 * <p>
	 * <b>Not intended for client use</b>. This array is read by UI startup code
	 * (specifically {@link PxUiUtils#createAndSaveCommonFonts(Device)})
	 * creating and storing these fonts in <tt>FontRegistry</tt>.
	 * </p>
	 */
	String[] AVAILABLE_FONTS = new String[] { FONT_DIALOG_BOLD,
			FONT_DIALOG_ITALIC };

	// --------------------------------------------------------------------------
	// Preferences keys
	// --------------------------------------------------------------------------

	// --------------------------------------------------------------------------
	// Commandline arguments
	//
	// Notation:
	// "CMDARG_" prefix
	// --------------------------------------------------------------------------

	// http://plexflow.virtage.com/wiki/Admin_Guide/Commandline_options
	String CMDARG_AUTOLOGIN = "--auto-login";
	String CMDARG_PASSWORD = "--password";
	String CMDARG_SYSTEMID = "--system-id";
	/* Don't ask questions. Go ahead with default answer. */
	String CMDARG_DONT_ASK = "--dont-ask";

	/** Launch bulk import wizard? */
	String CMDARG_BULK_IMPORT = "--bulk-import";

	// --------------------------------------------------------------------------
	// Exit return codes
	//
	// Notation:
	// "RT_" prefix
	// --------------------------------------------------------------------------

	/** Return code signaling there are no available systems. */
	int RT_NO_SYSTEM_PROVIDERS = -1;

	/** Return code signaling database failure. */
	int RT_DB_ERROR = -2;

	/** Return code signaling authentication failure. */
	int RT_AUTH_FAILED = -3;

	/** Return code signaling exit due to an exception. */
	int RT_EXCEPTION = -4;

}
