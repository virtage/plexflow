package com.virtage.plexflow.ui;

import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IPerspectiveFactory;

/**
 * Class defines only what cannot be done declaratively:
 * <ul>
 * <li>editor area made visible</li>
 * <li>prepare placeholder folder around editor where other plugins will
 * contribute views</li>
 * </ul>
 * 
 * <h4>HOW-TO other plug-in can add view into defined folders</h4>
 * <p>Keep this (basis) and other plug-ins decoupled. Other plug-ins can contribute
 * views without being basis will have to depend on them.</p>
 * 
 * <p>In org.eclipse.ui.perspectiveExtensions, <tt>view</tt> element use</p>
 * <ul>
 * <li>relative = folder ID value (see class constants)</li>
 * <li>relationship = stack</li>
 * </ul>
 * 
 * <p>Ignore plugin.xml editor warning that relative doesn't exist.</p>
 *
 * <p>Source:
 * http://pbwhiteboard.blogspot.com/2011/01/mostly-declarative-eclipse-perspective.html
 * </p>
 *  
 * @author libor
 */
public class DefaultPerspective implements IPerspectiveFactory {

	public static final String LEFT_FOLDER_ID =
			"com.virtage.plexflow.ui.defaultPerspective.leftFolder"; 
	public static final String BOTTOM_FOLDER_ID =
			"com.virtage.plexflow.ui.defaultPerspective.bottomFolder";
	
	@Override
	public void createInitialLayout(IPageLayout layout) {
		
		layout.setEditorAreaVisible(true);
		
		layout.createPlaceholderFolder(LEFT_FOLDER_ID, IPageLayout.LEFT,
				0.2f, IPageLayout.ID_EDITOR_AREA);
		
		layout.createPlaceholderFolder(BOTTOM_FOLDER_ID, IPageLayout.BOTTOM,
				0.8f, IPageLayout.ID_EDITOR_AREA);

	}

}