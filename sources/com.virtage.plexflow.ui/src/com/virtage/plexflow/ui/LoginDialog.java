package com.virtage.plexflow.ui;

import java.util.List;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.IDialogSettings;
import org.eclipse.jface.dialogs.IMessageProvider;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import com.virtage.plexflow.logging.ISysLog;
import com.virtage.plexflow.logging.Logger;
import com.virtage.plexflow.system.BadUsernameOrPasswordException;
import com.virtage.plexflow.system.LoginService;
import com.virtage.plexflow.system.SystemInstance;
import com.virtage.plexflow.system.SystemsProperties;
import com.virtage.plexflow.system.SystemsPropertiesRecord;

/**
 * Login start-up dialog.
 * 
 * Not designed to be instantiated by clients (no public constructors). Provides
 * only single static method {@link #isLogged()} that do everything.
 *   
 * 
 * @author libor
 */
public class LoginDialog extends TitleAreaDialog {
	
	//--------------------------------------------------------------------------
    // Class fields - public
    //--------------------------------------------------------------------------

    

	//--------------------------------------------------------------------------
    // Class fields - non-public
    //--------------------------------------------------------------------------
		
	// Key for IDialogSettings to remember last used username
	private static final String SETTINGS_LAST_USERNAME = "login.lastUsername";
	private static final String SETTINGS_LAST_SYSTEM_ID = "login.lastSystemId";
	
    //--------------------------------------------------------------------------
    // Instance fields - public
    //--------------------------------------------------------------------------
	
    //--------------------------------------------------------------------------
    // Instance fields - non-public
    //--------------------------------------------------------------------------

	// Dialog settings
	private IDialogSettings settings = Activator.getDefault().getDialogSettings();
	
	// Controls used in dialog
	private Text txtUsername;
	private Text txtPassword;
	private Combo cmbSystem;
	private Button btnOk;

	// All available systems
	private List<SystemsPropertiesRecord> availableSystems = SystemsProperties.listAvailableSystems();	
	
	// Flag controlling whether field validating make sence in certain situation
	private boolean doValidation = true;
	
	// If logged will contains logged system
	private SystemInstance systemInstance = null;
		
    //--------------------------------------------------------------------------
    // Instance fields - bean properties
    //--------------------------------------------------------------------------

    //--------------------------------------------------------------------------
    // Constructors or factories
    //--------------------------------------------------------------------------

	/**
	 * Create the dialog.
	 */
	public LoginDialog() {
		super((Shell) null);		// create top-level shell for dialog
		
		// replace JFace default exception handler that just to printStackTrace() 
		// to re-thrown exception up (it's catched in PxApplication) 
		setExceptionHandler(new IExceptionHandler() {			
			@Override
			public void handleException(Throwable t) {				
				throw new RuntimeException(t);
			}
		});
	}
	
	//--------------------------------------------------------------------------
    // Public API - static
    //--------------------------------------------------------------------------
	
    //--------------------------------------------------------------------------
    // Public API - instance
    //--------------------------------------------------------------------------

	/**
	 * Opens and manage dialog, tries to authenticate, and in case of success
	 * 		returns filled {@link LoggedSystem} instance..
	 * 
	 * @return filled LoggedSystem instance, or null if failed to authenticate or Cancel/X
	 * 		pressed. 
	 */
	public SystemInstance doTheJob() {		
		this.setBlockOnOpen(true);		// stay in open() method		
		this.open();
		this.close();

		return systemInstance;			// result of login
	}
	
    //--------------------------------------------------------------------------
    // Overrides and implementations
    //--------------------------------------------------------------------------

	@Override
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		
		// Sets title of dialog's shell
		newShell.setText(PxUiConstants.DIALOG_TITLE + " - Login please");
	};

	
	/**
	 * Create contents of the dialog.
	 * @param parent
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		
		setTitle("Virtage Plexflow");		
		setMessageDefault();		// sets default dialog message
		
		Composite area = (Composite) super.createDialogArea(parent);
		Composite container = new Composite(area, SWT.NONE);
		container.setLayout(new GridLayout(2, false));
		container.setLayoutData(new GridData(GridData.FILL_BOTH));
		
		Label lblNewLabel = new Label(container, SWT.NONE);
		lblNewLabel.setText("Username:");
		
		txtUsername = new Text(container, SWT.BORDER);
		txtUsername.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				//validUsername();
			}
		});
		txtUsername.addModifyListener(new ModifyListener() {			
			@Override
			public void modifyText(ModifyEvent e) {
				validFields();			
			}
		});
		
		txtUsername.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));		
		// Restore last used username
		// Text is not accepting null
		String lastUsername = (settings.get(SETTINGS_LAST_USERNAME) == null)
				? "" : settings.get(SETTINGS_LAST_USERNAME);		
		txtUsername.setText(lastUsername);		
		
		Label lblNewLabel_1 = new Label(container, SWT.NONE);
		lblNewLabel_1.setText("Password:");
		
		txtPassword = new Text(container, SWT.BORDER | SWT.PASSWORD);
		txtPassword.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		txtPassword.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				//validPassword();
			}
		});	
		txtPassword.addModifyListener(new ModifyListener() {			
			@Override
			public void modifyText(ModifyEvent e) {
				validFields();				
			}
		});
		
		Label lblNewLabel_2 = new Label(container, SWT.NONE);
		lblNewLabel_2.setText("System:");
		
		cmbSystem = new Combo(container, SWT.READ_ONLY);
		cmbSystem.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
	
		// Fill from system.properties
		fillCmbSystem(cmbSystem);	
		
		return area;
	}

	/**
	 * Create contents of the button bar.
	 * @param parent
	 */
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		
		btnOk = createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL,
				true);
		
		createButton(parent, IDialogConstants.CANCEL_ID,
				IDialogConstants.CANCEL_LABEL, false);
		
		//if (availableSystems.isEmpty()) {		
		btnOk.setEnabled(false);
		//}
			
	}

	/**
	 * Return the initial size of the dialog.
	 */
	@Override
	protected Point getInitialSize() {
		return new Point(450, 250);
	}	
	
	
	@Override
	protected void buttonPressed(int buttonId) {
		if (buttonId == IDialogConstants.OK_ID)
			handleOk();				// Ok button pressed
		else
			handleCancelOrX();		// Cancel button pressed
	}
	
	@Override
	protected void handleShellCloseEvent() {
		handleCancelOrX();			// window closing X button pressed
	}
	
	
    //--------------------------------------------------------------------------
    // Getters/setters of properties
    //--------------------------------------------------------------------------

    //--------------------------------------------------------------------------
    // Helper methods (mostly private)
    //--------------------------------------------------------------------------
	
	//** Fills combo from system.properties file */
	private void fillCmbSystem(Combo combo) {
				
		if (availableSystems.isEmpty()) {
			txtUsername.setEnabled(false);
			txtPassword.setEnabled(false);
			combo.setEnabled(false);
			
			setMessage("No systems available to login", IMessageProvider.ERROR);
			
			MessageDialog.openError(getShell(), PxUiConstants.DIALOG_TITLE,
				"No systems available to login. Make sure that systems.properties file " +
				"exists and contains at least one enabled system.");
			
			doValidation = false;
		}
		
		for (SystemsPropertiesRecord si : availableSystems) {
			String label = si.getDisplayName() + " (" + si.getSystemId() + ")";			
			combo.add(label);
		}
		
		// Select last used
		String lastSystemId = settings.get(SETTINGS_LAST_SYSTEM_ID);
		
		if (lastSystemId == null) {
			// no previously used system, select first one
			combo.select(0);
			
		} else {
			// traverse all combo items
			for (int i = 0; i < combo.getItemCount(); i++) {
				// and match using "(id)" pattern?
				if (combo.getItem(i).contains("(" + lastSystemId + ")"))
					combo.select(i);
			}
		}
		
	}
	
	
	/**
	 * Responses to OK button, i.e. tries to sign-in and set boolean variable 
	 * 'authenticated' with result.
	 */
	private void handleOk() {
		
		// *** Extract ID from systems combo ***
		// Combo's label is in form "displayName (id)", e.g. "Dev system (dev)"
		String label = cmbSystem.getText();
		// So extract id, e.g. "dev"
		String id = label.substring(label.indexOf("(") + 1, label.lastIndexOf(")"));
		
		// Find SystemsPropertiesRecord and authenticate against with typed username + pwd 
		for (SystemsPropertiesRecord sysinfo : availableSystems) {
			
			if (sysinfo.getSystemId().equals(id)) {
				
				try {
					systemInstance = LoginService.getInstance().
							login(sysinfo, txtUsername.getText(), txtPassword.getText());
					
				} catch (BadUsernameOrPasswordException e) {
					setMessage("Username or password for chosen system is bad.",
							IMessageProvider.ERROR);					
					return;
					
//				TODO: Account disabling not supported
//				} catch (InvalidBusinessObjectException e) {
//					setMessage("Your account is not valid (is disabled, expired etc.). " +
//								"Contact administrator for detail.",
//							IMessageProvider.ERROR);					
//					return;
//					
				} catch (Throwable e) {					
			        // Quit
			        close();
		        	throw new RuntimeException(e);
				}
			}
		}		
		
		if (systemInstance != null) {
			// Save settings upon sucessful login
			settings.put(SETTINGS_LAST_USERNAME, txtUsername.getText());			
			settings.put(SETTINGS_LAST_SYSTEM_ID, id);
			
			
		}			
		
		close();		// don't forgot!
	}
	
	
	/** Responses to Cancel button or window's 'X' with confirming question. */
	private void handleCancelOrX() {
		//java.lang.System.out.println("handleCancelOrX()");
		
		boolean exit = MessageDialog.openQuestion(getShell(), PxUiConstants.DIALOG_TITLE,
				"Do you really want to exit?");
		
		if (exit)
			close();	// don't forgot!
	}
	
	private void setMessageDefault() {
		setMessage("Welcome and please sign-in bellow.", IMessageProvider.INFORMATION);
	}
	
	private void validFields() {
		
		if (txtPassword == null || txtUsername == null || doValidation == false)
			return;
		
		if ((txtUsername.getText() == null) || txtUsername.getText().equals("")) {
			setMessage("Username field cannot be empty.", IMessageProvider.WARNING);
			if (btnOk != null)
				btnOk.setEnabled(false);
			
		} else if
			((txtPassword.getText() == null) || txtPassword.getText().equals("")) {			
			setMessage("Password field cannot be empty.", IMessageProvider.WARNING);
			if (btnOk != null)
				btnOk.setEnabled(false);
		
		} else {
			setMessageDefault();			
			if (btnOk != null)
				btnOk.setEnabled(true);
		}
	}
}
