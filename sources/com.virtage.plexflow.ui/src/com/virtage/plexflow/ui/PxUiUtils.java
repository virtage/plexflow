package com.virtage.plexflow.ui;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.MultiStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Device;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;

import com.virtage.plexflow.utils.PxUtils;


/**
 * Various UI utility static methods.
 * 
 **/
public final class PxUiUtils {
	
	//--------------------------------------------------------------------------
    // Class fields - public
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Class fields - non-public
    //--------------------------------------------------------------------------
 
    //private static final ISysLog SYSLOG = Logger.getSysLog(ThisClass.class);
 
    //--------------------------------------------------------------------------
    // Instance fields - public
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Instance fields - non-public
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Instance fields - bean properties
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Constructors, static initializers, factories
    //--------------------------------------------------------------------------
    
	private PxUiUtils() {};
	
    //--------------------------------------------------------------------------
    // Public API - class (static)
    //--------------------------------------------------------------------------
 
	/**
	 * Shows JFace ErrorDialog but improved by constructing full stack trace in
	 * detail area.
	 * 
	 * <p><em>Limited use:</em> This is "local way" of reporting errors. Normally you should rely
	 * throw an exception caught by global status handler facility (that may
	 * use this utility method, indeed).</p>
	 */
	public static void errorDialogWithStackTrace(String msg, Throwable t) {
		
		final String trace = PxUtils.stackTraceAsString(t);
		
		// Temp holder of child statuses
		List<Status> childStatuses = new ArrayList<>();
		
		// Split output by OS-independent new-line
		for (String line : trace.split(System.getProperty("line.separator"))) {
			// build & add status
			childStatuses.add(new Status(IStatus.ERROR, Activator.PLUGIN_ID, line));
		}
		
		MultiStatus ms = new MultiStatus(Activator.PLUGIN_ID, IStatus.ERROR,
				childStatuses.toArray(new Status[] {}),	// convert to array of statuses
				t.getLocalizedMessage(), t);
		
        ErrorDialog.openError(null, PxUiConstants.DIALOG_TITLE, msg, ms);
	}	    
 
    //--------------------------------------------------------------------------
    // Overrides and implementations
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Getters/setters of properties
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Helper methods (mostly private)
    //--------------------------------------------------------------------------
	
	/**
	 * Create and save fonts specified in {@link PxUiConstants#AVAILABLE_FONTS}
	 * to <tt>FontRegistry</tt>. Method is called during UI startup in
	 * {@link PxApplication#start(org.eclipse.equinox.app.IApplicationContext)}. 
	 *  
	 * @param device Device where to allocate font(s).
	 * @throws IllegalArgumentException If font unknown to this listed is
	 * named in {@link PxUiConstants#AVAILABLE_FONTS}.    
	 */
	protected static void createAndSaveCommonFonts(Device device) {
		/* == Note about Font.getFontData()[0] ==
		 * 
		 * To quote javadoc:
		 *  
		 * "Returns an array of FontDatas representing the receiver. On Windows,
		 * only one FontData will be returned per font. On X however, a Font
		 * object may be composed of multiple X fonts. To support this case,
		 * we return an array of font data objects."
		 *    
		 * However our tests on Ubuntu 11.10 showed that only one FontData is 
		 * returned. 
		 */
		
		for (String fontSymbolicName : PxUiConstants.AVAILABLE_FONTS) {
			switch (fontSymbolicName) {			
				// Standard dialog font modify to bold
				case PxUiConstants.FONT_DIALOG_BOLD: {					
					Font font = JFaceResources.getFont(JFaceResources.DIALOG_FONT);
					FontData fontData = font.getFontData()[0];
					fontData.setStyle(SWT.BOLD);
					
					JFaceResources.getFontRegistry().put(
						PxUiConstants.FONT_DIALOG_BOLD,
						new FontData[] { fontData });					
					
					break;
				}
				
				// Standard dialog font modify to italic
				case PxUiConstants.FONT_DIALOG_ITALIC: {
					Font font = JFaceResources.getFont(JFaceResources.DIALOG_FONT);
					FontData fontData = font.getFontData()[0];
					fontData.setStyle(SWT.ITALIC);
					
					JFaceResources.getFontRegistry().put(
						PxUiConstants.FONT_DIALOG_ITALIC,
						new FontData[] { fontData });
					
					break;
				}
				
				// Unknown symbolic name
				default: {
					throw new IllegalArgumentException("Uknown font symbolic " +
						"name listen in PxUiConstants.AVAILABLE_FONTS[].");
				}
			}			
		}
	}
	
}
