/*
 * Copyright (c) 2012 Virtage Software - Libor Jelinek.
 * * All rights reserved. Please see http://plexflow.virtage.com/wiki/Licence.
 */
package com.virtage.plexflow.tools.cli;

import java.io.File;

import javax.crypto.SecretKey;

import com.virtage.plexflow.crypto.Encrypter;

/**
 * encrypt-pxdba-password CLI utility generating encrypted password of Plexflow DB user.
 *
 *
 * @author libor
 */
public class EncryptPassword {

    private static final String ARG_SILENT = "--silent";
    private static final String ARG_KEY = "--key";
    private static final String ARG_PASS = "--pass";
    private static final int    RT_ERROR = -1;
    private static final int    RT_OKAY = 0;

    private String[] args;


    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //args = new String[] { "--key", "/path/to/serverkey.ser", "--pass", "dbapx" };
        //args = new String[] { "--key", "serverkey.ser", "--pass", "něco" };

        new EncryptPassword(args);
    }

    public EncryptPassword(String[] args) {
        this.args = args;

        if (args.length == 0) {
                printHeader();
                System.out.println("ERROR: You have not passed any argument! See usage bellow.");
                System.out.println();
                printHelp();
                System.exit(RT_ERROR);
        }

        if (!isSilent()) {
            printHeader();
        }

        String keyPath = parseKeyPath();          // Finds key path from args

        if (keyPath == null) {
            System.out.println("ERROR: You have not passed path to key file! See usage bellow.");
            System.out.println();
            printHelp();
            System.exit(RT_ERROR);
        }

        SecretKey key = null;
        try {
            key = Encrypter.getInstance().deserKey(new File(keyPath)); // Deserialize key from key file
        } catch (Exception ex) {
            System.out.println("ERROR: Failed to deserialize specified key file! Check path or stack trace bellow.");
            ex.printStackTrace();
            System.exit(RT_ERROR);
        }

        String password = parsePassword();       // Finds passwd from args

        if (password == null) {
            System.out.println("ERROR: You have not passed password of database user! See usage bellow.");
            System.out.println();
            printHelp();
            System.exit(RT_ERROR);
        }

        String encrypted = null;
        try {
            encrypted = Encrypter.getInstance().encrypt(password);
        } catch (Exception ex) {
            System.out.println("ERROR: Failed to create encryptor. See stack trace bellow.");
            ex.printStackTrace();
            System.exit(RT_ERROR);
        }

        if (isSilent())
            System.out.println(encrypted);
        else
            System.out.println("Encrypted password is '" + encrypted + "' (without quotes).");

        System.exit(RT_OKAY);
    }



    private void printHeader() {
        System.out.println("------------------------------------------------------------------------");
        System.out.println("Virtage Plexflow / Encrypt PXDBA password");
        System.out.println("------------------------------------------------------------------------");
        System.out.println("Copyright (c) 2012 Virtage Software - Libor Jelinek.");
        System.out.println("All rights reserved. Please see http://plexflow.virtage.com/wiki/Licence.");
        System.out.println();
    }

    private void printHelp() {
        System.out.println("Usage:");
        System.out.println("$ encrypt-pxdba-password --key /path/to/serverkey.ser [--silent] --pass dbPass");
        System.out.println("");
        System.out.println("where");
        System.out.println("        --key defines path to Plexflow serverkey.ser file");
        System.out.println("        --silent will no output any text except key");
        System.out.println("        --pass dbPass is password of Plexflow database user");
        System.out.println();
        System.out.println("This simple utility will generate encrypted password for Plexflow database");
        System.out.println("account. Result hexa string use in common.properties configuration file");
        System.out.println("like in this example:");
        System.out.println("        ...");
        System.out.println("        system.<yourSystem>.jdbc.key = <generatedKey>");
        System.out.println("        ...");
        System.out.println("where replace");
        System.out.println("        <yourSystem> with system name and");
        System.out.println("        <generatedKey> with key generated with this utility.");
    }

    /**
     *
     * @param password Password to encrypt
     * @return Used as return code of utility. 0 - succeded, -1 - failed.
     */
    private int genKey(String password) {


        return 0;
    }

    /** True if --silent arg has been passed. */
    private boolean isSilent() {
        boolean silent = false;

        for (String arg : args) {
            if (ARG_SILENT.equals(arg))
                silent = true;
        }

        return silent;
    }

    /** Finds key path in args. */
    private String parseKeyPath() {
        String path = null;

        for (int i = 0; i < args.length; i++) {
            if (ARG_KEY.equals(args[i])) {
                if ((i+1) < args.length) {
                    if (!args[i+1].startsWith("--")) {
                        path = args[i+1];
                    }
                }
            }

        }

        return path;
    }

    /** Finds password in args.
     * @return null if password is missing
     */
    private String parsePassword() {
        String pwd = null;

        //
        for (int i = 0; i < args.length; i++) {
            if (ARG_PASS.equals(args[i])) {
                if ((i+1) < args.length) {
                    if (!args[i+1].startsWith("--")) {
                        pwd = args[i+1];
                    }
                }
            }

        }

        return pwd;
    }
}
