package com.virtage.plexflow.tools.cli;

import java.io.File;
import java.io.IOException;

import com.virtage.plexflow.utils.IoUtils;

public class CRC32long {
	
	public static void main(String[] args) throws IOException {
				
		if (args.length != 1) {
			System.out.println("Missing file parameter to compute CRC32 checksum long value!");
			System.exit(-1);
		}
		
		File file = new File(args[0]);
		
		System.out.println("CRC32long=" + IoUtils.crc32(file));
		System.out.println("len=" + file.length());
	}

}
