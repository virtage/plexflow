/*
 * Copyright (c) 2012 Virtage Software - Libor Jelinek.
 * All rights reserved. Please see http://plexflow.virtage.com/wiki/Licence.
 */
package com.virtage.plexflow.tools.cli;

import java.io.File;

import javax.crypto.SecretKey;

import com.virtage.plexflow.crypto.Encrypter;

/**
 * gen-server-key CLI utility.
 *
 * @author libor
 */
public class GenSeckey {

    private static final String ARG_SILENT = "--silent";
    private static final String ARG_OUTPUT = "--output";
    private static final int    RT_ERROR = -1;
    private static final int    RT_OKAY = 0;

    private String[] args;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //args = new String[] { "--key", "/path/to/serverkey.ser", "--pass", "dbapx" };
        //args = new String[] { "--output", "/var/tmp/serverkey2.ser" };

        new GenSeckey(args);
    }

    public GenSeckey(String[] args) {
        this.args = args;

        if (!isSilent()) {
            printHeader();
        }

        if (args.length == 0) {
                printHeader();
                System.out.println("ERROR: You have not passed any argument! See usage bellow.");
                System.out.println();
                printHelp();
                System.exit(RT_ERROR);
        }

        if (args.length < 2) {
                printHeader();
                System.out.println("ERROR: You have not passed all arguments! See usage bellow.");
                System.out.println();
                printHelp();
                System.exit(RT_ERROR);
        }

        String output = parseOutputPath();

        if (output == null) {
            System.out.println("ERROR: You have not passed path to key file! See usage bellow.");
            System.out.println();
            printHelp();
            System.exit(RT_ERROR);
        }

        SecretKey key = null;
        key = Encrypter.getInstance().genKey();

        Encrypter.getInstance().serKey(key, new File(output));

        if (!isSilent())
            System.out.println("New server key saved as '" + output + "' file.");

        System.exit(RT_OKAY);

    }

    private void printHeader() {
        System.out.println("------------------------------------------------------------------------");
        System.out.println("Virtage Plexflow / Generate server key");
        System.out.println("------------------------------------------------------------------------");
        System.out.println("Copyright (c) 2012 Virtage Software - Libor Jelinek.");
        System.out.println("All rights reserved. Please see http://plexflow.virtage.com/wiki/Licence.");
        System.out.println();
    }

    private void printHelp() {
        System.out.println("Usage:");
        System.out.println("        $ gen-server-key --output /path/to/serverkey.ser [--silent]");
        System.out.println("where");
        System.out.println("        --output path where to save server key file. Directory must exists.");
        System.out.println("        --silent will no output hearer and this help");
        System.out.println();
        System.out.println("This simple utility will generate serialized secret key file used by");
        System.out.println("Plexflow server-side components for various cryptographic procedures.");
    }

    /** True if --silent arg has been passed. */
    private boolean isSilent() {
        boolean silent = false;

        for (String arg : args) {
            if (ARG_SILENT.equals(arg))
                silent = true;
        }

        return silent;
    }

        /** Finds key path in args. */
    private String parseOutputPath() {
        String path = null;

        for (int i = 0; i < args.length; i++) {
            if (ARG_OUTPUT.equals(args[i])) {
                if ((i+1) < args.length) {
                    if (!args[i+1].startsWith("--")) {
                        path = args[i+1];
                    }
                }
            }

        }

        return path;
    }
}
