package com.virtage.plexflow.server.commands;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.virtage.plexflow.config.ConfigException;
import com.virtage.plexflow.server.api.BadRequestException;
import com.virtage.plexflow.server.api.ServerConsts;
import com.virtage.plexflow.storage.datafolder.provider.DatafolderProvider;
import com.virtage.plexflow.storage.datafolder.provider.DatafolderProviderException;
import com.virtage.plexflow.storage.datafolder.provider.IDatafolderProvider;
import com.virtage.plexflow.utils.PxUtils;

/**
 * Servlet implementation class Pull
 */
public class PushBeginCommand extends HttpServlet implements ICommand {
	
	//--------------------------------------------------------------------------
    // Class fields - public
    //--------------------------------------------------------------------------
	
	
    //--------------------------------------------------------------------------
    // Class fields - non-public
    //--------------------------------------------------------------------------
	private static final long serialVersionUID = -3136064506125179534L;
	
    //--------------------------------------------------------------------------
    // Instance fields - public
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Instance fields - non-public
    //--------------------------------------------------------------------------
	
	private ServletConfig config;
	
    //--------------------------------------------------------------------------
    // Instance fields - bean properties
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Constructors, static initializers, factories
    //--------------------------------------------------------------------------
 	
    //--------------------------------------------------------------------------
    // Public API - class (static)
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Public API - instance
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Overrides and implementations
    //--------------------------------------------------------------------------
 
	@Override
	public void init(ServletConfig config) throws ServletException {
		this.config = config;
	}
	
	@Override
	protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		try {
			handle(req, resp);
			
		} catch (Throwable t) {
			CommandUtils.bad(req, resp, t);
		}
	}
	
	@Override
	public String getCommandURI() {
		return "/push-begin";
	}
	
    //--------------------------------------------------------------------------
    // Getters/setters of properties
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Helper methods (mostly private)
    //--------------------------------------------------------------------------
	
	private void handle(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException, ConfigException, DatafolderProviderException, BadRequestException {
		
		// *****************************************************************************************
		// *** Check params
		// *****************************************************************************************
		 
		// File ID valid and present in DB
		long fileId = CommandUtils.parseFileOdOrException(req);				

				
		// *****************************************************************************************
		// *** Upload to tmp/
		// *****************************************************************************************		
			
		IDatafolderProvider provider = DatafolderProvider.getInstance();	
		
		// Do the job: copy from request stream to file specified in tmpFilePath variable
		long bytes = provider.pushBegin(fileId, req.getInputStream(), null, 0);
			
		// Compose response					
		// CRC header
		String crc32 = String.valueOf(provider.countCRC32inTmp(fileId));
		resp.setHeader(ServerConsts.HEADER_FILE_CRC32, crc32);

		// Length header
		String length = String.valueOf(provider.countLengthInTmp(fileId));
		resp.setHeader(ServerConsts.HEADER_FILE_LENGTH, length);
				
		String msg = String.format("Command PUSH-BEGIN on file ID %d " +
				"succeeded. Received size: %,d bytes (%s). Continue with " +
				"PUSH-COMMIT or PUSH-ROLLBACK command.",
				fileId,
				bytes,
				PxUtils.humanReadableByteCount(bytes));
		
		CommandUtils.ok(req, resp, msg);
	}
	
}
