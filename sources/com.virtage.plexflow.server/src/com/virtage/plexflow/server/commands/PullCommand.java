package com.virtage.plexflow.server.commands;

import java.io.IOException;
import java.io.InputStream;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.virtage.plexflow.server.api.ServerConsts;
import com.virtage.plexflow.storage.datafolder.provider.DatafolderProvider;
import com.virtage.plexflow.storage.datafolder.provider.IDatafolderProvider;
import com.virtage.plexflow.utils.IoUtils;

/**
 * Servlet implementation class Pull
 */
public class PullCommand extends HttpServlet implements ICommand {
	
	//--------------------------------------------------------------------------
    // Class fields - public
    //--------------------------------------------------------------------------
	
    //--------------------------------------------------------------------------
    // Class fields - non-public
    //--------------------------------------------------------------------------
 
	private static final long serialVersionUID = -2120067798974961444L;	
	 
    //--------------------------------------------------------------------------
    // Instance fields - public
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Instance fields - non-public
    //--------------------------------------------------------------------------
	
	private ServletConfig config;
	
    //--------------------------------------------------------------------------
    // Instance fields - bean properties
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Constructors, static initializers, factories
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Public API - class (static)
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Public API - instance
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Overrides and implementations
    //--------------------------------------------------------------------------
 
	@Override
	public void init(ServletConfig config) throws ServletException {
		this.config = config;
	}
	
	@Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
    		throws ServletException, IOException {
    	
		try {			
			handle(req, resp);			
			
		} catch (Throwable t) {
			CommandUtils.bad(req, resp, t);
		}

	}
	
	@Override
	public String getCommandURI() {
		return "/pull";
	}
	
    //--------------------------------------------------------------------------
    // Getters/setters of properties
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Helper methods (mostly private)
    //--------------------------------------------------------------------------   
    
    private void handle(HttpServletRequest req, HttpServletResponse resp) throws Exception {

    	// *****************************************************************************************
		// *** Check params
		// *****************************************************************************************
		
    	// File ID valid
		long fileId = CommandUtils.parseFileOdOrException(req);
			
				
		// *****************************************************************************************
		// *** Prepare headers
		// *****************************************************************************************
		
		// setBufferSize() was not in Servlet 2.1
		// (unusable Equinox HTTP Service implementation)
		resp.setBufferSize(IoUtils.BUFFER_SIZE);
			
		resp.setContentType(ServerConsts.CONTENT_TYPE_BINARY);
		
		
		IDatafolderProvider provider = DatafolderProvider.getInstance();
		
		// File length
		long length = provider.countLengthInFiles(fileId); 
		// No setContentLenght() because it expects int, but files length are longs
		resp.setHeader(ServerConsts.HEADER_FILE_LENGTH, String.valueOf(length));
		
		// Add CRC32
		long crc = provider.countCRC32inFiles(fileId);
		resp.setHeader(ServerConsts.HEADER_FILE_CRC32, String.valueOf(crc));
			
		// Do the job: stream from file to client
		// try-with-resources ensure closing streams		
		try (InputStream is = DatafolderProvider.getInstance().pull(fileId);
			 ServletOutputStream os = resp.getOutputStream()) {
			
			IoUtils.copyStream(is, os, true);			
		}

		// No composing response, response is already sent!
    }
}
