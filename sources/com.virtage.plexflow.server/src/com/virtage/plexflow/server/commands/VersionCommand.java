package com.virtage.plexflow.server.commands;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.virtage.plexflow.server.api.ServerConsts;

/**
 * Servlet implementation class Pull
 */
public class VersionCommand extends HttpServlet implements ICommand {
	
	//--------------------------------------------------------------------------
	// Class fields - public
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Class fields - non-public
	//--------------------------------------------------------------------------

	//private static final ISysLog SYSLOG = Logging.getSysLog(ThisClass.class);
	
	private static final long serialVersionUID = 2597631190389166717L;	

	//--------------------------------------------------------------------------
	// Instance fields - public
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Instance fields - non-public
	//--------------------------------------------------------------------------
	private ServletConfig config;
	
	//--------------------------------------------------------------------------
	// Instance fields - bean properties
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Constructors, static initializers, factories
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Public API - class (static)
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Public API - instance
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Overrides and implementations
	//--------------------------------------------------------------------------

	@Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
    		throws ServletException, IOException {
		
		CommandUtils.ok(req, resp, CommandUtils.getVersionInfo());
    }
	
	@Override
	public void init(ServletConfig config) throws ServletException {
		this.config = config;
	}
	
	@Override
	public String getCommandURI() {
		return "/version";		
	}
	
	//--------------------------------------------------------------------------
	// Consider toString(), equals(), hashCode()
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Getters/setters of properties
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	// Helper methods (mostly private)
	//--------------------------------------------------------------------------	

}
