package com.virtage.plexflow.server.commands;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.virtage.plexflow.server.api.BadRequestException;
import com.virtage.plexflow.server.api.ServerConsts;
import com.virtage.plexflow.utils.PxUtils;

/**
 * Utility method for file-based storage **exclusively** needed by HTTP commands.
 * 
 */
public abstract class CommandUtils {
	
	//--------------------------------------------------------------------------
    // Class fields - public
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Class fields - non-public
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Instance fields - public
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Instance fields - non-public
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Instance fields - bean properties
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Constructors, static initializers, factories
    //--------------------------------------------------------------------------
	private CommandUtils() {}
	
    //--------------------------------------------------------------------------
    // Public API - class (static)
    //--------------------------------------------------------------------------
	
	//--------------------------------------------------------------------------
    // Response and misc
    //--------------------------------------------------------------------------
	
	/**
	 * Builds OK response with custom message.
	 * 
	 * <p>
	 * Message is used for both {@link ServerConsts#HEADER_RESPONSE_MESSAGE}
	 * header and response body.
	 * </p>
	 * 
	 * <p>
	 * Method sets common headers (or changes if headers already exist)
	 * {@link ServerConsts#HEADER_FILE_ID} (if known from request),
	 * {@link ServerConsts#HEADER_SERVER_VERSION}.
	 * </p>
	 * 
	 * @throws IOException re-thrown from underlying Servlet API 
	 **/
	public static void ok(HttpServletRequest req, HttpServletResponse resp, String msg) throws IOException  {
		if ((msg == null) || (msg.isEmpty()))
			msg = "<Message missing>"; 
		
		// Headers
		resp.setStatus(HttpServletResponse.SC_OK);
		resp.setContentType(ServerConsts.CONTENT_TYPE_TEXT);
		resp.setHeader(ServerConsts.HEADER_SERVER_VERSION, getVersionInfo());
		
		long fileId = parseFileIdOr0(req);
		if (fileId != 0) {		
			resp.setHeader(ServerConsts.HEADER_FILE_ID, String.valueOf(fileId));
		}
		
		resp.setHeader(ServerConsts.HEADER_RESPONSE_TYPE, ServerConsts.HEADER_RESPONSE_TYPE_OK);
		resp.setHeader(ServerConsts.HEADER_RESPONSE_MESSAGE, msg);
		
		// Body
		resp.getWriter().println(ServerConsts.HEADER_RESPONSE_TYPE_OK + ": " + msg);
	}
	
	/**
	 * Reports BAD response to header and body. Optionally including exception
	 * stack trace for not null <tt>Throwable</tt> parameter and <tt>trace</tt>
	 * parameter in request present.
	 * 
	 * <p>
	 * Message is used for both {@link ServerConsts#HEADER_RESPONSE_MESSAGE}
	 * header and response body.
	 * </p>
	 * 
	 * <p>
	 * Method sets common headers (or changes if headers already exist)
	 * {@link ServerConsts#HEADER_FILE_ID} (if known from request),
	 * {@link ServerConsts#HEADER_SERVER_VERSION}.
	 * </p>
	 * 
	 * @throws IOException
	 *             re-thrown from underlying Servlet API
	 */
	public static void bad(HttpServletRequest req, HttpServletResponse resp, Throwable cause) throws IOException {
		// Set headers
		resp.setStatus(HttpServletResponse.SC_OK);
		resp.setContentType(ServerConsts.CONTENT_TYPE_TEXT);
		resp.setHeader(ServerConsts.HEADER_RESPONSE_TYPE, ServerConsts.HEADER_RESPONSE_TYPE_BAD);
		resp.setHeader(ServerConsts.HEADER_SERVER_VERSION, getVersionInfo());
		
		long fileId = parseFileIdOr0(req);
		if (fileId != 0) {		
			resp.setHeader(ServerConsts.HEADER_FILE_ID, String.valueOf(fileId));
		}		
		
		// Set message as cause or inform that cause is missing
		String message = "<cause missing>";
		if (cause != null) {		
			message = "Exception " + cause.getClass().toString() + ": " + cause.getLocalizedMessage();
		}	
		
		resp.setHeader(ServerConsts.HEADER_RESPONSE_MESSAGE, PxUtils.toQuotedPritable(message));		
			
		// Set body
		resp.getWriter().println(ServerConsts.HEADER_RESPONSE_TYPE_BAD + ": " + message);
		
		// Stack tract if debug mode
		if (isTrace(req) && (cause != null)) {
			// Stack trace as one string
			String stackTrace = PxUtils.stackTraceAsString(cause);
			
			resp.getWriter().println();
			resp.getWriter().println(stackTrace);
		}		
	}
	
	/** Tells whether <tt>trace</tt> request parameter is present. */
	public static boolean isTrace(HttpServletRequest req) {
		if (req.getParameter(ServerConsts.PARAM_TRACE) == null) {
			return false;
		} else {
			return true;
		}
	}
	
	/**
	 * Return <tt>fileId</tt> parameter from request or throw
	 * {@link BadRequestException} if missing/not number.
	 * 
	 * @param req
	 * @return
	 * @throws BadRequestException if parameter is missing, or not a number
	 */
	public static long parseFileOdOrException(HttpServletRequest req) throws BadRequestException {
		long fileId;
		
		try {
			fileId = Long.parseLong(req.getParameter(ServerConsts.PARAM_FILE_ID));
		} catch (NumberFormatException ex) {
			throw new BadRequestException("Missing or incorrent data type of parameter '" +
					ServerConsts.PARAM_FILE_ID + "'.");
		}
		
		return fileId;
	}
	
	/**
	 * Return <tt>fileId</tt> parameter from request or 0 if missing/not number.
	 *  
	 * @param req
	 * @return
	 */
	public static long parseFileIdOr0(HttpServletRequest req) {
		long fileId = 0;
		
		try {
			fileId = Long.parseLong(req.getParameter(ServerConsts.PARAM_FILE_ID));
		} catch (NumberFormatException ex) {			
		}
		
		return fileId;
	}
	
	
	/** Returns version and build number (qualifier). */
	// TODO: Read build (qualifier) from text file prepared by CI build server
	public static String getVersionInfo() {
		return "Virtage Plexflow Server/0.1";
	}
	
	
	//--------------------------------------------------------------------------
    // Folder, file manipulation
    //--------------------------------------------------------------------------
	
	//...
	
	
    //--------------------------------------------------------------------------
    // Public API - instance
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Overrides and implementations
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Getters/setters of properties
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Helper methods (mostly private)
    //-------------------------------------------------------------------------- 
	
	
}
