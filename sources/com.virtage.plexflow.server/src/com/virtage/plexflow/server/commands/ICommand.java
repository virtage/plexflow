package com.virtage.plexflow.server.commands;


/**
 * Every class wishing to handle URI commands must implement this interface.
 * 
 * @author libor
 *
 */
public interface ICommand {
	
	/**
	 * Returns URI at which command will listen. <b>Must begin with '/'.</b> 
	 */
	String getCommandURI();
}