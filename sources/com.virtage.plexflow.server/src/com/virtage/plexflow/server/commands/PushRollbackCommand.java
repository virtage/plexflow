package com.virtage.plexflow.server.commands;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.virtage.plexflow.server.api.BadRequestException;
import com.virtage.plexflow.storage.datafolder.provider.DatafolderProvider;
import com.virtage.plexflow.storage.datafolder.provider.DatafolderProviderException;


/**
 * Servlet implementation class Pull
 */
public class PushRollbackCommand extends HttpServlet implements ICommand {
	
	//--------------------------------------------------------------------------
    // Class fields - public
    //--------------------------------------------------------------------------

	
    //--------------------------------------------------------------------------
    // Class fields - non-public
    //--------------------------------------------------------------------------
	private static final long serialVersionUID = -3136064506125179534L;
	
    //--------------------------------------------------------------------------
    // Instance fields - public
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Instance fields - non-public
    //--------------------------------------------------------------------------
	private ServletConfig config;
	
    //--------------------------------------------------------------------------
    // Instance fields - bean properties
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Constructors, static initializers, factories
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Public API - class (static)
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Public API - instance
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Overrides and implementations
    //--------------------------------------------------------------------------
 
	@Override
	public void init(ServletConfig config) throws ServletException {
		this.config = config;
	}
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		try {
			handle(req, resp);
			
		} catch (Throwable t) {
			CommandUtils.bad(req, resp, t);
		}
	}	
		
	@Override
	public String getCommandURI() {
		return "/push-rollback";
	}
	
    //--------------------------------------------------------------------------
    // Getters/setters of properties
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Helper methods (mostly private)
    //--------------------------------------------------------------------------
	
	private void handle(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException, DatafolderProviderException, BadRequestException {
		
		// File ID valid and present in DB
		long fileId = CommandUtils.parseFileOdOrException(req);		

		//  Delete in tmp/
		DatafolderProvider.getInstance().pushRollback(fileId);		

		// Compose response
		
		CommandUtils.ok(req, resp, "Command PUSH-ROLLBACK on file " + fileId + " succeeded.");
	}
	
}
