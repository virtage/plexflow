package com.virtage.plexflow.server.commands;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.virtage.plexflow.server.api.BadRequestException;
import com.virtage.plexflow.storage.datafolder.provider.DatafolderProvider;
import com.virtage.plexflow.storage.datafolder.provider.DatafolderProviderException;

/**
 * Delete command.
 */
public class DeleteCommand extends HttpServlet implements ICommand {
	
	//--------------------------------------------------------------------------
    // Class fields - public
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Class fields - non-public
    //--------------------------------------------------------------------------
 
	private static final long serialVersionUID = -8130648552041702231L;
    //private static final ISysLog SYSLOG = Logging.getSysLog(ThisClass.class);
 
    //--------------------------------------------------------------------------
    // Instance fields - public
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Instance fields - non-public
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Instance fields - bean properties
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Constructors, static initializers, factories
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Public API - class (static)
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Public API - instance
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Overrides and implementations
    //--------------------------------------------------------------------------	

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		try {
			handle(req, resp);
			
		} catch (Throwable t) {
			CommandUtils.bad(req, resp, t);
		}
		
	}
	
	@Override
	public String getCommandURI() {
		return "/delete";		
	}
	
    //--------------------------------------------------------------------------
    // Getters/setters of properties
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Helper methods (mostly private)
    //--------------------------------------------------------------------------
	
	private void handle(HttpServletRequest req, HttpServletResponse resp) throws DatafolderProviderException, ServletException, IOException, BadRequestException {
		// File ID valid and present in DB
		long fileId = CommandUtils.parseFileOdOrException(req);
		
		// *****************************************************************************************
		// *** Delete in files/
		// *****************************************************************************************
				
		DatafolderProvider.getInstance().delete(fileId);
		
		// Compose response
			
		CommandUtils.ok(req, resp, "Command DELETE on file ID " + fileId + " succeeded.");
		
	}
}
