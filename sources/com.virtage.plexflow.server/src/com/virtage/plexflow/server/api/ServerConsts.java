package com.virtage.plexflow.server.api;

public interface ServerConsts {
	
	//--------------------------------------------------------------------------
    // Misc
    //--------------------------------------------------------------------------
	
	
	//--------------------------------------------------------------------------
    // URIs
    //--------------------------------------------------------------------------
	
	/**
	 * URI prefix of HTTP API.
	 * 
	 * <p>All commands will be then URI_BASE + "/some-command".</p>
	 * 
	 * <p><em>Warning:</em> Value must be valid context path, i.e. must begins with '/' and 
	 * must not ends with '/'!</p>
	 *  
	 * Value {@value}. */
	String URI_PREFIX = "/plexflow-api";	
	
	
	/** Default HTTP port. Must be String because is stored by system property capable holding strings only. */
	String DEFAULT_HTTP_PORT = "8080";
	
	//--------------------------------------------------------------------------
    // Control system properties
    //--------------------------------------------------------------------------
	
	// HTTP API uses two - sysprop to locating systems.properties and system ID from that file to
	// work with.
	
    /**
     * System ID to work with (value {@value}). <strong>Sysprop for locating systems.properties is taken
     * from {@link com.virtage.plexflow.PxConstants#SYSPROP_SYSTEMS_PROPERTIES_PATH}.</strong> 
     **/
    String SYSPROP_SYSTEMS_PROPERTIES_SYSTEM_ID = "com.virtage.plexflow.systemsProperties.systemID";	
    
	
    /** OSGi property controlling port of HTTP service */
	String SYSPROP_OSGI_HTTP_PORT = "org.osgi.service.http.port";
	
    
	//--------------------------------------------------------------------------
    // Response headers and constants
    //--------------------------------------------------------------------------
	
	/** Custom response header to store a result of request (value {@value}). */
	String CONTENT_TYPE_TEXT = "text/plain;charset=UTF-8";
	
	/** Custom response header to store a result of request (value {@value}). */
	String CONTENT_TYPE_BINARY = "application/octet-stream";
	
	/** Custom response header to store a result of request (value {@value}). */
	String HEADER_FILE_CRC32 = "X-Plexflow-File-CRC32";
	
	/** Custom response header to store a result of request (value {@value}). */
	String HEADER_FILE_LENGTH = "X-Plexflow-File-Length";
	
	/** Custom response header to store file ID (value {@value}). */
	String HEADER_FILE_ID = "X-Plexflow-File-ID";
	
	/**
	 * Custom response header to store type of response (OK or BAD) (value {@value}).
	 * Compare to constants {@link #HEADER_RESPONSE_TYPE_OK} and {@link #HEADER_RESPONSE_TYPE_BAD}.
	 * */
	String HEADER_RESPONSE_TYPE = "X-Plexflow-Response-Type";
	
	/** Value denoting BAD response to request (value {@value}). */
	String HEADER_RESPONSE_TYPE_BAD = "BAD";
	
	/** Value denoting OK response to request (value {@value}). */
	String HEADER_RESPONSE_TYPE_OK = "OK";
	
	/** Custom response header to store detail text of response (value {@value}). */
	String HEADER_RESPONSE_MESSAGE = "X-Plexflow-Response-Message";
	
	/** Custom response header to carry Plexflow Server version (value {@value}). */
	String HEADER_SERVER_VERSION = "X-Plexflow-Server-Version";

	//--------------------------------------------------------------------------
    // Params
    //--------------------------------------------------------------------------
	
	/** Throughout used file ID request parameter (value {@value}). */
	String PARAM_FILE_ID = "fileId";
	
	/** Turns on trace (full stack trace on exception) (value {@value}). */
	String PARAM_TRACE = "trace";
}
