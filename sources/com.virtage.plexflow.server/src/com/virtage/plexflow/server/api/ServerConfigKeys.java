package com.virtage.plexflow.server.api;

public interface ServerConfigKeys {
	/** HTTP API full URL config key (value {@value}). */
	String URL = "server.url";	
	
	/** HTTP API authentication style config key (value {@value}). */
	String AUTH_METHOD = "server.authMethod";
	
	/** Value denoting BASIC HTTP authentication (value {@value}). */
	String AUTH_METHOD_BASIC = "BASIC";
	
	/** Value denoting no HTTP authentication (value {@value}). */
	String AUTH_METHOD_NONE = "NONE";
}
