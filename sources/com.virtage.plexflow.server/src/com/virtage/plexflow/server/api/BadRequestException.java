package com.virtage.plexflow.server.api;

/**
 * Thrown when BAD response should be sent to client. 
 * 
 * Don't confuse with {@linkplain com.virtage.plexflow.http.client.ApiBadResponseResponse} class. 
 * 
 */
public class BadRequestException extends Exception {

	private static final long serialVersionUID = -4109592891224784649L;

	public BadRequestException(Throwable cause) {
		super(cause);
	}
	
	public BadRequestException(String message, Throwable cause) {
		super(message, cause);
	}

	public BadRequestException(String message) {
		super(message);
	}
	

}
