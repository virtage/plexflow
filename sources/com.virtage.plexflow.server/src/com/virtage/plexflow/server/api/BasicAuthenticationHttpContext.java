package com.virtage.plexflow.server.api;

import java.io.IOException;
import java.net.URL;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.digest.DigestUtils;
import org.osgi.service.http.HttpContext;

import com.virtage.plexflow.server.commands.CommandUtils;
import com.virtage.plexflow.system.User;

/**
 * Implements OSGi HttpContext with HTTP BASIC authentication.
 * 
 * Only implemented method is handleSecurity().
 * 
 * 
 * @see http://tools.ietf.org/html/rfc2617 - "HTTP Authentication: Basic and Digest Access Authentication"
 * @see http://en.wikipedia.org/wiki/Basic_access_authentication
 * @author libor
 */
public class BasicAuthenticationHttpContext implements HttpContext {

	//--------------------------------------------------------------------------
    // Class fields - public
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Class fields - non-public
    //--------------------------------------------------------------------------

    //--------------------------------------------------------------------------
    // Instance fields - public
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Instance fields - non-public
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Instance fields - bean properties
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Constructors, static initializers, factories
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Public API - class (static)
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Public API - instance
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Overrides and implementations
    //--------------------------------------------------------------------------
 
    /** {@inheritDoc} */
	@Override
	public boolean handleSecurity(HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		
    	// "Authorization" header is missing, i.e. this is a first request
    	String authHeader = request.getHeader("Authorization");
		if (authHeader == null) {
			// Response with 401 Unauthorized
			failAuthorization(request, response);
			return false;
		}
		
		// "Authorization" header example: "Authorization: Basic QWxhZGRpbjpvcGVuIHNlc2FtZQ==".
		
		// Split into auth method and base64-encoded credentials
		int indexOfSpace = authHeader.indexOf(' ');
		
		// Malformed header value - must have two parts
		if (indexOfSpace == -1) {
			failAuthorization(request, response);
			return false;
		}
		
		// Parse method and decode credentials
		String method = authHeader.substring(0, indexOfSpace);
		// Second part is Base64-encoded "username:password"		
		String encodedCredentials = authHeader.substring(indexOfSpace + 1);
		String credentials = new String(
				org.apache.commons.codec.binary.Base64.decodeBase64(encodedCredentials));
		
		// Make sure that it is BASIC authentication - first part must be "Basic"
		if (!method.equals("Basic")) {
			failAuthorization(request, response);
			return false;
		}
		
		// Split credentials to username and password
		int colon = credentials.indexOf(':');
		String username = credentials.substring(0, colon);
		String password = credentials.substring(colon + 1);
		
		// Authentication itself
		boolean authenticated = User.authenticate(username, DigestUtils.md5Hex(password));
		
		if (!authenticated) {
			failAuthorization(request, response);
			return false;
		}			
		
		// By method contract we're requested to set these attributes. Servlets may read it later.
		request.setAttribute(HttpContext.REMOTE_USER, username);
		request.setAttribute(HttpContext.AUTHENTICATION_TYPE, method);	// "Basic"
				
		return true;
	}

	/*
	 * What is this method good for? (javadoc excerpt)
	 * 
	 * For servlet registrations, Http Service will call this method to support the ServletContext
	 * methods
	 * 		* getResource and
	 * 		* getResourceAsStream.
	 * 
	 * For resource registrations, Http Service will call this method to locate the named resource.
	 * The context can control from where resources come. 
	 */
	/**
	 * Not implemented - this functionality is needless and returns always null.
	 * {@inheritDoc}
	 * */
	@Override
	public URL getResource(String name) {
		// Have no qualms about omitting implementation - we don't need this functionality
		return null;
	}

	/*
	 * What is this method good for? (javadoc excerpt)
	 * 
	 * For servlet registrations, the Http Service will call this method to support the
	 * ServletContext method getMimeType().
	 * 
	 * For resource registrations, the Http Service will call this method to determine the MIME
	 * type for the Content-Type header in the response.
	 * 
	 */
	/**
	 * Not implemented - this functionality is needless and returns always null.
	 * {@inheritDoc}
	 * */
	@Override
	public String getMimeType(String name) {
		// Have no qualms about omitting implementation - we don't need this functionality
		return null;
	}

    
    //--------------------------------------------------------------------------
    // Getters/setters of properties
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Helper methods (mostly private)
    //--------------------------------------------------------------------------

	/**
	 * Response with 401 Unauthorized with "WWW-Authenticate" header.
	 *
	 */
	private void failAuthorization(HttpServletRequest request, HttpServletResponse response) {
		// force a session to be created
		request.getSession(true);
		response.setHeader("WWW-Authenticate", "Basic realm=\"" + CommandUtils.getVersionInfo() + "\"");
		try {
			response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
		} catch (IOException e) {
			// do nothing
		}
	}
}
