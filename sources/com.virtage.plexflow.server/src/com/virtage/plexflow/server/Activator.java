package com.virtage.plexflow.server;

import java.io.File;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.virtage.plexflow.config.ConfigException;
import com.virtage.plexflow.server.api.ServerConsts;
import com.virtage.plexflow.system.LoginException;
import com.virtage.plexflow.system.LoginService;
import com.virtage.plexflow.system.SystemInstance;
import com.virtage.plexflow.system.SystemInstanceFactory;
import com.virtage.plexflow.system.SystemsProperties;
import com.virtage.plexflow.system.SystemsPropertiesRecord;

public class Activator implements BundleActivator {

	// --------------------------------------------------------------------------
	// Class fields - public
	// --------------------------------------------------------------------------

	// The plug-in ID
	public static final String PLUGIN_ID = "com.virtage.plexflow.server"; //$NON-NLS-1$

	// --------------------------------------------------------------------------
	// Class fields - non-public
	// --------------------------------------------------------------------------

	private final static Logger SYSLOG = LoggerFactory
			.getLogger(Activator.class);

	// --------------------------------------------------------------------------
	// Instance fields - public
	// --------------------------------------------------------------------------

	// --------------------------------------------------------------------------
	// Instance fields - non-public
	// --------------------------------------------------------------------------
	private HttpServiceTracker httpServiceTracker;

	// --------------------------------------------------------------------------
	// Instance fields - bean properties
	// --------------------------------------------------------------------------

	// --------------------------------------------------------------------------
	// Constructors, static initializers, factories
	// --------------------------------------------------------------------------

	// --------------------------------------------------------------------------
	// Public API - class (static)
	// --------------------------------------------------------------------------

	// --------------------------------------------------------------------------
	// Public API - instance
	// --------------------------------------------------------------------------

	// --------------------------------------------------------------------------
	// Overrides and implementations
	// --------------------------------------------------------------------------

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.osgi.framework.BundleActivator#start(org.osgi.framework.BundleContext
	 * )
	 */
	@Override
	public void start(BundleContext context) throws Exception {
		welcomeBanner();

		// All init-code placed here
		init();

		// If HTTP port is not specified
		// MUST BE SET HERE before first HTTP Service use
    	if (System.getProperty(ServerConsts.SYSPROP_OSGI_HTTP_PORT) == null)
    		System.setProperty(ServerConsts.SYSPROP_OSGI_HTTP_PORT, ServerConsts.DEFAULT_HTTP_PORT);

		// Register HTTP Service tracker
		httpServiceTracker = new HttpServiceTracker(context);
		httpServiceTracker.open();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.osgi.framework.BundleActivator#stop(org.osgi.framework.BundleContext)
	 */
	@Override
	public void stop(BundleContext context) throws Exception {
		// Close and release service trackers
		// Always null tracker references! Otherwise service cannot be removed
		// by OSGi Framework
		httpServiceTracker.close();
		httpServiceTracker = null;
	}

	// --------------------------------------------------------------------------
	// Getters/setters of properties
	// --------------------------------------------------------------------------

	// --------------------------------------------------------------------------
	// Helper methods (mostly private)
	// --------------------------------------------------------------------------

	// ** Prints welcome banner. */
	private void welcomeBanner() {
		System.out.println();
		System.out
				.println("------------------------------------------------------------------------");
		System.out.println("Virtage Plexflow Server");
		System.out
				.println("------------------------------------------------------------------------");
		System.out
				.println("Copyright (c) 2012 Virtage Software - Libor Jelinek.");
		System.out
				.println("All rights reserved. Please see http://plexflow.virtage.com/wiki/Licence.");
		System.out.println();
	}

	private void init() throws ConfigException, IllegalArgumentException,
			LoginException {
		// *****************************************************************************************
		// Set appropriate SystemInstanceFactory to be used by the rest of code
		// *****************************************************************************************
		// Find system
		SystemsPropertiesRecord foundSystem = findSystemToLogin();
		// Login as su
		SystemInstance systemInstance = LoginService.getInstance().loginAsSu(
				foundSystem);
		// Set SystemInstanceFactory
		SystemInstanceFactory.setInstance(systemInstance);
		// *****************************************************************************************
	}

	/**
	 * Locates systems.properties in system property, user-wide or system-wide,
	 * and finds system ID to use that will be returned as
	 * SystemsPropertiesRecord.
	 **/
	private SystemsPropertiesRecord findSystemToLogin() throws ConfigException {
		// List systems. Will throw IAE if systems.properties cannot be found
		File systemsPropertiesFile = SystemsProperties.locateFile();

		// TODO: Log used systems.properties absolute path

		List<SystemsPropertiesRecord> systems = SystemsProperties
				.listAvailableSystems(systemsPropertiesFile);

		// System property which system ID to use from systems.properties
		String systemIdToUse = System
				.getProperty(ServerConsts.SYSPROP_SYSTEMS_PROPERTIES_SYSTEM_ID);

		if (StringUtils.isBlank(systemIdToUse)) {
			// Build easy readable list enabled system IDs from
			// systems.properties for error message
			StringBuffer sb = new StringBuffer();
			for (SystemsPropertiesRecord record : systems) {
				sb.append("'");
				sb.append(record.getSystemId());
				sb.append("', ");
			}
			// Cut off last ", "
			String sysIDs = sb.toString().substring(0,
					sb.toString().length() - 2);

			throw new ConfigException(
					"Required system property '"
							+ ServerConsts.SYSPROP_SYSTEMS_PROPERTIES_SYSTEM_ID
							+ "' is missing or is null. Available system IDs defined in file '"
							+ systemsPropertiesFile.getAbsolutePath()
							+ "' are " + sysIDs + ".");
		}

		// Found system will be hold here
		SystemsPropertiesRecord foundSystem = null;

		// Loop for matching system ID
		for (SystemsPropertiesRecord system : systems) {
			if (system.getSystemId().equals(systemIdToUse))
				foundSystem = system;
		}

		if (foundSystem == null)
			throw new ConfigException("System property '"
					+ ServerConsts.SYSPROP_SYSTEMS_PROPERTIES_SYSTEM_ID
					+ "' is referring to not existing or disabled system ID ("
					+ systemIdToUse + ") in file '"
					+ systemsPropertiesFile.getAbsolutePath() + "'.");

		SYSLOG.info("Choosen system ID to use is {}.", foundSystem);

		return foundSystem;
	}

}
