package com.virtage.plexflow.server;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Collections;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.service.http.HttpContext;
import org.osgi.service.http.HttpService;
import org.osgi.service.http.NamespaceException;
import org.osgi.util.tracker.ServiceTracker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.virtage.plexflow.config.ConfigException;
import com.virtage.plexflow.config.ConfigFactory;
import com.virtage.plexflow.server.api.BasicAuthenticationHttpContext;
import com.virtage.plexflow.server.api.ServerConfigKeys;
import com.virtage.plexflow.server.api.ServerConsts;
import com.virtage.plexflow.server.commands.DeleteCommand; 
import com.virtage.plexflow.server.commands.ICommand;
import com.virtage.plexflow.server.commands.PullCommand;
import com.virtage.plexflow.server.commands.PushBeginCommand;
import com.virtage.plexflow.server.commands.PushCommitCommand;
import com.virtage.plexflow.server.commands.PushRollbackCommand;
import com.virtage.plexflow.server.commands.VersionCommand;

/**
 * Simple ServiceTracker for OSGi HttpService that registers command servlets with URL. 
 *  
 */
class HttpServiceTracker extends ServiceTracker<HttpService, HttpService> {

	//--------------------------------------------------------------------------
    // Class fields - public
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Class fields - non-public
    //--------------------------------------------------------------------------

	private final static Logger SYSLOG = LoggerFactory.getLogger(HttpServiceTracker.class);
	
	// Iterate all commands to register with server. Maybe later fetched from extension point etc.
	private static final ICommand[] COMMANDS = new ICommand[] {
		new DeleteCommand(),
		new PullCommand(),
		new PushBeginCommand(),
		new PushCommitCommand(),
		new PushRollbackCommand(),
		new VersionCommand(),
	};
	
    //--------------------------------------------------------------------------
    // Instance fields - public
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Instance fields - non-public
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Instance fields - bean properties
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Constructors, static initializers, factories
    //--------------------------------------------------------------------------
	   

	public HttpServiceTracker(BundleContext context) {    	
		super(context, HttpService.class, null);
	}
    
    //--------------------------------------------------------------------------
    // Public API - class (static)
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Public API - instance
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Overrides and implementations
    //--------------------------------------------------------------------------
    
    @Override
	public HttpService addingService(ServiceReference<HttpService> reference) {
		SYSLOG.info("HTTP Service is about to start on port " + System.getProperty(ServerConsts.SYSPROP_OSGI_HTTP_PORT));	
    	
		HttpService httpService = context.getService(reference);
		
		// Set HttpContext eventually that requires authentication
		HttpContext defaultHttpContext = null;		// null will mean "no authentication"		
		String authMethod;
		
		try {
			authMethod = ConfigFactory.getGlobal().getString(ServerConfigKeys.AUTH_METHOD);
			
		} catch (ConfigException e1) {
			throw new RuntimeException(e1);
			
		}
		
		if (ServerConfigKeys.AUTH_METHOD_BASIC.equals(authMethod)) {			
			defaultHttpContext = new BasicAuthenticationHttpContext();
			SYSLOG.info("with BASIC authentication...");
			
		} else {
			SYSLOG.info("without an authentication...");
		}
		
		try {
			for (ICommand command : COMMANDS) {				
				// All command register with context from config (usually doing authentication)
				// but version command register with no special context do not to require auth
				HttpContext context;				
				if (command instanceof VersionCommand)
					context = null;
				else
					context = defaultHttpContext;
				
				SYSLOG.info("Registering command '{}'...", command.getCommandURI());
				
				// Register
				httpService.registerServlet(
						ServerConsts.URI_PREFIX + command.getCommandURI(),
						(HttpServlet) command,
						null,
						context);
				
				SYSLOG.info("Command {} registered", command.getCommandURI());
			}			
			
			// Don't forget to unregister all these commands in #removedService() bellow
			
		} catch (ServletException | NamespaceException e) {
			throw new RuntimeException("Failed to register command servlets.", e);
			
		}
			
		
		// Print URL at what server listen to
		String url;
		try {
			url = getServerListeningURL();
			
		} catch (SocketException e) {
			url = "<caused SocketException, see syslog>";
			SYSLOG.error("Determining listening URL caused an exception", e);
			
		}
		
		SYSLOG.info("HTTP Service started and listening on {} (best guess).", url);
		
		return httpService;
	}
	

	@Override
	public void removedService(ServiceReference<HttpService> reference, HttpService service) {
		for (ICommand command : COMMANDS) {
			service.unregister(ServerConsts.URI_PREFIX + command.getCommandURI());
		}		
		
		super.removedService(reference, service);
	}
	
    //--------------------------------------------------------------------------
    // Getters/setters of properties
    //--------------------------------------------------------------------------
 
    //--------------------------------------------------------------------------
    // Helper methods (mostly private)
    //--------------------------------------------------------------------------
	
	/**
	 * Returns best guess of URL at server listen to.
	 * @throws SocketException 
	 */
	private String getServerListeningURL() throws SocketException {
		// *** Host ***
		// Host is a problem - there is no standard property for interface to
		// bind HTTP service to. See http://wiki.osgi.org/wiki/WebExperience#Configuration
		// We assume that on the first non-loopback interface returned by Java
		// Code from http://docs.oracle.com/javase/tutorial/networking/nifs/listing.html
		String addr = null;

		// All known interfaces
		Enumeration<NetworkInterface> nets = NetworkInterface.getNetworkInterfaces();
		search:
			for (NetworkInterface netint : Collections.list(nets)) {
				// All known addresses of interface
	            Enumeration<InetAddress> inetAddresses = netint.getInetAddresses();
	            for (InetAddress inetAddress : Collections.list(inetAddresses)) {
	                // Quit the loop on the first non loopback address detected
	            	if (!InetAddress.getLoopbackAddress().equals(inetAddress)) {
	            		// But skip IPv6 addresses (containing ":" as delimiter)
	            		if (!inetAddress.toString().contains(":")) {
		                	addr = inetAddress.toString();
		                	break search;
	            		}
	                }
	            }
	        }
		
		// This computer have loopback only
		if (addr == null) {
			addr = "localhost";
		}
		
		
		// *** Port *** from OSGi property
		String port = System.getProperty(ServerConsts.SYSPROP_OSGI_HTTP_PORT);
		
				
		String url = "http://" + addr + ":" + port + ServerConsts.URI_PREFIX;
	
		return url;
	}
	
}